import os
import sys
from traceback import format_exc

from tests import utils
import tests.integrations.src as src

if __name__ == "__main__":
    print("Integration tests started...")

    try:
        admin_email = os.environ["ADMIN_EMAIL"]
        admin_password = os.environ["ADMIN_PASSWORD"]
        monitor_api = os.environ["MONITOR_API"]

        bucket_name = os.environ["BUCKET_NAME"]
        s3_address = os.environ["MINIO_ADDRESS"]
        s3_id = os.environ["MINIO_ACCESS_KEY"]
        s3_key = os.environ["MINIO_SECRET_KEY"]

        robot_name = os.environ["ROBOT_NAME"]

        # get auth data
        auth_headers = utils.auth(monitor_api, admin_email, admin_password)

        # checking
        src.check_monitor_api(monitor_api, robot_name, auth_headers)
        src.check_stream_api(monitor_api, robot_name)
        src.check_aws(s3_address, s3_id, s3_key, bucket_name)
    except Exception:
        print(f"Test error: {format_exc()}")
        sys.exit(1)

    print("Integration tests completed successfully.")
