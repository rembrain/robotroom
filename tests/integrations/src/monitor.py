import json

import requests

from tests import utils


@utils.highlight_section
def check_monitor_api(monitor_api: str, robot_name: str, auth_headers: dict) -> None:
    print("Checking of MONITOR API started...")

    print("Test: push command to robot.")
    response: requests.Response = requests.post(
        url=f"{monitor_api}/api/push_command/{robot_name}",
        json={"command": '{"op":"ask_for_idle","source":"tests" }'},
        headers=auth_headers,
    )
    if response.status_code != 200:
        raise Exception(f"Cannot push command. Response: {response.text}")

    print("Test: send command to robot.")
    response = requests.get(url=f"{monitor_api}/api/task_to_robot/{robot_name}/tester/tester", headers=auth_headers)
    if response.status_code != 200:
        raise Exception(f"Cannot set task to robot. Response: {response.text}")

    print("Test: save task.")
    response = requests.put(
        url=f"{monitor_api}/api/task",
        params={"author": "tester", "name": "tester"},
        json={
            "description": "Tester picrk task",
            "template": "one-shot",
            "working_zone": [0.11875, 0.011111111111111112, 0.8328125, 0.9611111111111111],
        },
        headers=auth_headers,
    )
    if response.status_code != 200:
        raise Exception(f"Cannot save task. Response: {response.text}")

    print("Test: get wifi qr-code.")
    qr_request_data = json.dumps({
        "SSID": "tester",
        "password": "tester"
    })
    response = requests.post(url=f"{monitor_api}/api/wifi_qr_generator",
                             headers=auth_headers, data=qr_request_data)
    if response.status_code != 200 or response.headers.get("Content-Type") != "image/png":
        raise Exception(f"Cannot get wifi_qr. Response: {response.text}")

    print("Checking of MONITOR API completed successfully.")


# todo remake on new api!
@utils.highlight_section
def check_former_register_api(monitor_api: str, robot_name: str, headers) -> None:
    robot_password = "tester"

    print("Test: register robot.")
    response = requests.post(
        url=f"{monitor_api}/api/robots",
        json={"name": robot_name, "password": robot_password},
        headers=headers,
    )
    if not (response.status_code == 200 or (response.status_code == 400 and response.text == "name dublicate")):
        raise Exception(f"Cannot register_robot. Response: {response.text}")

    print("Test: check robot was registered.")
    response = requests.get(url=f"{monitor_api}/api/robots", headers=headers)
    if not (
        response.status_code == 200 and (robot_name in {item.get("username") for item in response.json()["answer"]})
    ):
        raise Exception(
            f"Robot was not registered - register_robot has worked incorrectly. " f"Response: {response.text}"
        )

    print("Test: add robot config.")
    response = requests.post(
        url=f"{monitor_api}/api/config?robot=" + robot_name,
        json={"value": '{"task_name": "tester:tester"}'},
        headers=headers,
    )
    if not (response.status_code == 200 and response.json()["answer"] is True):
        raise Exception(f"Cannot set_config. Response: {response.text}")

    print("Test: check config was added.")
    response = requests.get(url=f"{monitor_api}/api/config", params={"robot": robot_name}, headers=headers)
    if not (
        response.status_code == 200
        and response.json()["answer"].get("robot") == robot_name
        and ("tester" in json.loads(response.json()["answer"].get("value", "{}")).get("task_name"))
    ):
        raise Exception(
            f"Config for robot was not added - set_config has worked incorrectly. " f"Response: {response.text}"
        )

    print("Test: save task.")
    response = requests.put(
        url=f"{monitor_api}/api/task",
        params={"author": "tester", "name": "tester"},
        data='{"description": "Tester picrk task","template": "one-shot",'
        '"working_zone": [0.11875, 0.011111111111111112, 0.8328125, 0.9611111111111111]}',
        headers=headers,
    )
    if not (response.status_code == 200):
        raise Exception(f"Cannot save_task. Response: {response.text}")

    print("Test: check task was added.")
    response = requests.get(url=f"{monitor_api}/api/tasks", params={"author": "tester"}, headers=headers)
    if not (
        response.status_code == 200
        and "tester" in {i.get("author") for i in response.json()["answer"]}
        and "tester" in {i.get("name") for i in response.json()["answer"]}
    ):
        raise Exception(f"Task did not added - add_task has worked incorrectly. " f"Response: {response.text}")

    print("Test: request if a processor is connected.")
    response = requests.get(url=f"{monitor_api}/api/processors", headers=headers)
    list_of_processors: list = response.json()["answer"]
    if not (
        response.status_code == 200
        and len(list_of_processors) > 0
        and (robot_name in {item.get("associated_robot") for item in list_of_processors})
    ):
        raise Exception(f"There was no robot '{robot_name}' in associated processors.")
