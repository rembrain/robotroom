import requests

from tests import utils


@utils.highlight_section
def check_stream_api(stream_api: str, robot_name: str) -> None:
    print("Checking  of STREAM API started...")

    print("Test: check stream rgb.")
    response: requests.Response = requests.get(url=f"{stream_api}/stream/stream_rgb/{robot_name}", stream=True)
    if response.status_code != 200:
        raise Exception(f"Cannot call stream_rgb. Response: {response.text}")

    data = next(response.iter_content(chunk_size=100))
    if b"--frame\r\nContent-Type: image/jpeg" not in data:
        raise Exception(f"Url 'stream_rgb' return incorrect data. Response: {data}")

    print("Test: check stream rgb preview.")
    response = requests.get(url=f"{stream_api}/stream/stream_rgb_preview/{robot_name}", stream=True)
    if response.status_code != 200:
        raise Exception(f"Cannot call stream_rgb_preview. Response: {response.text}")

    data = next(response.iter_content(chunk_size=100))
    if b"--frame\r\nContent-Type: image/jpeg" not in data:
        raise Exception(f"Url 'stream_rgb_preview' return incorrect data. Response: {data}")

    print("Test: check getting one img.")
    response = requests.get(
        url=f"{stream_api}/stream/get_subimg/{robot_name}?rect=[0.5,0.5,0.3,0.3]",
        stream=True,
    )
    if not (
            response.status_code == 200
            and len(response.content) > 0
            and isinstance(response.content, bytes)
            and response.headers.get("Content-Type", "") == "image/jpeg"
    ):
        raise Exception(f"Cannot make 'get_subimg' or response has errors.")

    print("Checking  of STREAM API completed successfully.")
