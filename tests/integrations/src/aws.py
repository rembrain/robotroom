import io
from typing import Any

import boto3
from botocore.client import Config

from tests import utils


@utils.highlight_section
def check_aws(endpoint_url: str, aws_id: str, aws_key: str, bucket_name: str) -> None:
    print("Checking  of AWS started...")

    print("Test: check file downloading to S3.")
    file_name = 'tester_example.txt'
    binary_message = b'HELLO!'

    s3_client: Any = boto3.client(
        's3',
        endpoint_url=endpoint_url,
        aws_access_key_id=aws_id,
        aws_secret_access_key=aws_key,
        config=Config(signature_version='s3v4')
    )
    s3_client.upload_fileobj(io.BytesIO(binary_message), bucket_name, file_name)

    download_text = io.BytesIO()
    s3_client.download_fileobj(bucket_name, file_name, download_text)

    download_text.seek(0)
    if download_text.read() != binary_message:
        raise Exception('Cannot download file to S3.')

    print("Checking  of AWS completed successfully.")
