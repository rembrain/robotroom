import functools
from typing import Dict, Callable

import requests


def auth(url: str, email: str, password: str) -> Dict[str, str]:
    auth_data = {'username': email, 'password': password}

    response: requests.Response = requests.post(url=f'{url}/login', json=auth_data)
    if response.status_code != 200:
        raise Exception(f'Cannot login. Response: {response.text}')

    token: dict = response.json()
    headers = {
        "Authorization": token.get('access_token'),
        "Refresh": token.get('refresh_token')
    }
    return headers


def highlight_section(func: Callable):
    symbols_part: str = '-' * 20
    start_block: str = f'\n{symbols_part} START BLOCK {symbols_part}'
    end_block: str = f'{symbols_part} END BLOCK {symbols_part}\n'

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        print(start_block)

        func(*args, **kwargs)

        print(end_block)

    return wrapper
