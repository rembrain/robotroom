import json
import os
import unittest

import cv2
import numpy as np


class TestCalibration(unittest.TestCase):
    def test_calib_3d3d(self):
        xs = np.array(
            [[164.015259, -105.518463, 538.7759],
             [-149.8186, -124.266266, 540.0133],
             [-207.904068, 6.108761, 558.1124],
             [15.5343018, 69.7926254, 550.172],
             [176.910049, 46.8852539, 588.1157],
             [152.078339, 13.199091, 604.0808],
             [-3.19551873, 7.281184, 458.380463]]
        )

        ys = np.array(
            [[290.7425, 141.586655, 220.124161],
             [280.315643, -161.404556, 220.206375],
             [413.11557, -216.2401, 199.048477],
             [466.2996, 4.307665, 198.855774],
             [438.0387, 153.789368, 169.730347],
             [406.630432, 129.691437, 152.066589],
             [407.861176, -23.9479771, 291.374847]]
        )

        t, r = self.rigid_transform_3d(xs.transpose(), ys.transpose())
        trans = self.trans_from_rt(r, t)
        acc = self.get_accuracy(trans, xs, ys)
        print(acc)

    def test_calib_2d3d(self):
        with open(os.path.join(os.path.dirname(__file__), 'data', 'calib2d.txt')) as f:
            points2D = np.array(json.load(f), dtype=np.float32)

        with open(os.path.join(os.path.dirname(__file__), 'data', 'calib3d.txt')) as f:
            points3D = np.array(json.load(f), dtype=np.float32)

        camera_matrix = np.array(
            [[927.002685546875, 0, 653.373291015625],
             [0, 927.553955078125, 365.9266052246094],
             [0, 0, 1]], dtype=np.float32
        )

        t, r = self.calibration_2d3d(points3D, points2D, camera_matrix)
        dt = np.abs(t - np.array([[-39.78163648], [-17.76233519], [986.51696447]]))
        self.assertLess(np.max(dt), 100)
        dr = np.abs(
            r - np.array(
                [[6.26385847e-03, -8.31408822e-01, -5.55625895e-01],
                 [-9.99967793e-01, -7.99591707e-03, 6.91507105e-04],
                 [-5.01766369e-03, 5.55603669e-01, -8.31432130e-01]])
        )
        self.assertLess(np.max(dr), 0.1)

        trans = self.trans_from_rt(r, t)
        error = self.get_accuracy_2d3d(trans, points3D, points2D, camera_matrix)
        self.assertLess(error, 10)

    def get_accuracy_2d3d(self, trans, robot_points, image_points, camera_matrix):
        rot = trans[:3, :3].T

        t = np.array([np.dot(rot, trans[:, 3]) * (-1)]).T
        camera_matrix = np.array(camera_matrix)
        total_err = 0

        for i in range(len(image_points)):
            s = np.dot(rot, np.array([robot_points[i]]).T) + t
            s = np.dot(camera_matrix, s)
            s /= s[2]
            err = np.sum(np.square(s[:2, 0] - image_points[i]))
            total_err += err
            print(np.sqrt(err))

        return np.sqrt(total_err / (i + 1))

    def calibration_2d3d(self, robot_points, image_points, camera_matrix):
        camera_matrix = np.array(camera_matrix)
        robot_points = np.array(robot_points).astype(np.float64)
        image_points = np.array(image_points).astype(np.float32)

        _, r, t = cv2.solvePnP(robot_points, image_points, camera_matrix, np.zeros(5), flags=cv2.SOLVEPNP_ITERATIVE)
        inverse_r = cv2.Rodrigues(r)[0].T
        inverse_t = - np.dot(inverse_r, t)

        return inverse_t, inverse_r

    def rigid_transform_3d(self, xs, ys):
        """
        expects 2 arrays of shape (3, N)

        rigid transform algorithm from
        http://nghiaho.com/?page_id=671
        """
        assert xs.shape == ys.shape
        assert xs.shape[0] == 3, 'The points must be of dimmensionality 3'

        # find centroids and H
        x_centroid = np.mean(xs, axis=1, keepdims=True)
        y_centroid = np.mean(ys, axis=1, keepdims=True)
        H = np.dot((xs - x_centroid), (ys - y_centroid).T)

        # find rotation
        U, S, Vt = np.linalg.svd(H)
        rotation = np.dot(Vt.T, U.T)

        # handling reflection
        if np.linalg.det(rotation) < 0:
            Vt[2, :] *= -1
            rotation = np.dot(Vt.T, U.T)

        # find translation
        translation = y_centroid - np.dot(rotation, x_centroid)
        return translation, rotation

    def get_accuracy(self, trans, xs, ys):
        av_acc = 0

        for i in range(len(xs)):
            p = np.zeros((4))
            p[0:3] = xs[i][0:3]
            p[3] = 1.0
            p2 = np.dot(trans, p)

            def norm(x):
                return (x[0] ** 2 + x[1] ** 2 + x[2] ** 2)

            av_acc += norm(p2 - ys[i])
            print(np.sqrt(norm(p2 - ys[i])))

        return np.sqrt(av_acc / len(xs))

    def trans_from_rt(self, r, t):
        trans = np.zeros((3, 4))
        trans[:, :3] = r[:, :]
        trans[:, 3] = t[:, 0]

        return trans


if __name__ == '__main__':
    unittest.main()
