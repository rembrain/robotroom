import json
import logging
import os
import time
from threading import Thread
from typing import Set

import redis
from envyaml import EnvYAML
from rembrain_robot_framework import RobotDispatcher
from rembrain_robot_framework.logger import get_log_handler

from common.src.services import register_connection
from common.src.utils import get_rabbit_connection
from monitor.processes import (
    ArchiveWriter,
    CommandsCapture,
    DataUploader,
    MonitoringProcessors,
    StatusRepack,
    StreamExtractor,
    MonitoringWebsockets,
)
from monitor.processes.robot_monitoring.robot_monitor import RobotMonitor


def check_robot(robot_name: str, result):
    camera0 = False
    sensors = False

    connection = get_rabbit_connection()
    channel = connection.channel()

    try:
        queue_name_camera0 = "monitor_camera0_" + robot_name
        channel.queue_declare(queue_name_camera0, durable=False, auto_delete=True, arguments={"x-max-length": 1})
        channel.queue_bind(exchange="camera0_" + robot_name, queue=queue_name_camera0)
        channel.queue_purge(queue_name_camera0)

        time.sleep(3.0)

        method_frame, header_frame, body = channel.basic_get(queue=queue_name_camera0, auto_ack=True)
        if method_frame:
            camera0 = True
    except Exception:
        logging.info("no camera0 exchange " + robot_name)

    try:
        queue_name_sensors = "monitor_state_" + robot_name
        channel.queue_declare(queue_name_sensors, auto_delete=True, arguments={"x-max-length": 1})
        channel.queue_bind(exchange="state_" + robot_name, queue=queue_name_sensors)
        channel.queue_purge(queue_name_sensors)

        time.sleep(10.0)

        method_frame, header_frame, body = channel.basic_get(queue=queue_name_sensors, auto_ack=True)
        if method_frame:
            sensors = True
    except Exception as e:
        logging.info("no state exchange " + robot_name + str(e))

    result["camera0_connected"] = camera0
    result["sensors_connected"] = sensors

    if not channel.is_closed:
        channel.close()

    if not connection.is_closed:
        connection.close()


def observe_robots_status(robots: Set[str]) -> None:
    threads = []
    results = []

    for robot in robots:
        result = {}
        th = Thread(
            target=check_robot,
            args=(
                robot,
                result,
            ),
        )
        results.append(result)

        th.daemon = True
        th.start()
        threads.append(th)

    for th in threads:
        th.join()

    robots_status: dict = {robot: results[i] for i, robot in enumerate(robots)}

    r = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)
    r.set("robots_status", json.dumps(robots_status))

    # send result
    logging.info(f"Updating status with {robots_status}")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
    project_description = {"project": "brainless_robot", "subsystem": "monitor"}
    rabbit = get_log_handler(project_description)
    logging.root.addHandler(rabbit)

    logging.info("start logging")
    robots = set()
    host = RobotDispatcher(in_cluster=True, project_description=project_description)
    host.add_process("pusher_to_s3", DataUploader)
    host.add_process("check_processors", MonitoringProcessors, keep_alive=True)
    host.add_process("check_websockets", MonitoringWebsockets, keep_alive=True)

    cfg_path = os.path.join(os.path.dirname(__file__), "config", "robot_monitor_config.yaml")
    robot_monitor_config = dict(EnvYAML(cfg_path, strict=False))
    host.add_process("robot_monitor", RobotMonitor, keep_alive=True, **robot_monitor_config)

    while not host.check_queues_overflow():
        try:
            connect = register_connection.Connection(
                os.environ["REGISTER_ADDRESS"], os.environ["ADMIN_NAME"], os.environ["ADMIN_PASSWORD"]
            )

            while True:
                robots_prev = robots.copy()
                robots = connect.get_request("robots", "api")
                robots = set([robot["username"] for robot in robots["answer"]])

                logging.info(f"robots under monitoring {robots} ")
                # if some new robots or old missed - relaunch all repacking processes
                for robot in robots_prev - robots:
                    host.stop_process(f"{robot}_stream_extractor")
                    host.stop_process(f"{robot}_status_aggregation")
                    host.stop_process(f"{robot}_data_writer")
                    host.stop_process(f"{robot}_archive_writer")

                    host.del_shared_object(f"{robot}_status")

                for robot in robots - robots_prev:
                    host.add_shared_object(f"{robot}_status", "dict")

                    archive_queue = host.mp_context.Queue(maxsize=1000)
                    host.add_process(
                        f"{robot}_stream_extractor",
                        StreamExtractor,
                        publish_queues={f"{robot}_archive": [archive_queue]},
                        robot_name=robot,
                    )

                    host.add_process(
                        f"{robot}_status_aggregation",
                        StatusRepack,
                        shared_objects_names=[f"{robot}_status"],
                        publish_queues={f"{robot}_archive": [archive_queue]},
                        robot_name=robot,
                    )

                    host.add_process(
                        f"{robot}_commands_capture",
                        CommandsCapture,
                        shared_objects_names=[f"{robot}_status"],
                        publish_queues={f"{robot}_archive": [archive_queue]},
                        robot_name=robot,
                    )
                    host.add_process(
                        f"{robot}_archive_writer",
                        ArchiveWriter,
                        consume_queues={f"{robot}_archive": archive_queue},
                        robot_name=robot,
                    )

                for _ in range(10):
                    observe_robots_status(robots)
                    time.sleep(5.0)

        except ConnectionError as e:
            logging.error(f"ConnectionError: {e}", exc_info=True)

        except Exception as e:
            logging.error(e, exc_info=True)
