import asyncio
import json
import logging
from typing import Dict, List

import aiohttp

from monitor.processes.robot_monitoring.notify import NotificationDriver, NotificationMessageType, NotificationMessage


class SlackDriver(NotificationDriver):
    def __init__(self, *args, **kwargs):
        self.webhook_url = kwargs["webhook_url"]
        self.channel = kwargs["channel"]
        self.log = logging.getLogger(__name__)
        self.hook_name = "Rembrain Monitor Service"

    async def notify(self, msgs: Dict[NotificationMessageType, List[NotificationMessage]]):
        async with aiohttp.ClientSession() as session:
            try:
                data = self._generate_payload(msgs)
                await session.post(self.webhook_url, data=data)
            except Exception as ex:
                self.log.error(ex)

    def _generate_payload(self, messages: Dict[NotificationMessageType, List[NotificationMessage]]):
        payload = {"channel": self.channel, "username": self.hook_name, "icon_emoji": ":mag:"}
        msg_count = sum([len(m) for m in messages.values()])
        pretext = "Monitoring alert!"
        fallback = f"Monitoring: {msg_count} new alert"
        if msg_count != 1:
            fallback += "s"
        fields = []
        for msg_type, msgs in messages.items():
            text = "\n".join([f"{msg.to_text()}" for msg in msgs])
            fields.append({"title": f"🔸 {msg_type.value}", "value": text})
        payload["attachments"] = [{"fallback": fallback, "pretext": pretext, "color": "warning", "fields": fields}]
        return json.dumps(payload)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    driver = SlackDriver()
    messages = {
        NotificationMessageType.EXCHANGE_CREATED: [
            NotificationMessage(message_type=NotificationMessageType.EXCHANGE_CREATED, data="aaa", key="bbb"),
            NotificationMessage(message_type=NotificationMessageType.EXCHANGE_CREATED, data="aaaa", key="bbbb"),
            NotificationMessage(message_type=NotificationMessageType.EXCHANGE_CREATED, data="aaaaa", key="bbbb"),
        ],
        NotificationMessageType.EXCHANGE_INFLOW_STOPPED: [
            NotificationMessage(message_type=NotificationMessageType.EXCHANGE_INFLOW_STOPPED, data="ghjsdfhgksdfg"),
        ],
    }
    asyncio.run(driver.notify(messages))
