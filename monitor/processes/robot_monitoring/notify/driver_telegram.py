import logging

import asyncio
from typing import List, Dict

from aiogram import Bot, md
from aiogram.types import ParseMode

from monitor.processes.robot_monitoring.notify import NotificationDriver, NotificationMessage, NotificationMessageType


class TelegramDriver(NotificationDriver):
    def __init__(self, *args, **kwargs):
        self.bot_token = kwargs["token"]
        self.channel_id = kwargs["channel"]
        self.bot = Bot(token=self.bot_token)
        self.log = logging.getLogger(__name__)

    async def notify(self, messages: Dict[NotificationMessageType, List[NotificationMessage]]):
        msg_content = [md.bold("Monitoring alert!"), ""]
        for msg_type, msgs in messages.items():
            msg_content.append(md.bold(f"🔸 {msg_type.value}:"))
            for msg in msgs:
                msg_content.append(md.escape_md(msg.to_text()))
        msg_text = md.text(*msg_content, sep="\n")
        try:
            await self.bot.send_message(self.channel_id, msg_text, parse_mode=ParseMode.MARKDOWN_V2)
        except Exception as e:
            self.log.error(f"Error while sending telegram message: {e}")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    driver = TelegramDriver()
    messages = {
        NotificationMessageType.EXCHANGE_CREATED: [
            NotificationMessage(message_type=NotificationMessageType.EXCHANGE_CREATED, data="aaa", key="bbb"),
            NotificationMessage(message_type=NotificationMessageType.EXCHANGE_CREATED, data="aaaa", key="bbbb"),
            NotificationMessage(message_type=NotificationMessageType.EXCHANGE_CREATED, data="aaaaa", key="bbbb"),
        ],
        NotificationMessageType.EXCHANGE_INFLOW_STOPPED: [
            NotificationMessage(message_type=NotificationMessageType.EXCHANGE_INFLOW_STOPPED, data="ghjsdfhgksdfg"),
        ],
    }
    asyncio.run(driver.notify(messages))
