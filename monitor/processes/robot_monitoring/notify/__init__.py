import enum
from abc import ABC
from dataclasses import dataclass
from typing import Optional, List, Dict


class NotificationMessageType(enum.Enum):
    EXCHANGE_CREATED = "New exchange created"
    EXCHANGE_DELETED = "Exchange deleted"
    EXCHANGE_TOO_MUCH_OUTFLOW = "Too much outflow compared to inflow for an exchange"
    EXCHANGE_INFLOW_STOPPED = "Inflow stopped on a critical exchange"
    EXCHANGE_INFLOW_STARTED = "Inflow started on a critical exchange"
    MONITOR_STARTED = "Monitor service started"
    RABBIT_MGMT_UNAVAILABLE = "Couldn't connect to Rabbit's management service"
    RABBIT_MEMORY_PERCENTAGE_CRITICAL = "Approaching Rabbit's memory limit"
    RABBIT_MEMORY_TOO_BIG = "RabbitMQ memory usage alert"
    RABBIT_NOT_ENOUGH_DISK_SPACE = "RabbitMQ free disk space alert"
    RABBIT_SOCKET_PERCENTAGE_CRITICAL = "Rabbit's socket limit alert"


@dataclass
class NotificationMessage:
    message_type: NotificationMessageType
    data: object
    # For cooldowns, so one message won't get spammed a lot
    key: Optional[str] = None

    def __str__(self):
        return f"{self.message_type}: {self.data}"

    def to_text(self):
        if self.key and self.key != self.data:
            return f"({self.key}) {self.data}"
        else:
            return f"{self.data}"


class NotificationDriver(ABC):
    async def notify(self, msgs: Dict[NotificationMessageType, List[NotificationMessage]]):
        ...
