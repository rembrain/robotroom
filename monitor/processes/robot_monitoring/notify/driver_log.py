import logging
from typing import List, Dict

from monitor.processes.robot_monitoring.notify import NotificationDriver, NotificationMessage, NotificationMessageType


class LogDriver(NotificationDriver):
    def __init__(self):
        self.log = logging.getLogger(__name__)

    async def notify(self, msgs: Dict[NotificationMessageType, List[NotificationMessage]]):
        for msg in msgs:
            self.log.info(msg)
