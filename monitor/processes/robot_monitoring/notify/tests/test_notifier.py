import asyncio
from unittest.mock import AsyncMock

import pytest

from monitor.processes.robot_monitoring.notify import NotificationMessageType
from monitor.processes.robot_monitoring.notify.notifier import NotifierService


@pytest.fixture
def proc_with_mock_driver():
    proc = NotifierService(notification_delay=0.1)
    mock_driver = AsyncMock()
    proc.drivers.append(mock_driver)
    return proc, mock_driver


@pytest.mark.asyncio
async def test_notification_delay(proc_with_mock_driver):
    proc, mock_driver = proc_with_mock_driver
    await proc.send_msg(NotificationMessageType.EXCHANGE_CREATED, "test", "test123")
    mock_driver.notify.assert_not_awaited()
    await asyncio.sleep(proc.notification_delay * 2)
    mock_driver.notify.assert_awaited()


@pytest.mark.asyncio
async def test_notification_aggregation(proc_with_mock_driver):
    proc, mock_driver = proc_with_mock_driver
    await proc.send_msg(NotificationMessageType.EXCHANGE_CREATED, "test", "test123")
    await proc.send_msg(NotificationMessageType.EXCHANGE_CREATED, "test", "test1234")
    await proc.send_msg(NotificationMessageType.EXCHANGE_CREATED, "test", "test12345")
    await asyncio.sleep(proc.notification_delay * 2)

    mock_driver.notify.assert_awaited_once()
    await_args = mock_driver.notify.await_args[0][0]
    assert len(await_args) == 1
    assert len(await_args[NotificationMessageType.EXCHANGE_CREATED]) == 3
