import asyncio
from datetime import datetime, timedelta
import logging
from typing import List, Optional, Dict

from monitor.processes.robot_monitoring.notify import NotificationDriver, NotificationMessage, NotificationMessageType
from monitor.processes.robot_monitoring.notify.driver_log import LogDriver
from monitor.processes.robot_monitoring.notify.driver_slack import SlackDriver
from monitor.processes.robot_monitoring.notify.driver_telegram import TelegramDriver


class NotifierService:
    def __init__(self, **kwargs):
        self.notification_queue = asyncio.Queue()
        self.notification_delay = kwargs.get("notification_delay", 30.0)  # Seconds
        notification_cooldown = kwargs.get("notification_cooldown", 15.0)  # Minutes
        self.notification_cooldown = timedelta(minutes=notification_cooldown)
        self.drivers: List[NotificationDriver] = self.generate_drivers(**kwargs)
        self.log = logging.getLogger(__name__)
        self._last_sent_times: Dict[NotificationMessageType, Dict[str, datetime]] = {}

    async def send_msg(self, msg_type: NotificationMessageType, msg_data: object, msg_key: Optional[str] = None):
        msg = NotificationMessage(message_type=msg_type, data=msg_data, key=msg_key)
        if await self._is_message_in_cooldown(msg):
            return
        # If there are already items queued up for sending - add them to the queue
        if not self.notification_queue.empty():
            await self._enqueue_message(msg)
        # If there aren't any items, then in this coroutine run we wait for some time
        # and after that send out all messages at once
        else:
            await self._enqueue_message(msg)
            asyncio.create_task(self._send_msgs_after_delay())

    async def _is_message_in_cooldown(self, msg: NotificationMessage) -> bool:
        """
        Returns whether message is in cooldown
        For that we get last time a message of this type with this key was sent
        Then check if the cooldown is still active
        """
        if msg.message_type not in self._last_sent_times:
            return False
        last_sent_time = self._last_sent_times[msg.message_type].get(msg.key, None)
        if last_sent_time is None:
            return False
        cooldown_lift_time = last_sent_time + self.notification_cooldown
        return datetime.now() <= cooldown_lift_time

    async def _enqueue_message(self, msg: NotificationMessage):
        """
        Adds message to the queue to send, and also logs the message sending time for cooldown management
        """
        if msg.message_type not in self._last_sent_times:
            self._last_sent_times[msg.message_type] = {}
        self._last_sent_times[msg.message_type][msg.key] = datetime.now()
        await self.notification_queue.put(msg)

    async def _send_msgs_after_delay(self):
        await asyncio.sleep(self.notification_delay)
        to_send = []
        while not self.notification_queue.empty():
            to_send.append(await self.notification_queue.get())
        await self._notify(self._msg_list_to_dict(to_send))

    @classmethod
    def generate_drivers(cls, **kwargs) -> List[NotificationDriver]:
        res = [LogDriver()]
        driver_map = {"slack": SlackDriver, "telegram": TelegramDriver}
        # Transform a {"notification_drivers": {"telegram": (some config), "slack": (some config)}} dict
        # into drivers (if those entries exist)
        if "notification_drivers" in kwargs:
            driver_cfg = kwargs["notification_drivers"]
            for key, driver_class in driver_map.items():
                if key in driver_cfg and cls.is_driver_enabled(**driver_cfg[key]):
                    res.append(driver_class(**driver_cfg[key]))
        return res

    @staticmethod
    def is_driver_enabled(**kwargs) -> bool:
        # By default - if there's an entry for a driver in a config file, consider it enabled
        if "enabled" not in kwargs:
            return True
        # Since we usually get config values from env vars, they are strings, so handle stringed 0/false
        enabled = kwargs["enabled"]
        if isinstance(enabled, str):
            return enabled.lower() not in ("0", "false")
        # Otherwise cast it
        return bool(enabled)

    @staticmethod
    def _msg_list_to_dict(msgs: List[NotificationMessage]):
        res = {}
        for msg in msgs:
            if msg.message_type not in res:
                res[msg.message_type] = [msg]
            else:
                res[msg.message_type].append(msg)
        return res

    async def _notify(self, msgs: Dict[NotificationMessageType, List[NotificationMessage]]):
        asyncio.ensure_future(asyncio.gather(*(driver.notify(msgs) for driver in self.drivers)))
