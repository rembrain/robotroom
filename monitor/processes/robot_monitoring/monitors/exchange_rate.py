from collections import deque
from typing import Dict, Any, List, Tuple
import itertools

from monitor.processes.robot_monitoring.monitors import MonitorBase
from monitor.processes.robot_monitoring.notify import NotificationMessageType


class RabbitExchangeRatesMonitor(MonitorBase):
    """
    Tracks rabbit's exchange rates.
    Things being monitored
        - Some exchange has too much outflow (10x+ the inflow)
        - An exchange dropping inflow (for specific important queues)
    The notifications are sent out only after a consecutive ammoung of samples
     for the exchange have the issue
    """

    NOTIFICATION_SEND_THRESHOLD = 3

    def __init__(self, notifier, *args, **kwargs):
        super(RabbitExchangeRatesMonitor, self).__init__(notifier)
        self.test_functions = {
            NotificationMessageType.EXCHANGE_TOO_MUCH_OUTFLOW: self.test_too_much_outflow,
            NotificationMessageType.EXCHANGE_INFLOW_STOPPED: self.test_inflow_stopped,
            NotificationMessageType.EXCHANGE_INFLOW_STARTED: self.test_inflow_started,
        }
        self.notification_counters: Dict[NotificationMessageType, Dict[str, int]] = {
            msg_type: {} for msg_type in self.test_functions
        }
        self.inflow_history: Dict[str, deque] = {}

    async def on_data_received(self, data: Dict[str, Any]):
        if "exchange_stats" not in data:
            return
        for exch, (rate_in, rate_out) in self.parse_exchange_rates(data["exchange_stats"]).items():
            if exch not in self.inflow_history:
                self.inflow_history[exch] = deque(maxlen=self.NOTIFICATION_SEND_THRESHOLD * 2)
            self.inflow_history[exch].append(rate_in)
            for msg_type, test_fn in self.test_functions.items():
                if test_fn(exch, rate_in, rate_out):
                    self.notification_tick(msg_type, exch)
                else:
                    self.reset_notification_counter(msg_type, exch)
        await self.send_out_notifications()

    def reset_notification_counter(self, msg_type: NotificationMessageType, key: str):
        self.notification_counters[msg_type][key] = 0

    def notification_tick(self, msg_type: NotificationMessageType, key: str):
        counters = self.notification_counters[msg_type]
        if key not in counters:
            counters[key] = 1
        else:
            counters[key] += 1

    async def send_out_notifications(self):
        for msg_type, counters in self.notification_counters.items():
            for exchange, counter in counters.items():
                if counter >= self.NOTIFICATION_SEND_THRESHOLD:
                    await self.notifier.send_msg(msg_type, exchange, exchange)
                    self.reset_notification_counter(msg_type, exchange)
                    # For the outflow notifications - clear history
                    if msg_type == NotificationMessageType.EXCHANGE_INFLOW_STOPPED:
                        self.inflow_history[exchange].clear()

    @staticmethod
    def parse_exchange_rates(data: List[Dict]) -> Dict[str, Tuple[int, int]]:
        """
        Transform the exchange_stats list to a dictionary of:
        `{exchange: (in, out)}`
        If either of in/out is missing, their value will be 0
        """
        res = {}
        for exchange in data:
            name = exchange["name"]
            in_rate = 0.0
            out_rate = 0.0
            if "message_stats" not in exchange:
                res[name] = (in_rate, out_rate)
                continue
            stats = exchange["message_stats"]
            if "publish_in_details" in stats:
                in_rate = stats["publish_in_details"].get("rate", 0.0)
            if "publish_out_details" in stats:
                out_rate = stats["publish_out_details"].get("rate", 0.0)
            res[name] = (in_rate, out_rate)
        return res

    @staticmethod
    def test_too_much_outflow(exchange: str, rate_in: float, rate_out: float) -> bool:
        return rate_in > 1 and rate_out / rate_in >= 10

    @staticmethod
    def is_important_exchange(exchange: str) -> bool:
        return exchange.startswith("camera0") or exchange.startswith("state")

    def test_inflow_stopped(self, exchange: str, rate_in: float, rate_out: float) -> bool:
        # Test if we are tracking this exchange for inflow stops
        if not self.is_important_exchange(exchange):
            return False
        return sum(self.inflow_history[exchange]) > 0 and rate_in == 0

    def test_inflow_started(self, exchange: str, rate_in: float, rate_out: float) -> bool:
        if not self.is_important_exchange(exchange):
            return False
        history = self.inflow_history[exchange]
        # If the deque is not filled up yet, abandon
        if len(history) < history.maxlen:
            return False
        # split history in half, if the left part's sum is 0, and the right part is not, return true
        # this is relying on the fact that our deque is exactly the size of message send threshold * 2
        left_half = itertools.islice(history, 0, self.NOTIFICATION_SEND_THRESHOLD)
        right_half = itertools.islice(history, self.NOTIFICATION_SEND_THRESHOLD, len(history))
        return sum(left_half) == 0 and sum(right_half) > 0
