import math
from typing import Any, Dict

from monitor.processes.robot_monitoring.monitors import MonitorBase
from monitor.processes.robot_monitoring.notify import NotificationMessageType


class RabbitResourceMonitor(MonitorBase):
    """
    Tracks resource usage of rabbit nodes
    Consumes "node_stats" data
    Tracks following resources:
        - RAM
        - Free disk space (on rabbit node's disk)
        - Sockets
    """

    RABBIT_RAM_PERCENTAGE_THRESHOLD = 0.9
    RABBIT_SOCKETS_PERCENTAGE_THRESHOLD = 0.5
    TOO_MUCH_MEMORY_THRESHOLD = 1024 ** 3
    TOO_LITTLE_DISK_SPACE_THRESHOLD = 10 * (1024 ** 3)

    def __init__(self, notifier, *args, **kwargs):
        super(RabbitResourceMonitor, self).__init__(notifier)

    async def on_data_received(self, data: Dict[str, Any]):
        if "node_stats" not in data:
            return
        # For now assuming we have only 1 rabbit node
        stats = data["node_stats"][0]
        # All values are in bytes
        mem_used = stats["mem_used"]
        mem_limit = stats["mem_limit"]
        disk_free = stats["disk_free"]

        sockets_used = stats["sockets_used"]
        sockets_total = stats["sockets_total"]

        # Warn on reaching 90%+ of rabbit's limit
        if mem_limit > 0:
            mem_percentage = mem_used / float(mem_limit)
            if mem_percentage >= self.RABBIT_RAM_PERCENTAGE_THRESHOLD:
                warn_msg = (
                    f"Rabbit's memory usage is at {mem_percentage:.2%} of RabbitMQ's limit "
                    f"({self.convert_size(mem_used)}/{self.convert_size(mem_limit)})"
                )
                await self.notifier.send_msg(NotificationMessageType.RABBIT_MEMORY_PERCENTAGE_CRITICAL, warn_msg)
        # Warn on RAM usage exceeding 1 GB
        if mem_used >= self.TOO_MUCH_MEMORY_THRESHOLD:
            warn_msg = f"Rabbit's memory usage too big: {self.convert_size(mem_used)}"
            await self.notifier.send_msg(NotificationMessageType.RABBIT_MEMORY_TOO_BIG, warn_msg)
        # Warn on free disk space being less than 10 Gb
        if disk_free <= self.TOO_LITTLE_DISK_SPACE_THRESHOLD:
            warn_msg = f"Too little free disk space on RabbitMQ's partition: {self.convert_size(disk_free)}"
            await self.notifier.send_msg(NotificationMessageType.RABBIT_NOT_ENOUGH_DISK_SPACE, warn_msg)
        # Warn on ammount of open sockets reaching some threshold
        if sockets_total > 0:
            socket_percentage = sockets_used / float(sockets_total)
            if socket_percentage > self.RABBIT_SOCKETS_PERCENTAGE_THRESHOLD:
                warn_msg = (
                    f"Rabbit's socket usage is at {socket_percentage:.2%} of RabbitMQ's limit "
                    f"({sockets_used}/{socket_percentage})"
                )
                await self.notifier.send_msg(NotificationMessageType.RABBIT_SOCKET_PERCENTAGE_CRITICAL, warn_msg)

    @staticmethod
    def convert_size(size_bytes: int):
        if size_bytes == 0:
            return "0B"
        size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
        i = int(math.floor(math.log(size_bytes, 1024)))
        p = math.pow(1024, i)
        s = round(size_bytes / p, 2)
        return f"{s} {size_name[i]}"
