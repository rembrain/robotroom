from unittest.mock import AsyncMock, ANY

import pytest

from monitor.processes.robot_monitoring.monitors import NewExchangeMonitor
from monitor.processes.robot_monitoring.notify import NotificationMessageType


@pytest.fixture
def init_data() -> dict:
    return {
        "exchange_stats": [
            {"name": "exch1"},
            {"name": "exch2"},
        ]
    }


@pytest.fixture
async def proc(init_data) -> NewExchangeMonitor:
    notifier = AsyncMock()
    p = NewExchangeMonitor(notifier)
    await p.on_data_received(init_data)
    return p


@pytest.mark.asyncio
def test_first_data_doesnt_notify(proc):
    notifier: AsyncMock = proc.notifier
    notifier.send_msg.assert_not_awaited()


@pytest.mark.asyncio
async def test_exchange_add(proc, init_data):
    notifier: AsyncMock = proc.notifier
    init_data["exchange_stats"].append({"name": "exch3"})
    await proc.on_data_received(init_data)
    notifier.send_msg.assert_awaited_with(NotificationMessageType.EXCHANGE_CREATED, "exch3", ANY)


@pytest.mark.asyncio
async def test_exchange_delete(proc, init_data):
    notifier: AsyncMock = proc.notifier
    del init_data["exchange_stats"][1]
    await proc.on_data_received(init_data)
    notifier.send_msg.assert_awaited_with(NotificationMessageType.EXCHANGE_DELETED, "exch2", ANY)
