from unittest.mock import AsyncMock, ANY

import pytest

from monitor.processes.robot_monitoring.monitors import RabbitResourceMonitor
from monitor.processes.robot_monitoring.notify import NotificationMessageType


@pytest.fixture
def data() -> dict:
    return {
        "node_stats": [
            {
                "mem_used": 10,
                "mem_limit": 1024 ** 3,
                "disk_free": 60 * (1024 ** 3),
                "sockets_used": 10,
                "sockets_total": 1024 ** 2,
            }
        ]
    }


@pytest.fixture
def proc() -> RabbitResourceMonitor:
    notifier = AsyncMock()
    p = RabbitResourceMonitor(notifier)
    return p


@pytest.mark.asyncio
async def test_memory_limit_percentage(proc, data):
    data["node_stats"][0]["mem_limit"] = 10
    await proc.on_data_received(data)
    proc.notifier.send_msg.assert_any_await(NotificationMessageType.RABBIT_MEMORY_PERCENTAGE_CRITICAL, ANY)


@pytest.mark.asyncio
async def test_memory_too_big(proc, data):
    data["node_stats"][0]["mem_used"] = 3 * (1024 ** 3)
    await proc.on_data_received(data)
    proc.notifier.send_msg.assert_any_await(NotificationMessageType.RABBIT_MEMORY_TOO_BIG, ANY)


@pytest.mark.asyncio
async def test_disk_usage(proc, data):
    data["node_stats"][0]["disk_free"] = 100
    await proc.on_data_received(data)
    proc.notifier.send_msg.assert_any_await(NotificationMessageType.RABBIT_NOT_ENOUGH_DISK_SPACE, ANY)


@pytest.mark.asyncio
async def test_sockets_percentage(proc, data):
    data["node_stats"][0]["sockets_used"] = data["node_stats"][0]["sockets_total"]
    await proc.on_data_received(data)
    proc.notifier.send_msg.assert_any_await(NotificationMessageType.RABBIT_SOCKET_PERCENTAGE_CRITICAL, ANY)
