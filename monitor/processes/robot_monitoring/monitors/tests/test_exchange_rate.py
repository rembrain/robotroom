from unittest.mock import AsyncMock, ANY

import pytest

from monitor.processes.robot_monitoring.monitors.exchange_rate import RabbitExchangeRatesMonitor
from monitor.processes.robot_monitoring.notify import NotificationMessageType


@pytest.fixture
def proc() -> RabbitExchangeRatesMonitor:
    notifier = AsyncMock()
    p = RabbitExchangeRatesMonitor(notifier)
    return p


def test_rate_parsing(proc):
    in_data = [
        {"name": "exch1"},
        {
            "name": "exch2",
            "message_stats": {"publish_in_details": {"rate": 7.6}},
        },
        {
            "name": "exch3",
            "message_stats": {"publish_in_details": {"rate": 5.0}, "publish_out_details": {"rate": 5.0}},
        },
    ]
    expected_data = {"exch1": (0.0, 0.0), "exch2": (7.6, 0.0), "exch3": (5.0, 5.0)}

    actual_data = proc.parse_exchange_rates(in_data)
    assert actual_data == expected_data


@pytest.mark.asyncio
async def test_out_rate_multiplication_sends(proc):
    in_data = {
        "exchange_stats": [
            {
                "name": "exch1",
                "message_stats": {
                    "publish_in_details": {"rate": 2.0},
                    "publish_out_details": {"rate": 30.0},
                },
            }
        ]
    }
    for i in range(proc.NOTIFICATION_SEND_THRESHOLD):
        await proc.on_data_received(in_data)
    proc.notifier.send_msg.assert_awaited_once_with(NotificationMessageType.EXCHANGE_TOO_MUCH_OUTFLOW, ANY, ANY)


@pytest.mark.asyncio
async def test_out_rate_multiplication_doesnt_send_under_threshold(proc):
    in_data = {
        "exchange_stats": [
            {
                "name": "exch1",
                "message_stats": {
                    "publish_in_details": {"rate": 1.0},
                    "publish_out_details": {"rate": 20.0},
                },
            }
        ]
    }
    for i in range(proc.NOTIFICATION_SEND_THRESHOLD - 1):
        await proc.on_data_received(in_data)
    proc.notifier.send_msg.assert_not_awaited()


@pytest.mark.asyncio
async def test_out_rate_multiplication_doesnt_send_if_no_longer_valid(proc):
    in_data = {
        "exchange_stats": [
            {
                "name": "exch1",
                "message_stats": {
                    "publish_in_details": {"rate": 1.0},
                    "publish_out_details": {"rate": 20.0},
                },
            }
        ]
    }

    in_data_valid = {
        "exchange_stats": [
            {
                "name": "exch1",
                "message_stats": {
                    "publish_in_details": {"rate": 1.0},
                    "publish_out_details": {"rate": 1.0},
                },
            }
        ]
    }
    for i in range(proc.NOTIFICATION_SEND_THRESHOLD - 1):
        await proc.on_data_received(in_data)
    await proc.on_data_received(in_data_valid)
    await proc.on_data_received(in_data)
    proc.notifier.send_msg.assert_not_awaited()


@pytest.mark.asyncio
async def test_out_rate_multiplication_no_in_doesntsend(proc):
    in_data = {
        "exchange_stats": [
            {
                "name": "exch1",
                "message_stats": {
                    "publish_in_details": {"rate": 0.0},
                    "publish_out_details": {"rate": 20.0},
                },
            }
        ]
    }
    for i in range(proc.NOTIFICATION_SEND_THRESHOLD):
        await proc.on_data_received(in_data)
    proc.notifier.send_msg.assert_not_awaited()


@pytest.mark.asyncio
async def test_out_rate_multiplication_small_out_doesntsend(proc):
    in_data = {
        "exchange_stats": [
            {
                "name": "exch1",
                "message_stats": {
                    "publish_in_details": {"rate": 2.0},
                    "publish_out_details": {"rate": 2.0},
                },
            }
        ]
    }
    for i in range(proc.NOTIFICATION_SEND_THRESHOLD):
        await proc.on_data_received(in_data)
    proc.notifier.send_msg.assert_not_awaited()


def get_data_for_inflow(in_rate: float) -> dict:
    return {"exchange_stats": [{"name": "camera0_test", "message_stats": {"publish_in_details": {"rate": in_rate}}}]}


@pytest.mark.asyncio
async def test_inflow_stop(proc):
    zero_inflow = get_data_for_inflow(0.0)
    some_inflow = get_data_for_inflow(10.0)

    for i in range(5):
        await proc.on_data_received(some_inflow)
    proc.notifier.send_msg.assert_not_awaited()
    for i in range(proc.NOTIFICATION_SEND_THRESHOLD):
        await proc.on_data_received(zero_inflow)
    proc.notifier.send_msg.assert_awaited_with(NotificationMessageType.EXCHANGE_INFLOW_STOPPED, ANY, ANY)


@pytest.mark.asyncio
async def test_inflow_stop_no_send_on_empty_data(proc):
    zero_inflow = get_data_for_inflow(0.0)
    for i in range(20):
        await proc.on_data_received(zero_inflow)
    proc.notifier.send_msg.assert_not_awaited()


@pytest.mark.asyncio
async def test_inflow_start(proc):
    zero_inflow = get_data_for_inflow(0.0)
    some_inflow = get_data_for_inflow(10.0)
    for i in range(20):
        await proc.on_data_received(zero_inflow)
    for i in range(proc.NOTIFICATION_SEND_THRESHOLD):
        await proc.on_data_received(some_inflow)
    proc.notifier.send_msg.assert_awaited_with(NotificationMessageType.EXCHANGE_INFLOW_STARTED, ANY, ANY)
