from abc import ABC
from typing import Any, Dict

from monitor.processes.robot_monitoring.notify.notifier import NotifierService


class MonitorBase(ABC):
    def __init__(self, notifier: NotifierService):
        self.notifier = notifier

    async def on_data_received(self, data: Dict[str, Any]):
        ...
