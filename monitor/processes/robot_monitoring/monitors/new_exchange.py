from typing import Dict, Any, Set, Optional

from monitor.processes.robot_monitoring.monitors.monitor_base import MonitorBase
from monitor.processes.robot_monitoring.notify import NotificationMessageType


class NewExchangeMonitor(MonitorBase):
    """
    Tracks creation/deletion of exchanges from rabbit
    Consumes "exchange_stats" data
    """

    def __init__(self, notifier, *args, **kwargs):
        super(NewExchangeMonitor, self).__init__(notifier)
        self.prev_exchanges: Optional[Set[str]] = None

    async def on_data_received(self, data: Dict[str, Any]):
        if "exchange_stats" not in data:
            return
        exchanges = set([exchange["name"] for exchange in data["exchange_stats"]])
        if self.prev_exchanges is not None:
            for added_exchange in exchanges - self.prev_exchanges:
                await self.notifier.send_msg(NotificationMessageType.EXCHANGE_CREATED, added_exchange, added_exchange)
            for deleted_exchange in self.prev_exchanges - exchanges:
                await self.notifier.send_msg(
                    NotificationMessageType.EXCHANGE_DELETED, deleted_exchange, deleted_exchange
                )
        self.prev_exchanges = exchanges
