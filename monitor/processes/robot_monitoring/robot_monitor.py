import asyncio
import logging
import os
import queue
import re
from typing import List, Tuple

import aiohttp
from aiostream import stream
from aiohttp import ClientConnectorError
from envyaml import EnvYAML
from rembrain_robot_framework import RobotProcess

from monitor.processes.robot_monitoring.monitors import NewExchangeMonitor, MonitorBase, RabbitResourceMonitor
from monitor.processes.robot_monitoring.monitors.exchange_rate import RabbitExchangeRatesMonitor
from monitor.processes.robot_monitoring.notify import NotificationMessageType
from monitor.processes.robot_monitoring.notify.notifier import NotifierService


class RobotMonitor(RobotProcess):
    def __init__(self, *args, **kwargs):
        # Force start an event loop because it might be required for some constructors down the line
        asyncio.set_event_loop(asyncio.new_event_loop())
        super(RobotMonitor, self).__init__(*args, **kwargs)
        self.rabbit_address, self.rabbit_user, self.rabbit_password = self.parse_rabbit_mgmt_url(
            kwargs["rabbit_mgmt_url"]
        )
        if self.rabbit_address.endswith("/"):
            self.rabbit_address = self.rabbit_address[:-1]
        self._notification_service = NotifierService(**kwargs)
        self._data_generators = [self.get_rabbit_exchange_stats(), self.get_rabbit_node_stats()]

        # DON'T FORGET TO ADD MONITORS HERE
        # TODO: maybe check those at runtime
        self._monitors: List[MonitorBase] = [
            NewExchangeMonitor(self._notification_service),
            RabbitResourceMonitor(self._notification_service),
            RabbitExchangeRatesMonitor(self._notification_service),
        ]

    def run(self):
        self.log.info("Robot monitor running")
        asyncio.run(self.arun())

    async def get_rabbit_exchange_stats(self):
        auth = aiohttp.BasicAuth(login=self.rabbit_user, password=self.rabbit_password)
        endpoint = f"{self.rabbit_address}/api/exchanges"
        while True:
            try:
                async with aiohttp.ClientSession(auth=auth) as session:
                    async with session.get(endpoint) as resp:
                        exchanges = await resp.json()
                        filtered_exchanges = filter(lambda e: e["name"] and not e["name"].startswith("amq."), exchanges)
                        yield {"exchange_stats": list(filtered_exchanges)}
                        await asyncio.sleep(5.0)
            except ClientConnectorError as e:
                reconnect_timeout = 15
                await self._notification_service.send_msg(
                    NotificationMessageType.RABBIT_MGMT_UNAVAILABLE,
                    f"Couldn't connect to {self.rabbit_address}, reconnecting in {reconnect_timeout}s\r\n{e}",
                )
                await asyncio.sleep(reconnect_timeout)

    async def get_rabbit_node_stats(self):
        auth = aiohttp.BasicAuth(login=self.rabbit_user, password=self.rabbit_password)
        endpoint = f"{self.rabbit_address}/api/nodes"
        while True:
            try:
                async with aiohttp.ClientSession(auth=auth) as session:
                    async with session.get(endpoint) as resp:
                        node_stats = await resp.json()
                        yield {"node_stats": node_stats}
                        await asyncio.sleep(5.0)
            except ClientConnectorError as e:
                reconnect_timeout = 15
                await self._notification_service.send_msg(
                    NotificationMessageType.RABBIT_MGMT_UNAVAILABLE,
                    f"Couldn't connect to {self.rabbit_address}, reconnecting in {reconnect_timeout}s:\r\n{e}",
                )
                await asyncio.sleep(reconnect_timeout)

    # TODO: Gather errors from ELK
    # TODO: Gather robot list from WebAPI (also will help to check site's uptime)

    async def arun(self):
        await self._notification_service.send_msg(NotificationMessageType.MONITOR_STARTED, "Monitor started!")

        # Creating an async generator that will spew out data from all our data gatherers
        async for data in stream.merge(*self._data_generators):
            for datamon in self._monitors:
                asyncio.create_task(datamon.on_data_received(data))

    @staticmethod
    def parse_rabbit_mgmt_url(url: str) -> Tuple[str, str, str]:
        regex = re.compile(r"http(s)?://((.+):(.+)@).*")
        match = regex.match(url)
        if match is None:
            raise ValueError(
                f"{url} is not a valid rabbit mgmt url. "
                f"It should have a format like 'http(s)://user:password@web.site:port'"
            )
        groups = match.groups()
        user_pass, username, password = groups[1], groups[2], groups[3]
        clean_url = url.replace(user_pass, "")
        return clean_url, username, password


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    cfg_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "..", "config", "robot_monitor_config.yaml")
    )
    robot_monitor_config = dict(EnvYAML(cfg_path, strict=False))
    monitor = RobotMonitor(
        name="robot_monitor",
        shared_objects={},
        consume_queues={},
        publish_queues={},
        system_queues={},
        watcher_queue=queue.Queue(),
        **robot_monitor_config,
    )
    monitor.run()
