import json
import logging
import os
import threading
from datetime import datetime, timezone
import time

import redis
from rembrain_robot_framework import RobotProcess

from common.src.utils import get_rabbit_connection


class MonitoringWebsockets(RobotProcess):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.pings = []


    def callback(self,ch, method, properties, body):

        ch.basic_ack(delivery_tag=method.delivery_tag)
        o = json.loads(body.decode("utf-8"))
        temp = {'username': '', 'exchange': '', 'command': ''}

        if 'username' in o.keys():
            temp['username'] = o['username']
        if 'exchange' in o.keys():
            temp['exchange'] = o['exchange']
        if 'command' in o.keys():
            temp['command'] = o['command']
        was = False
        for p in self.pings:
            if p['exchange'] == temp['exchange'] and p['command'] == temp['command'] and \
                    p['username'] == temp['username']:
                was = True
        if not was:
            self.pings.append(temp)
    def update(self):
        while True:
            self.pings.clear()
            time.sleep(10.0)
            self.r.set("websocket_connections",json.dumps(self.pings))
        return
    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")
        self.log.info("wait for new pings")
        th = threading.Thread(target=self.update)
        th.daemon=True
        th.start()

        while True:
            try:
                self.r = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)

                self.connection = get_rabbit_connection()
                exchange_name = "websocket_ping_stream"
                self.channel = self.connection.channel()
                self.channel.exchange_declare(exchange=exchange_name, exchange_type="fanout")

                result = self.channel.queue_declare(queue="websocket_ping_stream", durable=False, auto_delete=True)
                queue_name = result.method.queue
                self.channel.queue_bind(exchange=exchange_name, queue=queue_name)

                self.channel.basic_consume(queue=queue_name, on_message_callback=self.callback)
                self.channel.start_consuming()
            except Exception as e:
                self.log.ERROR('websocket_connections exception in start_consuming '+str(e))
                self.channel.close()
                self.connection.close()
