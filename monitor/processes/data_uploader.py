import datetime
import json
import os
import time

import boto3
import pymongo
from botocore.client import Config
from botocore.exceptions import ClientError
from rembrain_robot_framework import RobotProcess

from common.commands import send_robot_command
from common.src.services.mongo.archive import MongoArchiveService
from common.src.utils import get_rabbit_connection, is_local_debug

# in some dockers certificates don't work in other ways
os.environ["SSL_CERT_DIR"] = "/etc/ssl/certs"


class DataUploader(RobotProcess):
    def __init__(self, *args, **kwargs):
        super(DataUploader, self).__init__(*args, **kwargs)
        self.mongo_connection = pymongo.MongoClient(
            os.environ["MONGODB_URI"], connect=False, maxPoolSize=1
        )
        self.archive_db = MongoArchiveService(self.mongo_connection)

        self.rabbit_connection = get_rabbit_connection()
        self.rabbit_channel = self.rabbit_connection.channel()

    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")
        while True:
            for root, subdirs, files in os.walk("to_upload"):
                if len(subdirs) == 0 and len(files) == 0:
                    os.rmdir(root)
                    continue

                if len(files) == 0:
                    continue

                for file in files:
                    f_name = os.path.join(root, file)

                    self.log.info("to upload: " + f_name)
                    object_name = os.path.join(os.path.relpath(root, "to_upload"), file)
                    robot_date_time_file = object_name.split("/")

                    robot = robot_date_time_file[0]
                    date_and_time = (
                        robot_date_time_file[1] + ":" + robot_date_time_file[2]
                    )
                    path = f"{robot_date_time_file[0]}/{robot_date_time_file[1]}/{robot_date_time_file[2]}"
                    file_name = robot_date_time_file[3]

                    if (
                        file_name == "command.json"
                        and self._get_archive_command_on_path(path) is None
                    ):
                        with open(f_name, "r") as f:
                            try:
                                command_json = json.loads(json.load(f))
                                self.handle_command_json(
                                    robot, command_json, date_and_time, path
                                )
                            except ValueError:
                                self.log.error("Command is not a json")

                    if self._upload_file(
                        f_name, os.environ["BUCKET_NAME_ARCHIVE"], object_name
                    ):
                        os.remove(f_name)
                    else:
                        self.log.error(
                            f"Could not upload to {os.environ['BUCKET_NAME_ARCHIVE']} {object_name}"
                        )

            time.sleep(10.0)

    def handle_command_json(self, robot, command_json, date_and_time, path):
        if "op" in command_json.keys():
            command_op = command_json["op"]
            command_body = command_json
        elif "event_type" in command_json.keys():
            command_body = command_json["content"]
            if command_json["event_type"] == "request":
                command_op = "rpc_request:" + command_json["command"]
            else:
                command_op = "rpc_response:" + command_json["command"]
        else:
            return

        if command_body.get("type", "") == "system":
            return

        db_command = self._add_archive_command(
            robot,
            date_and_time,
            path,
            command_operation=command_op,
            command_content=command_body,
        )

        send_robot_command(
            robot,
            {
                "type": "system",
                "op": "add_command",
                "command_id": str(db_command.inserted_id),
            },
            self.rabbit_channel,
        )

    def _get_archive_command_on_path(self, path):
        return self.archive_db.get_command_by_path(path)

    def _add_archive_command(
        self, robot, date_time, path, command_operation, command_content
    ):
        return self.archive_db.save_command(
            {
                "robot": robot,
                "datetime": datetime.datetime.strptime(
                    date_time, "%Y_%b_%d:%H_%M_%S.%f"
                ),
                "path": path,
                "command": command_operation,
                "content": command_content,
            }
        )

    def _upload_file(self, file_name, bucket, object_name) -> bool:
        if is_local_debug():
            return True

        # Upload the file
        if os.environ.get("S3_REGION") is not None:
            s3_client = boto3.client(
                "s3",
                region_name=os.environ["S3_REGION"],
                aws_access_key_id=os.environ["S3_ACCESS_KEY"],
                aws_secret_access_key=os.environ["S3_SECRET_KEY"],
            )
        elif os.environ.get("S3_ADDRESS") is not None:
            s3_client = boto3.client(
                "s3",
                endpoint_url=os.environ["S3_ADDRESS"],
                aws_access_key_id=os.environ["S3_ACCESS_KEY"],
                aws_secret_access_key=os.environ["S3_SECRET_KEY"],
                config=Config(signature_version="s3v4"),
            )
        else:
            self.log.error("There is no region or address to connect to S3 storage")
            return False

        try:
            s3_client.upload_file(file_name, bucket, object_name)
        except ClientError as e:
            self.log.error(e)
            return False

        return True
