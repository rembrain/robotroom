import datetime
import json
import os
import time
import redis
import cv2
from rembrain_robot_framework import RobotProcess

from common.src.utils import get_rabbit_connection


class CommandsCapture(RobotProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.robot = kwargs["robot_name"]

    def run(self):

        r = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)

        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")
        connection = get_rabbit_connection()
        channel = connection.channel()

        exchange_robot = f"commands_{self.robot}"
        channel.exchange_declare(exchange=exchange_robot, exchange_type="fanout", durable=False)

        queue_name = f"data_save_trigger_{self.robot}"
        channel.queue_declare(queue=queue_name, durable=False, auto_delete=True, arguments={"x-max-length": 10})
        channel.queue_bind(exchange=exchange_robot, queue=queue_name)

        def callback(ch, method, properties, body):
            if body is not None:
                self.publish(("command", body, time.time()), queue_name=f"{self.robot}_archive")

                s = body.decode("utf-8")

                # skip system commands
                if s.get('type') == 'system' or s.get('content', {}).get('type') == 'system':
                    return

                if (s is not None) and \
                        (getattr(self.shared, f"{self.robot}_status")["camera0_frame"][0] is not None) and \
                        (getattr(self.shared, f"{self.robot}_status")["camera0_frame"][2] is not None):

                    base_name = (
                        f"./to_upload/{self.robot}/{datetime.datetime.now().strftime('%Y_%b_%d')}/"
                        f"{datetime.datetime.now().strftime('%H_%M_%S.%f')}/"
                    )
                    os.makedirs(os.path.split(base_name)[0], exist_ok=True)

                    with open(base_name + "command.json", "w+") as f:
                        json.dump(s, f)

                    sensors=b'{}'
                    s=r.get(f"{self.robot}_sensors")
                    if not s is None:
                        sensors=s
                    with open(base_name+'state.json','wb') as f:
                        f.write(sensors)

                    cv2.imwrite(base_name + "camera0_rgb.jpg",
                                getattr(self.shared, f"{self.robot}_status")["camera0_frame"][0])
                    if (getattr(self.shared, f"{self.robot}_status")["camera0_frame"][1] is not None):
                        cv2.imwrite(base_name + "camera0_depth.png",
                                getattr(self.shared, f"{self.robot}_status")["camera0_frame"][1])

                    with open(base_name + "camera.json", "w+") as f:
                        json.dump(getattr(self.shared, f"{self.robot}_status")["camera0_frame"][2], f)

                    if "camera1_frame" in getattr(self.shared, f"{self.robot}_status").keys() and \
                        (getattr(self.shared, f"{self.robot}_status")["camera1_frame"][0] is not None) and \
                        (getattr(self.shared, f"{self.robot}_status")["camera1_frame"][2] is not None):

                        cv2.imwrite(base_name + "camera1_rgb.jpg",
                                    getattr(self.shared, f"{self.robot}_status")["camera1_frame"][0])
                        if (getattr(self.shared, f"{self.robot}_status")["camera1_frame"][1] is not None):
                            cv2.imwrite(base_name + "camera1_depth.png",
                                    getattr(self.shared, f"{self.robot}_status")["camera1_frame"][1])
                        with open(base_name + "camera1.json", "w+") as f:
                            json.dump(getattr(self.shared, f"{self.robot}_status")["camera1_frame"][2], f)


                else:
                    self.log.info("Not enough data to write the command to minio")

        channel.basic_consume(queue_name, callback, auto_ack=True)
        channel.start_consuming()
