import json
import os
from datetime import datetime, timezone

import redis
from rembrain_robot_framework import RobotProcess

from common.src.utils import get_rabbit_connection


class MonitoringProcessors(RobotProcess):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.processors = []
        self.r = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)

        self.connection = get_rabbit_connection()
        exchange_name = "robot_ping_stream"
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange=exchange_name, exchange_type="fanout")

        result = self.channel.queue_declare(queue="monitor_robot_ping_stream", durable=False, auto_delete=True)
        queue_name = result.method.queue
        self.channel.queue_bind(exchange=exchange_name, queue=queue_name)

        def callback(ch, method, properties, body):
            ch.basic_ack(delivery_tag=method.delivery_tag)
            o = json.loads(body.decode("utf-8"))
            now = datetime.now(timezone.utc)
            o["last_alive"] = now.timestamp()

            if o["id"] not in [p["id"] for p in self.processors]:
                self.processors.append(o)

            self.processors = [
                p for p in self.processors
                if (now - datetime.fromtimestamp(p["last_alive"], timezone.utc)).seconds <= 3
            ]

            for p in self.processors:
                if p["id"] == o["id"]:
                    p["last_alive"] = now.timestamp()

            self.r.set("processors", json.dumps(self.processors))

        self.channel.basic_consume(queue=queue_name, on_message_callback=callback)

    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")
        self.log.info("wait for new pings")
        self.channel.start_consuming()
