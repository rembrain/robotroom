import os
import time

import redis
from rembrain_robot_framework import RobotProcess

from common.src.utils import get_rabbit_connection


class StatusRepack(RobotProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.robot = kwargs["robot_name"]

    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")
        r = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)

        connection = get_rabbit_connection()
        channel = connection.channel()

        exchange_robot = f"state_{self.robot}"
        channel.exchange_declare(exchange=exchange_robot, exchange_type="fanout", durable=False)

        queue_name = f"sensors_agregator_{self.robot}"
        channel.queue_declare(queue=queue_name, durable=False, auto_delete=True, arguments={"x-max-length": 10})
        channel.queue_bind(exchange=exchange_robot, queue=queue_name)

        def callback(ch, method, properties, body):
            if body is not None:
                self.publish(("state", body, time.time()), queue_name=f"{self.robot}_archive")
                s = body.decode("utf-8")
                r.set(f"{self.robot}_sensors", s)
                getattr(self.shared, f"{self.robot}_status")["sensors"] = s

        channel.basic_consume(queue_name, callback, auto_ack=True)
        channel.start_consuming()
