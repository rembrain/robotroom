import time

import cv2
from rembrain_robot_framework import RobotProcess
from rembrain_robot_framework.pack import PackType, Packer, Unpacker

from common.src.utils import get_rabbit_connection


class StreamExtractor(RobotProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.robot = kwargs["robot_name"]

        self.width = 640
        self.height = 360

        self.width_preview = 260
        self.height_preview = 146

        self.unpacker_0 = Unpacker()
        self.unpacker_1 = Unpacker()
        self.packer = Packer(PackType.JPG)

    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")
        connection = get_rabbit_connection()
        channel = connection.channel()

        publisher_name = f"rgbjpeg_{self.robot}"

        channel.exchange_declare(exchange=publisher_name, exchange_type="fanout", durable=False)
        channel.exchange_declare(exchange=f"{publisher_name}_preview", exchange_type="fanout", durable=False)
        channel.exchange_declare(exchange=f"camera0_{self.robot}", exchange_type="fanout", durable=False)
        channel.exchange_declare(exchange=f"camera1_{self.robot}", exchange_type="fanout", durable=False)

        queue_name_0 = f"camera0_{self.robot}_repacker"
        channel.queue_declare(queue=queue_name_0, durable=False, auto_delete=True, arguments={"x-max-length": 10})
        channel.queue_bind(exchange=f"camera0_{self.robot}", queue=queue_name_0)

        queue_name_1 = f"camera1_{self.robot}_repacker"
        channel.queue_declare(queue=queue_name_1, durable=False, auto_delete=True, arguments={"x-max-length": 10})
        channel.queue_bind(exchange=f"camera1_{self.robot}", queue=queue_name_1)

        def callback_0(ch, method, properties, body):
            if body is not None:
                rgb, depth16, camera = self.unpacker_0.unpack(body)
                self.publish(("camera0", body, time.time()), queue_name=f"{self.robot}_archive")

                getattr(self.shared, f"{self.robot}_status")["camera0_frame"] = rgb, depth16, camera

                rgb = cv2.resize(rgb, (self.width, self.height))
                rgb_preview = cv2.resize(rgb, (self.width_preview, self.height_preview))

                channel.basic_publish(
                    exchange=f"{publisher_name}_preview", routing_key="", body=self.packer.pack(rgb_preview, None)
                )
                channel.basic_publish(exchange=publisher_name, routing_key="", body=self.packer.pack(rgb, None))

        def callback_1(ch, method, properties, body):
            if body is not None:
                rgb, depth16, camera = self.unpacker_1.unpack(body)
                getattr(self.shared, f"{self.robot}_status")["camera1_frame"] = rgb, depth16, camera

        channel.basic_consume(queue_name_0, callback_0, auto_ack=True)
        channel.basic_consume(queue_name_1, callback_1, auto_ack=True)
        channel.start_consuming()
