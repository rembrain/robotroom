import json
import logging
import os
import struct
import time
from datetime import datetime

import boto3
import pymongo
from botocore.client import Config
from rembrain_robot_framework import RobotProcess

from common.src.utils import is_local_debug

# todo replace it in other place - it is not correct to do so hard code!
os.environ["SSL_CERT_DIR"] = "/etc/ssl/certs"
admin_folder = "archive"


class ArchiveWriter(RobotProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.robot = kwargs["robot_name"]
        if os.environ.get("S3_REGION") is not None:
            self.s3_client = boto3.client(
                "s3", 
                region_name=os.environ['S3_REGION'],
                aws_access_key_id=os.environ['S3_ACCESS_KEY'], 
                aws_secret_access_key=os.environ['S3_SECRET_KEY']
            )
        elif os.environ.get("S3_ADDRESS") is not None:
            self.s3_client = boto3.client(
                "s3", 
                endpoint_url=os.environ['S3_ADDRESS'],
                aws_access_key_id=os.environ['S3_ACCESS_KEY'], 
                aws_secret_access_key=os.environ['S3_SECRET_KEY'],
                config=Config(signature_version='s3v4')
            )
        else:
            logging.error("There is no region or address to connect to S3 storage")
        self.client = pymongo.MongoClient(os.environ["MONGODB_URI"], connect=False, maxPoolSize=1)

    def run(self):
        self.log.info(f"{self.__class__.__name__} started, name: {self.name}.")

        frames = []
        start = time.time()
        robot_queue_name = f"{self.robot}_archive"

        while True:
            time.sleep(0.01)
            if not self.is_empty(robot_queue_name):
                data = self.consume(queue_name=robot_queue_name)
                frames.append(data)

            if time.time() - start > 60.0:
                start = time.time()
                o = [{
                    "exchange": frames[i][0],
                    "length": len(frames[i][1]),
                    "time": frames[i][2],
                } for i in range(len(frames))]
                o_bytes = json.dumps(o).encode("utf-8")

                byte_list = [struct.pack("I", len(o_bytes)), o_bytes]
                for i in range(len(frames)):
                    byte_list.append(frames[i][1])
                to_save = b"".join(byte_list)

                if len(to_save) > 6 and not is_local_debug():
                    path = self.robot + '/' + datetime.fromtimestamp(o[0]['time']).strftime(
                        "%Y_%b_%d/%H_%M_%S.%f") + '.bin'
                    self.s3_client.put_object(
                        Body=to_save,
                        Bucket=os.environ['BUCKET_NAME_STREAMS'],
                        Key=path
                    )
                    self._add_archive_streams(self.robot, o[0]['time'], path)

                frames = []

    def _add_archive_streams(self, robot, date_time, path):
        self.client[admin_folder]["streams"].insert_one({
            "robot": robot,
            "datetime": datetime.fromtimestamp(date_time),
            "path": path
        })
