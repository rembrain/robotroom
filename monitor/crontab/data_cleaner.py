import logging
import os
from datetime import datetime, timedelta

import boto3
import botocore
from botocore.client import Config
import pymongo

os.environ["SSL_CERT_DIR"] = "/etc/ssl/certs"

client = pymongo.MongoClient(os.environ["MONGODB_URI"], connect=False, maxPoolSize=1)
archive_folder = "archive"

try:
    if os.environ.get("S3_REGION") is not None:
        s3_client = boto3.client(
            "s3",
            region_name=os.environ['S3_REGION'],
            aws_access_key_id=os.environ['S3_ACCESS_KEY'],
            aws_secret_access_key=os.environ['S3_SECRET_KEY']
        )
    elif os.environ.get("S3_ADDRESS") is not None:
        s3_client = boto3.client(
            "s3",
            endpoint_url=os.environ['S3_ADDRESS'],
            aws_access_key_id=os.environ['S3_ACCESS_KEY'],
            aws_secret_access_key=os.environ['S3_SECRET_KEY'],
            config=Config(signature_version='s3v4')
        )
    else:
        logging.error("There is no region or address to connect to S3 storage")
except (botocore.exceptions.BotoCoreError, boto3.exceptions.Boto3Error) as e:
    logging.error("Couldnt connect AWS S3" + str(e))


def list_folders(s3_client, bucket_name, prefix_name):
    try:
        response = s3_client.list_objects_v2(Bucket=bucket_name, Prefix=prefix_name, Delimiter='/')
        for content in response.get('CommonPrefixes', []):
            yield content.get('Prefix')
    except botocore.exceptions.ClientError as e:
        logging.error("Error in minio", str(e))


def delete_command_minio(s3_client, bucket_name, prefix_name):
    try:
        objects_to_delete = s3_client.list_objects(Bucket=bucket_name, Prefix=prefix_name)

        delete_keys = {'Objects': []}
        delete_keys['Objects'] = [{'Key': k} for k in [obj['Key'] for obj in objects_to_delete.get('Contents', [])]]
        s3_client.delete_objects(Bucket=bucket_name, Delete=delete_keys)
    except botocore.exceptions.ClientError as e:
        logging.error("Error in minio", str(e))


def delete_mongo(data, robot, datetime_storage):
    client[archive_folder][data].delete_many(
        {"robot": robot.rstrip('/'), 'datetime': {'$lt': datetime_storage}}
    )


if __name__ == "__main__":
    logging.info(f"Запуск {__file__}.")

    # date_point_delete = datetime.now().date() - timedelta(days=int(os.environ['STORAGE_DAYS']))
    bucket_list = {
        os.environ['BUCKET_NAME_ARCHIVE']: datetime.now().date() - timedelta(days=int(os.environ['STORAGE_DAYS_ARCHIVE'])),
        os.environ['BUCKET_NAME_STREAMS']: datetime.now().date() - timedelta(
            days=int(os.environ['STORAGE_DAYS_STREAMS'])),
    }
    mongo_list = {
        "command": datetime.now().date() - timedelta(days=int(os.environ['STORAGE_DAYS_ARCHIVE'])),
        "streams": datetime.now().date() - timedelta(days=int(os.environ['STORAGE_DAYS_STREAMS'])),
    }

    # Удаляем данные из minio
    for bucket in bucket_list.keys():
        for robot in list_folders(s3_client, bucket, ''):
            for folder_data in list_folders(s3_client, bucket, robot):
                str_data_command = folder_data.split("/")[1]
                data_command = datetime.strptime(str_data_command, "%Y_%b_%d").date()
                if data_command < bucket_list[bucket]:
                    delete_command_minio(s3_client, bucket, folder_data)

                logging.info("Command data deleted in minio!")

    # Удаляем данные из mongo
    for mongo in mongo_list.keys():
        for robot in client[archive_folder][mongo].distinct("robot"):
            delete_mongo(mongo, robot, datetime.fromordinal(mongo_list[mongo].toordinal()))
            logging.info("Command data deleted in mongo!")
