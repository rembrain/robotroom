import json
import logging
import os
import time
from threading import Lock

import boto3
import qrcode
import redis
from fastapi import APIRouter, HTTPException, Depends, Request
from fastapi.responses import JSONResponse, FileResponse
from keycloak.exceptions import KeycloakError
from starlette import status as status_codes

from common.src.services.keycloak import login_request, get_keycloak_admin, get_keycloak_openid
from common.src.utils import get_rabbit_connection
from tools import depends, util
from tools.db import admin_db
from tools.schemas import RobotCommand, Config, Robot, CurrentUserOut, WiFiQR, SpeakData

router = APIRouter(prefix="/api", tags=["api"], responses={404: {"description": "Not found"}})
redis_connection = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)
voice_lock = Lock()

try:
    if os.environ.get("S3_REGION") is not None:
        polly_client = boto3.client(
            "polly",
            region_name=os.environ["S3_REGION"],
            aws_access_key_id=os.environ["S3_ACCESS_KEY"],
            aws_secret_access_key=os.environ["S3_SECRET_KEY"],
        )
    else:
        logging.error("There is no region or address to connect to S3 storage")

except Exception as e:
    logging.error(f"Couldn't connect AWS S3 {e}")


@router.get("/websocket_connections")
def get_websocket_connections(user_info: dict = Depends(login_request)):
    try:
        websocket_connections = json.loads(redis_connection.get("websocket_connections"))
    except ValueError:
        return HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "The string coming from redis is not json"}
        )

    if websocket_connections is None:
        websocket_connections = []

    return JSONResponse(content={"answer": websocket_connections})


@router.get("/current_user", response_model=CurrentUserOut)
def get_current_user(user_info: dict = Depends(depends.user_info_with_email)):
    """Get user id"""
    try:
        user_robots = admin_db.get_subscription_robot(user_info["email"])
        result = []

        for robot_name in user_robots:
            d = {"name": robot_name}
            robots_status = json.loads(redis_connection.get("robots_status"))

            if robot_name in robots_status.keys():
                connected = (
                    robots_status[robot_name]["camera0_connected"] and robots_status[robot_name]["sensors_connected"]
                )
                d["connected"] = connected

            redis_data = redis_connection.get(robot_name + "_sensors")
            if redis_data is not None and "state_machine" in json.loads(redis_data).keys():
                d["state"] = json.loads(redis_data)["state_machine"]
            else:
                d["state"] = "IDLE"

            if redis_data is not None and "snapshot" in json.loads(redis_data).keys():
                d["shapshot"] = True
            else:
                d["snapshot"] = False

            if redis_data is not None and "arm_connected" in json.loads(redis_data).keys():
                d["arm_connected"] = json.loads(redis_data)["arm_connected"]
            else:
                d["arm_connected"] = False

            d["calibration"] = "OK"

            conf = json.loads(admin_db.get_config(robot_name)["value"])
            if "task_name" in conf.keys():
                d["currentTask"] = conf["task_name"]
            else:
                d["currentTask"] = None

            result.append(d)

        user_info["subscription_robot"] = result
        return JSONResponse(content={"answer": user_info})
    except KeycloakError as e:
        logging.exception(e, exc_info=True)
        raise HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Unable to retrieve list of robots"}
        )


@router.post("/subscription_robot")
def add_subscription_robot(robot_name: str, user_info: dict = Depends(depends.user_info_with_email)):
    try:
        kc_admin = get_keycloak_admin()
        user_robots = kc_admin.get_realm_role_members(role_name="robot")

        for robot in user_robots:
            if robot["username"] == robot_name:

                if not admin_db.add_subscription_robot(user_info["email"], robot_name):
                    raise HTTPException(
                        status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Robot paired already"}
                    )

                return JSONResponse(content={"answer": True})

        raise HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "No robot found with that name"}
        )
    except KeycloakError as e:
        logging.exception(e, exc_info=True)
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Keycloak error"})


@router.delete("/subscription_robot")
def delete_subscription_robot(robot_name: str, user_info: dict = Depends(login_request)):
    try:
        if not admin_db.del_subscription_robot(user_info["email"], robot_name):
            raise HTTPException(
                status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "You are not subscribed to this robot"}
            )

        return JSONResponse(content={"answer": True})

    except KeycloakError as e:
        logging.exception(e, exc_info=True)

        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Wrong name password pair"})


@router.get("/robots_status")
def get_robots_status(user_info: dict = Depends(depends.user_info_with_group)):
    """Get robots status"""
    redis_data = redis_connection.get("robots_status")

    if redis_data is None:
        logging.error("No robots_status in redis")
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail="No robots_status in redis")

    return JSONResponse(content={"answer": json.loads(redis_data)})


@router.get("/processors")
def get_processors(user_info: dict = Depends(depends.user_info_with_group)):
    """Get processors"""
    redis_data = redis_connection.get("processors")
    if redis_data is None:
        return JSONResponse(content={"answer": []})

    return JSONResponse(content={"answer": json.loads(redis_data)})


@router.get("/extract/{robot_name}/{room_name}/{field}")
def extract_one_field(
    robot_name: str, room_name: str, field: str, user_info: dict = Depends(depends.user_info_with_group)
):
    """Get extract"""
    redis_data = redis_connection.get(f"{robot_name}_{room_name}")
    if redis_data is None:
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail="Wrong room or robot")

    redis_data = json.loads(redis_data)
    if field not in redis_data.keys():
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail="Wrong field")

    return JSONResponse(content={"answer": redis_data[field]})


@router.get("/extract/sensors/{robot_name}")
def extract_all_state(robot_name: str, user_info: dict = Depends(depends.user_info_with_group)):
    """Get extract"""
    redis_data = redis_connection.get(f"{robot_name}_sensors")
    if redis_data is None:
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail="Wrong room or robot")

    return JSONResponse(content={"answer": json.loads(redis_data)})


@router.post(
    "/push_command/{robot_name}", responses={200: {"content": {"application/json": {"example": {"answer": True}}}}}
)
def push_command(robot_name: str, robot_comand: RobotCommand, user_info: dict = Depends(depends.user_info_with_group)):
    """Push command"""
    logging.info(f"Push received {robot_name}, time: {time.time()}")

    redis_data = redis_connection.get("robots_status")
    if redis_data is None:
        logging.info("Redis is not ready")
        return HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail="Redis is not ready")

    robots_status = json.loads(redis_data)

    if not (robot_name in robots_status.keys() and robots_status[robot_name]["sensors_connected"]):
        logging.info("Robot does not exist or not connected.")
        raise HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST, detail="Robot does not exist or not connected"
        )

    # todo does query to rabbit block?
    connection = get_rabbit_connection()
    logging.info(f"Push command to pika {robot_comand.command} , time: {time.time()}")
    channel = connection.channel()
    channel.basic_publish(exchange=f"commands_{robot_name}", routing_key="", body=robot_comand.command)
    channel.close()
    connection.close()

    return JSONResponse(content={"answer": True})


@router.get("/users", responses={200: {"content": {"application/json": {"example": {"answer": [{"sub": "..."}]}}}}})
def users(user_info: dict = Depends(depends.user_info_with_group)):
    """Get all users"""
    if "admin" not in user_info["groups"]:
        raise HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Wrong role")

    kc_admin = get_keycloak_admin()
    users_data = kc_admin.get_users()
    robots_data = {robot["username"] for robot in kc_admin.get_realm_role_members(role_name="robot")}

    return JSONResponse(content={"answer": [user for user in users_data if user["username"] not in robots_data]})


@router.delete("/del_user", responses={200: {"content": {"application/json": {"example": {"answer": True}}}}})
def del_user(email: str, user_info: dict = Depends(depends.user_info_with_group)):
    """Delete user"""
    if "admin" not in user_info["groups"]:
        raise HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Wrong role")

    try:
        kc_admin = get_keycloak_admin()
        user = kc_admin.get_users({"email": email})[0]
        kc_admin.delete_user(user_id=user["id"])

    except KeycloakError as e:
        util.keycloak_exception_handler(e)

    except IndexError as e:
        logging.error(e, exc_info=True)
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail="User was not found")

    return JSONResponse(content={"answer": True})


@router.get("/config")
def get_config(robot: str, user_info: dict = Depends(depends.user_info_with_group)):
    """Get config"""
    if "robot" in user_info["groups"]:
        logging.info(f"Get config for robot {user_info['preferred_username']}")
        content = {"answer": admin_db.get_config(user_info["preferred_username"])}
    else:
        content = {"answer": admin_db.get_config(robot)}

    return JSONResponse(content=content)


@router.post("/config", responses={200: {"content": {"application/json": {"example": {"answer": True}}}}})
@router.put("/config", responses={200: {"content": {"application/json": {"example": {"answer": True}}}}})
def set_config(robot: str, config: Config, user_info: dict = Depends(depends.user_info_with_group)):
    """Set config"""
    if not util.is_json(config.value):
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail="Value is not JSON")

    if "robot" in user_info["groups"]:
        admin_db.set_config(user_info["preferred_username"], config.value)
    else:
        admin_db.set_config(robot, config.value)

    return JSONResponse(content={"answer": True})


@router.get("/robots", responses={200: {"content": {"application/json": {"example": {"answer": [{"name": "..."}]}}}}})
def robots(user_info: dict = Depends(depends.user_info_with_email)):
    """Get all robots"""
    kc_admin = get_keycloak_admin()

    if "groups" in user_info and "admin" in user_info["groups"]:
        return JSONResponse(content={"answer": kc_admin.get_realm_role_members(role_name="robot")})

    if ("groups" in user_info) and ("operator" in user_info["groups"] or "processor" in user_info["groups"]):
        user_robots = kc_admin.get_realm_role_members(role_name="robot")
        available_robots = admin_db.get_available_robots(user_info["email"])

        return JSONResponse(
            content={"answer": [robot for robot in user_robots if robot["username"] in available_robots]}
        )


@router.post("/robots", responses={200: {"content": {"application/json": {"example": {"answer": True}}}}})
def new_robot(robot: Robot, user_info: dict = Depends(depends.user_info_with_email)):
    """Create robot"""
    logging.info(f"Adding robot: {robot.name}")
    logging.info(f"User: {user_info}")
    if "groups" in user_info and "admin" in user_info["groups"]:
        try:
            kc_admin = get_keycloak_admin()
            id_new_user = kc_admin.create_user(
                {"username": robot.name, "enabled": True, "credentials": [{"value": robot.password, "type": "password"}]}
            )

            id_role = kc_admin.get_realm_role("robot")
            kc_admin.assign_realm_roles(id_new_user, id_role)
            admin_db.set_config(robot.name, json.dumps({}))
            return JSONResponse(content={"answer": True})

        except KeycloakError as e:
            util.keycloak_exception_handler(e)

    elif "groups" in user_info and ("operator" in user_info["groups"] or "processor" in user_info["groups"]):
        try:
            kc = get_keycloak_openid()
            robot_info = kc.userinfo(kc.token(robot.name, robot.password)["access_token"])
            if "robot" not in robot_info["groups"]:
                raise HTTPException(
                    status_code=status_codes.HTTP_403_FORBIDDEN, detail={"reason": "This user is not a robot"}
                )

            if not admin_db.add_external_user_robot_pair(user_info["email"], robot.name):
                raise HTTPException(
                    status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Robot paired already"}
                )

            return JSONResponse(content={"answer": True})

        except KeycloakError as e:
            logging.exception(e, exc_info=True)
            raise HTTPException(
                status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Wrong name password pair"}
            )


@router.delete("/robots", responses={200: {"content": {"application/json": {"example": {"answer": True}}}}})
def del_robot(name: str, user_info: dict = Depends(depends.user_info_with_email)):
    """Delete robot"""
    if "groups" in user_info and "admin" in user_info["groups"]:
        kc_admin = get_keycloak_admin()
        user_id = kc_admin.get_user_id(name)
        roles = kc_admin.get_realm_roles_of_user(user_id)

        for role in roles:
            if role["name"] == "robot":
                kc_admin.delete_user(user_id=user_id)
                admin_db.del_all_subscription_robot(name)
                return JSONResponse(content={"answer": True})

        raise HTTPException(status_code=status_codes.HTTP_401_UNAUTHORIZED, detail="Robot not found")

    elif "groups" in user_info and ("operator" in user_info["groups"] or "processor" in user_info["groups"]):
        if name not in admin_db.get_available_robots(user_info["email"]):
            raise HTTPException(
                status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Robot is not available"}
            )

        if admin_db.del_external_user_robot_pair(user_info["email"], name):
            return JSONResponse(content={"answer": True}, headers={"Access-Control-Allow-Origin": "*"})

        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Robot does not exist"})


@router.get("/tasks")
def tasks(author: str = None, user_info: dict = Depends(login_request)):
    """Get author tasks or all tasks"""
    if "groups" in user_info and ("admin" in user_info["groups"] or "processor" in user_info["groups"]):
        if author:
            tasks_data = admin_db.get_all_tasks(author)
        else:
            tasks_data = admin_db.get_all_tasks()

        return JSONResponse(content={"answer": tasks_data})
    else:
        if user_info.get("email") is None:
            raise HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="The user has no email")

        return JSONResponse(content={"answer": admin_db.get_all_tasks(user_info["email"])})


@router.get("/task")
def get_task(name: str, author: str, user_info: dict = Depends(login_request)):
    """Get one task with a specified name and author"""
    if "groups" in user_info and "robot" in user_info["groups"]:
        task = admin_db.get_task(author_name=author, name=name)
        return JSONResponse(content={"answer": task})

    if "email" not in user_info:
        raise HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "The user does not have email"}
        )

    task = admin_db.get_task(author_name=author, name=name)
    return JSONResponse(content={"answer": task})


@router.get(
    "/task_to_robot/{robot_name}/{task_name}/{task_author}",
    responses={200: {"content": {"application/json": {"example": {"answer": True}}}}},
)
def task_to_robot(
    robot_name: str, task_name: str, task_author: str, user_info: dict = Depends(depends.user_info_with_group)
):
    """Get task to robot"""
    if "admin" not in user_info["groups"]:
        raise HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Wrong role")

    config = {}
    value_str = admin_db.get_config(robot_name)["value"]
    if value_str is not None:
        config = json.loads(value_str)

    if ":" in task_name:
        raise HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST, detail="symbol : can not be a part of a task name"
        )

    config["task_name"] = f"{task_name}:{task_author}"
    admin_db.set_config(robot_name, json.dumps(config))
    util.update_config(robot_name)
    return JSONResponse(content={"answer": True})


@router.put("/task", responses={200: {"content": {"application/json": {"example": {"answer": True}}}}})
async def put_task(author: str, name: str, request: Request, user_info: dict = Depends(depends.user_info_with_group)):
    """Put task"""
    if "admin" not in user_info["groups"] and "processor" not in user_info["groups"] and "robot" not in user_info['groups']:
        logging.info('wrong role: '+str(user_info['groups']))
        raise HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Wrong role")

    task = await request.body()

    try:
        json.loads(task)
    except Exception as e:
        logging.error(str(e))
        return HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Task is not a json")

    if admin_db.check_task(author, name):
        admin_db.del_task(author, name)

    admin_db.add_task(author, name, task)
    try:
        logging.info(f"Looking for {name} task")
        configs = admin_db.get_all_configs()

        for c in configs:
            conf = json.loads(c["value"])
            if "task_name" in conf and conf["task_name"] == f"{name}:{author}":
                robot_name = c["robot"]
                util.update_config(robot_name)

    except Exception as e:
        logging.error(e, exc_info=True)

    return JSONResponse(content={"answer": True})


@router.post("/save_task")
def save_task(author: str, name: str, task: str, user_info: dict = Depends(depends.user_info_with_group)):
    if "admin" not in user_info["groups"]:
        return HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Wrong role")

    try:
        json.loads(task)
    except Exception as e:
        logging.error(str(e))
        return HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Task is not a json")

    if admin_db.check_task(author, name):
        admin_db.del_task(author, name)

    admin_db.add_task(author, name, task)
    try:
        logging.info(f"Looking for {name} task")
        configs = admin_db.get_all_configs()

        for c in configs:
            conf = json.loads(c["value"])
            if "task_name" in conf and conf["task_name"] == f"{name}:{author}":
                robot_name = c["robot"]
                util.update_config(robot_name)

    except Exception as e:
        logging.error(e, exc_info=True)
        return HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Task doesn't exist."})

    return JSONResponse(content={"answer": "success"})


@router.delete("/task")
def del_task(author: str, name: str, user_info: dict = Depends(depends.user_info_with_group)):
    """Delete task"""
    if "admin" not in user_info["groups"] and "processor" not in user_info["groups"]:
        raise HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Wrong role")

    admin_db.del_task(author, name)
    return JSONResponse(content={"answer": True})


@router.post("/voice_generation/{robot}")
async def voice_generation(robot: str, data: SpeakData, user_info: dict = Depends(login_request)):
    try:
        if user_info.get("groups") is None or ("operator" in user_info["groups"] or "processor" in user_info["groups"]):
            if robot in admin_db.get_available_robots(user_info["email"]):
                util.run_voice_generator_thread(robot, data, polly_client, voice_lock)
                return JSONResponse(content={"answer": True})

            raise HTTPException(
                status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "You are not subscribed to this robot"}
            )

        if "robot" in user_info["groups"]:
            if user_info["preferred_username"] == robot:
                util.run_voice_generator_thread(robot, data, polly_client, voice_lock)
                return JSONResponse(content={"answer": True})

            raise HTTPException(
                status_code=status_codes.HTTP_400_BAD_REQUEST,
                detail={"reason": "The robot can only use this method on itself"},
            )

        if "admin" in user_info["groups"]:
            util.run_voice_generator_thread(robot, data, polly_client, voice_lock)
            return JSONResponse(content={"answer": True})

        return JSONResponse(content={"answer": user_info.get("groups")})
    except Exception as e:
        logging.error(e, exc_info=True)
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail={"reason": "Something went wrong."})


# todo where is auth
@router.post("/wifi_qr_generator")
async def wifi_qr_generator(wifi: WiFiQR):
    """Get wi-fi QR code"""

    s = f"WIFI:S:{wifi.SSID};T:WPA;P:{wifi.password};"
    logging.info(f"wifi_qr_generator {s}")

    img = qrcode.make(s)
    img.save("qr.png")

    return FileResponse("qr.png", media_type="image/png")
