import logging
import os
import datetime

from botocore.exceptions import ClientError
from fastapi import APIRouter, HTTPException, Depends, UploadFile, File
from fastapi.responses import JSONResponse, StreamingResponse
from starlette import status as status_codes

from tools import depends, util
from tools.schemas import Logs
from tools.db import admin_db, logs_db

from common.src.services.keycloak import login_request

router = APIRouter(prefix="/logs", tags=["logs"], responses={404: {"description": "Not found"}})
bucket_name_logs = os.environ["BUCKET_NAME_LOGS"]
bucket_name_artifacts = os.environ["BUCKET_NAME_ARTIFACTS"]
s3_client = util.get_s3_client()


@router.post(
    "/add_record/{robot_name}",
    responses={200: {"content": {"application/json": {"example": {"answer": True}}}}}
)
def add_record(robot_name: str, logs: Logs, user_info: dict = Depends(depends.user_info_with_group)):
    """Add a record to the logs"""
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Wrong role")

    logs_db.add_record(robot_name, logs.filenames, logs.params)
    return JSONResponse(content={"answer": True})


@router.get(
    "/get_records/{robot_name}",
    responses={200: {"content": {"application/json": {"example": ["..."]}}}}
)
def get_records(robot_name: str, user_info: dict = Depends(depends.user_info_with_email)):
    """Get all the log records"""
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        if robot_name not in admin_db.get_available_robots(user_info["email"]):
            raise HTTPException(status_code=400, detail={"reason": "Robot is not available"})

    return JSONResponse(content=logs_db.get_all_records(robot_name))


@router.get("/get_records_count/{robot_name}", responses={200: {"content": {"application/json": {"example": "2"}}}})
def get_records_count(robot_name: str, user_info: dict = Depends(login_request)):
    """Get the number of records from the logs"""
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        if robot_name not in admin_db.get_available_robots(user_info["email"]):
            raise HTTPException(
                status_code=status_codes.HTTP_400_BAD_REQUEST,
                detail={"reason": "Robot is not available"}
            )

    return JSONResponse(content={"answer": logs_db.get_records_count()})


# todo where is auth
@router.get("/get_file/{robot_name}/{filename}")
def get_file(
        robot_name: str,
        filename: str
        # , user_info: dict = Depends(login_request)
):
    """Get a certain record from the logs"""
    # if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
    #    if robot_name not in db.get_available_robots(user_info["email"]):
    #        raise HTTPException(status_code=400, detail={"reason": "Robot is not available"})
    logging.info(f"Returning file  with robot_name={robot_name}, filename = {filename}")
    file_key = f"{robot_name}/{filename}"

    try:
        response = s3_client.get_object(Bucket=bucket_name_logs, Key=file_key)
    except ClientError:
        logging.error("Couldn't load file")
        return HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST,
            detail="No corresponding file in datastorage"
        )

    return StreamingResponse(response["Body"])


@router.get("/artifacts/{filename}")
def get_artifact(filename: str, user_info: dict = Depends(login_request)):
    try:
        logging.info(filename)
        response = s3_client.get_object(Bucket=bucket_name_artifacts, Key=filename)
    except ClientError:
        logging.error("Couldnt load artifact")
        return HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST,
            detail="No corresponding file in datastorage"
        )

    return StreamingResponse(response["Body"])


@router.post("/artifacts")
async def upload_artifact(file: UploadFile = File(...), user_info: dict = Depends(login_request)):
    try:
        data = file.file._file
        path_file_minio = f"{datetime.datetime.now().strftime('%Y_%b_%d_%H_%M_%S.%f')}--{file.filename}"
        s3_client.upload_fileobj(data, bucket_name_artifacts, path_file_minio)
    except ClientError as e:
        logging.error(e)
        return False

    return {"filename": path_file_minio}
