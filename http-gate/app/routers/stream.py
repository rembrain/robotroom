import datetime
import io
import json
import logging
import math
import os

import boto3
import botocore
import redis
from botocore.client import Config
from botocore.exceptions import ClientError
from fastapi import APIRouter, HTTPException, Depends
from fastapi.responses import StreamingResponse, JSONResponse
from starlette import status as status_code

from common.src.services.keycloak import login_request
from tools import depends
from tools.amqp_stream import AmqpStream
from tools.db import archive_db
from tools.util import get_one_sub_image, get_s3_client

router = APIRouter(prefix="/stream", tags=["stream"], responses={404: {"description": "Not found"}})
red = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)
s3_client = get_s3_client()


# todo where is auth
@router.get("/stream_rgb/{robot}")
def video_feed(robot: str):
    robots_status = json.loads(red.get("robots_status"))
    if robot in robots_status.keys():
        stream = AmqpStream(robot, redis_connect=red)
        return StreamingResponse(stream.stream_generation(), media_type="multipart/x-mixed-replace; boundary=frame")

    raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="Robot does not exist")


# todo it is the same that 'video_feed' but with preview
# todo where is auth
@router.get("/stream_rgb_preview/{robot}")
def video_preview_feed(robot: str):
    robots_status = json.loads(red.get("robots_status"))
    if robot in robots_status.keys():
        stream = AmqpStream(robot, redis_connect=red, preview=True)
        return StreamingResponse(stream.stream_generation(), media_type="multipart/x-mixed-replace; boundary=frame")

    raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="Robot does not exist")


# todo where is auth
@router.get("/get_subimg/{robot}")
def get_subimg(robot: str, rect: str):
    rect = json.loads(rect)
    logging.info(f"rect {rect} {type(rect)}")

    if isinstance(rect, str):
        rect = json.loads(rect)

    robots_status = json.loads(red.get("robots_status"))
    if robot not in robots_status.keys():
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="Robot does not exist")

    logging.info(f"get_subimg {robot} {rect}")
    return StreamingResponse(io.BytesIO(get_one_sub_image(robot, rect)), media_type="image/jpeg")


# todo dublicate 'get_list_archive_files' from session ?
@router.get("/list_archive_stream", )
def get_list_archive_stream(
        page_num: int,
        robot: str,
        start: str = None,
        stop: str = None,
        sort: str = None,
        sort_direction: int = None,
        user_info: dict = Depends(depends.user_info_with_email)
):
    page_size = 20

    if ("groups" not in user_info) or ("robot" in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    filter_params = {"robot": robot}
    if start or stop:
        filter_params["datetime"] = {}
        try:
            if start:
                filter_params["datetime"]["$gte"] = datetime.datetime.strptime(start, "%Y-%b-%d : %H-%M-%S.%f")

            if stop:
                filter_params["datetime"]["$lt"] = datetime.datetime.strptime(stop, "%Y-%b-%d : %H-%M-%S.%f")
        except ValueError as e:
            logging.warning(str(e))
            raise HTTPException(
                status_code=status_code.HTTP_400_BAD_REQUEST,
                detail={"reason": "Not valid date and time format"}
            )

    total_pages = math.ceil(archive_db.get_filter_count_archive_streams(filter_params) / page_size)
    if total_pages == 0:
        return JSONResponse(content={"answer": {"number_page": 0, "total_pages": 0, "amount_logs": 0, "files": []}})

    if (page_num > total_pages) or (page_num < 1):
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail={"reason": "This page does not exist"})

    if sort is not None and sort_direction is not None and (sort_direction == -1 or sort_direction == 1):
        logging.error('check')
        files = archive_db.get_filter_pagination_archive_streams(page_num, page_size, filter_params, sort,
                                                                 sort_direction)
    else:
        files = archive_db.get_filter_pagination_archive_streams(page_num, page_size, filter_params)

    return JSONResponse(content={"answer": {"number_page": page_num, "total_pages": total_pages, "files": files}})


@router.get("/robot_archive_streams")
def count_archive_streams(user_info: dict = Depends(login_request)):
    return archive_db.get_robots_archive_streams()


@router.get("/archive_stream")
def get_list_archive_stream(path: str, user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("robot" in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    try:
        s3_client.head_object(Bucket=os.environ['BUCKET_NAME_STREAMS'], Key=path)
    except ClientError as e:
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="There is no such file")

    return s3_client.generate_presigned_url(
        'get_object',
        Params={'Bucket': os.environ['BUCKET_NAME_STREAMS'], 'Key': path},
        ExpiresIn=60
    )
