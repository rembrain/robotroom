from typing import Optional

from fastapi import APIRouter, HTTPException, Depends, Header
from fastapi.responses import JSONResponse
from keycloak.exceptions import KeycloakError

from tools import depends, util
from tools.docs import user
from tools.keycloak import get_keycloak_token, login_request, get_keycloak_openid, get_keycloak_admin
from tools.schemas import Login, LoginSocial, UserRegister, UserRegisterClient, UserUpdate, UserPassword

router = APIRouter(tags=["user"], responses=user.responses)


# LOGIN <---------------------------------------------------------------------->
@router.post("/login", responses=user.description["POST:login"])
async def login(user: Login):
    """
    This method allows you to get access tokens to use the RemBrain platform. 
    These tokens need to be sent with each request in the **Authorisation** and **Refresh** headers.
    """
    try:
        token = get_keycloak_token(user.username, user.password)
        content = {
            "access_token": token["access_token"],
            "refresh_token": token["refresh_token"]
        }
    except KeycloakError as e:
        util.keycloak_exception_handler(e)

    return JSONResponse(content=content)


# LOGIN SOCIAL <--------------------------------------------------------------->
@router.post("/login_social", responses=user.description["POST:login_social"])
async def login_social(user: LoginSocial):
    """
    User login using social networks. If the user is not registered in the system, 
    it will automatically register, but if an account with the same email already existed, 
    it will not be possible to log in using google.

    At the moment the api only supports 1 providers: google  
    You need to log in to the ISP application and get an access_token there, and then pass it to this method.  
    client_id of social providers to be used:
    * google: 164929131475-e181sv008q04gvftp7qnrik0aodf6p8e.apps.googleusercontent.com
    """
    try:
        if user.provider not in ["google"]:
            raise HTTPException(status_code=400, detail="Inappropriate social provider")

        kc = get_keycloak_openid()
        token = kc.token_exchange(user.token, user.provider)
        content = {
            "access_token": token["access_token"],
            "refresh_token": token["refresh_token"]
        }

    except KeycloakError as e:
        util.keycloak_exception_handler(e)

    return JSONResponse(content=content)


# todo where is auth
# REFRESH <-------------------------------------------------------------------->
@router.post("/refresh", responses=user.description["POST:refresh"])
async def refresh(refresh: Optional[str] = Header(None)):
    """
    This method should be used to update the access token, it is about to expire. 
    If the request succeeds, you will get the same response as in the "login" method"""
    if refresh:
        try:
            kc = get_keycloak_openid()
            token = kc.refresh_token(refresh)
            content = {
                "access_token": token["access_token"],
                "refresh_token": token["refresh_token"]
            }
            return JSONResponse(content=content)

        except KeycloakError as e:
            util.keycloak_exception_handler(e)

    raise HTTPException(status_code=400, detail="There is no refresh token in the header")


# todo where is auth
# LOGOUT <--------------------------------------------------------------------->
@router.post("/logout", responses=user.description["POST:logout"])
def logout(refresh: Optional[str] = Header(None)):
    """
    This method is necessary to instruct the system to log out. 
    If requested correctly, the keycloak will retrieve the logout information and also 
    remove the tokens currently in use (they cannot be used to log in). 
    """
    if refresh:
        try:
            kc = get_keycloak_openid()
            kc.logout(refresh)
            return JSONResponse(content={"answer": True})

        except KeycloakError as e:
            util.keycloak_exception_handler(e)

    raise HTTPException(status_code=400, detail="There is no refresh token in the header")


# USER <------------------------------------------------------------------------>
@router.get("/user", responses=user.description["GET:user"])
def get_user(user_id: str = None, user_info: dict = Depends(depends.user_info_with_email)):
    """
    The method allows you to get information about the user. 
    The administrator can view information about any user by passing user_id. 
    If user_id is not passed, the information about the user who sent the request will be shown.
    """
    try:
        kc_admin = get_keycloak_admin()

        if ("groups" in user_info) and ("admin" in user_info["groups"]):
            if user_id is not None:
                roles = [
                    role["name"] for role in kc_admin.get_realm_roles_of_user(user_id)
                    if "default-roles" not in role["name"]
                ]
                info_user = kc_admin.get_user(user_id)
                info_user["groups"] = roles

                return JSONResponse(content={"answer": info_user})

            if user_info.get("sub") is not None:
                roles = [
                    role["name"] for role in kc_admin.get_realm_roles_of_user(user_info.get("sub"))
                    if "default-roles" not in role["name"]
                ]

                info_user = kc_admin.get_user(user_info.get("sub"))
                info_user["groups"] = roles
                return JSONResponse(content={"answer": info_user})

            raise HTTPException(status_code=400, detail="User id not found.")

        if user_info.get("sub") is not None:
            roles = [
                role["name"] for role in kc_admin.get_realm_roles_of_user(user_info.get("sub"))
                if not "default-roles" in role["name"]
            ]
            info_user = kc_admin.get_user(user_info.get("sub"))
            info_user["groups"] = roles

            return JSONResponse(content={"answer": info_user})

        raise HTTPException(status_code=400, detail="User id not found.")

    except KeycloakError as e:
        util.keycloak_exception_handler(e)


# REGISTER <------------------------------------------------------------------->
@router.post("/register", responses=user.description["POST:register"])
def register(user: UserRegister, user_info: dict = Depends(depends.user_info_with_group)):
    """
    This method is only available for logged in users with the "Admin" role. 
    It allows you to create any user with any role. Once this method is executed, 
    it will be possible to log in with the new user details.

    Possible roles:
    * "admin"
    * "operator"
    * "processor"
    * "robot"
    * "default" normal user
    """

    kc_admin = get_keycloak_admin()

    if "admin" not in user_info["groups"]:
        raise HTTPException(status_code=403, detail="Wrong role")

    if (kc_admin.get_users({"username": user.username})) or (kc_admin.get_users({"email": user.email})):
        raise HTTPException(status_code=400, detail="A user with this email or username exists")

    id_new_user = kc_admin.create_user({
        "username": user.username,
        "email": user.email,
        "enabled": True,
        "credentials": [{"value": user.password, "type": "password"}]
    })

    if user.role != "default":
        for role in kc_admin.get_realm_roles():

            if role["name"] == user.role and user.role != "robot":
                id_role = kc_admin.get_realm_role(user.role)
                kc_admin.assign_realm_roles(id_new_user, id_role)
                return JSONResponse(content={"answer": True})

        raise HTTPException(status_code=400, detail="There is no such role")

    return JSONResponse(content={"answer": True})


# UPDATE <--------------------------------------------------------------------->
@router.put("/update", responses=user.description["PUT:update"])
def update(user: UserUpdate, user_id: str = None, user_info: dict = Depends(depends.user_info_with_email)):
    """
    This method is used to change user data. The administrator can change the data of any user by specifying, user_id. 
    Users with other roles can only change their data. Only the administrator can change the role.
    """

    update_data = {}
    if user.email is not None:
        update_data["email"] = user.email

    if user.firstName is not None:
        update_data["firstName"] = user.firstName

    if user.lastName is not None:
        update_data["lastName"] = user.lastName

    if user.vendor is not None:
        update_data["attributes"] = {"vendor": user.vendor}

    try:
        kc_admin = get_keycloak_admin()

        if ("groups" in user_info) and ("admin" in user_info["groups"]):
            if user.role is not None:
                id_target_user = user_id if user_id is not None else user_info.get("sub")

                if user.role != "default":
                    for role in kc_admin.get_realm_roles():
                        if role["name"] == user.role and user.role != "robot":
                            roles = kc_admin.get_realm_roles_of_user(user_id=id_target_user)
                            kc_admin.delete_user_realm_role(user_id=id_target_user, payload=roles)

                            id_role = kc_admin.get_realm_role(user.role)
                            kc_admin.assign_realm_roles(id_target_user, id_role)
                else:
                    roles = kc_admin.get_realm_roles_of_user(user_id=id_target_user)
                    kc_admin.delete_user_realm_role(user_id=id_target_user, payload=roles)

            if user_id is not None:
                kc_admin.update_user(user_id=user_id, payload=update_data)
                return JSONResponse(content={"answer": kc_admin.get_user(user_id)})

            if user_info.get("sub") is not None:
                kc_admin.update_user(user_id=user_info.get("sub"), payload=update_data)
                return JSONResponse(content={"answer": kc_admin.get_user(user_info.get("sub"))})

            raise HTTPException(status_code=400, detail="User id not found.")

        if user_info.get("sub") is not None:
            kc_admin.update_user(user_id=user_info.get("sub"), payload=update_data)
            return JSONResponse(content={"answer": kc_admin.get_user(user_info.get("sub"))})

        raise HTTPException(status_code=400, detail="User id not found.")

    except KeycloakError as e:
        util.keycloak_exception_handler(e)


# UPDATE_PASSWORD <------------------------------------------------------------>
@router.put("/update_password", responses=user.description["PUT:update_password"])
def update_password(user: UserPassword, user_id: str = None, user_info: dict = Depends(depends.user_info_with_email)):
    """
    This method is used to update a user's password. 
    The administrator can change the password of any user by passing the user_id parameter, 
    the administrator does not need to know the old password. Users with other roles can only change their password.
    """
    try:
        kc_admin = get_keycloak_admin()

        if ("groups" in user_info) and ("admin" in user_info["groups"]):
            if user_id is not None:
                kc_admin.set_user_password(user_id=user_id, password=user.new_password, temporary=False)
                return JSONResponse(content={"answer": True})

            if get_keycloak_token(user_info["preferred_username"], user.old_passord).get("access_token") is None:
                raise HTTPException(status_code=400, detail="No user with this username and password was found.")

            if user_info.get("sub") is not None:
                kc_admin.set_user_password(user_id=user_info.get("sub"), password=user.new_password, temporary=False)
                return JSONResponse(content={"answer": True})

        if user.old_passord is not None:
            if get_keycloak_token(user_info["preferred_username"], user.old_passord).get("access_token") is None:
                raise HTTPException(status_code=400, detail="No user with this username and password was found.")

            if user_info.get("sub") is not None:
                kc_admin.set_user_password(user_id=user_info.get("sub"), password=user.new_password, temporary=False)
                return JSONResponse(content={"answer": True})

            raise HTTPException(status_code=400, detail="User id not found.")

        raise HTTPException(status_code=422, detail="The old password is not listed.")

    except KeycloakError as e:
        util.keycloak_exception_handler(e)


# DELETE <--------------------------------------------------------------------->
@router.delete("/delete", responses=user.description["DELETE:delete"])
def delete(user_id: str = None, user_info: dict = Depends(depends.user_info_with_email)):
    """
    This method is necessary to remove users from the platform system. 
    The administrator can remove any user by passing the user id to the query parameter 
    *user_id*, but if this parameter is not passed, the administrator will remove himself.
    A user with another role, can only delete his account, regardless of whether he sent the user id or not.
    """
    try:
        kc_admin = get_keycloak_admin()

        if ("groups" in user_info) and ("admin" in user_info["groups"]):
            if user_id is not None:
                kc_admin.delete_user(user_id=user_id)
                return JSONResponse(content={"answer": True})

            if user_info.get("sub") is not None:
                kc_admin.delete_user(user_id=user_info.get("sub"))
                return JSONResponse(content={"answer": True})

            raise HTTPException(status_code=400, detail="User id not found.")

        if user_info.get("sub") is not None:
            kc_admin.delete_user(user_id=user_info.get("sub"))
            return JSONResponse(content={"answer": True})

        raise HTTPException(status_code=400, detail="User id not found.")

    except KeycloakError as e:
        util.keycloak_exception_handler(e)


# REGISTER CLIENT <------------------------------------------------------------>
@router.post("/register_client", responses=user.description["POST:register_client"])
def register_client(user: UserRegisterClient):
    """
    This method is available to all users, even those who are not logged in. 
    It registers a user without a role (client), with the data entered.
    """
    kc_admin = get_keycloak_admin()

    if (kc_admin.get_users({"username": user.username})) or (kc_admin.get_users({"email": user.email})):
        raise HTTPException(status_code=400, detail="A user with this email or username exists")

    kc_admin.create_user({
        "username": user.username,
        "email": user.email,
        "enabled": True,
        "credentials": [{"value": user.password, "type": "password"}]
    })
    return JSONResponse(content={"answer": True})
