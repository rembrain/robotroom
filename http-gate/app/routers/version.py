from fastapi import APIRouter
from fastapi.responses import JSONResponse

router = APIRouter(tags=["version"], prefix="/version")


@router.get("/")
def get_current_api_version():
    return JSONResponse({"version": "0.0.1"})
