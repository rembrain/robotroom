import json
import logging
import os
import redis

from botocore.exceptions import ClientError
from fastapi import APIRouter, HTTPException
from fastapi.responses import StreamingResponse
from starlette import status as status_code

from tools import util

router = APIRouter(prefix="/snapshot", tags=["snapshot"], responses={404: {"description": "Not found"}})
redis_connection = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)
s3_client = util.get_s3_client()


# todo why without auth ?
@router.get("/get_file/{robot_name}")
def get_file(robot_name: str):
    """Get snapshot with robot_name"""

    logging.info(f"Returning snapshot with robot_name={robot_name}")
    bucket_name = "robots.oneshot"
    redis_data = redis_connection.get(robot_name + "_sensors")

    if redis_data is None:
        logging.error(f"No {robot_name}_sensors in redis")
        return HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail=f"No {robot_name}_sensors")

    redis_data = json.loads(redis_data)
    if "snapshot" not in redis_data.keys():
        logging.error(f"No snapshot in {robot_name}_sensors in redis")
        return HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="No snapshot")

    filename = redis_data["snapshot"]["subimg"]
    file_key = robot_name + "/" + filename

    try:
        response = s3_client.get_object(Bucket=bucket_name, Key=file_key)
    except ClientError as e:
        logging.error(f"Couldn't load file {str(e)}")
        return HTTPException(
            status_code=status_code.HTTP_400_BAD_REQUEST,
            detail="No corresponding file in datastorage"
        )

    return StreamingResponse(response["Body"])
