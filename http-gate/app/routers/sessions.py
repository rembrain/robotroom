import datetime
import json
import logging
import math
import os
import threading
import typing as T
import requests
from requests.auth import HTTPBasicAuth

from botocore.exceptions import ClientError
from bson.objectid import ObjectId
from fastapi import APIRouter, HTTPException, Depends, Query, Body
from fastapi.responses import JSONResponse, StreamingResponse
from starlette import status as status_code

from common.src.services.keycloak import login_request
from tools import depends, util
from tools.db import archive_db, sessions_db, session_train_db
from tools.schemas import Sessions, GetListArchiveFilesOut, SessionsName, SessionsTrain, Login

router = APIRouter(prefix="/sessions", tags=["sessions"], responses={404: {"description": "Not found"}})
bucket_name_sessions = os.environ["BUCKET_NAME_SESSIONS"]
s3_client = util.get_s3_client()


# Archive
@router.get("/get_robot_in_archive_command")
def get_robot_in_archive_command(user_info: dict = Depends(login_request)):
    return archive_db.get_robots_archive_command()


@router.get("/get_list_command")
def get_robot_in_archive_command(user_info: dict = Depends(login_request)):
    return archive_db.get_list_command()


@router.get("/list_archive_files", response_model=GetListArchiveFilesOut)
def get_list_archive_files(
        page_num: int,
        robot: str,
        command: T.Optional[T.List[str]] = Query(None),
        start: str = None,
        stop: str = None,
        sort: str = None,
        sort_direction: int = None,
        user_info: dict = Depends(depends.user_info_with_group)
):
    page_size = 20

    if ("groups" not in user_info) or ("robot" in user_info["groups"]):
        if "robot" in user_info["groups"]:
            if  user_info["upn"]!=robot:
                raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Robot can not access others sessions than his own")
        else:
            raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    filter_params = {"robot": robot}
    if command is not None:
        filter_params["$or"] = [{"command": c} for c in command]

    if start or stop:
        filter_params["datetime"] = {}

        try:
            if start:
                filter_params["datetime"]["$gte"] = datetime.datetime.strptime(start, "%Y-%b-%d : %H-%M-%S.%f")

            if stop:
                filter_params["datetime"]["$lt"] = datetime.datetime.strptime(stop, "%Y-%b-%d : %H-%M-%S.%f")

        except ValueError as e:
            logging.warning(str(e))
            raise HTTPException(
                status_code=status_code.HTTP_400_BAD_REQUEST,
                detail={"reason": "Not valid date and time format"}
            )

    total_pages = math.ceil(archive_db.get_filter_count_archive_command(filter_params) / page_size)
    if total_pages == 0:
        return JSONResponse(content={"answer": {"number_page": 0, "total_pages": 0, "amount_logs": 0, "files": []}})

    if (page_num > total_pages) or (page_num < 1):
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail={"reason": "This page does not exist"})

    if sort is not None and sort_direction is not None and (sort_direction == -1 or sort_direction == 1):
        files = archive_db.get_filter_pagination_archive_command(page_num, page_size, filter_params, sort,
                                                                 sort_direction)
    else:
        files = archive_db.get_filter_pagination_archive_command(page_num, page_size, filter_params)

    return JSONResponse(content={"answer": {"number_page": page_num, "total_pages": total_pages, "files": files}})


@router.get("/img_file")
def get_robot_in_archive_command(file: str, session_id: str = None,
                                 util_s3_client = Depends(util.get_s3_client),
                                 user_info: dict = Depends(login_request)):
    try:
        if session_id is not None:
            path = file.split('/')
            path = session_id + "/" + path[1] + "|" + path[2] + 'camera0_rgb.jpg'

            response = util_s3_client.get_object(Bucket=os.environ['BUCKET_NAME_SESSIONS'], Key=path)
            return StreamingResponse(response["Body"])

        response = util_s3_client.get_object(Bucket=os.environ['BUCKET_NAME_ARCHIVE'], Key=file + "/camera0_rgb.jpg")
        return StreamingResponse(response["Body"])

    except ClientError:
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail={"reason": "File not found"})


@router.get("/command_json")
def get_robot_command_json(file: str, session_id: str = None,
                           util_s3_client = Depends(util.get_s3_client),
                           user_info: dict = Depends(login_request)):
    try:
        if session_id is not None:
            path = file.split('/')
            path = session_id + "/" + path[1] + "|" + path[2] + 'command.json'

            response = util_s3_client.get_object(Bucket=os.environ['BUCKET_NAME_SESSIONS'], Key=path)
            return JSONResponse(json.loads(json.loads(response["Body"].read().decode())))

        response = util_s3_client.get_object(Bucket=os.environ['BUCKET_NAME_ARCHIVE'], Key=file + "/command.json")
        return JSONResponse(json.loads(json.loads(response["Body"].read().decode())))

    except ClientError:
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail={"reason": "File not found"})


@router.post("/sessions_data")
def add_sessions(session: Sessions, user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    if session.name is not None and sessions_db.find_record(
            {"user": user_info["preferred_username"], "name": session.name}
    ):
        raise HTTPException(
            status_code=status_code.HTTP_400_BAD_REQUEST,
            detail="This is the name of the session already in use"
        )

    files, files_minio = [], []
    for file in session.files:
        files_minio.append(file.file)
        files.append({"file": file.file, "command": file.command})

    data = {"user": user_info["preferred_username"], "files": files}
    if session.name is not None:
        data["name"] = session.name

    session_id = sessions_db.add_record(data).inserted_id
    str_id = str(sessions_db.find_record(session_id)["_id"])
    object_name = f"{str_id}/session.json"

    try:
        minio_data = {"user": user_info["preferred_username"], "files": files}
        if session.name is not None:
            minio_data["name"] = session.name

        s3_client.put_object(Body=json.dumps(minio_data), Bucket=bucket_name_sessions, Key=object_name)
    except ClientError as e:
        logging.error(e)

    threading.Thread(target=util.add_final_session, args=(s3_client, str_id, minio_data)).start()
    return JSONResponse({"answer": str_id})


@router.get("/sessions_data")
def add_sessions(user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    sessions = []
    for session in sessions_db.find_records({"user": user_info["preferred_username"]}):
        data = {"id": str(session["_id"])}
        if session.get("name") is not None:
            data["name"] = session.get("name")

        sessions.append(data)

    return JSONResponse({"answer": sessions})


@router.get("/sessions_data/{session_id}")
def add_sessions(session_id: str, user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    session = sessions_db.find_record({"_id": ObjectId(session_id)})
    files = []
    for file in session["files"]:
        robot_date_time = file["file"].split("/")
        date_time = robot_date_time[1] + ":" + robot_date_time[2]

        files.append({
            "file": file["file"],
            "robot": robot_date_time[0],
            "datetime": datetime.datetime.strptime(
                date_time, "%Y_%b_%d:%H_%M_%S.%f"
            ).strftime("%Y-%b-%d : %H-%M-%S.%f"),
            "command": file["command"]
        })

    session_ans = {
        "id": str(session["_id"]),
        "user": session["user"],
        "files": files
    }
    if session.get("name") is not None:
        session_ans["name"] = session.get("name")

    return JSONResponse({"answer": session_ans})


@router.put("/sessions_data/{session_id}")
def add_sessions(session_id: str, session: SessionsName, user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    # todo optimize query
    if sessions_db.find_record({
        "user": user_info["preferred_username"],
        "name": session.name
    }) and sessions_db.find_record({
        "_id": ObjectId(session_id),
        "user": user_info["preferred_username"],
        "name": session.name
    }) is None:
        raise HTTPException(
            status_code=status_code.HTTP_400_BAD_REQUEST,
            detail="This is the name of the session already in use"
        )

    data = {"user": user_info["preferred_username"]}
    if session.name is not None:
        data["name"] = session.name

    up_session = sessions_db.update_records(session_id, data)
    session_ans = {
        "id": session_id,
        "user": user_info["preferred_username"],
    }
    if up_session.get("name") is not None:
        session_ans["name"] = up_session.get("name")

    object_name = session_id + "/" + "session.json"
    try:
        minio_data = {
            "user": user_info["preferred_username"],
            "files": sessions_db.find_record({"_id": ObjectId(session_id)})["files"],
        }
        if session.name is not None:
            minio_data["name"] = session.name

        s3_client.put_object(Body=json.dumps(minio_data), Bucket=bucket_name_sessions, Key=object_name)
    except ClientError as e:
        logging.error(e)

    return JSONResponse({"answer": session_ans})


@router.delete("/sessions_data/{session_id}")
def add_sessions(session_id: str, force: bool = False, user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    if sessions_db.find_record({"_id": ObjectId(session_id), "user": user_info["preferred_username"]}) is None:
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="This session does not exist")

    train_session = session_train_db.find_record({"sessions": {"$elemMatch": {"__var.id": session_id}}})
    if train_session is not None:
        if not force:
            return JSONResponse({"answer": False, "detail": "This data session is used in the training session"})

        sessions = [up for up in train_session["sessions"] if not up["__var"]["id"] == session_id]

        data = {
            "learning_rate": train_session["learning_rate"],
            "max_iterations": train_session["max_iterations"],
            "batch_size": train_session["batch_size"],
            "weight": train_session["weight"],
            "sessions": sessions
        }
        session_train_db.update_records(str(train_session["_id"]), data)

    sessions_db.delete_record({'_id': ObjectId(session_id)})
    objects_to_delete = s3_client.list_objects(Bucket=bucket_name_sessions, Prefix=session_id + "/")
    delete_keys = {
        'Objects': [
            {'Key': k} for k in [obj['Key'] for obj in objects_to_delete.get('Contents', [])]
        ]
    }
    s3_client.delete_objects(Bucket=bucket_name_sessions, Delete=delete_keys)

    return JSONResponse({"answer": True})


@router.post("/sessions_train")
def add_session_train(session_train: SessionsTrain, user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    sessions = [
        {"__var": {"id": session.id, "weight": session.weight}, "__type": "dict"}
        for session in session_train.sessions
    ]

    data = {
        "user": user_info["preferred_username"],
        "learning_rate": session_train.learning_rate,
        "max_iterations": session_train.max_iterations,
        "batch_size": session_train.batch_size,
        "weight": session_train.weight,
        "sessions": sessions
    }
    if session_train.name is not None:
        data["name"] = session_train.name

    session_id = session_train_db.add_record(data).inserted_id
    str_id = str(session_train_db.find_record(session_id)["_id"])
    return JSONResponse({"answer": str_id})


@router.get("/sessions_train")
def add_sessions(user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    sessions_train = []
    for session in session_train_db.find_records({"user": user_info["preferred_username"]}):
        data = {"id": str(session["_id"])}
        if session.get("name") is not None:
            data["name"] = session.get("name")

        sessions_train.append(data)

    return JSONResponse({"answer": sessions_train})


@router.get("/sessions_train/{session_id}")
def add_sessions(session_id: str, user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    session = session_train_db.find_record({"_id": ObjectId(session_id)})

    session_ans = {
        "training_session": str(session["_id"]),
        "sessions": session["sessions"],
        "learning_rate": session["learning_rate"],
        "max_iterations": session["max_iterations"],
        "batch_size": session["batch_size"],
        "weight": session["weight"],
    }
    if session.get("name") is not None:
        session_ans["name"] = session.get("name")

    paginator = s3_client.get_paginator('list_objects')

    checked_nn = []
    for nn in [
        page['Key'] for page in
        paginator.paginate(Bucket=os.environ["BUCKET_NAME_WEIGHTS"]).search("Contents[?Size > `0`][]")
    ]:
        try:
            if session_id in nn:
                checked_nn.append(nn)

        except ClientError:
            logging.info(nn + " not uploaded")

    session_ans["nn_weights"] = checked_nn
    return JSONResponse({"answer": session_ans})


@router.put("/sessions_train/{session_id}")
def add_sessions(
        session_id: str,
        session_train: SessionsTrain,
        user_info: dict = Depends(depends.user_info_with_group)
):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    sessions = [
        {"__var": {"id": session.id, "weight": session.weight}, "__type": "dict"}
        for session in session_train.sessions
    ]

    data = {
        "learning_rate": session_train.learning_rate,
        "max_iterations": session_train.max_iterations,
        "batch_size": session_train.batch_size,
        "weight": session_train.weight,
        "sessions": sessions
    }
    if session_train.name is not None:
        data["name"] = session_train.name

    session_train_db.update_records(session_id, data)
    data = {
        "training_session": session_id,
        "learning_rate": session_train.learning_rate,
        "max_iterations": session_train.max_iterations,
        "batch_size": session_train.batch_size,
        "weight": session_train.weight,
        "sessions": sessions
    }

    if session_train.name is not None:
        data["name"] = session_train.name

    return JSONResponse({"answer": data})


@router.delete("/sessions_train/{session_id}")
def add_sessions(session_id: str, user_info: dict = Depends(depends.user_info_with_group)):
    if "operator" in user_info["groups"]:
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    if session_train_db.find_record({
        "_id": ObjectId(session_id), "user": user_info["preferred_username"]
    }) is None:
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="This session does not exist")

    session_train_db.delete_record({'_id': ObjectId(session_id)})
    return JSONResponse({"answer": True})


@router.get("/nn_weight")
def get_weight(nn_weight: str, user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("robot" in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")

    try:
        s3_client.head_object(Bucket=os.environ['BUCKET_NAME_WEIGHTS'], Key=nn_weight)
    except ClientError:
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="There is no such file")

    return s3_client.generate_presigned_url(
        'get_object',
        Params={'Bucket': os.environ['BUCKET_NAME_WEIGHTS'], 'Key': nn_weight},
        ExpiresIn=60
    )


@router.get("/train_user")
def get_train_user(user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")
    try:
        return JSONResponse({"answer": session_train_db.get_user(user_info.get("sub"))})
    except Exception as e:
        logging.error(e, exc_info=True)


@router.post("/train_user")
def add_train_user(user: Login, user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")
    try:
        session_train_db.add_user(user_info.get("sub"), user.username, user.password)
        return JSONResponse({"answer": True})
    except Exception as e:
        logging.error(e, exc_info=True)


@router.delete("/train_user")
def delete_train_user(user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")
    if session_train_db.delete_user(user_info.get("sub")) == False:
        raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="No user found")
    return JSONResponse({"answer": True})


@router.get("/train")
def get_train(url: str, user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")
    user_data = session_train_db.get_user_data(user_info.get("sub"))
    if user_data is not None:
        res = requests.get(url, auth=(user_data["username"], user_data["password"]))
        if res.status_code == 200:
            return JSONResponse({"answer": res.json()})
        raise HTTPException(status_code=res.status_code, detail=res.text)
    raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="User not found for authorization")


@router.post("/train")
def post_train(url: str, payload: dict = Body(...), user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")
    
    user_data = session_train_db.get_user_data(user_info.get("sub"))
    if user_data is not None:
        res = requests.post(url, auth=(user_data["username"], user_data["password"]), json=payload)
        if res.status_code == 200:
            return JSONResponse({"answer": res.json()})
        raise HTTPException(status_code=res.status_code, detail=res.text)
    raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="User not found for authorization")


@router.patch("/train")
def patch_train(url: str, payload: dict = Body(...), user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")
    
    user_data = session_train_db.get_user_data(user_info.get("sub"))
    if user_data is not None:
        res = requests.patch(url, auth=(user_data["username"], user_data["password"]), json=payload)
        if res.status_code == 200:
            return JSONResponse({"answer": res.json()})
        raise HTTPException(status_code=res.status_code, detail=res.text)
    raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="User not found for authorization")

@router.delete("/train")
def delete_train(url: str, user_info: dict = Depends(depends.user_info_with_email)):
    if ("groups" not in user_info) or ("admin" not in user_info["groups"]):
        raise HTTPException(status_code=status_code.HTTP_403_FORBIDDEN, detail="Wrong role")
    
    user_data = session_train_db.get_user_data(user_info.get("sub"))
    if user_data is not None:
        res = requests.delete(url, auth=(user_data["username"], user_data["password"]))
        if res.status_code == 200:
            return JSONResponse({"answer": res.json()})
        raise HTTPException(status_code=res.status_code, detail=res.text)
    raise HTTPException(status_code=status_code.HTTP_400_BAD_REQUEST, detail="User not found for authorization")
    
