import json
import logging
import os
import time
from typing import Any, List, Optional

from fastapi import APIRouter, Body, Depends
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel

from common.commands import send_robot_command
from common.src.utils import get_rabbit_connection
from tools.db import archive_db
from tools.util import get_s3_client

COMMANDS_BUCKET = os.environ["BUCKET_NAME_ARCHIVE"]


router = APIRouter(tags=["commands"], prefix="/commands")


class CommandSchema(BaseModel):
    id: str
    images: List[str]
    content: Any


def get_command_by_id(command_id: str):
    command_entity = archive_db.get_command_by_id(command_id)
    if command_entity is None:
        raise HTTPException(status_code=404)

    return command_entity


@router.get("", response_model=List[CommandSchema])
async def get_robot_commands(robot: str, command_operation: Optional[str] = None):
    filter = {"robot": robot}
    if command_operation:
        filter["command"] = command_operation

    start = time.perf_counter()
    db_result = archive_db.get_filter_pagination_archive_command(
        filter=filter,
        page_num=1,
        page_size=100500,  # TODO
    )
    end = time.perf_counter()
    logging.info(
        "get_robot_commands get_filter_pagination_archive_command %d ms.",
        1000 * (end - start),
    )

    return [
        CommandSchema(
            id=str(command["id"]),
            images=[command["path"]],  # TODO: what should we do with multiple images?
            content=command["content"],
        )
        for command in db_result
    ]


@router.get("/{command_id}", response_model=CommandSchema)
async def get_command(command=Depends(get_command_by_id)):
    return CommandSchema(
        id=str(command["id"]),
        images=[command["path"]],
        content=command["content"],
    )


@router.delete("/{command_id}")
def delete_command(
    command_id: str,
    command=Depends(get_command_by_id),
    util_s3_client=Depends(get_s3_client),
):
    archive_db.delete_command_by_id(command_id)
    util_s3_client.delete_object(
        Bucket=COMMANDS_BUCKET, Key=get_command_json_key(command["path"])
    )

    send_command_to_robot(
        command["robot"],
        {"op": "delete_command", "type": "system", "command_id": command_id},
    )

    return JSONResponse()


@router.put("/{command_id}")
def update_command(
    command_id: str,
    command=Depends(get_command_by_id),
    command_json_data: Any = Body(None),
    util_s3_client=Depends(get_s3_client),
):
    util_s3_client.put_object(
        Body=json.dumps(json.dumps(command_json_data)),
        Bucket=COMMANDS_BUCKET,
        Key=get_command_json_key(command["path"]),
    )

    archive_db.update_command_by_id(command_id, {"content": command_json_data})

    send_command_to_robot(
        command["robot"],
        {"op": "update_command", "type": "system", "command_id": command_id},
    )

    return JSONResponse()


def send_command_to_robot(robot_name: str, command_content: dict):
    connection = get_rabbit_connection()
    channel = connection.channel()

    send_robot_command(robot_name, command_content, channel)

    channel.close()
    connection.close()


def get_command_json_key(path: str):
    return f"{path}/command.json"
