from fastapi import FastAPI

from . import api, logs, sessions, snapshot, stream, user, version, web_api, commands


def initialize_app_routers(app: FastAPI):
    app.include_router(version.router)
    app.include_router(user.router)
    app.include_router(logs.router)
    app.include_router(snapshot.router)
    app.include_router(api.router)
    app.include_router(web_api.router)
    app.include_router(stream.router)
    app.include_router(sessions.router)
    app.include_router(commands.router)
    return app
