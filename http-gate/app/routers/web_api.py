import io
import json
import logging
import os
import time

import redis
from botocore.exceptions import ClientError

from fastapi import APIRouter, HTTPException, Depends, Request
from fastapi.responses import JSONResponse, StreamingResponse

from common.src.services.keycloak import login_request, get_keycloak_openid, KeycloakError
from common.src.utils import get_rabbit_connection
from tools.db import admin_db, archive_db, logs_db
from tools.docs import web_api
from tools.schemas import Robot
from tools.util import get_one_sub_image, update_task, get_s3_client

redis_connect = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)
s3_client = get_s3_client()

available_commands = [
    {"id": "ask_for_idle", "description": "Request for the ready state"},
    {"id": "ask_for_operator", "description": "Request for the operator assistance"},
    {"id": "ask_for_ml_one_shot", "description": "Perform replacing of one object in the AI mode"},
    {"id": "ask_for_ml_continual", "description": "Perform replacing of the whole bin in the AI mode"},
    {"id": "ask_for_manual", "description": "Request the manual mode to move the arm"},
    {"id": "calibrate_on_ml", "description": "Request automated calibration"},
    {"id": "vacuum_on", "description": "Turn on vacuum"},
    {"id": "vacuum_off", "description": "Turn off vacuum"},
    {"id": "ask_for_ml_one_shot", "description": "Launch picking of one object"},
    {"id": "ask_for_ml_continual", "description": "Launch picking of one bin"},
    {"id": "remember_pick", "description": "Remember pick"},
    {"id": "detect_target_bin", "description": "Detect the target bin"},
    {"id": "reset_state", "description": "reset robot arm"},
]

router = APIRouter(prefix="/web_api", tags=["web_api"], responses=web_api.responses)


def get_robot(robot_name):
    d = {"name": robot_name}
    robots_status = json.loads(redis_connect.get("robots_status"))

    if robot_name in robots_status.keys():
        connected = robots_status[robot_name]["camera0_connected"] and robots_status[robot_name]["sensors_connected"]
        d["connected"] = connected

    res = redis_connect.get(robot_name + "_sensors")
    if res is not None and "state_machine" in json.loads(res).keys():
        d["state"] = json.loads(res)["state_machine"]
    else:
        d["state"] = "IDLE"

    if res is not None and "snapshot" in json.loads(res).keys():
        d["shapshot"] = True
    else:
        d["snapshot"] = False

    if res is not None and "arm_connected" in json.loads(res).keys():
        d["arm_connected"] = json.loads(res)["arm_connected"]
    else:
        d["arm_connected"] = False

    d["calibration"] = "OK"

    conf = json.loads(admin_db.get_config(robot_name)["value"])
    if "task_name" in conf.keys():
        d["currentTask"] = conf["task_name"]
    else:
        d["currentTask"] = None

    return d


# USER <--------------------------------------------------------------------->
@router.get("/user", responses=web_api.description["GET:user"])
def get_current_user(user_info: dict = Depends(login_request)):
    """
    This method shows information about the user, namely his **email** and his **role** in the system.
    """
    return JSONResponse(content={"email": user_info["email"], "roles": user_info["groups"]})


# POST ROBOTS <-------------------------------------------------------------->
@router.post("/robots", responses=web_api.description["POST:robots"])
def add_robot(robot: Robot, user_info: dict = Depends(login_request)):
    """
    This method allows you to **subscribe** to the robot, then it will be displayed in the GET: robots. 
    It is necessary to pass the name of the robot and its password.
    """

    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )
    try:
        kc = get_keycloak_openid()
        robot_info = kc.userinfo(kc.token(robot.name, robot.password)["access_token"])
        if ("groups" not in robot_info) or ("robot" not in robot_info["groups"]):
            raise HTTPException(
                status_code=403,
                detail={"reason": "This user is not a robot"},
                headers={"Access-Control-Allow-Origin": "*"},
            )

        if not admin_db.add_external_user_robot_pair(user_info["email"], robot.name):
            raise HTTPException(
                status_code=400, detail={"reason": "Robot paired already"}, headers={"Access-Control-Allow-Origin": "*"}
            )

        try:
            r = get_robot(robot.name)
            return JSONResponse(content=r, headers={"Access-Control-Allow-Origin": "*"})
        except TypeError as e:
            logging.info(str(e))

    except KeycloakError as e:
        logging.exception(e, exc_info=True)
        raise HTTPException(
            status_code=400, detail={"reason": "Wrong name password pair"}, headers={"Access-Control-Allow-Origin": "*"}
        )


# DELETE ROBOTS <------------------------------------------------------------>
@router.delete("/robots/{robot_name}", responses=web_api.description["DELETE:robots"])
def del_robot(robot_name: str = None, robot: str = None, user_info: dict = Depends(login_request)):
    """
    This method allows you to **unsubscribe** from a robot by passing its name using either (or both) of the parameters.
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    if (robot_name is None) and (robot_name is None):
        raise HTTPException(
            status_code=400, detail={"reason": "Robot not in args"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    if robot_name is None:
        robot_name = robot

    if robot_name not in admin_db.get_available_robots(user_info["email"]):
        raise HTTPException(
            status_code=400, detail={"reason": "Robot is not available"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    if admin_db.del_external_user_robot_pair(user_info["email"], robot_name):
        return JSONResponse(content={"answer": True}, headers={"Access-Control-Allow-Origin": "*"})

    raise HTTPException(
        status_code=400, detail={"reason": "Robot does not exist"}, headers={"Access-Control-Allow-Origin": "*"}
    )


# GET ROBOTS <--------------------------------------------------------------->
@router.get("/robots", responses=web_api.description["GET:robots"])
def get_robots(user_info: dict = Depends(login_request)):
    """
    This method will show a list of all the robots you are subscribed to.
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    robots = admin_db.get_available_robots(user_info["email"])
    res = []
    for r in robots:
        d = get_robot(r)
        res.append(d)

    return JSONResponse(content=res, headers={"Access-Control-Allow-Origin": "*"})


# GET TASKS <---------------------------------------------------------------->
@router.get("/tasks", responses=web_api.description["GET:tasks"])
def get_tasks(user_info: dict = Depends(login_request)):
    """
    Method will show a list of all tasks created by the user.
    A task is usually heterogeneous JSON.
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    tasks = admin_db.get_all_tasks(author_name=user_info["email"])
    return JSONResponse(content=tasks, headers={"Access-Control-Allow-Origin": "*"})


# GET TASKS by task_name <--------------------------------------------------->
@router.get("/tasks/{task_name}", responses=web_api.description["GET:tasks/task_name"])
def get_task(task_name: str, user_info: dict = Depends(login_request)):
    """
    Get a specific task by its name.
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    task = admin_db.get_task(author_name=user_info["email"], name=task_name)
    return JSONResponse(content=task, headers={"Access-Control-Allow-Origin": "*"})


# PUT TASKS by task_name <--------------------------------------------------->
@router.put("/tasks/{task_name}", responses=web_api.description["PUT:tasks/task_name"])
async def put_task(task_name: str, request: Request, user_info: dict = Depends(login_request)):
    """
    Allows you to **change a task already created by the user by its name**.

    Because of the fact that tasks don't have a clear structure, 
    it is impossible to specify the necessary parameters that need to be sent to the body, 
    so it is impossible to reflect this in the documentation. 
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    t = {"name": task_name, "author": user_info["email"]}
    try:
        t["task"] = await request.json()
    except json.decoder.JSONDecodeError as e:
        logging.exception(e, exc_info=True)
        raise HTTPException(
            status_code=400, detail={"reason": "Not json"},
            headers={"Access-Control-Allow-Origin": "*"}
        )

    if admin_db.get_task(author_name=user_info["email"], name=task_name) is None:
        raise HTTPException(
            status_code=400, detail={"reason": "Task does not exist"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    if "template" in t["task"].keys() and "description" in t["task"].keys():
        if admin_db.check_task(user_info["email"], task_name):
            admin_db.del_task(user_info["email"], task_name)

        admin_db.add_task(user_info["email"], task_name, json.dumps(t["task"]))
        update_task(task_name, user_info["email"])
        return JSONResponse(content=t, headers={"Access-Control-Allow-Origin": "*"})

    raise HTTPException(
        status_code=400, detail={"reason": "Wrong task format"}, headers={"Access-Control-Allow-Origin": "*"}
    )


# POST TASKS by task_name <-------------------------------------------------->
@router.post("/tasks/{task_name}", responses=web_api.description["POST:tasks/task_name"])
async def post_task(task_name: str, request: Request, user_info: dict = Depends(login_request)):
    """
    Created task

    Because of the fact that tasks don't have a clear structure, 
    it is impossible to specify the necessary parameters that need to be sent to the body, 
    so it is impossible to reflect this in the documentation. 
    """

    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    t = {"name": task_name, "author": user_info["email"]}
    try:
        t["task"] = await request.json()
    except json.decoder.JSONDecodeError as e:
        logging.exception(e, exc_info=True)
        raise HTTPException(status_code=400, detail={"reason": "Not json"},
                            headers={"Access-Control-Allow-Origin": "*"})

    if admin_db.get_task(author_name=user_info["email"], name=task_name) is not None:
        raise HTTPException(
            status_code=400, detail={"reason": "Task exists already"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    if "template" in t["task"].keys() and "description" in t["task"].keys():
        if admin_db.check_task(user_info["email"], task_name):
            admin_db.del_task(user_info["email"], task_name)

        admin_db.add_task(user_info["email"], task_name, json.dumps(t["task"]))
        update_task(task_name, user_info["email"])
        return JSONResponse(content=t, headers={"Access-Control-Allow-Origin": "*"})

    raise HTTPException(
        status_code=400, detail={"reason": "Wrong task format"}, headers={"Access-Control-Allow-Origin": "*"}
    )


# DELETE TASKS by task_name <------------------------------------------------>
@router.delete("/tasks/{task_name}", responses=web_api.description["DELETE:tasks/task_name"])
def del_task(task_name: str = None, name: str = None, user_info: dict = Depends(login_request)):
    """
    Delete a task by its name
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    if (name is None) and (task_name is None):
        raise HTTPException(
            status_code=400, detail={"reason": "Name argument is missed"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    if task_name is None:
        task_name = name

    if admin_db.get_task(author_name=user_info["email"], name=task_name) is None:
        raise HTTPException(
            status_code=400, detail={"reason": "Task does not exist"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    admin_db.del_task(user_info["email"], task_name)
    return JSONResponse(content={"answer": True}, headers={"Access-Control-Allow-Origin": "*"})


# DELETE TASKS_TO_ROBOT <---------------------------------------------------->
@router.delete("/task_to_robot/{robot_name}", responses=web_api.description["DELETE:tasks_to_robot"])
def task_to_robot_delete(robot_name: str, user_info: dict = Depends(login_request)):
    """
    Deletes the task assigned to the robot (from the robot's config)
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    if robot_name not in admin_db.get_available_robots(user_info["email"]):
        raise HTTPException(
            status_code=400, detail={"reason": "Robot is not available"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    config = json.loads(admin_db.get_config(robot_name)["value"])
    config["task_name"] = None
    admin_db.set_config(robot_name, json.dumps(config))
    return JSONResponse(content={"answer": True}, headers={"Access-Control-Allow-Origin": "*"})


# POST TASKS_TO_ROBOT <------------------------------------------------------>
@router.post("/task_to_robot/{robot_name}/{task_name}", responses=web_api.description["POST:tasks_to_robot"])
def task_to_robot(robot_name: str, task_name: str, user_info: dict = Depends(login_request)):
    """
    A method for **adding an existing task to a robot**. Once it is added, the robot can execute it.
    Be careful, you can add a non-existent task to the robot.
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    if robot_name not in admin_db.get_available_robots(user_info["email"]):
        raise HTTPException(
            status_code=400, detail={"reason": "Robot is not available"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    value_str = admin_db.get_config(robot_name)["value"]
    if value_str is not None:
        config = json.loads(value_str)
    else:
        config = {}

    author = user_info["email"]
    config["task_name"] = task_name + ":" + author

    admin_db.set_config(robot_name, json.dumps(config))
    update_task(task_name, user_info["email"])
    return JSONResponse(content=get_robot(robot_name), headers={"Access-Control-Allow-Origin": "*"})


# GET AVAILABLE_COMMANDS <--------------------------------------------------->
@router.get("/available_commands", responses=web_api.description["GET:available_commands"])
def get_commands(user_info: dict = Depends(login_request)):
    """
    Returns a list of commands available to the user.
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    return JSONResponse(content=available_commands, headers={"Access-Control-Allow-Origin": "*"})


# POST COMMAND by command_name <--------------------------------------------->
@router.post("/command/{robot}/{command_name}", responses=web_api.description["POST:command by command_name"])
def push_command_simple(robot: str, command_name: str, user_info: dict = Depends(login_request)):
    """
    Tells the robot to execute one of the available commands.
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    found = False
    for c in available_commands:
        if c["id"] == command_name:
            found = True

    if not found:
        raise HTTPException(
            status_code=400, detail={"reason": "Command is not available"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    robots = admin_db.get_available_robots(user_info["email"])
    if robot not in robots:
        raise HTTPException(
            status_code=400, detail={"reason": "Robot is not available"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    command_body = '{"op":"' + command_name + '", "source": "website", "reason":"' + user_info["email"] + ' request"}'
    connection = get_rabbit_connection()
    logging.info("push command to pika " + command_body + " " + str(time.time()))

    channel = connection.channel()
    channel.basic_publish(exchange="commands_" + robot, routing_key="", body=command_body)
    channel.close()
    connection.close()

    return JSONResponse(content={"answer": True}, headers={"Access-Control-Allow-Origin": "*"})


# POST COMMAND <------------------------------------------------------------->
@router.post("/command/{robot}", responses=web_api.description["POST:command"])
async def push_command(robot: str, request: Request, user_info: dict = Depends(login_request)):
    """
    Allows you to send a user command to the robot. 

    Since commands do not have a fixed standard, 
    it is impossible to describe the format of the commands in the documentation.
    """

    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    try:
        data = await request.json()
    except json.decoder.JSONDecodeError as e:
        logging.exception(e, exc_info=True)
        raise HTTPException(status_code=400, detail={"reason": "Not json"},
                            headers={"Access-Control-Allow-Origin": "*"})

    if "command" not in data.keys():
        raise HTTPException(
            status_code=400, detail={"message": "Wrong json"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    command = data["command"]
    found = False
    for c in available_commands:
        if c["id"] == command:
            found = True

    if not found:
        raise HTTPException(
            status_code=400, detail={"reason": "Command is not available"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    robots = admin_db.get_available_robots(user_info["email"])
    if robot not in robots:
        raise HTTPException(
            status_code=400, detail={"reason": "Robot is not available"}, headers={"Access-Control-Allow-Origin": "*"}
        )

    command_body = '{"op":"' + command + '", "source": "website", "reason":"' + user_info["email"] + ' request"}'
    connection = get_rabbit_connection()
    logging.info("push command to pika " + command_body + " " + str(time.time()))

    channel = connection.channel()
    channel.basic_publish(exchange="commands_" + robot, routing_key="", body=command_body)
    channel.close()
    connection.close()

    return JSONResponse(content={"answer": True}, headers={"Access-Control-Allow-Origin": "*"})


# GET LOGS <----------------------------------------------------------------->
@router.get("/logs/{robot}", responses=web_api.description["GET:logs"])
def get_logs(robot: str, limit: int = None, skip: int = None, user_info: dict = Depends(login_request)):
    """
    Get a list of robot logs, the **skip** and **limit** arguments are needed to skip some logs, 
    and output a certain number of logs.
    """
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    robots = admin_db.get_available_robots(user_info["email"])
    if robot in robots:
        res = {}
        records, N = logs_db.get_records(robot, limit=limit, skip=skip)
        res["records_number"] = N
        res["records"] = records
        return JSONResponse(content=res, headers={"Access-Control-Allow-Origin": "*"})

    else:
        raise HTTPException(
            status_code=400,
            detail={"reason": "robot_name is not from the list of available robots"},
            headers={"Access-Control-Allow-Origin": "*"},
        )


# GET FILES <---------------------------------------------------------------->
@router.get("/files/{robot_name}/{filename}", responses=web_api.description["GET:files"])
def get_file(robot_name: str, filename: str
             # , user_info: dict = Depends(login_request)
             ):
    """
    This method sends the log file you specified (you can get its name from the web_api/logs method)
    """
    # if "email" not in user_info:
    #    raise HTTPException(
    #        status_code=400,
    #        detail={"reason": "The user does not have email"},
    #        headers={"Access-Control-Allow-Origin": "*"},
    #    )
    # robots = db.get_available_robots(user_info["email"])
    # if robot_name in robots:
    logging.info("Returning file  with robot_name={} filename = {}".format(robot_name, filename))
    bucket_name = os.environ["BUCKET_NAME_LOGS"]
    file_key = robot_name + "/" + filename

    try:
        response = s3_client.get_object(Bucket=bucket_name, Key=file_key)
    except ClientError as e:
        logging.error("couldnt load file " + str(e))
        raise HTTPException(
            status_code=400,
            detail={"reason": "No corresponding file in datastorage"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    data = response["Body"]
    return StreamingResponse(data)


# Additional functionality
@router.get("/subimg/{robot_name}")
def get_subimg(robot_name: str, rect: str, user_info: dict = Depends(login_request)):
    # if "email" not in user_info:
    #    raise HTTPException(
    #        status_code=400,
    #        detail={"reason": "The user does not have email"},
    #        headers={"Access-Control-Allow-Origin": "*"}
    #    )
    # robots = db.get_available_robots(user_info["email"])

    try:
        rect = json.loads(rect)
    except Exception as e:
        raise HTTPException(
            status_code=400,
            detail={"reason": "could not parse rect " + str(e)},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    logging.info("get_subimg " + robot_name + " " + str(rect))
    return StreamingResponse(io.BytesIO(get_one_sub_image(robot_name, rect)), media_type="image/jpeg")


@router.get("/target_bin/{robot_name}")
def target_bin(robot_name: str, user_info: dict = Depends(login_request)):
    if "email" not in user_info:
        raise HTTPException(
            status_code=400,
            detail={"reason": "The user does not have email"},
            headers={"Access-Control-Allow-Origin": "*"},
        )

    robots = admin_db.get_available_robots(user_info["email"])
    if robot_name in robots:
        conf = json.loads(admin_db.get_config(robot_name)["value"])

        if "detected_bin_2d" in conf.keys() and "camera0_width" in conf.keys() and "camera0_height" in conf.keys():
            ar = conf["detected_bin_2d"]
            width = conf["camera0_width"]
            height = conf["camera0_height"]
            res = []

            for i in range(len(ar)):
                x = ar[i][0][0] / width
                y = ar[i][0][1] / height
                res.append([x, y])

            return JSONResponse(content=res, headers={"Access-Control-Allow-Origin": "*"})
        else:
            raise HTTPException(
                status_code=400,
                detail={"reason": "A problem with robot config"},
                headers={"Access-Control-Allow-Origin": "*"},
            )

    else:
        raise HTTPException(
            status_code=400, detail={"reason": "No such robot"}, headers={"Access-Control-Allow-Origin": "*"}
        )
