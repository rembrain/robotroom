import asyncio
import io
import json
import os
from typing import Iterable

import aioboto3


async def get_object_data(bucket, key: str):
    data = io.BytesIO()
    await bucket.download_fileobj(key, data)
    data.seek(0)
    content = data.read().decode()
    return key, json.loads(json.loads(content))  # TODO why do we want to use it twice?


async def download_s3_data_by_keys(keys: Iterable):
    session = aioboto3.Session(
        aws_access_key_id=os.environ["S3_ACCESS_KEY"],
        aws_secret_access_key=os.environ["S3_SECRET_KEY"],
    )
    async with session.resource("s3", endpoint_url=os.environ["S3_ADDRESS"]) as s3:
        bucket_name = os.environ["BUCKET_NAME_ARCHIVE"]
        bucket = await s3.Bucket(bucket_name)

        return {
            key: content
            for key, content in await asyncio.gather(
                *[
                    get_object_data(bucket, s3_object.key)
                    async for s3_object in bucket.objects.all()
                    if s3_object.key in keys
                ]
            )
        }
