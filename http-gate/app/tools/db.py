from common.src.services.mongo import *
from common.src.utils import get_mongo_connect

mongo_connection = get_mongo_connect(pool_size=1)
admin_db = MongoAdminService(mongo_connection)
archive_db = MongoArchiveService(mongo_connection)
logs_db = MongoLogsService(mongo_connection)
sessions_db = MongoSessionsService(mongo_connection)
session_train_db = MongoSessionTrainService(mongo_connection)
