title = "Http-Gate 🤖​🕶​🏍​🔫​​"

version = "1.0.0"

description_app = """
This app will allow you to interact with the RemBrain platform to control your robots.

### User &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; १|˚ –˚ |५
>In this section you can register **administrators**, 
>**robots** and **clients**, as well as get tokens to log on to the platform (even with some social networks).

### Logs &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; [ -c°▥°]-c
>Here you can manage **logs** and **artifacts**.

### Api &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; └[∵┌]└[ ∵ ]┘[┐∵]┘
>This is the main interaction section for administrators (and beyond). 
>Here you can manage **users**, **robots**, **commands** and **tasks** for robots, 
>as well as get a QR code to connect your robot to wi-fi, (and little else)

### Web_api &nbsp; &nbsp; &nbsp; &nbsp; d[o_0]b
>This section is similar to "api", but it is designed for users to manage their own personal robots 

### Stream &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ┫ ￣旦￣ ┣
>In this section you can receive a **stream of the connected robot's cameras** (you can also receive a stream using WebSocket)

### Sessions &nbsp; &nbsp; &nbsp; °⌈ ▂▂ ⌉°
>Here you will be able to manage **sessions for training neural networks**, 
>retrieve **commands** that have been executed by robots, create or modify a session.
"""
