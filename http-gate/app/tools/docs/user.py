responses = {404: {"description": "Not found"}}

description = {
    "POST:login": {
        200: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "access_token": "KYJ4lMDy26QWtb6V0apBg5uDgT6zQMSFqAewdHnZDHT81u7hYsG0wlbgLQD7ApcRpIDKCZHUZmdS3---UZTIPgMt_F89z-Ysw",
                        "refresh_token": "2NjkDMyMDksImlc3IiwiaXNzIjoiaHWM2MDlmMiIsInNjb3BlIjoicmMiJ9.QObSXy7TNKbwLAU"
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "access_token",
                            "refresh_token"
                        ],
                        "properties": {
                            "access_token": {
                                "title": "Authorisation",
                                "type": "string"
                            },
                            "refresh_token": {
                                "title": "Refresh",
                                "type": "string"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "You have the wrong user data",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Invalid user credentials",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:login_social": {
        200: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "access_token": "KYJ4lMDy26QWtb6V0apBg5uDgT6zQMSFqAewdHnZDHT81u7hYsG0wlbgLQD7ApcRpIDKCZHUZmdS3---UZTIPgMt_F89z-Ysw",
                        "refresh_token": "2NjkDMyMDksImlc3IiwiaXNzIjoiaHWM2MDlmMiIsInNjb3BlIjoicmMiJ9.QObSXy7TNKbwLAU"
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "access_token",
                            "refresh_token"
                        ],
                        "properties": {
                            "access_token": {
                                "title": "Authorisation",
                                "type": "string"
                            },
                            "refresh_token": {
                                "title": "Refresh",
                                "type": "string"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "Either the user is already registered with this email or an unknown keycloak error has occurred",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "User already exists",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:refresh": {
        200: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "access_token": "KYJ4lMDy26QWtb6V0apBg5uDgT6zQMSFqAewdHnZDHT81u7hYsG0wlbgLQD7ApcRpIDKCZHUZmdS3---UZTIPgMt_F89z-Ysw",
                        "refresh_token": "2NjkDMyMDksImlc3IiwiaXNzIjoiaHWM2MDlmMiIsInNjb3BlIjoicmMiJ9.QObSXy7TNKbwLAU"
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "access_token",
                            "refresh_token"
                        ],
                        "properties": {
                            "access_token": {
                                "title": "Authorisation",
                                "type": "string"
                            },
                            "refresh_token": {
                                "title": "Refresh",
                                "type": "string"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "Indicates a keycloak error or lack of required parameters",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "There is no refresh token in the header",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:logout": {
        200: {
            "description": "Receiving 'True' in the 'answer' field means that the request was \
                            successful and you have specified your logout information.",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer",
                        ],
                        "properties": {
                            "answer": {
                                "title": "Answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "Indicates a keycloak error or lack of required parameters",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "There is no refresh token in the header",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:register": {
        200: {
            "description": "If the 'True' value in the 'answer' \
                            field means that the request was successful and you have created a new user",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer",
                        ],
                        "properties": {
                            "answer": {
                                "title": "Answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "This error code indicates that you have entered the wrong role or that the user already exists",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "There is no such role",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        403: {
            "description": "Indicates that you do not have rights to use this method (most likely your role is not suitable)",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Wrong role",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "GET:user": {
        200: {
            "description": "Shows user data.",
            "content": {
                "application/json": {
                    "example": {
                        "answer": {
                            "id": "ec3201bf-4c1c-4a0e-b0b6-15e4250eb77b",
                            "createdTimestamp": 1633369158793,
                            "username": "op13",
                            "enabled": True,
                            "totp": False,
                            "emailVerified": False,
                            "firstName": "string",
                            "lastName": "string",
                            "email": "user@example.com",
                            "disableableCredentialTypes": [],
                            "requiredActions": [],
                            "federatedIdentities": [],
                            "notBefore": 0,
                            "groups": [
                                "operator"
                            ],
                            "access": {
                                "manageGroupMembership": True,
                                "view": True,
                                "mapRoles": True,
                                "impersonate": True,
                                "manage": True
                            }
                        }
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer",
                        ],
                        "properties": {
                            "answer": {
                                "title": "Answer",
                                "type": "object"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "indicates a keycloak error, maybe a user with this id was not found",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "User id not found.",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "PUT:update_password": {
        200: {
            "description": "If the 'True' value in the 'answer' \
                            field means that the request was successful and you have created a new user",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer",
                        ],
                        "properties": {
                            "answer": {
                                "title": "Answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "indicates a keycloak error, maybe a user with this id was not found",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "User id not found.",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "PUT:update": {
        200: {
            "description": "Shows updated user data in the keycloak.",
            "content": {
                "application/json": {
                    "example": {
                        "answer": {
                            "id": "ec3201bf-4c1c-4a0e-b0b6-15e4250eb77b",
                            "createdTimestamp": 1633369158793,
                            "username": "op13",
                            "enabled": True,
                            "totp": False,
                            "emailVerified": False,
                            "firstName": "string",
                            "lastName": "string",
                            "email": "user@example.com",
                            "disableableCredentialTypes": [],
                            "requiredActions": [],
                            "federatedIdentities": [],
                            "notBefore": 0,
                            "access": {
                                "manageGroupMembership": True,
                                "view": True,
                                "mapRoles": True,
                                "impersonate": True,
                                "manage": True
                            }
                        }
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer",
                        ],
                        "properties": {
                            "answer": {
                                "title": "Answer",
                                "type": "object"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "indicates a keycloak error, maybe a user with this id was not found",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "User id not found.",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "DELETE:delete": {
        200: {
            "description": "If the 'True' value in the 'answer' \
                            field means that the request was successful and and you deleted the user",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer",
                        ],
                        "properties": {
                            "answer": {
                                "title": "Answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "Indicates some kind of keycloak error, possibly a user with this id is not found or an unknown error in the keycloak.",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Unknown keycloak error",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:register_client": {
        200: {
            "description": "If the 'True' value in the 'answer' \
                            field means that the request was successful and you have created a new user",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer",
                        ],
                        "properties": {
                            "answer": {
                                "title": "Answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "Indicates that a user with this data already exists",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "A user with this email or username exists",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
}
