responses = {404: {"description": "Not found"}}

description = {
    "GET:user": {
        200: {
            "description": "Minimal user information",
            "content": {
                "application/json": {
                    "example": {
                        "email": "test@test.test",
                        "roles": [
                            "admin"
                        ]
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "email",
                            "roles"
                        ],
                        "properties": {
                            "access_token": {
                                "title": "Email",
                                "type": "string"
                            },
                            "refresh_token": {
                                "title": "Roles",
                                "type": "array"
                            }
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
    },
    "POST:robots": {
        200: {
            "description": "A valid request will give you information about the robot you signed up for.",
            "content": {
                "application/json": {
                    "example": {
                        "name": "tester",
                        "connected": False,
                        "state": "NEED_ML",
                        "snapshot": False,
                        "arm_connected": False,
                        "calibration": "OK",
                        "currentTask": "tester:tester"
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "name",
                            "connected",
                            "state",
                            "snapshot",
                            "arm_connected",
                            "calibration",
                            "currentTask",
                        ],
                        "properties": {
                            "name": {
                                "title": "NameRobot",
                                "type": "string"
                            },
                            "connected": {
                                "title": "ConnectedInfo",
                                "type": "string"
                            },
                            "state": {
                                "title": "StateInfo",
                                "type": "string"
                            },
                            "snapshot": {
                                "title": "SnapshotInfo",
                                "type": "string"
                            },
                            "arm_connected": {
                                "title": "ArmConnectedInfo",
                                "type": "string"
                            },
                            "calibration": {
                                "title": "CalibrationInfo",
                                "type": "string"
                            },
                            "currentTask": {
                                "title": "TaskInfo",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        400: {
            "description": "Getting this error can mean several things, namely: \
                1. You have entered a wrong robot name or password, \
                2. You don't have a verified email address in the system, \
                3. You have already subscribed to the robot. The text of the error will tell \
                you what the problem is, more precisely.",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Robot paired already"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        403: {
            "description": "Indicates that the username and password do not belong to a robot",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "This user is not a robot",
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "DELETE:robots": {
        200: {
            "description": "Response to a valid request",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer"
                        ],
                        "properties": {
                            "name": {
                                "title": "answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "If you get this error, it could mean several things: \
                1. You do not have a verified email, 2. You didn't pass the name of the robot in the query arguments, \
                3. Robot is not available to you, \
                4. Robot does not exist. Clarification of the problem will be in the error response ",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Robot is not available"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },

        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "GET:robots": {
        200: {
            "description": "Response to a valid request",
            "content": {
                "application/json": {
                    "example": [
                        {
                            "name": "tester",
                            "connected": False,
                            "state": "NEED_ML",
                            "snapshot": False,
                            "arm_connected": False,
                            "calibration": "OK",
                            "currentTask": "tester:tester"
                        }
                    ],
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "array",
                        "items": {
                            "type": "object",
                        }
                    }
                }
            }
        },
        400: {
            "description": "You do not have a valid email",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "The user does not have email"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "GET:tasks": {
        200: {
            "description": "List of tasks created by the user.",
            "content": {
                "application/json": {
                    "example": [
                        {
                            "author": "test@gmail.com",
                            "name": "test",
                            "task": {
                                "stages": [],
                                "description": "test",
                                "name": "test",
                                "template": "test",
                            }
                        }
                    ],
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "array",
                        "items": {
                            "type": "object",
                        }
                    }
                }
            }
        },
        400: {
            "description": "You do not have a valid email",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "The user does not have email"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "GET:tasks/task_name": {
        200: {
            "description": "A specific task by its name, created by the user.",
            "content": {
                "application/json": {
                    "example": {
                        "author": "test@gmail.com",
                        "name": "test",
                        "task": {
                            "stages": [],
                            "description": "test",
                            "name": "test",
                            "template": "test",
                        }
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                    }
                }
            }
        },
        400: {
            "description": "You do not have a valid email",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "The user does not have email"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "PUT:tasks/task_name": {
        200: {
            "description": "If done correctly, it will return the modified task..",
            "content": {
                "application/json": {
                    "example": {
                        "author": "test@gmail.com",
                        "name": "test",
                        "task": {
                            "description": "test",
                            "template": "test",
                        }
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                    }
                }
            }
        },
        400: {
            "description": "This error could mean the following: \
                1. The user has an invalid email address, \
                2. The data sent is not valid json, \
                3. Tasks with this name were not found for this user, \
                4. Minimum obligatory task format is not observed (check if the fields: template and description). \
                detail in the error response, will indicate what exactly went wrong.",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Task does not exist"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:tasks/task_name": {
        200: {
            "description": "If done correctly, it will return the modified task..",
            "content": {
                "application/json": {
                    "example": {
                        "author": "test@gmail.com",
                        "name": "test",
                        "task": {
                            "description": "test",
                            "template": "test",
                        }
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                    }
                }
            }
        },
        400: {
            "description": "This error could mean the following: \
                1. The user has an invalid email address, \
                2. The data sent is not valid json, \
                3. Task exists already, \
                4. Minimum obligatory task format is not observed (check if the fields: template and description). \
                detail in the error response, will indicate what exactly went wrong.",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Task does not exist"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "DELETE:tasks/task_name": {
        200: {
            "description": "If all went well, you will get this result.",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer"
                        ],
                        "properties": {
                            "name": {
                                "title": "answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "If you get this error, then one of the following may have happened: \
                1. The user does not have a valid email address, \
                2. You didn't pass any of the task name arguments, \
                3. The task with that name was not found. The answer will tell you exactly what went wrong.",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Task does not exist"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "DELETE:tasks_to_robot": {
        200: {
            "description": "If the removal was successful you will see:",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer"
                        ],
                        "properties": {
                            "name": {
                                "title": "answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "If you get this error, then either the \
                user does not have a valid email address or \
                this robot is not found in the user's subscriptions",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Robot is not available"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:tasks_to_robot": {
        200: {
            "description": "If the removal was successful you will see:",
            "content": {
                "application/json": {
                    "example": {
                        "name": "tester",
                        "connected": False,
                        "state": "NEED_ML",
                        "snapshot": False,
                        "arm_connected": False,
                        "calibration": "OK",
                        "currentTask": "tester:test@test.test"
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "name",
                            "connected",
                            "state",
                            "snapshot",
                            "arm_connected",
                            "calibration",
                            "currentTask",
                        ],
                        "properties": {
                            "name": {
                                "title": "NameRobot",
                                "type": "string"
                            },
                            "connected": {
                                "title": "ConnectedInfo",
                                "type": "string"
                            },
                            "state": {
                                "title": "StateInfo",
                                "type": "string"
                            },
                            "snapshot": {
                                "title": "SnapshotInfo",
                                "type": "string"
                            },
                            "arm_connected": {
                                "title": "ArmConnectedInfo",
                                "type": "string"
                            },
                            "calibration": {
                                "title": "CalibrationInfo",
                                "type": "string"
                            },
                            "currentTask": {
                                "title": "TaskInfo",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        400: {
            "description": "If you get this error, then either the \
                user does not have a valid email address or \
                this robot is not found in the user's subscriptions",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Robot is not available"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "GET:available_commands": {
        200: {
            "description": "If the removal was successful you will see:",
            "content": {
                "application/json": {
                    "example": [
                        {
                            "id": "name command",
                            "description": "description command"
                        }
                    ],
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "array",
                        "required": [
                            "answer"
                        ]
                    }
                }
            }
        },
        400: {
            "description": "You will get this error if you do not have a verified email address.",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "The user does not have email"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:command by command_name": {
        200: {
            "description": "If the removal was successful you will see:",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer"
                        ],
                        "properties": {
                            "name": {
                                "title": "answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "This error can mean: \
                1. The user does not have a valid email address, \
                2. The specified command is not in the list of commands available to the user, \
                3. The specified robot is not found in the user's subscriptions. \
                The answer will indicate exactly what went wrong.",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "The user does not have email"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "POST:command": {
        200: {
            "description": "If the removal was successful you will see:",
            "content": {
                "application/json": {
                    "example": {
                        "answer": True
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "answer"
                        ],
                        "properties": {
                            "name": {
                                "title": "answer",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }
        },
        400: {
            "description": "This error can mean: \
                1. The user does not have a valid email address, \
                2. The data sent is not a json, \
                3. The specified robot is not found in the user's subscriptions. \
                4. The 'command' field was not found (Wrong json)\
                The answer will indicate exactly what went wrong.",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "The user does not have email"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "GET:logs": {
        200: {
            "description": "If the removal was successful you will see:",
            "content": {
                "application/json": {
                    "example": {
                        "records_number": 1,
                        "records": [],
                    },
                    "schema": {
                        "title": "SuccessSchema",
                        "type": "object",
                        "required": [
                            "records_number",
                            "records"
                        ],
                    },
                    "properties": {
                        "records_number": {
                            "title": "records_number",
                            "type": "integer"
                        },
                        "records": {
                            "title": "records",
                            "type": "array"
                        }
                    }
                }
            }
        },
        400: {
            "description": "If you get this error, then either you do not have a valid \
                email address or the specified robot was not found in your subscriptions",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "The user does not have email"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
        401: {
            "description": "It is these tokens that are needed to authorise in the platform and update these tokens",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "Not authenticated"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        }
    },
    "GET:files": {
        400: {
            "description": "If you get this error, it means that the specified file was not found in the data storage",
            "content": {
                "application/json": {
                    "example": {
                        "detail": "No corresponding file in datastorage"
                    },
                    "schema": {
                        "title": "ErrorSchema",
                        "type": "object",
                        "required": [
                            "detail",
                        ],
                        "properties": {
                            "detail": {
                                "title": "Text error",
                                "type": "string"
                            },
                        }
                    }
                }
            }
        },
    },
}
