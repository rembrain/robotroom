import logging
import time

# Try to connect to keycloak, if keycloak is unavailable - retry in 10 seconds
while True:
    try:
        from common.src.services.keycloak import get_keycloak_openid, get_keycloak_admin, login_request

        keycloak_openid = get_keycloak_openid()
        keycloak_admin = get_keycloak_admin()
        break
    except:
        logging.error('keycloak server is not available')
        time.sleep(10.0)


def get_keycloak_token(username, password):
    return get_keycloak_openid().token(username, password)
