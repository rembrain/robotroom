import json
import logging
import os
import typing as T
from threading import Thread, Timer

import boto3
import cv2
import numpy as np
import scipy.interpolate as interp
from boto3.exceptions import Boto3Error
from botocore.client import Config
from botocore.exceptions import ClientError, BotoCoreError
from fastapi import HTTPException
from starlette import status as status_codes

from common.src.utils import get_rabbit_connection


def is_json(json_data: str) -> bool:
    try:
        json.loads(json_data)
    except ValueError:
        return False

    return True


def get_one_sub_image(robot_name, rect):
    connection = get_rabbit_connection()

    channel = connection.channel()
    publisher_name = "rgbjpeg_" + robot_name
    channel.exchange_declare(exchange=publisher_name, exchange_type="fanout")

    result = channel.queue_declare(queue="", exclusive=True)
    queue_name = result.method.queue
    logging.info(publisher_name + " " + queue_name)

    channel.queue_bind(exchange=publisher_name, queue=queue_name)
    logging.info("after queue bind")

    for msg in channel.consume(queue_name, inactivity_timeout=0.3, auto_ack=True):

        if msg is None:
            logging.info("get_one_sub_image msg is None")
            black = np.zeros((256, 256, 3), np.uint8)
            result, buf = cv2.imencode(".jpg", black)
            return buf.tobytes()

        method, properties, body = msg

        if body is None:
            logging.info("get_one_sub_image  body is None")
            black = np.zeros((256, 256, 3), np.uint8)
            result, buf = cv2.imencode(".jpg", black)
            return buf.tobytes()

        else:
            image = cv2.imdecode(np.frombuffer(body, np.uint8), 1)
            if rect[0] < 1.0 and rect[1] < 1.0 and rect[0] + rect[2] < 1.0 and rect[1] + rect[3] < 1.0:
                left = int(rect[0] * image.shape[1])
                right = int((rect[0] + rect[2]) * image.shape[1])
                top = int(rect[1] * image.shape[0])
                bottom = int((rect[1] + rect[3]) * image.shape[0])

                result, buf = cv2.imencode(
                    ".jpg",
                    image[top:bottom, left:right, :],
                )
                return buf.tobytes()
            else:
                logging.info("get_one_sub_image rect out of image")
                black = np.zeros((256, 256, 3), np.uint8)
                result, buf = cv2.imencode(".jpg", black)
                return buf.tobytes()


def update_config(robot_name):
    connection = get_rabbit_connection()

    channel = connection.channel()
    channel.basic_publish(
        exchange="processor_commands_" + robot_name,
        routing_key="processor",
        body=json.dumps({"message": "update_config"}),
    )
    connection.close()

    logging.info(f"Update_config sent {robot_name}")


def keycloak_exception_handler(e):
    try:
        error = json.loads(e.error_message.decode("utf-8"))
        logging.error(error["error_description"])
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail=error["error_description"])

    except AttributeError as e:
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail=str(e.error_message))

    except Exception as e:
        logging.error(str(e))
        raise HTTPException(status_code=status_codes.HTTP_400_BAD_REQUEST, detail="Unknown keycloak error")


def voice_generation(robot: str, text: str, voice: str, polly_client, lock):
    lock.acquire()

    try:
        data = (
                np.frombuffer(
                    polly_client.synthesize_speech(
                        Text=text, OutputFormat="pcm", VoiceId=voice, SampleRate="16000"
                    )["AudioStream"].read(),
                    dtype=np.int16,
                ).astype(np.float32) / 2 ** 15
        )

        x: np.ndarray = np.arange(0, data.shape[0], dtype=np.int32)
        f = interp.interp1d(x, data, kind="cubic")
        x_new: T.Any = np.linspace(0, x.shape[0] - 1, int(x.shape[0] * 2), dtype=np.float32)

        wav: T.Any = f(x_new).astype(np.float32).tobytes()
        connection = get_rabbit_connection()
        channel = connection.channel()

        def func(wav):
            if len(wav) > 8192:
                frame, wav = wav[:8192], wav[8192:]
                Timer(0.064, func, [wav]).start()
            else:
                frame = wav[: len(wav)] + b"\x00" * (8192 - len(wav))

            channel.basic_publish(exchange=f"audio_from_world_{robot}", routing_key="", body=frame)

        # start the timer the first time
        Timer(0.0, func, [wav]).start()
    except Exception as e:
        logging.error(e, exc_info=True)
    finally:
        lock.release()


def run_voice_generator_thread(robot, data, polly_client, voice_lock):
    thread = Thread(
        target=voice_generation,
        args=(robot, data.text, data.voice, polly_client, voice_lock)
    )
    thread.start()


def add_final_session(s3_client, session_id, data):
    from tools.db import sessions_db

    new_data_files, new_m_data_files = [], []
    for file in data["files"]:
        path = file["file"].split('/')
        path = f"{path[1]}|{path[2]}"

        try:
            s3_client.copy_object(
                Bucket=os.environ['BUCKET_NAME_SESSIONS'],
                CopySource="/" + os.environ['BUCKET_NAME_ARCHIVE'] + "/" + file["file"] + "/camera0_rgb.jpg",
                Key=session_id + "/" + path + "camera0_rgb.jpg"
            )

            try:
                s3_client.head_object(Bucket=os.environ['BUCKET_NAME_ARCHIVE'], Key=file["file"] + "/camera1_rgb.jpg")
                s3_client.head_object(
                    Bucket=os.environ['BUCKET_NAME_ARCHIVE'],
                    Key=file["file"] + "/camera1_depth.png",
                )
                s3_client.head_object(Bucket=os.environ['BUCKET_NAME_ARCHIVE'], Key=file["file"] + "/camera1.json", )

                s3_client.copy_object(
                    Bucket=os.environ['BUCKET_NAME_SESSIONS'],
                    CopySource="/" + os.environ['BUCKET_NAME_ARCHIVE'] + "/" + file["file"] + "/camera1_rgb.jpg",
                    Key=session_id + "/" + path + "camera1_rgb.jpg"
                )
                try:
                    s3_client.copy_object(
                        Bucket=os.environ['BUCKET_NAME_SESSIONS'],
                        CopySource="/" + os.environ['BUCKET_NAME_ARCHIVE'] + "/" + file["file"] + "/camera1_depth.png",
                        Key=session_id + "/" + path + "camera1_depth.png"
                    )
                except ClientError:
                    logging.info('no camera1 depth image')

                s3_client.copy_object(
                    Bucket=os.environ['BUCKET_NAME_SESSIONS'],
                    CopySource="/" + os.environ['BUCKET_NAME_ARCHIVE'] + "/" + file["file"] + "/camera1.json",
                    Key=session_id + "/" + path + "camera1.json"
                )
            except ClientError:
                logging.info("No camera1 files")

            try:
                s3_client.copy_object(
                    Bucket=os.environ['BUCKET_NAME_SESSIONS'],
                    CopySource="/" + os.environ['BUCKET_NAME_ARCHIVE'] + "/" + file["file"] + "/camera0_depth.png",
                    Key=session_id + "/" + path + "camera0_depth.png"
                )
            except ClientError:
                logging.info("No depth image")

            s3_client.copy_object(
                Bucket=os.environ['BUCKET_NAME_SESSIONS'],
                CopySource="/" + os.environ['BUCKET_NAME_ARCHIVE'] + "/" + file["file"] + "/camera.json",
                Key=session_id + "/" + path + "camera.json"
            )

            s3_client.copy_object(
                Bucket=os.environ['BUCKET_NAME_SESSIONS'],
                CopySource="/" + os.environ['BUCKET_NAME_ARCHIVE'] + "/" + file["file"] + "/command.json",
                Key=session_id + "/" + path + "command.json"
            )
            s3_client.copy_object(
                Bucket=os.environ['BUCKET_NAME_SESSIONS'],
                CopySource="/" + os.environ['BUCKET_NAME_ARCHIVE'] + "/" + file["file"] + "/state.json",
                Key=session_id + "/" + path + "state.json"
            )
            new_data_files.append(file)
            new_m_data_files.append(file["file"])
        except ClientError as e:
            logging.error("One of the files is missing")
            logging.error(e,exc_info=True)

    try:
        minio_data = {"user": data["user"], "files": new_m_data_files}
        if data.get("name") is not None:
            minio_data["name"] = data.get("name")

        s3_client.put_object(
            Body=json.dumps(minio_data),
            Bucket=os.environ['BUCKET_NAME_SESSIONS'],
            Key=session_id + "/" + "session.json"
        )

        sessions_db.update_records(session_id, {"user": data["user"], "files": new_data_files})
    except ClientError as e:
        logging.error("Failed to overwrite file")
        logging.error(e)


def update_task(task_name, author):
    from tools.db import admin_db

    try:
        logging.info("looking for " + task_name + " task " + author)
        configs = admin_db.get_all_configs()

        for c in configs:
            conf = json.loads(c["value"])

            if "task_name" in conf and conf["task_name"] == task_name + ":" + author:
                robot_name = c["robot"]

                connection = get_rabbit_connection()
                channel = connection.channel()
                channel.basic_publish(
                    exchange=f"processor_commands_{robot_name}",
                    routing_key="processor",
                    body=json.dumps({"message": "update_config"}),
                )
                connection.close()

                logging.info(f"update_config sent {robot_name}")
    except Exception as e:
        logging.error(e, exc_info=True)


def get_s3_client():
    try:
        if os.environ.get("S3_REGION") is not None:
            return boto3.client(
                "s3",
                region_name=os.environ["S3_REGION"],
                aws_access_key_id=os.environ["S3_ACCESS_KEY"],
                aws_secret_access_key=os.environ["S3_SECRET_KEY"],
            )
        elif os.environ.get("S3_ADDRESS") is not None:
            return boto3.client(
                "s3",
                endpoint_url=os.environ["S3_ADDRESS"],
                aws_access_key_id=os.environ["S3_ACCESS_KEY"],
                aws_secret_access_key=os.environ["S3_SECRET_KEY"],
                config=Config(signature_version="s3v4"),
            )

        logging.error("There is no region or address to connect to S3 storage.")
    except (BotoCoreError, Boto3Error) as e:
        logging.error(f"Could not connect AWS S3 {e}.")
