import asyncio
import json
import logging
import os

import aio_pika
from rembrain_robot_framework.pack import Unpacker


class AmqpStream:
    queue_buffer = asyncio.Queue()

    def __init__(self, robot, redis_connect, preview=False):
        self.robot = robot
        self.channel = None
        self.queue = None
        self.redis_connect = redis_connect

        preview_image_name = "images/no_camera_preview.jpeg" if preview else "images/no_camera.jpeg"
        with open(preview_image_name, "rb") as f:
            self.no_cam_jpg = f.read()

        self.publisher_name = f"rgbjpeg_{self.robot}"
        if preview:
            self.publisher_name += "_preview"

    async def _connect(self):
        try:
            robots_status = json.loads(self.redis_connect.get("robots_status"))
            if self.robot not in robots_status.keys():
                self.supposed_connected = False
            else:
                self.supposed_connected = robots_status[self.robot]["sensors_connected"]

            if not self.supposed_connected:
                logging.info("Robot " + self.robot + " seems not connected ")
                return

            if "RABBIT_ADDRESS" in os.environ:
                connection = await aio_pika.connect(os.environ["RABBIT_ADDRESS"])
            else:
                connection = await aio_pika.connect(host="rabbitmq")

            self.channel = await connection.channel()
            self.exchange = await self.channel.declare_exchange(self.publisher_name, type="fanout")
            self.queue = await self.channel.declare_queue("", exclusive=True, auto_delete=True)

            await self.queue.bind(self.exchange)
        except Exception as e:
            logging.error(str(e))

    async def _close(self):
        if self.channel is None:
            logging.info("Channel is None")

        else:
            await self.channel.close()
            self.channel = None
            self.queue = None
            logging.info("Channel closed")

    async def _get_message(self, message, max_queue_buf_size=4):
        if self.queue_buffer.qsize() < max_queue_buf_size:

            if message.body is None:
                logging.info("Message is None")
                await self.queue_buffer.put(self.no_cam_jpg)

            else:
                await self.queue_buffer.put(message.body)

    async def stream_generation(self):
        try:
            await self._connect()
            await self.queue.consume(self._get_message, timeout=3.0, no_ack=True)
            loop = asyncio.get_event_loop()

            while True:
                while self.queue_buffer.qsize() > 1:
                    _ = await self.queue_buffer.get()

                message = await self.queue_buffer.get()
                result = await loop.run_in_executor(None, self.unpack, message)

                if result is None:
                    result = self.no_cam_jpg

                yield b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + result + b"\r\n"

        except Exception as e:
            logging.error(str(e))
        finally:
            logging.info("Stream close")
            await self._close()

    @classmethod
    def unpack(cls, buffer):
        return Unpacker().pre_unpack(buffer)[0]
