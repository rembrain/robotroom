from typing import List

from pydantic import BaseModel, EmailStr


class Login(BaseModel):
    username: str
    password: str


class LoginSocial(BaseModel):
    token: str
    provider: str


class UserUpdate(BaseModel):
    email: EmailStr = None
    firstName: str = None
    lastName: str = None
    role: str = None
    vendor: str = None


class UserPassword(BaseModel):
    old_password: str = None
    new_password: str


class UserRegister(BaseModel):
    username: str
    email: EmailStr
    role: str = "default"
    password: str


class UserRegisterClient(BaseModel):
    username: str = None
    email: EmailStr
    password: str


class Logs(BaseModel):
    filenames: str
    params: str


class Robot(BaseModel):
    name: str
    password: str


class RobotCommand(BaseModel):
    command: str


class CurrentUserOut(BaseModel):
    answer: str


class Config(BaseModel):
    value: str


class FilesInGetListArchiveFilesOut(BaseModel):
    robot: str
    datetime: str
    path: str


class GetListArchiveFilesOut(BaseModel):
    number_page: int
    total_pages: int
    files: List[FilesInGetListArchiveFilesOut]


class File(BaseModel):
    command: str
    file: str


class SpeakData(BaseModel):
    text: str
    voice: str = "Joanna"


class WiFiQR(BaseModel):
    SSID: str
    password: str


class Sessions(BaseModel):
    name: str = None
    files: List[File]


class SessionsName(BaseModel):
    name: str = None


class SessionsData(BaseModel):
    id: str
    weight: float


class SessionsTrain(BaseModel):
    name: str = None
    learning_rate: float
    max_iterations: int
    batch_size: int
    weight: str = "zero"
    sessions: List[SessionsData]
