import typing as T

from fastapi import HTTPException, Depends
from starlette import status as status_codes

from common.src.services.keycloak import login_request


def user_info_with_email(user_info: dict = Depends(login_request)) -> T.Union[dict, set]:
    if "email" not in user_info:
        raise HTTPException(
            status_code=status_codes.HTTP_400_BAD_REQUEST,
            detail={"reason": "The user does not have email"}
        )

    return user_info


def user_info_with_group(user_info: dict = Depends(login_request)) -> T.Union[dict, set]:
    if "groups" not in user_info:
        raise HTTPException(status_code=status_codes.HTTP_403_FORBIDDEN, detail="Wrong role")

    return user_info
