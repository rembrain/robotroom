import argparse
import asyncio
import json
import os

from tqdm import tqdm

from tools.db import archive_db
from tools.util import get_s3_client

BUCKET_NAME = os.environ["BUCKET_NAME_ARCHIVE"]


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--process_all", type=bool, default=False)
    return parser.parse_args()


async def main(args):
    commands = archive_db.get_filter_pagination_archive_command(
        page_num=1, page_size=1_000_000
    )
    print("total commands", len(commands))

    for i, command in tqdm(enumerate(commands), total=len(commands)):
        print("current command", i, command)
        file_key = get_command_json_key(command["path"])
        command_json_data = get_data(file_key)
        if "content" in command_json_data:
            command_json_data = command_json_data["content"]

        archive_db.update_command_by_id(command["id"], {"content": command_json_data})
        update_json_data(command_json_data, file_key)

        if not args.process_all:
            break


def get_data(path):
    s3_client = get_s3_client()
    response = s3_client.get_object(Bucket=BUCKET_NAME, Key=path)
    return json.loads(json.loads(response["Body"].read().decode()))


def update_json_data(command_json_data, path):
    s3_client = get_s3_client()
    s3_client.put_object(
        Body=json.dumps(json.dumps(command_json_data)),
        Bucket=BUCKET_NAME,
        Key=get_command_json_key(path),
    )


def get_command_json_key(path: str):
    return f"{path}/command.json"


if __name__ == "__main__":
    asyncio.run(main(parse_args()))
