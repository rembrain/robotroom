import os
import time
import json
import logging
import traceback
from typing import Type

import redis

from fastapi import FastAPI, HTTPException, Request, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from rembrain_robot_framework.logger.utils import get_log_handler

from routers import initialize_app_routers

from common.src.services.keycloak import login_request
from tools.docs import main
from tools.db import admin_db


logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
logger = logging.getLogger("werkzeug")

r = redis.Redis(host=os.environ["REDIS_ADDRESS"], port=6379, db=0)
r.set("robots_status", "{}")

if "@" in os.environ["RABBIT_ADDRESS"]:
    rabbit = get_log_handler({"project": "brainless_robot", "subsystem": "http-gate"}, in_cluster=True)
    if rabbit is not None:
        rabbit = logging.StreamHandler()
    for log in [logger, logging.root]:
        log.addHandler(rabbit)

logging.info("start logging")


app = FastAPI(
    title=main.title,
    description=main.description_app,
    version=main.version,
)
vr = FastAPI(docs_url=None)


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_app_middleware(app: FastAPI, middleware_class: Type):
    middleware_index = None
    for index, middleware in enumerate(app.user_middleware):
        if middleware.cls == middleware_class:
            middleware_index = index
    return None if middleware_index is None else app.user_middleware[middleware_index]


@app.exception_handler(Exception)
async def internal_exception_handler(request, exc):
    trace = traceback.format_exc()
    logger.critical(f"{request.url.path} {status.HTTP_500_INTERNAL_SERVER_ERROR} Internal Server Error \n {trace}.")
    response = JSONResponse(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
    cors_middleware = get_app_middleware(app=request.app, middleware_class=CORSMiddleware)
    request_origin = request.headers.get("origin", "")
    if cors_middleware and "*" in cors_middleware.options["allow_origins"]:
        response.headers["Access-Control-Allow-Origin"] = "*"
    elif cors_middleware and request_origin in cors_middleware.options["allow_origins"]:
        response.headers["Access-Control-Allow-Origin"] = request_origin
    return response


app = initialize_app_routers(app)


templates = Jinja2Templates(directory="build")
#templates_vr = Jinja2Templates(directory="vr")


@app.get("/robots/{robot}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/robots", response_class=HTMLResponse, include_in_schema=False)
@app.get("/tasks/{name_author}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/tasks", response_class=HTMLResponse, include_in_schema=False)
@app.get("/retraining/{stage}/{robot}/{id}/{file}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/retraining/{stage}/{robot}/{id}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/retraining/{stage}/{robot}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/retraining/{stage}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/retraining", response_class=HTMLResponse, include_in_schema=False)
@app.get("/web_operators/{operator}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/web_operators", response_class=HTMLResponse, include_in_schema=False)
@app.get("/watchtower/{robotname}/{filepath}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/watchtower/{robotname}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/watchtower", response_class=HTMLResponse, include_in_schema=False)
@app.get("/system_monitoring/{processors}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/auth", response_class=HTMLResponse, include_in_schema=False)
@app.get("/users/{actions}", response_class=HTMLResponse, include_in_schema=False)
@app.get("/users", response_class=HTMLResponse, include_in_schema=False)
@app.get("/", response_class=HTMLResponse, include_in_schema=False)
async def read_item(request: Request):
    urls = {
        "APP_HTTP_GATE_URL": os.environ["APP_HTTP_GATE_URL"],
        "APP_WEB_SOCKET_GATE_URL": os.environ["APP_WEB_SOCKET_GATE_URL"],
        "SERVER_OFFSET": time.timezone / 3600,
    }
    urls = json.dumps(urls)
    return templates.TemplateResponse("index.html", {"request": request, "urls": urls})


@vr.get("/", response_class=HTMLResponse)
async def read_item(robot: str, authorization: str, refresh: str, request: Request):
    config = json.loads(admin_db.get_config(robot)["value"])
    calibration_1 = config.get("calibration")
    calibration_2 = config.get("calibration2")
    if calibration_2 is None or calibration_1 is None:
        raise HTTPException(status_code=400, detail="No calibration matrix found")
    urls = {
        "WSS_URL": os.environ["APP_WEB_SOCKET_GATE_URL"],
        "BASE_URL": os.environ["APP_HTTP_GATE_URL"],
        "ROBOT": robot,
        "AUTHORIZATION": authorization,
        "REFRESH": refresh,
        "CALIBRATION_1": json.dumps(calibration_1),
        "CALIBRATION_2": json.dumps(calibration_2),
    }
    urls = json.dumps(urls)
    return templates_vr.TemplateResponse("index.html", {"request": request, "urls": urls})


#vr.mount("/", StaticFiles(directory="vr", html=True), name="vr")
#app.mount("/vr", vr)
app.mount("/", StaticFiles(directory="build", html=True), name="build")

# TODO: DELETE
import uvicorn

if __name__ == "__main__":
    uvicorn.run("main:app", port=8081)
