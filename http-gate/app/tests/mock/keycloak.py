from fastapi import HTTPException
from keycloak.exceptions import KeycloakAuthenticationError


def login_token_400(username, password):
    raise KeycloakAuthenticationError(
        error_message=b'{"error":"invalid_grant", "error_description":"Invalid user credentials"}',
        response_code=401
    )


def login_token_200(username, password):
    return {"access_token": "test", "refresh_token": "test"}


def login_request_response_keycloak_401():
    raise HTTPException(status_code=401, detail="Could not log in to the system")
