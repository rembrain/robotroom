from fastapi.testclient import TestClient


def test_api_version(test_client: TestClient):
    response = test_client.get("/version")
    assert response.ok
    assert response.json()["version"] == "0.0.1"
