import io
import json
from unittest.mock import MagicMock, Mock

from fastapi.testclient import TestClient
from pytest import fixture, mark

from common.src.services.keycloak import login_request
from tools.db import archive_db
from tools.util import get_s3_client


@fixture
def app(app, command_json):
    s3_mock = MagicMock()
    s3_mock.get_object = Mock(
        return_value={"Body": io.BytesIO(json.dumps(json.dumps(command_json)).encode())}
    )
    app.dependency_overrides[get_s3_client] = lambda: s3_mock
    app.dependency_overrides[login_request] = lambda: {"groups": ["admin"]}

    return app


def test_get_robot_command_list(test_client: TestClient, command_db_entity, robot_id):
    response = test_client.get(
        url="/sessions/list_archive_files",
        params={
            "robot": robot_id,
            "page_num": 1,
        },
    )

    assert response.ok
    data = response.json().get("answer", {})
    assert data["total_pages"] == 1
    files = data["files"]
    assert len(files) == 1
    assert files[0]["id"] == str(command_db_entity["_id"])


def test_get_command_json_data(test_client: TestClient, command_file: str):
    response = test_client.get("/sessions/command_json", params={"file": command_file})
    assert response.ok
    data = response.json()
    assert data


def test_get_command_image_data(test_client: TestClient, command_file: str):
    response = test_client.get("/sessions/img_file", params={"file": command_file})
    assert response.ok
    data = response.json()
    assert data


def test_delete_robot_command(
    test_client: TestClient, command_db_entity, rabbit_exchange
):
    command_id = command_db_entity["_id"]
    command_db = archive_db.get_command_by_id(command_id)
    assert command_db is not None

    response = test_client.delete(f"/commands/{command_id}")
    assert response.ok

    command_db = archive_db.get_command_by_id(command_id)
    assert command_db is None


def test_update_robot_command(
    test_client: TestClient, command_db_entity, rabbit_exchange
):
    command_id = command_db_entity["_id"]
    command_db = archive_db.get_command_by_id(command_id)
    assert command_db is not None

    response = test_client.put(
        f"/commands/{command_id}",
        json={
            **command_db_entity,
            "command": "stop_remember!",
            "datetime": command_db_entity["datetime"].isoformat(),
        },
    )

    assert response.ok


def test_get_robot_commands(test_client: TestClient, command_db_entity, command_json):
    response = test_client.get(
        "/commands",
        params={
            "robot": command_db_entity["robot"],
        },
    )
    assert response.ok
    data = response.json()
    assert len(data) == 1
    command = data[0]

    assert command["content"] == command_json


@mark.parametrize(
    "command_operation, expected_commands",
    [["test_command", 0], ["remember_arm_trajectory", 1]],
)
def test_get_robot_commands_with_operation_parameter(
    test_client: TestClient,
    command_db_entity,
    command_operation,
    expected_commands,
):
    response = test_client.get(
        "/commands",
        params={
            "robot": command_db_entity["robot"],
            "command_operation": command_operation,
        },
    )
    assert response.ok
    data = response.json()
    assert len(data) == expected_commands


def test_get_command_by_id(test_client: TestClient, command_db_entity):
    command_id = command_db_entity["_id"]
    response = test_client.get(f"/commands/{command_id}")
    assert response.ok
    assert response.json()
