from fastapi import FastAPI
from fastapi.testclient import TestClient

from common.src.services.keycloak import login_request
from app.tests.mock.keycloak import login_request_response_keycloak_401, login_token_200, login_token_400
from app.routers import user


def test_login_valid(monkeypatch):
    app = FastAPI()
    monkeypatch.setattr(user, "get_keycloak_token", login_token_200)
    app.include_router(user.router)

    # Этот метод независит от того залогинен пользователь или нет, поэтому мокаем просто чтобы не ругался
    app.dependency_overrides[login_request] = login_request_response_keycloak_401
    with TestClient(app) as test_client:
        resp = test_client.post(
            "/login",
            json={"username": "test", "password": "test"},
        )

        assert resp.status_code == 200
        assert resp.json() == {"access_token": "test", "refresh_token": "test"}


def test_login_invalid(monkeypatch):
    app = FastAPI()
    monkeypatch.setattr(user, "get_keycloak_token", login_token_400)
    app.include_router(user.router)

    # Этот метод независит от того залогинен пользователь или нет, поэтому мокаем просто чтобы не ругался
    app.dependency_overrides[login_request] = login_request_response_keycloak_401
    with TestClient(app) as test_client:
        resp = test_client.post(
            "/login",
            json={"username": "test", "password": "test"},
        )
        assert resp.status_code == 400
        assert resp.json()["detail"] == "Invalid user credentials"
