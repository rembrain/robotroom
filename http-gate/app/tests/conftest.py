import sys

sys.path.append("../..")

from datetime import datetime
from uuid import uuid4
import time

from fastapi import FastAPI
from fastapi.testclient import TestClient
from pytest import fixture
from routers import initialize_app_routers

from common.src.utils import get_rabbit_connection
from tools.db import archive_db


@fixture
def robot_id():
    return str(uuid4())


@fixture
def command_file():
    return f"{uuid4()}/{uuid4()}/{uuid4()}"


@fixture
def command_json():
    return {
        "type": "memory",
        "name": "bottle",
        "op": "remember_arm_trajectory",
        "track": [
            {"set_joints": list(range(7)), "timestamp": time.},
            {"set_joints": list(range(7))},
            {"command": "set_gripper_release", "speed": 500},
            {"set_joints": list(range(7))},
            {"set_joints": list(range(7))},
            {"command": "set_gripper_pick", "speed": 500, "force": 250},
        ],
    }


@fixture
def command_db_entity(robot_id, command_file, command_json):
    command_entity = {
        "robot": robot_id,
        "path": command_file,
        "datetime": datetime.now(),
        "command": command_json["op"],
        "content": command_json,
    }
    archive_db.save_command(command_entity)
    command_entity["_id"] = str(command_entity["_id"])
    return command_entity


@fixture
def rabbit_exchange(robot_id):
    connection = get_rabbit_connection()
    channel = connection.channel()
    exchange = f"commands_{robot_id}"
    channel.exchange_declare(exchange)

    yield

    channel.exchange_delete(exchange)


@fixture
def cleanup_mongo():
    yield
    archive_db.db.drop_collection("command")


@fixture
def app(cleanup_mongo):
    application = FastAPI()
    application = initialize_app_routers(application)

    return application


@fixture
def test_client(app):
    with TestClient(app) as test_client:
        yield test_client
