import axios from "axios";

import { calibration1, calibration2, baseUrl, robot, headers } from "./endpoints.js";
import { dataInRobot, getCalibrationMatrix, getCamera0Model } from "./tools.js";
import { multiply, sqrt } from "mathjs";
import { cameraWorker } from "./index";
import {
    $mode,
    $pickupData,
    $finalPoint,
    $paint,
    changePaint,
    $paintPoints,
    addPaintPoint,
    finalPaintPoint,
    changePickupData,
    removePickupData,
    finalVolume,
    $capturePoint,
    addOneCapturePoint,
    addTwoCapturePoint,
    removeAll,
    $markupPoints,
    addMarkupPoint,
    removeMarkupPoint,
    preFinalVolume,
    $cameraExchange
} from "./store";

const Quaternion = require("quaternion");

AFRAME.registerComponent("cursor_listener", {
    schema: { hand: { default: "right" } },

    init: async function () {
        const el = this.el;

        if (
            el.getAttribute("oculus-touch-controls").replace("hand: ", "") ==
            "right"
        ) {
            const arrow = document.getElementById("arrow");

            el.addEventListener("triggerup", function (evt) {
                const mode = $mode.getState();
                arrow.setAttribute("material", "color: blue");

                if (mode === "free") {
                    //const box = document.getElementById("box");
                    const posAndRot = dataInRobot(document.getElementById("box"));
                    //const log = document.getElementById("log");
                    //log.setAttribute("text", "value: " + JSON.stringify(posAndRot) + ";")                
                    axios.post(
                        baseUrl + "/api/push_command/" + robot,
                        {
                            command: JSON.stringify({
                                op: "goto",
                                pos: [
                                    posAndRot.pos[0],
                                    posAndRot.pos[1],
                                    posAndRot.pos[2],
                                ],
                                rot: posAndRot.rot,
                                calibration: getCalibrationMatrix(),
                                tcp: [0, 0, 0],
                            }),
                        },
                        { headers: headers }
                    );
                } else if (mode === "pickup" || mode === "shift") {
                    const posAndRot = dataInRobot(document.getElementById("box"));
                    changePickupData({
                        pos: posAndRot.pos,
                        rot: posAndRot.rot,
                    });
                } else if (mode === "pickup_box") {
                    const box = document.getElementById("box");
                    let position = box.object3D
                        .getWorldPosition(new THREE.Vector3())
                        .clone();
                    if ($finalPoint.getState() !== null) {
                        if ($paint.getState() === true) {
                            finalPaintPoint(position);
                            changePaint(false);
                        } else {
                            if ($paintPoints.getState()[1][0] === null) {
                                if (
                                    position.z < $paintPoints.getState()[0][0].z
                                ) {
                                    finalVolume(position);
                                }
                            }
                        }
                    }
                } else if (mode === "markup") {
                    const posAndRot = dataInRobot(document.getElementById("box"));
                    addMarkupPoint({
                        pos: posAndRot.pos,
                        rot: posAndRot.rot,
                    });
                    const oldPosition = arrow.object3D
                        .getWorldPosition(new THREE.Vector3())
                        .clone();
                    let qu = new THREE.Quaternion();
                    const position_qu = arrow.object3D.getWorldQuaternion(qu);
                    const addPoint = document.createElement("a-entity");
                    addPoint.setAttribute("id", "markupPoints");
                    addPoint.setAttribute("obj-model", "obj: #arrow-obj");
                    addPoint.setAttribute(
                        "position",
                        `${oldPosition.x} ${oldPosition.y} ${oldPosition.z}`
                    );
                    addPoint.setAttribute("scale", "0.1 0.15 0.15");
                    addPoint.setAttribute("material", "color: blue");
                    addPoint.object3D.setRotationFromQuaternion(position_qu);
                    document.getElementById("scene").append(addPoint);
                }
            });
            el.addEventListener("triggerdown", function (evt) {
                const mode = $mode.getState();
                arrow.setAttribute("material", "color: green");
                if (mode === "pickup_box") {
                    const box = document.getElementById("box");
                    let worldQ = new THREE.Quaternion();
                    box.object3D.getWorldQuaternion(worldQ);
                    let position = box.object3D
                        .getWorldPosition(new THREE.Vector3())
                        .clone();
                    if ($finalPoint.getState() === null) {
                        changePaint(true);
                        addPaintPoint(position);
                    } else {
                        if ($paintPoints.getState()[1][0] === null) {
                            if (position.z < $paintPoints.getState()[0][0].z) {
                                for (
                                    let i = 0;
                                    i < $paintPoints.getState();
                                    i++
                                ) {}
                                preFinalVolume(position);
                                //finalVolume(position);
                            }
                        } else {
                            if ($capturePoint.getState().one === null) {
                                addOneCapturePoint({
                                    pos: position,
                                    rot: worldQ,
                                });

                                while (
                                    typeof document.getElementById(
                                        "paintPoints"
                                    ) != "undefined" &&
                                    document.getElementById("paintPoints")
                                ) {
                                    let paintPoints =
                                        document.getElementById("paintPoints");
                                    let paintPointsPosition =
                                        paintPoints.object3D
                                            .getWorldPosition(
                                                new THREE.Vector3()
                                            )
                                            .clone();
                                    let worldQ = new THREE.Quaternion();
                                    box.object3D.getWorldQuaternion(worldQ);
                                    const worldRotation = new Quaternion({
                                        x: -worldQ.x,
                                        y: -worldQ.y,
                                        z: -worldQ.z,
                                        w: worldQ.w,
                                    });
                                    paintPoints.setAttribute(
                                        "position",
                                        `${
                                            paintPointsPosition.x - position.x
                                        } ${
                                            paintPointsPosition.y - position.y
                                        } ${paintPointsPosition.z - position.z}`
                                    );
                                    paintPoints.setAttribute("id", "offPoint");
                                    paintPoints.object3D.position.applyQuaternion(
                                        worldRotation
                                    );
                                    const oldPosition = arrow.object3D
                                        .getWorldPosition(new THREE.Vector3())
                                        .clone();
                                    let qu = new THREE.Quaternion();
                                    const position_qu =
                                        arrow.object3D.getWorldQuaternion(qu);
                                    const addPoint =
                                        document.createElement("a-entity");
                                    addPoint.setAttribute("id", "offPoint");
                                    addPoint.setAttribute(
                                        "obj-model",
                                        "obj: #arrow-obj"
                                    );
                                    addPoint.setAttribute(
                                        "position",
                                        `${oldPosition.x} ${oldPosition.y} ${oldPosition.z}`
                                    );
                                    addPoint.setAttribute(
                                        "scale",
                                        "0.1 0.15 0.15"
                                    );
                                    addPoint.setAttribute(
                                        "material",
                                        "color: blue"
                                    );
                                    addPoint.object3D.setRotationFromQuaternion(
                                        position_qu
                                    );
                                    document
                                        .getElementById("scene")
                                        .append(addPoint);

                                    box.object3D.add(paintPoints.object3D);
                                }
                            } else {
                                while (
                                    typeof document.getElementById(
                                        "offPoint"
                                    ) != "undefined" &&
                                    document.getElementById("offPoint")
                                ) {
                                    document
                                        .getElementById("offPoint")
                                        .remove();
                                }
                                const points = $paintPoints.getState();
                                let volume = { top_edge: [], vec: [] };

                                for (let j = 0; j < points[0].length; ++j) {
                                    let position = points[0][j];
                                    position.y -= 1.6;
                                    let posToCamera = [
                                        position.x * 1000,
                                        -position.y * 1000,
                                        -position.z * 1000,
                                        1.0,
                                    ];
                                    let posToRobot = multiply(
                                        getCalibrationMatrix(),
                                        posToCamera
                                    );
                                    volume.top_edge.push(posToRobot);
                                }
                                let fin_vec = points[1][0];
                                fin_vec.y -= 1.6;
                                let fin_vecToCamera = [
                                    fin_vec.x * 1000,
                                    -fin_vec.y * 1000,
                                    -fin_vec.z * 1000,
                                    1.0,
                                ];
                                let fin_vecToRobot = multiply(
                                    getCalibrationMatrix(),
                                    fin_vecToCamera
                                );
                                volume.vec = [
                                    volume.top_edge[0],
                                    fin_vecToRobot,
                                ];

                                let onePoint = $capturePoint.getState().one;

                                onePoint.pos.y -= 1.6;
                                let posOne = [
                                    onePoint.pos.x * 1000,
                                    -onePoint.pos.y * 1000,
                                    -onePoint.pos.z * 1000,
                                    1.0,
                                ];
                                let posOneRobot = multiply(getCalibrationMatrix(), posOne);
                                position.y -= 1.6;
                                let posTwo = [
                                    position.x * 1000,
                                    -position.y * 1000,
                                    -position.z * 1000,
                                    1.0,
                                ];
                                let posTwoRobot = multiply(getCalibrationMatrix(), posTwo);

                                const worldRotationOneMatrix = new Quaternion({
                                    x: onePoint.rot["_x"],
                                    y: -onePoint.rot["_y"],
                                    z: -onePoint.rot["_z"],
                                    w: onePoint.rot["_w"],
                                }).toMatrix();
                                const calib_r = getCalibrationMatrix().map((val) =>
                                    val.slice(0, -1)
                                );
                                const robotRotationOneMatrix = multiply(
                                    calib_r,
                                    [
                                        [
                                            worldRotationOneMatrix[0],
                                            worldRotationOneMatrix[1],
                                            worldRotationOneMatrix[2],
                                        ],
                                        [
                                            worldRotationOneMatrix[3],
                                            worldRotationOneMatrix[4],
                                            worldRotationOneMatrix[5],
                                        ],
                                        [
                                            worldRotationOneMatrix[6],
                                            worldRotationOneMatrix[7],
                                            worldRotationOneMatrix[8],
                                        ],
                                    ]
                                );

                                let worldQ = new THREE.Quaternion();
                                box.object3D.getWorldQuaternion(worldQ);
                                const worldRotationMatrix = new Quaternion({
                                    x: worldQ.x,
                                    y: -worldQ.y,
                                    z: -worldQ.z,
                                    w: worldQ.w,
                                }).toMatrix();

                                const robotRotationTwoMatrix = multiply(
                                    calib_r,
                                    [
                                        [
                                            worldRotationMatrix[0],
                                            worldRotationMatrix[1],
                                            worldRotationMatrix[2],
                                        ],
                                        [
                                            worldRotationMatrix[3],
                                            worldRotationMatrix[4],
                                            worldRotationMatrix[5],
                                        ],
                                        [
                                            worldRotationMatrix[6],
                                            worldRotationMatrix[7],
                                            worldRotationMatrix[8],
                                        ],
                                    ]
                                );

                                axios.post(
                                    baseUrl + "/api/push_command/" + robot,
                                    {
                                        command: JSON.stringify({
                                            op: "pickup_putdown_box",
                                            pos1: posOneRobot,
                                            pos2: posTwoRobot,
                                            rot1: robotRotationOneMatrix,
                                            rot2: robotRotationTwoMatrix,
                                            calibration: getCalibrationMatrix(),
                                            volume: volume,
                                            source: "operator",
                                        }),
                                    },
                                    { headers: headers }
                                );

                                removeAll(false);
                            }
                        }
                    }
                }
            });
            el.addEventListener("gripup", function (evt) {
                const mode = $mode.getState();
                arrow.setAttribute("material", "color: red");
                if (mode === "free") {
                    axios
                        .get(baseUrl + "/api/extract/sensors/" + robot, {
                            headers: headers,
                        })
                        .then((answer) => {
                            const status = answer.data.answer;
                            if (status.gripper === false) {
                                axios.post(
                                    baseUrl + "/api/push_command/" + robot,
                                    {
                                        command: JSON.stringify({
                                            op: "vacuum_on",
                                            use_source: true,
                                        }),
                                    },
                                    { headers: headers }
                                );
                            } else {
                                axios.post(
                                    baseUrl + "/api/push_command/" + robot,
                                    {
                                        command: JSON.stringify({
                                            op: "vacuum_off",
                                            use_source: true,
                                        }),
                                    },
                                    { headers: headers }
                                );
                            }
                        });
                } else if (mode === "pickup" || mode === "shift") {
                    removePickupData();
                } else if (mode === "pickup_box") {
                    while (
                        typeof document.getElementById("offPoint") !=
                            "undefined" &&
                        document.getElementById("offPoint")
                    ) {
                        document.getElementById("offPoint").remove();
                    }
                    while (
                        typeof document.getElementById("paintPoints") !=
                            "undefined" &&
                        document.getElementById("paintPoints")
                    ) {
                        document.getElementById("paintPoints").remove();
                    }
                    removeAll(false);
                } else if (mode === "markup") {
                    while (
                        typeof document.getElementById("markupPoints") !=
                            "undefined" &&
                        document.getElementById("markupPoints")
                    ) {
                        document.getElementById("markupPoints").remove();
                    }
                    removeMarkupPoint([]);
                }
                arrow.setAttribute("material", "color: blue");
            });
            el.addEventListener("bbuttondown", function (evt) {
                const cameraExchange = $cameraExchange.getState()
                if (cameraExchange === "camera1") {
                    axios.post(
                        baseUrl + "/api/push_command/" + robot,
                        {
                            command: JSON.stringify({
                                op: "take_snapshot",
                                use_source: true,
                            }),
                        },
                        { headers: headers }
                    );
                } else if (cameraExchange === "camera0") {
                    //Это относится к кнопке костылю, её лучше не трогать
                    const camera0Сrutch = document.getElementById("camera0Crutch")
                    camera0Сrutch.click()

                    //const log = document.getElementById("log");
                    //log.setAttribute("text", "value: " + JSON.stringify(1) + ";")
                }  
            });
            el.addEventListener("abuttondown", function (evt) {
                const mode = $mode.getState();
                if (mode === "markup") {
                    axios.post(
                        baseUrl + "/api/push_command/" + robot,
                        {
                            command: JSON.stringify({
                                op: "markup",
                                points: $markupPoints.getState(),
                                calibration: getCalibrationMatrix(),
                                source: "operator",
                            }),
                        },
                        { headers: headers }
                    );
                    while (
                        typeof document.getElementById("markupPoints") !=
                            "undefined" &&
                        document.getElementById("markupPoints")
                    ) {
                        document.getElementById("markupPoints").remove();
                    }
                    removeMarkupPoint([]);
                }
            });
            el.addEventListener("abuttonup", function (evt) {});
        }
    },

    tick: function (time, timeDelta) {
        if ($paint.getState() === true) {
            const finalPoint = $finalPoint.getState();
            const box = document.getElementById("box");
            const positionBox = box.object3D.getWorldPosition(
                new THREE.Vector3()
            );

            if (
                sqrt(
                    (finalPoint.x - positionBox.x) ** 2 +
                        (finalPoint.y - positionBox.y) ** 2 +
                        (finalPoint.z - positionBox.z) ** 2
                ) > 0.02
            ) {
                addPaintPoint(positionBox);
            }
        }
    },
});
