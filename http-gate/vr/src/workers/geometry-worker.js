self.onmessage = (ev) => {
    console.log(ev)
    const { cameraData, img, depth, exchange } = ev.data;
    const { data, width, height } = depth;

    let points = new Float32Array(width * height * 3);
    let uvs = new Float32Array(width * height * 2);
    let l = 0;
    let L = 0;
    let indices

    if (exchange === "camera1") {
        const { CameraMatrix, Distortion } = cameraData;
        
        const fx = CameraMatrix.FX;
        const fy = CameraMatrix.FY;
        const cx = CameraMatrix.CX;
        const cy = CameraMatrix.CY;

        const k1 = Distortion.K1;
        const k2 = Distortion.K2;
        const k3 = Distortion.K3;
        const p1 = Distortion.P1;
        const p2 = Distortion.P2;

        let kth = 0.01;
        let boolAr = new Array(width * height).fill(false);

        let dz = 0;
        for (l = width + 1; l < width * height; l++) {
            dz = kth * data[l];
            if (
                data[l] != 0 &&
                data[l - 1] != 0 &&
                data[l - width] != 0 &&
                data[l - width - 1] != 0 &&
                Math.abs(data[l - 1] - data[l]) < dz &&
                Math.abs(data[l - width] - data[l]) < dz &&
                Math.abs(data[l - width - 1] - data[l]) < dz &&
                Math.abs(data[l - 1] - data[l - width - 1]) < dz
            ) {
                L += 6;
                boolAr[l] = true;
            }
        }
        l = 0;

        for (let y0 = 0; y0 < height; y0++) {
            for (let x0 = 0; x0 < width; x0++) {
                let z = data[l];

                let x_src = (x0 - cx) / fx;
                let y_src = (y0 - cy) / fy;
                let x = x_src;
                let y = y_src;

                let r2, r4, r6, rad_dist, tang_dist_x, tang_dist_y, x_n, y_n;

                for (let a = 0; a < 3; ++a) {
                    r2 = x ** 2 + y ** 2;
                    r4 = r2 ** 2;
                    r6 = r2 * r4;

                    rad_dist = 1 / (1 + k1 * r2 + k2 * r4 + k3 * r6);
                    tang_dist_x = 2 * p1 * x * y + p2 * (r2 + 2 * x ** 2);
                    tang_dist_y = 2 * p2 * x * y + p1 * (r2 + 2 * y ** 2);

                    x = (x_src - tang_dist_x) * rad_dist;
                    y = (y_src - tang_dist_y) * rad_dist;
                }
                z *= (0.001 * 2000) / 65535;

                if (x0 === width - 1 && y0 === height - 1) {
                    console.log(x_src);
                    console.log(width);
                    console.log(cx);

                    console.log(y_src);
                    console.log(height);
                    console.log(cy);
                }

                points[l * 3] = x * z;
                points[l * 3 + 1] = -y * z;
                points[l * 3 + 2] = -z;
                uvs[l * 2] = x0 / width;
                uvs[l * 2 + 1] = y0 / height;

                l += 1;
            }
        }

        indices = new Uint32Array(L);
        l = 0;
        let l1 = 0;
        for (let y = 0; y < height; y++) {
            for (let x = 0; x < width; x++) {
                if (boolAr[l]) {
                    indices[l1 * 6] = l - width - 1;
                    indices[l1 * 6 + 1] = l;
                    indices[l1 * 6 + 2] = l - width;
                    indices[l1 * 6 + 3] = l - width - 1;
                    indices[l1 * 6 + 4] = l - 1;
                    indices[l1 * 6 + 5] = l;
                    l1 += 1;
                }
                l += 1;
            }
        }
    } else if (exchange === "camera0") {

        const fx = cameraData.fx / 2;
        const fy = cameraData.fy / 2;
        const cx = cameraData.ppx / 2;
        const cy = cameraData.ppy / 2;

        let l = 0;

        let L = 0;
        let kth = 0.05;
        let boolAr = new Array(width * height).fill(false);

        let dz = 0;
        for (l = width + 1; l < width * height; l++) {
            dz = kth * data[l];
            if (
                data[l] != 0 &&
                data[l - 1] != 0 &&
                data[l - width] != 0 &&
                data[l - width - 1] != 0 &&
                Math.abs(data[l - 1] - data[l]) < dz &&
                Math.abs(data[l - width] - data[l]) < dz &&
                Math.abs(data[l - width - 1] - data[l]) < dz &&
                Math.abs(data[l - 1] - data[l - width - 1]) < dz
            ) {
                L += 6;
                boolAr[l] = true;
            }
        }
        l = 0;

        for (let y = 0; y < height; y++) {
            for (let x = 0; x < width; x++) {
                let z = data[l];

                z *= 0.001;
                points[l * 3] = ((x - cx) * z) / fx;
                points[l * 3 + 1] = (-(y - cy) * z) / fy;
                points[l * 3 + 2] = -z;
                uvs[l * 2] = x / width;
                uvs[l * 2 + 1] = y / height;

                l += 1;
            }
        }

        indices = new Uint32Array(L);
        l = 0;
        let l1 = 0;
        for (let y = 0; y < height; y++) {
            for (let x = 0; x < width; x++) {
                if (boolAr[l]) {
                    indices[l1 * 6] = l - width - 1;
                    indices[l1 * 6 + 1] = l;
                    indices[l1 * 6 + 2] = l - width;
                    indices[l1 * 6 + 3] = l - width - 1;
                    indices[l1 * 6 + 4] = l - 1;
                    indices[l1 * 6 + 5] = l;
                    l1 += 1;
                }
                l += 1;
            }
        }
    }

    
    boolAr = null;
    postMessage({ img, positions: [points, uvs, indices] });
};
