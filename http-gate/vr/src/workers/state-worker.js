import axios from "axios";

self.onmessage = ({ data }) => {
    const { url, robot, headers } = data;
    
    let repeat = setInterval(() => {
        axios.get(
            url + "/api/extract/sensors/" + robot,
            { headers: headers }
        ).then(answer => {
            postMessage(answer.data);
        }).catch(error => {
            console.log(error.response.status);
        });
    }, 500);
};