import { Image } from "image-js";

self.onmessage = ({ data }) => {
    const { exchange } = data;
    
    if (exchange === 'rgbjpeg') {
        const { imageBlob } = data;
        Promise.all([
            imageBlob.arrayBuffer().then((imageBuffer) => {
                return Image.load(imageBuffer);
            }),
            exchange
        ]).then((resp) => {
            const [img, exchange] = resp;
            postMessage({ img, exchange });
        });
    } else if (exchange === 'camera1' || exchange === 'camera0') {
        const { imageBlob, depthBlob, cameraData } = data;
        Promise.all([
            imageBlob.arrayBuffer().then((imageBuffer) => {
                return Image.load(imageBuffer);
            }),
            depthBlob.arrayBuffer().then((imageBuffer) => {
                return Image.load(imageBuffer);
            }),
            cameraData.text(),
            exchange
        ]).then((resp) => {
            const [img, depth, camera, exchange] = resp;
            postMessage({ img, depth, cameraData: JSON.parse(camera), exchange });
        });
    }
    
};
