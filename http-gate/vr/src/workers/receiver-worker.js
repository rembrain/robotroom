let ws = null;

async function wsStart(url, ws_package) {
    if (ws) {
        ws.onopen = () => {
            if (ws.readyState === 0) {
                ws.close();
            }
            ws.send(JSON.stringify(ws_package));
        };

        ws.onmessage = async function (ev) {
            const data = ev.data;
            //if (ws_package.exchange !== "rgbjpeg") {
            //    console.log(data)
            //}
            if (typeof data === "string") {
                console.log("Error unpacking video feed:", data);
            } else {
                const dataType = new Uint8Array(
                    await data.slice(0, 1).arrayBuffer()
                )[0];
                if (dataType === 1) {
                    const HEADER_END = 13;
                    const lengths = new Uint32Array(
                        await data.slice(1, HEADER_END).arrayBuffer()
                    );
                    const imageBlob = data.slice(
                        HEADER_END,
                        HEADER_END + lengths[0]
                    );
                    const depthBlob = data.slice(
                        HEADER_END + lengths[0],
                        HEADER_END + lengths[0] + lengths[1]
                    );
                    const cameraData = data.slice(
                        HEADER_END + lengths[0] + lengths[1],
                        HEADER_END + lengths[0] + lengths[1] + lengths[2]
                    );
                    postMessage({
                        imageBlob,
                        depthBlob,
                        cameraData,
                        exchange: ws_package.exchange,
                    });
                    if (ws_package.exchange === "camera0") {
                        ws.close(1000, "new");
                    }
                } else if (dataType === 2) {
                    const HEADER_END = 9;
                    const lengths = new Uint32Array(
                        await data.slice(1, HEADER_END).arrayBuffer()
                    );
                    postMessage({
                        imageBlob: data.slice(
                            HEADER_END,
                            HEADER_END + lengths[0]
                        ),
                        exchange: ws_package.exchange,
                    });
                } else {
                    console.error(`Data type ${dataType} isn't JPG or JPG+PNG`);
                }
            }
        };

        ws.onclose = (ev) => {
            if (ev.reason === "new") {
                ws = null;
            } else {
                setTimeout(() => {
                    ws = new WebSocket(url);
                    wsStart(url, ws_package);
                }, 5000);
            }
        };

        ws.onerror = (err) => {
            console.error({ type: "error", payload: err });
        };
    }
}

self.onmessage = ({ data }) => {
    const { url, ws_package, action } = data;

    switch (ws_package.exchange) {
        case "rgbjpeg":
            if (ws) {
                ws.close(1000, "new");
            }
            setTimeout(() => {
                ws = new WebSocket(url);
                wsStart(url, ws_package);
            }, 1000);
            return;

        case "camera1":
            if (ws) {
                ws.close(1000, "new");
            }
            setTimeout(() => {
                ws = new WebSocket(url);
                wsStart(url, ws_package);
            }, 1000);
            return;

        case "camera0":
            if (action === "init") {
                if (ws) {
                    ws.close(1000, "new");
                }
                return;
            } else if (action === "connect") {
                if (ws) {
                    ws.close(1000, "new");
                }
                setTimeout(() => {
                    ws = new WebSocket(url);
                    wsStart(url, ws_package);
                }, 1000);
                return;
            }
    }
};
