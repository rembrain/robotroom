export let wssUrl = typeof window.WSS_URL == "undefined" 
    ? process.env.WSS_URL
    : window.WSS_URL;;
if (wssUrl.slice(0, 4) === "int/") {
    wssUrl = wssUrl.slice(4, wssUrl.length);
}

export let baseUrl = typeof window.BASE_URL == "undefined" 
    ? process.env.BASE_URL
    : window.BASE_URL;
if (baseUrl.slice(0, 4) === "int/") {
    baseUrl = baseUrl.slice(4, baseUrl.length);
}

export const robot = typeof window.ROBOT == "undefined"
    ? process.env.ROBOT
    : window.ROBOT;

export const headers = {
    Authorization: typeof window.AUTHORIZATION == "undefined"
        ? process.env.AUTHORIZATION
        : window.AUTHORIZATION,
    Refresh: typeof window.REFRESH == "undefined"
        ? process.env.REFRESH
        : window.REFRESH,
};

export const calibration1 = typeof window.CALIBRATION_1 == "undefined" 
    ? JSON.parse(process.env.CALIBRATION_1)
    : JSON.parse(window.CALIBRATION_1);
export const calibration2 = typeof window.CALIBRATION_2 == "undefined" 
    ? JSON.parse(process.env.CALIBRATION_2)
    : JSON.parse(window.CALIBRATION_2);
