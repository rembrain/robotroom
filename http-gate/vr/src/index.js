import axios from "axios";
import "aframe";
import "aframe-gui";

const loadWorker = new Worker(
    new URL("./workers/load-worker.js", import.meta.url)
);
const geometryWorker = new Worker(
    new URL("./workers/geometry-worker.js", import.meta.url)
);
const stateWorker = new Worker(
    new URL("./workers/state-worker.js", import.meta.url)
);
const rgbjpegWorker = new Worker(
    new URL("./workers/receiver-worker.js", import.meta.url)
);
const cameraWorker = new Worker(
    new URL("./workers/receiver-worker.js", import.meta.url)
);

import { wssUrl, robot, headers, baseUrl } from "./endpoints";
import { $mainObjPos } from "./store.js"
import "./UI.js";
import "./controls.js";

let mesh3D = null;
let mesh2D = null;

stateWorker.postMessage({
    url: baseUrl,
    robot: robot,
    headers: headers
});
rgbjpegWorker.postMessage({
    url: wssUrl,
    ws_package: {
        command: "pull",
        exchange: "rgbjpeg",
        access_token: headers.Authorization,
        robot_name: robot,
    },
    action: "connect"
});
cameraWorker.postMessage({
    url: wssUrl,
    ws_package: {
        command: "pull",
        exchange: "camera1",
        access_token: headers.Authorization,
        robot_name: robot,
    },
    action: "connect"
});

stateWorker.onmessage = (ev) => {
    const { arm_state, gripper, state_machine } = ev.data.answer;

    const arm_state_elem = document.getElementById("arm_state")
    if (arm_state === "error") {
        arm_state_elem.setAttribute("color", "red")
    } else {
        arm_state_elem.setAttribute("color", "blue")
    }
    arm_state_elem.setAttribute("value", "arm_state: " + arm_state)


    const gripper_elem = document.getElementById("gripper")
    gripper_elem.setAttribute("value", "gripper: " + gripper)

    const state_machine_elem = document.getElementById("state_machine")
    state_machine_elem.setAttribute("value", "state_machine: " + state_machine)
    //console.log(ev.data);   
};
rgbjpegWorker.onmessage = (ev) => {
    if (mesh2D == null) {
        mesh2D = camera2DScene.getOrCreateObject3D("mesh2D", THREE.Mesh);
    }
    loadWorker.postMessage(ev.data);
};
cameraWorker.onmessage = (ev) => {
    if (mesh3D == null) {
        mesh3D = camera3DScene.getOrCreateObject3D("mesh3D", THREE.Mesh);
    }
    loadWorker.postMessage(ev.data);
};

loadWorker.onmessage = (ev) => {
    const { exchange } = ev.data;
    
    if (exchange === "rgbjpeg") {
        const { img } = ev.data;
        const { data, width, height } = img;
        const geometry2D = new THREE.PlaneGeometry(1.28 * 2, 0.72 * 2);

        const texture = new THREE.DataTexture(
            data,
            width,
            height,
            THREE.RGBAFormat
        );
        const material = new THREE.MeshBasicMaterial({
            side: THREE.DoubleSide,
            map: texture,
        });

        const prev2DM = mesh2D.material;
        mesh2D.material = material;
        prev2DM.dispose();

        const prev2DG = mesh2D.geometry;
        mesh2D.geometry = geometry2D;
        prev2DG.dispose();
    } else if (exchange === "camera1" || exchange === "camera0") {
        geometryWorker.postMessage(ev.data);
    }
};

geometryWorker.onmessage = (ev) => {
    const { positions, img } = ev.data;
    const geometry3D = new THREE.BufferGeometry();
    geometry3D.setAttribute(
        "position",
        new THREE.BufferAttribute(positions[0], 3)
    );
    geometry3D.setAttribute("uv", new THREE.BufferAttribute(positions[1], 2));
    geometry3D.setIndex(new THREE.BufferAttribute(positions[2], 1));

    const { data, width, height } = img;
    const texture = new THREE.DataTexture(
        data,
        width,
        height,
        THREE.RGBAFormat
    );

    const material = new THREE.MeshBasicMaterial({
        side: THREE.DoubleSide,
        map: texture,
    });
    console.log(material);
    const prev3DM = mesh3D.material;
    mesh3D.material = material;
    prev3DM.dispose();

    const prev3DG = mesh3D.geometry;
    mesh3D.geometry = geometry3D;
    prev3DG.dispose();
    
    console.log($mainObjPos.getState())
    camera3DScene.object3D.position.setX($mainObjPos.getState().x);
    camera3DScene.object3D.position.setY($mainObjPos.getState().y);
    camera3DScene.object3D.position.setZ($mainObjPos.getState().z);
};

export { cameraWorker, rgbjpegWorker };
