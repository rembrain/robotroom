import axios from "axios";

import { changeMode } from "./store";
import { $cameraExchange, changeCameraExchange } from "./store"
import { $mainObjPos, changeMainObjPos } from "./store"
import { cameraWorker } from "./index"

import { wssUrl, baseUrl, robot, headers } from "./endpoints";


window.modeFreeButtonAction = function () {
    changeMode("free");
};
window.modePickupButtonAction = function () {
    changeMode("pickup");
};
window.modePickupBoxButtonAction = function () {
    changeMode("pickup_box");
};
window.modeShiftButtonAction = function () {
    changeMode("shift");
}
window.modeMarkupButtonAction = function () {
    changeMode("markup");
};

window.plusXButtonAction = function () {
    let pos = $mainObjPos.getState()
    pos.x += 0.1
    changeMainObjPos(pos)
    localStorage.setItem('camera1X', pos.x);
    const camera3DScene = document.getElementById("camera3DScene")
    camera3DScene.setAttribute("position", pos.x + " " + pos.y + " " + pos.z)
    console.log(camera3DScene.getAttribute("position"))
}
window.minusXButtonAction = function () {
    let pos = $mainObjPos.getState()
    pos.x -= 0.1
    changeMainObjPos(pos)
    localStorage.setItem('camera1X', pos.x);
    const camera3DScene = document.getElementById("camera3DScene")
    camera3DScene.setAttribute("position", pos.x + " " + pos.y + " " + pos.z)
    console.log(camera3DScene.getAttribute("position"))
}

window.camera1ButtonAction = function () {
    cameraWorker.postMessage({
        url: wssUrl,
        ws_package: {
            command: "pull",
            exchange: "camera1",
            access_token: headers.Authorization,
            robot_name: robot,
        },
        action: "connect"
    });
    changeCameraExchange("camera1")
    const stream2D = document.getElementById("camera2DScene")
    stream2D.setAttribute("rotation", "210 0 180")
}

window.camera0ButtonAction = function () {
    cameraWorker.postMessage({
        url: wssUrl,
        ws_package: {
            command: "pull",
            exchange: "camera0",
            access_token: headers.Authorization,
            robot_name: robot,
        },
        action: "init"
    });
    changeCameraExchange("camera0")
    const stream2D = document.getElementById("camera2DScene")
    stream2D.setAttribute("rotation", "210 0 0")
}

window.camera0Crutch = function () {
    cameraWorker.postMessage({
        url: wssUrl,
        ws_package: {
            command: "pull",
            exchange: "camera0",
            access_token: headers.Authorization,
            robot_name: robot,
        },
        action: "connect",
    });
}

window.commandGoHomeButtonAction = function () {
    axios.post(
        baseUrl + "/api/push_command/" + robot,
        {
            command: JSON.stringify({
                op: "go_home_safely"
            }),
        },
        { headers: headers }
    );
}
window.commandAskForIdleButtonAction = function () {
    axios.post(
        baseUrl + "/api/push_command/" + robot,
        {
            command: JSON.stringify({
                op: "ask_for_idle"
            }),
        },
        { headers: headers }
    );
}
window.commandResetStateButtonAction = function () {
    axios.post(
        baseUrl + "/api/push_command/" + robot,
        {
            command: JSON.stringify({
                op: "reset_state"
            }),
        },
        { headers: headers }
    );
}
window.commandAskForMlContinualButtonAction = function () {
    axios.post(
        baseUrl + "/api/push_command/" + robot,
        {
            command: JSON.stringify({
                op: "ask_for_ml_continual"
            }),
        },
        { headers: headers }
    );
}