import "aframe";
import { calibration1, calibration2 } from "./endpoints.js";
import { $cameraExchange, changeCameraExchange } from "./store.js";
import { multiply } from "mathjs";
import { cameraWorker } from "./index"

import { $mainObjPos } from "./store.js"

const Quaternion = require("quaternion");

export function dataInRobot(objectInVR) {
    const mainObjPos = $mainObjPos.getState()

    let position = objectInVR.object3D.getWorldPosition(new THREE.Vector3()).clone()
    position.x -= mainObjPos.x
    position.y -= mainObjPos.y
    position.z -= mainObjPos.z
    let posToRobot, robotRotationMatrix;
    const cameraExchange = $cameraExchange.getState();

    let calibration;
    if (cameraExchange === "camera1") {
        calibration = calibration2;
    } else if (cameraExchange === "camera0") {
        calibration = calibration1;
    }
    const posToCamera = [
        position.x * 1000,
        -position.y * 1000,
        -position.z * 1000,
        1.0,
    ];
    posToRobot = multiply(calibration, posToCamera);
    const calib_r = calibration.map((val) => val.slice(0, -1));
    let worldQ = new THREE.Quaternion();
    objectInVR.object3D.getWorldQuaternion(worldQ);
    const worldRotationMatrix = new Quaternion({
        x: worldQ.x,
        y: -worldQ.y,
        z: -worldQ.z,
        w: worldQ.w,
    }).toMatrix();
    robotRotationMatrix = multiply(calib_r, [
        [ worldRotationMatrix[0], worldRotationMatrix[1], worldRotationMatrix[2], ],
        [ worldRotationMatrix[3], worldRotationMatrix[4], worldRotationMatrix[5], ],
        [ worldRotationMatrix[6], worldRotationMatrix[7], worldRotationMatrix[8], ],
    ]);
    
    return {
        pos: posToRobot, 
        rot: robotRotationMatrix
    };
}

export function getCalibrationMatrix() {
    const cameraExchange = $cameraExchange.getState();

    let calibration;
    if (cameraExchange === "camera1") {
        calibration = calibration2;
    } else if (cameraExchange === "camera0") {
        calibration = calibration1;
    }
    return calibration;
}

export function getCamera0Model() {
    cameraWorker.postMessage({
        url: wssUrl,
        ws_package: {
            command: "pull",
            exchange: "camera0",
            access_token: headers.Authorization,
            robot_name: robot,
        },
        action: "connect"
    });
}