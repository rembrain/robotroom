import axios from "axios";
import { createStore, createEvent } from "effector";
import { calibration2, baseUrl, robot, headers } from "./endpoints.js";


const changeCameraExchange = createEvent((newExchange) => newExchange);
const $cameraExchange = createStore("camera1")
    .on(changeCameraExchange, (_, newExchange) => {
        const btns = {
            camera1: document.getElementById("camera1Button"),
            camera0: document.getElementById("camera0Button"),
        }
        Object.keys(btns).forEach((btn) =>
            btns[btn].removeAttribute("border-color")
        );
        btns[newExchange].setAttribute("border-color", "green");
        
        return newExchange;
    });


const changeMainObjPos = createEvent((newPos) => newPos);
const $mainObjPos = createStore({
    x: localStorage.getItem('camera1X') !== null ? Number(localStorage.getItem('camera1X')) : 0,
    y: localStorage.getItem('camera1Y') !== null ? Number(localStorage.getItem('camera1Y')) : 1.6,
    z: localStorage.getItem('camera1Z') !== null ? Number(localStorage.getItem('camera1Z')) : 0,
}).on(changeMainObjPos, (_, newMode) => {
        return {
            x: newMode.x,
            y: newMode.y,
            z: newMode.z,
        }
    });


const changeMode = createEvent((newMode) => newMode);
const $mode = createStore("pickup_box").on(changeMode, (_, newMode) => {
    const btns = {
        free: document.getElementById("modeFreeButton"),
        pickup: document.getElementById("modePickupButton"),
        pickup_box: document.getElementById("modePickupBoxButton"),
        shift: document.getElementById("modeShift"),
        markup: document.getElementById("modeMarkup"),
    };
    Object.keys(btns).forEach((btn) =>
        btns[btn].removeAttribute("border-color")
    );
    btns[newMode].setAttribute("border-color", "green");

    return newMode;
});

const changePickupData = createEvent((data) => data);
const removePickupData = createEvent();

const $pickupData = createStore([null, null])
    .on(changePickupData, (_, newData) => {
        const posNullEl = _.findIndex((el) => el === null);
        if (posNullEl !== -1) {
            _[posNullEl] = newData;
            const arrow = document.getElementById("arrow");
            const position_arrow = arrow.object3D
                .getWorldPosition(new THREE.Vector3())
                .clone();
            let qu = new THREE.Quaternion();
            const position_qu = arrow.object3D.getWorldQuaternion(qu);
            const arrow_n = document.createElement("a-entity");
            arrow_n.setAttribute("id", "trace");
            arrow_n.setAttribute("obj-model", "obj: #arrow-obj");
            arrow_n.setAttribute(
                "position",
                `${position_arrow.x} ${position_arrow.y} ${position_arrow.z}`
            );
            arrow_n.setAttribute("scale", "0.1 0.15 0.15");
            arrow_n.setAttribute("material", "color: blue");
            arrow_n.object3D.setRotationFromQuaternion(position_qu);

            const scene = document.getElementById("scene");
            scene.append(arrow_n);
        } else {
            while (
                typeof document.getElementById("trace") != "undefined" &&
                document.getElementById("trace")
            ) {
                document.getElementById("trace").remove();
            }
            let op_command
            if ($mode.getState() === "pickup") {
                op_command = "pickup_putdown"
            } else if ($mode.getState() === "shift") {
                op_command = "shift"
            }
            axios.post(
                baseUrl + "/api/push_command/" + robot,
                {
                    command: JSON.stringify({
                        op: op_command,
                        pos1: _[0].pos,
                        pos2: _[1].pos,
                        rot1: _[0].rot,
                        rot2: _[1].rot,
                        calibration: calibration2,
                        source: "operator",
                    }),
                },
                { headers: headers }
            );
            _ = [null, null];
        }
        return _;
    })
    .on(removePickupData, (_, newData) => {
        _ = [null, null];
        while (
            typeof document.getElementById("trace") != "undefined" &&
            document.getElementById("trace")
        ) {
            document.getElementById("trace").remove();
        }
        return _;
    });

const changePaint = createEvent((newMode) => newMode);
const $paint = createStore(false).on(changePaint, (_, newMode) => newMode);

const addPaintPoint = createEvent((data) => data);
const finalPaintPoint = createEvent((data) => data);
const finalVolume = createEvent((data) => data);
const preFinalVolume = createEvent((data) => data);
const removeAll = createEvent((data) => data);
const $finalPoint = createStore(null)
    .on(addPaintPoint, (_, newData) => {
        return newData;
    })
    .on(removeAll, (_, newData) => {
        return null;
    });
const addOneCapturePoint = createEvent((data) => data);
const addTwoCapturePoint = createEvent((data) => data);
const $capturePoint = createStore({ one: null, two: null })
    .on(addOneCapturePoint, (_, newData) => {
        _.one = newData;
        return _;
    })
    .on(addTwoCapturePoint, (_, newData) => {
        _.two = newData;
        return _;
    })
    .on(removeAll, (_, newData) => {
        return { one: null, two: null };
    });
const $paintPoints = createStore([[null], [null]])
    .on(addPaintPoint, (_, newData) => {
        const posNullEl = _[0].findIndex((el) => el === null);
        if (posNullEl !== -1) {
            _[0][posNullEl] = newData;
            _[0].push(null);
            const point = document.getElementById("box");
            const position_point = point.object3D
                .getWorldPosition(new THREE.Vector3())
                .clone();
            const addPoint = document.createElement("a-sphere");
            addPoint.setAttribute("id", "paintPoints");
            addPoint.setAttribute(
                "position",
                `${position_point.x} ${position_point.y} ${position_point.z}`
            );

            addPoint.setAttribute("radius", "0.007");
            addPoint.setAttribute("material", "color: green");
            document.getElementById("scene").append(addPoint);
        }
        return _;
    })
    .on(finalPaintPoint, (_, newData) => {
        const posNullEl = _[0].findIndex((el) => el === null);
        if (posNullEl !== -1) {
            _[0][posNullEl] = newData;
        }
        return _;
    })
    .on(finalVolume, (_, newData) => {
        const offsetX = newData.x - _[0][0].x;
        const offsetY = newData.y - _[0][0].y;
        const offsetZ = newData.z - _[0][0].z;
        while (
            typeof document.getElementById("downPaintPoints") != "undefined" &&
            document.getElementById("downPaintPoints")
        ) {
            document.getElementById("downPaintPoints").remove();
        }
        for (let i = 0; i < _[0].length; ++i) {
            let point = _[0][i];

            point = {
                x: point.x + offsetX,
                y: point.y + offsetY,
                z: point.z + offsetZ,
            };
            if (_[1][i] === null) {
                _[1].pop();
                _[1].push(point);
            } else {
                _[1].push(point);
            }

            const addPoint = document.createElement("a-sphere");
            addPoint.setAttribute("id", "paintPoints");
            addPoint.setAttribute(
                "position",
                `${point.x} ${point.y} ${point.z}`
            );
            addPoint.setAttribute("radius", "0.007");
            addPoint.setAttribute("material", "color: green");
            document.getElementById("scene").append(addPoint);
        }
    })
    .on(preFinalVolume, (_, newData) => {
        for (let i = 0; i < _[0].length; ++i) {
            let zeroPoint = _[0][0];
            let point = _[0][i];

            let newPoint = {
                x: point.x - zeroPoint.x,
                y: point.y - zeroPoint.y,
                z: point.z - zeroPoint.z,
            };

            const addPoint = document.createElement("a-sphere");
            addPoint.setAttribute("id", "downPaintPoints");
            addPoint.setAttribute(
                "position",
                `${newPoint.x} ${newPoint.y} ${newPoint.z}`
            );
            addPoint.setAttribute("radius", "0.007");
            addPoint.setAttribute("material", "color: green");
            document.getElementById("box").append(addPoint);
        }
    })
    .on(removeAll, (_, newData) => {
        return [[null], [null]];
    });

const addMarkupPoint = createEvent((data) => data);
const removeMarkupPoint = createEvent((data) => data);
const $markupPoints = createStore([])
    .on(addMarkupPoint, (_, newData) => {
        _.push(newData);
        return _;
    })
    .on(removeMarkupPoint, (_, newData) => {
        return [];
    });

export {
    $mode,
    changeMode,
    $pickupData,
    changePickupData,
    removePickupData,
    $paint,
    changePaint,
    $paintPoints,
    addPaintPoint,
    finalPaintPoint,
    $finalPoint,
    finalVolume,
    $capturePoint,
    addOneCapturePoint,
    addTwoCapturePoint,
    removeAll,
    $markupPoints,
    addMarkupPoint,
    removeMarkupPoint,
    preFinalVolume,
    $mainObjPos,
    changeMainObjPos,
    $cameraExchange,
    changeCameraExchange
};
