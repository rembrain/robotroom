/// <reference types="cypress" />

describe('Robot subscriptions tests', () => {
    let robot
    beforeEach(() => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('#RobotSubscription').click()
    })

    it('subscribe to robot', () => {
        cy.wait(100)
        cy.get('.rs-modal-content').should('exist')
        cy.get('.rs-modal-content > :nth-child(3) > :first-child').click()
        cy.get('.rs-modal-content > :nth-child(3) > :first-child').then(
            (resp) => {
                robot = resp[0].outerText
                cy.get('.selected-robot-all-item').should('have.text', robot)
            }
        )
        cy.wait(50)
        cy.get('.notifications-modal-btn-container > .green-button').click()
        cy.get('.rs-modal-content').should('not.exist')

        cy.get('#RobotSubscription').click()

        cy.get('.rs-modal-content > :nth-child(3)').then((resp) => {
            expect(resp[0].outerText.split('\n')).not.to.include(robot)
        })
        cy.get('.rs-modal-content > :nth-child(5)').then((resp) => {
            expect(resp[0].outerText.split('\n')).to.include(robot)
        })
        cy.wait(100)
    })

    it('unsubscribe from robot', () => {
        cy.wait(100)
        cy.get('.notification-robot-item').contains(robot).click()
        cy.wait(50)
        cy.get('.selected-robot-watch-item').should('exist')
        cy.get('.selected-robot-watch-item').should('have.text', robot)

        cy.get('.notifications-modal-btn-container > .green-button').click()
        cy.get('.rs-modal-content').should('not.exist')

        cy.get('#RobotSubscription').click()

        cy.get('.rs-modal-content > :nth-child(3)').then((resp) => {
            expect(resp[0].outerText.split('\n')).to.include(robot)
        })
        cy.get('.rs-modal-content > :nth-child(5)').then((resp) => {
            expect(resp[0].outerText.split('\n')).not.to.include(robot)
        })
        cy.wait(100)
    })
})
