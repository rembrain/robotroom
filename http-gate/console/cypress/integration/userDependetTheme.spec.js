/// <reference types="cypress" />

import aiveroUser from '../fixtures/aiveroThemeUser.json'

describe('User dependent theme tests', () => {
    beforeEach(() => {
        cy.bypassLogin()
    })

    it('Can create non-aivero user, log in and see rembrain styled console', () => {
        cy.visit('http://localhost:3000')
        cy.get('.header-rem').should('exist')
        cy.get('.header-brain').should('exist')
    })

    it('Can create aivero user, log in and see aivero styled console', () => {
        cy.intercept('GET', '**/current_user', aiveroUser)
        cy.visit('http://localhost:3000')

        cy.wait(1000)

        cy.get('.header-rem').should('not.exist')
        cy.get('.header-brain').should('not.exist')
        cy.get('.header-img-logo').should('exist')
    })
})
