describe('Retraining session tests', () => {
    let dataSessionId, retrainingSessionID
    const sessionName = chance.word({ syllables: 5 })
    const dataSessionTwo = chance.word({ syllables: 5 })
    const dataSessionThree = chance.word({ syllables: 5 })

    beforeEach(() => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('#NavRetraining').click()
        cy.get(
            '.rs-dropdown-expand > .rs-dropdown-menu > .rs-nav > ul > :nth-child(2) > .rs-nav-item-content'
        ).click()
    })

    it('create retrainig session', () => {
        cy.get('.header-content-wrapper > div > .rs-btn').click()
        cy.wait(500)
        cy.get('[aria-rowindex="2"] >').then((resp) => {
            dataSessionId = resp[1].outerText
        })

        cy.get(
            '[aria-rowindex="2"] > .rs-table-cell-group > .rs-table-cell-first > .rs-table-cell-content > .table-cell-wrapper > .rs-btn > .rs-icon'
        ).click()

        cy.get('.table-cell-wrapper > input').type(0.1)
        cy.get('.rs-modal-footer').click()
        cy.get('.rs-modal-footer > .green-button').click()
        cy.get('#CreateRetrainingSessionName').type(sessionName)

        cy.get('.rs-modal-footer > :nth-child(1)').click()

        cy.get('.table-tabs-wrapper > :nth-child(2) > :nth-child(2)').click()
        cy.get('#SessionCreateIdInput').type(dataSessionTwo)
        cy.get('.rs-input-group > .rs-btn').click()
        cy.get('.table-cell-wrapper > input').type(0.2)

        cy.get('.rs-modal-footer').click()
        cy.get('.rs-modal-footer > .green-button').click()
        cy.get('.rs-modal-footer > .green-button').click()

        cy.wait(2000)

        cy.get('.CodeMirror-lines').then((lines) => {
            const sessionConfig = JSON.parse(
                lines[0].outerText
                    .replace(/\n[0-9]+\n/g, '')
                    .replace(/[\s|\n]/g, '')
                    .replace('1', '')
            )

            const { name, sessions, training_session } = sessionConfig

            retrainingSessionID = training_session

            expect(name).to.equal(sessionName)

            expect(sessions[0].__var.id).to.equal(dataSessionId)
            expect(sessions[1].__var.id).to.equal(dataSessionTwo)

            expect(sessions[0].__var.weight).to.equal(0.1)
            expect(sessions[1].__var.weight).to.equal(0.2)
        })
    })

    it('find created session in session list', () => {
        cy.get('.rs-table-body-wheel-area >').then((rows) => {
            let createdSessionIdx = null
            for (let i = 0; i < rows.length; i++) {
                if (rows[i].outerText.split('\n')[0] === sessionName) {
                    createdSessionIdx = i
                    break
                }
            }
            expect(createdSessionIdx).to.not.equal(null)
            cy.get(
                `[aria-rowindex="${
                    2 + createdSessionIdx
                }"] > .rs-table-cell-group > .rs-table-cell-first > .rs-table-cell-content > .no-text-decor > .table-cell-wrapper`
            ).click()
            cy.get('.rs-modal-footer > :nth-child(2)').click()
            cy.get(
                '.table-tabs-wrapper > :nth-child(2) > :nth-child(2)'
            ).click()
            cy.get('#SessionCreateIdInput').type(dataSessionThree)
            cy.get('.rs-input-group > .rs-btn').click()
            cy.get(
                '[aria-rowindex="3"] > .rs-table-cell-group > .rs-table-cell-last > .rs-table-cell-content > .table-cell-wrapper > input'
            ).type(0.3)

            cy.get('.rs-modal-footer').click()
            cy.get('.rs-modal-footer > .green-button').click()
            cy.get('.rs-modal-footer > .green-button').click()

            cy.wait(4000)

            cy.get('.CodeMirror-lines').then((lines) => {
                console.log(
                    lines[0].outerText
                        .replace(/\n[0-9]+\n/g, '')
                        .replace(/[\s|\n]/g, '')
                        .replace('1', '')
                )
                const sessionConfig = JSON.parse(
                    lines[0].outerText
                        .replace(/\n[0-9]+\n/g, '')
                        .replace(/[\s|\n]/g, '')
                        .replace('1', '')
                )

                const { name, sessions } = sessionConfig

                expect(name).to.equal(sessionName)

                expect(sessions[2].__var.id).to.equal(dataSessionThree)
                expect(sessions[2].__var.weight).to.equal(0.3)
            })
        })
    })

    it('delete retraining session', () => {
        cy.get('.rs-table-body-wheel-area >').then((rows) => {
            let createdSessionIdx = null
            for (let i = 0; i < rows.length; i++) {
                if (rows[i].outerText.split('\n')[0] === sessionName) {
                    createdSessionIdx = i
                    break
                }
            }
            expect(createdSessionIdx).to.not.equal(null)

            cy.get(
                `[aria-rowindex="${
                    2 + createdSessionIdx
                }"] > .rs-table-cell-group > .rs-table-cell-last > .rs-table-cell-content > .table-cell-wrapper > .rs-btn > .rs-icon`
            ).click()
            cy.get('.red-button').click()

            cy.wait(1000)

            cy.get('.rs-table-body-wheel-area').should(
                'not.contain',
                sessionName
            )
            cy.get('.rs-table-body-wheel-area').should(
                'not.contain',
                retrainingSessionID
            )
        })
    })
})
