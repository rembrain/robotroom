/// <reference types="cypress" />

import taskList from '../fixtures/taskList.json'
import Chance from 'chance'
import { randomJSON } from '../support/commands'

describe('Task tests with mocked list', () => {
    beforeEach(() => {
        cy.intercept('GET', '**/tasks', taskList)
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('#NavTasks').click()
    })

    it('can be sorted by task name', () => {
        cy.get('.task-controls > :nth-child(2)').click()
        cy.get('.task-list-wrapper > :first-child > .task-item-header').should(
            'have.text',
            'zzz (vasyutka@mail.ru)'
        )
        cy.get('.task-controls > :nth-child(2)').click()
        cy.get('.task-list-wrapper > :first-child > .task-item-header').should(
            'have.text',
            'aaa (vasyutka@mail.ru)'
        )
    })

    it('can be sorted by task author', () => {
        cy.get('.task-controls > :nth-child(3)').click()

        cy.get('.task-list-wrapper > :first-child > .task-item-header').should(
            'have.text',
            'task_bin (zzz)'
        )
        cy.get('.task-controls > :nth-child(3)').click()
        cy.get('.task-list-wrapper > :first-child > .task-item-header').should(
            'have.text',
            'test4 (aaa)'
        )
    })

    it('can be filtered by name/author', () => {
        cy.get('.rs-input').type('abc')
        cy.get('.task-list-wrapper').children().should('have.length', 4)
        cy.get('.rs-input').type('d')
        cy.get('.task-list-wrapper').children().should('have.length', 2)
    })
})

describe('Task tests', () => {
    const chance = new Chance()
    const taskName = chance.word({ syllables: 5 })
    const taskAuthor = chance.word({ syllables: 5 })
    const taskID = `#${taskName}_${taskAuthor}`

    beforeEach(() => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('#NavTasks').click()
    })

    it('create tasks', () => {
        cy.get('.task-controls > .rs-btn').click()
        cy.get('#createTaskNameInput').type(taskName)
        cy.get('#createTaskAuthorInput').type(taskAuthor)
        cy.get('.rs-modal-footer > .green-button').click()
        cy.wait(1000)
        cy.get(taskID).should('exist')
        cy.get(taskID + ' > .json-input-textarea').should('contain', '{}')
    })

    it('edit task', () => {
        cy.get('.json-editor-error').should('not.exist')
        const json = randomJSON()
        const codeMirrorSelector =
            '[style="padding: 0px; margin: 0px;"] > .json-input-textarea > .react-codemirror2 > .CodeMirror > .CodeMirror-scroll > .CodeMirror-sizer > [style="position: relative; top: 0px;"] > .CodeMirror-lines'
        cy.get(taskID + ' > .task-item-footer > .green-button').click()

        cy.wait(500)

        cy.get(codeMirrorSelector).type('{backspace}').type('{backspace}')
        cy.wait(100)
        cy.get('.json-editor-error').should('exist')

        cy.get(codeMirrorSelector).type(json)
        cy.get('.json-editor-error').should('not.exist')

        cy.get('.rs-modal-footer > .green-button').click()

        cy.wait(500)

        cy.get(taskID).then((resp) => {
            const text = resp[0].outerText.replace(/\n[0-9]+\n/g, '')
            const header = text.split('\n')[0]
            let config = text.replace(header, '').replace(/[\s|\n]/g, '')
            expect(JSON.parse(config)).to.deep.equal(JSON.parse(json))
            expect(header).to.equal(`${taskName} (${taskAuthor})`)
        })
    })

    it('delete tasks', () => {
        cy.get(taskID).should('exist')
        cy.get(taskID + ' > .task-item-footer > .red-button').click()
        cy.get(':nth-child(2) > .red-button').click()
        cy.get(taskID).should('not.exist')
    })
})
