/// <reference types="cypress" />

describe('Authorization tests', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })

    it('invalid credentials login', () => {
        cy.get('.auth-container').should('exist')
        cy.get(':nth-child(1) > .auth-inputs').type('invalid_username')
        cy.get(':nth-child(2) > .auth-inputs').type('invalid_password')
        cy.get('.rs-btn').click()
        cy.get('.rs-sidebar').should('not.exist')
        cy.get('.auth-error-message').should('exist')
    })

    it('correct credentials login', () => {
        cy.get('.auth-container').should('exist')
        cy.get(':nth-child(1) > .auth-inputs').type(Cypress.env('username'))
        cy.get(':nth-child(2) > .auth-inputs').type(Cypress.env('password'))
        cy.get('.rs-btn').click()
        cy.wait(500)
        cy.get('.rs-sidebar').should('exist')
    })

    it('got corret userdata', () => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('.header-container > .rs-btn').should(
            'have.text',
            Cypress.env('email')
        )
        cy.get('.header-container > .rs-btn').click()
        cy.get('.rs-modal-body > :nth-child(1) > p').should(
            'have.text',
            'Email: ' + Cypress.env('email')
        )
    })

    it('log out', () => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('.header-container > .rs-btn').click()
        cy.get('.red-button').click()
        cy.wait(200)
        cy.get('.rs-sidebar').should('not.exist')
        cy.get('.auth-container').should('exist')
    })
})
