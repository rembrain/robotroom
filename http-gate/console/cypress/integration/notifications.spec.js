import currentUser from '../fixtures/currentUser.json'

it('no notifications if robots are ok', () => {
    cy.intercept('GET', '**/current_user', currentUser)
    cy.wait(3000)
    cy.get('.rs-notification-item-content').should('not.exist')
})

describe('notifications test', () => {
    beforeEach(() => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        let { answer } = currentUser
        answer.subscription_robot[0] = {
            ...answer.subscription_robot[0],
            state: 'NEED_OPERATOR',
        }
        cy.intercept('GET', '**/current_user', { answer })
    })

    it('notifications if robot with NEED_OPERATOR state exists', () => {
        cy.get('.rs-notification-item-content').should(
            'contain.text',
            'aivero_robot1'
        )
    })

    it('notification routes user to web operators', () => {
        cy.get('.notification-btn-cont > .rs-btn').click()
        cy.get('.web-operator-container').should('exist')
        cy.get('.wo-backdrop').should('exist')
    })

    it('dismissed notification will not appear in 30s', () => {
        cy.get('.rs-notification-item-content').should('exist')
        cy.get('.rs-notification-item-close-x').click()
        cy.get('.rs-notification-item-content').should('not.exist')
        cy.wait(30000)
        cy.get('.rs-notification-item-content').should('exist')
        cy.get('.rs-notification-item-content').should(
            'contain.text',
            'aivero_robot1'
        )
    })
})
