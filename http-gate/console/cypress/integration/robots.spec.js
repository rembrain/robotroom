import { randomJSON } from '../support/commands'
import Chance from 'chance'
const chance = new Chance()

describe('robot list tests', () => {
    const robot = chance.word({ syllables: 5 })
    const password = chance.word({ syllables: 5 })
    const robotConfig = randomJSON()
    const commandText = JSON.stringify(
        {
            op: 'testtesttest',
            use_source: true,
        },
        false,
        2
    )

    beforeEach(() => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('#NavRobots').click()
    })

    it('create robot', () => {
        cy.get('.header-content-wrapper > div > .rs-btn').click()
        cy.get('#createRobotName').type(robot)
        cy.get('#createRobotPassword').type(password)
        cy.get('.rs-modal-footer > .green-button').click()

        cy.wait(500)

        cy.get('.rs-table-body-wheel-area').then((elements) => {
            expect(elements[0].outerText.split('\n')).to.include(robot)
        })
    })

    it('can show robot details', () => {
        cy.get('.rs-input').type(robot)
        cy.get(
            '[aria-rowindex="2"] > .rs-table-cell-group > .rs-table-cell-first > .rs-table-cell-content > .no-text-decor > .table-cell-wrapper'
        ).click()

        cy.wait(500)

        cy.get('.rs-panel-title > h2').should('have.text', robot)
        cy.get('span > .breadcrumb-item').should('have.text', robot)
    })

    it('set config', () => {
        cy.visit('http://localhost:3000/robots/' + robot)
        cy.wait(500)

        const configMirror =
            '.robot-details-column > .robot-config-wrapper > .json-input-textarea > .react-codemirror2 > .CodeMirror > .CodeMirror-scroll > .CodeMirror-sizer > [style="position: relative; top: 0px;"] > .CodeMirror-lines'

        cy.get(configMirror)
            .type('{backspace}')
            .type('{backspace}')
            .type(robotConfig)
        cy.get(configMirror).then((resp) => {
            expect(
                JSON.parse(
                    resp[0].outerText
                        .replace(/\n[0-9]+\n/g, '')
                        .replace(/\s+/g, '')
                )
            ).to.deep.equal(JSON.parse(robotConfig))
        })
        cy.get('.robot-details-column > :nth-child(4)').click()
        cy.wait(500)
        cy.get(configMirror).then((resp) => {
            expect(
                resp[0].outerText.replace(/\n[0-9]+\n/g, '').replace(/\s+/g, '')
            ).to.deep.equal('1{}')
        })

        cy.get(configMirror)
            .type('{backspace}')
            .type('{backspace}')
            .type(robotConfig)

        cy.get('.robot-details-column > :nth-child(3)').click()

        cy.visit('http://localhost:3000/robots/' + robot)
        cy.wait(500)

        cy.get(configMirror).then((resp) => {
            expect(
                JSON.parse(
                    resp[0].outerText
                        .replace(/\n[0-9]+\n/g, '')
                        .replace(/\s+/g, '')
                        .replace('1', '')
                )
            ).to.deep.equal(JSON.parse(robotConfig))
        })
    })

    it('push command', () => {
        cy.visit('http://localhost:3000/robots/' + robot)
        cy.wait(500)

        const commandMirror =
            '[style="margin-top: 10px;"] > .robot-config-wrapper > .json-input-textarea > .react-codemirror2 > .CodeMirror > .CodeMirror-scroll > .CodeMirror-sizer > [style="position: relative; top: 0px;"] > .CodeMirror-lines'
        //this is horrible, blasphemous child of how codeMirror and cypress interracts
        cy.get(commandMirror)
            .type('{backspace}'.repeat(20))
            .type('{backspace}'.repeat(20))
            .type('{backspace}'.repeat(10))
            .type(commandText)

        cy.get('[style="margin-top: 10px;"] > .rs-btn').click()
        cy.wait(500)
        cy.get('.rs-notification-item-content').should(
            'have.text',
            'Command pushed successfully.'
        )
    })

    it('delete robot', () => {
        cy.get('.rs-input').type(robot)
        cy.get(
            '[aria-rowindex="2"] > .rs-table-cell-group > .rs-table-cell-last > .rs-table-cell-content > .table-cell-wrapper > .rs-btn'
        ).click()
        cy.get('.red-button').click()

        cy.wait(500)

        cy.get('.rs-table-body-wheel-area').then((elements) => {
            expect(elements[0].outerText.split('\n')).to.not.include(robot)
        })
    })
})
