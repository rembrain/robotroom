/// <reference types="cypress" />

import axios from 'axios'
import Chance from 'chance'
const chance = new Chance()

describe('User managment tests', () => {
    const username = chance.word({ syllables: 5 })
    const password = chance.word({ syllables: 5 })
    const newPassword = chance.word({ syllables: 5 })
    const firstName = chance.word({ syllables: 5 })
    const lastName = chance.word({ syllables: 5 })
    const email = chance.email()

    beforeEach(() => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('#NavUsers').click()
    })

    it('create user', () => {
        cy.get('.header-content-wrapper > .rs-btn').click()

        cy.get('#createUserUsername').type(username)
        cy.get('#createUserPassword').type(password)
        cy.get('#createUserEmail').type(email)
        cy.get('#createUserRole')

        cy.get('.rs-modal-footer > .green-button').click()

        cy.wait(1000)

        cy.get('.rs-table-cell-group').then((resp) => {
            let userRowIdx = null
            for (let i = 0; i < resp.length; i++) {
                const attrList = resp[i].outerText.split('\n')
                if (attrList.includes(username) && attrList.includes(email)) {
                    userRowIdx = i
                    break
                }
            }
            expect(userRowIdx).to.not.equal(null)
        })
    })

    it('change regular user password', async () => {
        await axios
            .post(Cypress.env('http_gate') + '/login', { username, password })
            .then(
                (resp) => {
                    expect(resp.status).to.equal(200)
                },
                () => {
                    throw new Error('Failed to use password to login')
                }
            )

        cy.get('.rs-table-cell-group').then((resp) => {
            let userRowIdx = null
            for (let i = 0; i < resp.length; i++) {
                const attrList = resp[i].outerText.split('\n')
                if (attrList.includes(username) && attrList.includes(email)) {
                    userRowIdx = i
                    break
                }
            }
            expect(userRowIdx).to.not.equal(null)
            const userRow = `[aria-rowindex="${
                userRowIdx + 2
            }"] > .rs-table-cell-group > .rs-table-cell-first > .rs-table-cell-content > .no-text-decor > .table-cell-wrapper`

            cy.get(userRow).click({ scrollBehavior: 'center' })

            cy.get('#editUserChangePasswordBtn').click()
            cy.get('newPasswordInput').type(newPassword)
            cy.get('oldPasswordInput').type(password)
            cy.get(
                ':nth-child(9) > .rs-modal > .rs-modal-dialog > .rs-modal-content > .rs-modal-footer > .green-button'
            )
                .click()
                .then(() => {
                    axios
                        .post(Cypress.env('http_gate') + '/login', {
                            username,
                            password,
                        })
                        .then(
                            () => {
                                throw new Error('Old password still works')
                            },
                            () => {
                                axios
                                    .post(Cypress.env('http_gate') + '/login', {
                                        username,
                                        newPassword,
                                    })
                                    .then((resp) => {
                                        expect(resp.status).to.equal(200)
                                    })
                            }
                        )
                })
        })
    })

    it('edit user', () => {
        let editedUserRowIdx = null
        let userRowIdx = null

        cy.get('.rs-table-cell-group').then((el) => {
            for (let i = 0; i < el.length; i++) {
                const attrList = el[i].outerText.split('\n')
                if (attrList.includes(username) && attrList.includes(email)) {
                    userRowIdx = i
                    break
                }
            }

            expect(userRowIdx).to.not.equal(null)
            const userRow = `[aria-rowindex="${
                userRowIdx + 2
            }"] > .rs-table-cell-group > .rs-table-cell-first > .rs-table-cell-content > .no-text-decor > .table-cell-wrapper`

            cy.get(userRow).click({ scrollBehavior: 'center' })

            cy.get('#userEditConfirm').click()
            cy.get('#userEditFirstName').type(firstName)
            cy.get('#userEditLastName').type(lastName)
            cy.get('#userEditConfirm').click()

            cy.wait(1000)

            cy.get('.rs-table-cell-group').then((el) => {
                for (let i = 0; i < el.length; i++) {
                    const attrList = el[i].outerText.split('\n')
                    if (
                        attrList.includes(username) &&
                        attrList.includes(email) &&
                        attrList.includes(firstName) &&
                        attrList.includes(lastName)
                    ) {
                        editedUserRowIdx = i
                        break
                    }
                }
                expect(editedUserRowIdx).to.not.equal(null)
            })
        })
    })

    it('cannot change admin user password', () => {
        let editedUserRowIdx = null
        let userRowIdx = null

        cy.get('.rs-table-cell-group').then((el) => {
            for (let i = 0; i < el.length; i++) {
                const attrList = el[i].outerText.split('\n')
                if (attrList.includes(username) && attrList.includes(email)) {
                    userRowIdx = i
                    break
                }
            }

            expect(userRowIdx).to.not.equal(null)
            const userRow = `[aria-rowindex="${
                userRowIdx + 2
            }"] > .rs-table-cell-group > .rs-table-cell-first > .rs-table-cell-content > .no-text-decor > .table-cell-wrapper`

            cy.get(userRow).click({ scrollBehavior: 'center' })

            cy.get('#editUserChangePasswordBtn').should('exist')

            cy.get('#userEditConfirm').click()
            cy.get('#userEditRole').click()
            cy.get('[data-key="admin"] > .rs-picker-select-menu-item').click()
            cy.get('#userEditConfirm').click()

            cy.wait(1000)

            cy.get('.rs-table-cell-group').then((el) => {
                for (let i = 0; i < el.length; i++) {
                    const attrList = el[i].outerText.split('\n')
                    if (
                        attrList.includes(username) &&
                        attrList.includes(email) &&
                        attrList.includes(firstName) &&
                        attrList.includes(lastName)
                    ) {
                        editedUserRowIdx = i
                        break
                    }
                }
                const editedUserRow = `[aria-rowindex="${
                    editedUserRowIdx + 2
                }"] > .rs-table-cell-group > .rs-table-cell-first > .rs-table-cell-content > .no-text-decor > .table-cell-wrapper`
                cy.get(editedUserRow).click({ scrollBehavior: 'center' })
                cy.get('#editUserChangePasswordBtn').should('not.exist')
            })
        })
    })

    it('change current user password', () => {
        cy.get('.header-container > .rs-btn').click()
        cy.get('.red-button').click()
        cy.wait(1000)
        cy.bypassLogin(username, password)
        cy.visit('http://localhost:3000')
        cy.wait(10000)
    })

    it('delete user', () => {
        cy.get('.rs-table-cell-group').then((resp) => {
            let userRowIdx = null

            for (let i = 0; i < resp.length; i++) {
                const attrList = resp[i].outerText.split('\n')
                if (attrList.includes(username) && attrList.includes(email)) {
                    userRowIdx = i
                    break
                }
            }
            expect(userRowIdx).to.not.equal(null)

            const deleteBtn = `[aria-rowindex="${
                userRowIdx + 2
            }"] > .rs-table-cell-group > .rs-table-cell-last > .rs-table-cell-content > .table-cell-wrapper > .rs-btn`

            cy.get(deleteBtn).click({ scrollBehavior: 'center' })

            cy.get('.red-button').click()

            cy.wait(1000)

            cy.get('.rs-table-cell-group').then((resp) => {
                let deletedUserIdx = null
                for (let i = 0; i < resp.length; i++) {
                    const attrList = resp[i].outerText.split('\n')
                    if (
                        attrList.includes(username) &&
                        attrList.includes(email)
                    ) {
                        deletedUserIdx = i
                        break
                    }
                }
                expect(deletedUserIdx).to.equal(null)
            })
        })
    })
})
