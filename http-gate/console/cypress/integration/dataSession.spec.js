/// <reference types="cypress" />

const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
]

describe('Data session tests', () => {
    let datetimeMax, datetimeMin
    const sessionName = chance.word({ syllables: 5 })
    let sessionCommand

    beforeEach(() => {
        cy.bypassLogin()
        cy.visit('http://localhost:3000')
        cy.get('#NavRetraining').click()
        cy.get(
            '.rs-dropdown-expand > .rs-dropdown-menu > .rs-nav > ul > :nth-child(1) > .rs-nav-item-content'
        ).click()
        cy.wait(500)
    })

    it('list files by robot', () => {
        let robot

        cy.get('.dropdown-toggle').click()
        cy.get(':nth-child(1) > .rs-dropdown-item-content > p')
            .click()
            .then((el) => {
                robot = el[0].outerText
                cy.get(
                    '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area >'
                ).then((el) => {
                    for (let i = 0; i < el.length; i++) {
                        expect(el[i].outerText.split('\n')[0]).to.equal(robot)
                    }
                })
            })
    })

    it('sort by datetime', () => {
        cy.get('.dropdown-toggle').click()
        cy.get(':nth-child(1) > .rs-dropdown-item-content > p').click()
        cy.get(
            '.non-selectable.rs-table-cell-header-sortable > .rs-table-cell > .rs-table-cell-content'
        ).click({ scrollBehavior: 'center' })

        cy.wait(1000)

        cy.get(
            '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area > [aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="2"] > .rs-table-cell-content > .table-cell-wrapper'
        ).then((el) => {
            datetimeMin = new Date(
                ...el[0].outerText
                    .replace(':', '')
                    .replace(/\s/g, '-')
                    .split('-')
                    .map((el) => (!isNaN(el) ? el : monthNames.indexOf(el)))
            )

            cy.get(
                '.non-selectable.rs-table-cell-header-sortable > .rs-table-cell > .rs-table-cell-content'
            ).click({ scrollBehavior: 'center' })

            cy.wait(1000)

            cy.get(
                '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area > [aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="2"] > .rs-table-cell-content > .table-cell-wrapper'
            ).then((el) => {
                datetimeMax = new Date(
                    ...el[0].outerText
                        .replace(':', '')
                        .replace(/\s/g, '-')
                        .split('-')
                        .map((el) => (!isNaN(el) ? el : monthNames.indexOf(el)))
                )
                expect(datetimeMin.getTime() <= datetimeMax.getTime()).to.equal(
                    true
                )
            })
        })
    })

    it('sort by command', () => {
        cy.get('.dropdown-toggle').click()
        cy.get(':nth-child(1) > .rs-dropdown-item-content > p').click()

        let firstCommand, lastCommand

        cy.get(
            '.selectable-column > .rs-table-cell > .rs-table-cell-content'
        ).click({ scrollBehavior: 'center' })

        cy.wait(500)

        cy.get(
            '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area > [aria-rowindex="3"] > .rs-table-cell-group > [aria-colindex="2"] > .rs-table-cell-content > .table-cell-wrapper'
        ).then((el) => {
            firstCommand = el[0].outerText

            cy.get(
                '.selectable-column > .rs-table-cell > .rs-table-cell-content'
            ).click({ scrollBehavior: 'center' })

            cy.wait(500)

            cy.get(
                '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area > [aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="3"] > .rs-table-cell-content > .table-cell-wrapper'
            ).then((el) => {
                lastCommand = el[0].outerText
                expect(lastCommand.localeCompare(firstCommand)).to.equal(1)
            })
        })
    })

    it('filter by commands', () => {
        cy.get('.dropdown-toggle').click()
        cy.get(':nth-child(1) > .rs-dropdown-item-content > p').click()

        cy.get('.data-table-tag-picker').click({
            scrollBehavior: 'center',
        })

        cy.get(
            '.rs-checkbox > .rs-checkbox-checker > label > .tag-picker-item'
        ).then((elements) => {
            let commandList = []
            for (let i = 0; i < 3; i++) {
                if (i > 0) {
                    cy.get('.data-table-tag-picker').click({
                        scrollBehavior: 'center',
                    })
                }
                commandList.push(elements[i].outerText)
                let commandItem = `[data-key="${elements[i].outerText}"] > .rs-checkbox > .rs-checkbox-checker > label > .rs-checkbox-wrapper`
                cy.get(commandItem).click({
                    scrollBehavior: 'center',
                })

                cy.get('.header-brain').click()

                cy.get('.rs-table-cell-group').then((resp) => console.log(resp))

                cy.get(
                    '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area >'
                ).then((elements) => {
                    for (let i = 0; i < elements.length; i++) {
                        expect(commandList).to.contain(
                            elements[i].outerText.split('\n')[2]
                        )
                    }
                })
            }
        })
    })

    it('filter datetime', () => {
        let newStartDate = new Date(datetimeMin)
        newStartDate = new Date(
            newStartDate.setDate(newStartDate.getDate() + 1)
        )

        let newEndDate = new Date(datetimeMax)
        newEndDate = new Date(newEndDate.setDate(newEndDate.getDate() - 1))

        cy.get('.dropdown-toggle').click()
        cy.get(':nth-child(1) > .rs-dropdown-item-content > p').click()

        cy.get('.table-addition > :nth-child(2) > .rs-btn').click({
            scrollBehavior: 'center',
        })

        cy.get(
            '.rs-calendar-header-month-toolbar > .rs-calendar-header-title'
        ).click()

        cy.get('.rs-calendar-month-dropdown-row').then((rows) => {
            let yearIdx = null
            for (let i = 0; i <= rows.length; i++) {
                if (
                    rows[i].outerText.split('\n')[0] ==
                    newStartDate.getFullYear()
                ) {
                    yearIdx = i
                    break
                }
            }

            expect(yearIdx).to.not.equal(null)

            cy.get(
                `[style="${
                    rows[yearIdx].style.cssText
                }"] > .rs-calendar-month-dropdown-list > :nth-child(${
                    newStartDate.getMonth() + 1
                }) > .rs-calendar-month-dropdown-cell-content`
            ).click()

            cy.get(
                `[title="${newStartDate.getDate()} ${
                    monthNames[newStartDate.getMonth()]
                } ${newStartDate.getFullYear()}"] > .rs-calendar-table-cell-content`
            ).click()
            cy.get('.rs-picker-toolbar-right-btn-ok').click()

            cy.get(':nth-child(4) > .rs-btn').click({
                scrollBehavior: 'center',
            })
            cy.get(
                '.rs-calendar-header-month-toolbar > .rs-calendar-header-title'
            ).click()

            cy.get('.rs-calendar-month-dropdown-row').then((rows) => {
                let yearIdx = null
                for (let i = 0; i <= rows.length; i++) {
                    if (
                        rows[i].outerText.split('\n')[0] ==
                        newEndDate.getFullYear()
                    ) {
                        yearIdx = i
                        break
                    }
                }

                expect(yearIdx).to.not.equal(null)

                cy.get(
                    `[style="${
                        rows[yearIdx].style.cssText
                    }"] > .rs-calendar-month-dropdown-list > :nth-child(${
                        newEndDate.getMonth() + 1
                    }) > .rs-calendar-month-dropdown-cell-content`
                ).click()

                cy.get(
                    `[title="${newEndDate.getDate()} ${
                        monthNames[newEndDate.getMonth()]
                    } ${newEndDate.getFullYear()}"] > .rs-calendar-table-cell-content`
                ).click()
                cy.get('.rs-picker-toolbar-right-btn-ok').click()

                let filteredMax, filteredMin

                cy.get(
                    '.non-selectable.rs-table-cell-header-sortable > .rs-table-cell > .rs-table-cell-content'
                ).click({ scrollBehavior: 'center' })

                cy.wait(1000)

                cy.get(
                    '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area > [aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="2"] > .rs-table-cell-content > .table-cell-wrapper'
                ).then((el) => {
                    filteredMin = new Date(
                        ...el[0].outerText
                            .replace(':', '')
                            .replace(/\s/g, '-')
                            .split('-')
                            .map((el) =>
                                !isNaN(el) ? el : monthNames.indexOf(el)
                            )
                    )

                    cy.get(
                        '.non-selectable.rs-table-cell-header-sortable > .rs-table-cell > .rs-table-cell-content'
                    ).click({ scrollBehavior: 'center' })

                    cy.wait(1000)

                    cy.get(
                        '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area > [aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="2"] > .rs-table-cell-content > .table-cell-wrapper'
                    ).then((el) => {
                        filteredMax = new Date(
                            ...el[0].outerText
                                .replace(':', '')
                                .replace(/\s/g, '-')
                                .split('-')
                                .map((el) =>
                                    !isNaN(el) ? el : monthNames.indexOf(el)
                                )
                        )

                        expect(
                            filteredMin.getTime() <= filteredMax.getTime()
                        ).to.equal(true)
                        expect(
                            filteredMax.getTime() <= datetimeMax.getTime()
                        ).to.equal(true)
                        expect(
                            filteredMin.getTime() >= datetimeMin.getTime()
                        ).to.equal(true)
                    })
                })
            })
        })
    })

    it('view file', () => {
        cy.get('.dropdown-toggle').click()
        cy.get(':nth-child(1) > .rs-dropdown-item-content > p').click()

        cy.get(
            '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area > :nth-child(1)'
        ).then((row) => {
            const [robot, time, command] = row[0].outerText.split('\n')
            cy.get(
                '[aria-rowindex="2"] > .rs-table-cell-group > .rs-table-cell-last > .rs-table-cell-content > .checkbox-div > .table-cell-wrapper > .rs-btn > .rs-icon'
            ).click()

            cy.wait(500)

            cy.get('.rs-modal-content').should('exist')
            cy.get('#fileImage').should('exist')
            cy.get('#fileImage')
                .should('be.visible')
                .and(($img) => {
                    expect($img[0].naturalWidth).to.be.greaterThan(0)
                })
            cy.get('h4').should('contain', robot)
            cy.get('h4').should(
                'contain',
                time.split(' ')[0].replace(/-/g, '_')
            )

            cy.get('.notifications-modal-btn-container > :nth-child(1)').click()

            cy.get('.CodeMirror-lines').then((lines) => {
                const commandJson = JSON.parse(
                    lines[0].outerText
                        .replace(/\n[0-9]+\n/g, '')
                        .replace(/[\s|\n]/g, '')
                        .replace('1', '')
                )
                expect(commandJson.op).to.equal(command)
            })
        })
    })

    it('check/uncheck files', () => {
        cy.get('.dropdown-toggle').click()
        cy.get(':nth-child(1) > .rs-dropdown-item-content > p').click()

        cy.get(
            '[aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="4"] > .rs-table-cell-content > .checkbox-div > .rs-checkbox > .rs-checkbox-checker > label > .rs-checkbox-wrapper'
        ).then((resp) => {
            expect(resp[0].firstChild.checked).to.equal(false)
        })

        cy.get(
            '[style="display: flex; flex-direction: row;"] > .rs-btn'
        ).should('contain', '0')

        cy.get(
            '[aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="4"] > .rs-table-cell-content > .checkbox-div > .rs-checkbox > .rs-checkbox-checker > label > .rs-checkbox-wrapper'
        ).click()

        cy.get(
            '[aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="4"] > .rs-table-cell-content > .checkbox-div > .rs-checkbox > .rs-checkbox-checker > label > .rs-checkbox-wrapper'
        ).then((resp) => {
            expect(resp[0].firstChild.checked).to.equal(true)
        })

        cy.get(
            '[style="display: flex; flex-direction: row;"] > .rs-btn'
        ).should('contain', '1')

        cy.get(
            '[aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="4"] > .rs-table-cell-content > .checkbox-div > .rs-checkbox > .rs-checkbox-checker > label > .rs-checkbox-wrapper'
        ).click()

        cy.get(
            '[style="display: flex; flex-direction: row;"] > .rs-btn'
        ).should('contain', '0')

        cy.get(
            ':nth-child(4) > .rs-table-cell > .rs-table-cell-content > .checkbox-div > .rs-checkbox > .rs-checkbox-checker > label > .rs-checkbox-wrapper'
        ).click()

        cy.get(
            '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area >'
        ).then((rows) => {
            cy.get(
                '[style="display: flex; flex-direction: row;"] > .rs-btn'
            ).should('contain', rows.length)
        })

        cy.get(
            ':nth-child(4) > .rs-table-cell > .rs-table-cell-content > .checkbox-div > .rs-checkbox > .rs-checkbox-checker > label > .rs-checkbox-wrapper'
        ).click()

        cy.get(
            '[style="display: flex; flex-direction: row;"] > .rs-btn'
        ).should('contain', '0')
    })

    it('create session ', () => {
        cy.get('.dropdown-toggle').click()
        cy.get(':nth-child(1) > .rs-dropdown-item-content > p').click()
        cy.get(
            '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area >'
        ).then((rows) => {
            sessionCommand = rows[0].outerText
        })
        cy.get(
            '[aria-rowindex="2"] > .rs-table-cell-group > [aria-colindex="4"] > .rs-table-cell-content > .checkbox-div > .rs-checkbox > .rs-checkbox-checker > label > .rs-checkbox-wrapper'
        ).click()
        cy.get(
            '[style="display: flex; flex-direction: row;"] > .rs-input'
        ).type(sessionName)
        cy.get(
            '[style="display: flex; flex-direction: row;"] > .rs-btn'
        ).click()

        cy.get(
            '.session-table-list > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area'
        )
            .contains(sessionName)
            .should('exist')
    })

    it('list session files', () => {
        cy.get(
            '.session-table-list > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area >'
        ).then((rows) => {
            let createdSessionIdx = null
            for (let i = 0; i <= rows.length; i++) {
                if (rows[i].outerText.split('\n')[0] === sessionName) {
                    createdSessionIdx = i
                    break
                }
            }
            expect(createdSessionIdx).to.not.equal(null)
            cy.get(
                `.session-table-list > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area > [aria-rowindex="${
                    2 + createdSessionIdx
                }"] > .rs-table-cell-group > [aria-colindex="2"] > .rs-table-cell-content > .table-cell-wrapper`
            ).click()

            cy.wait(500)

            cy.get(
                '.session-table > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area >'
            ).then((rows) => {
                expect(rows.length).to.equal(1)
                expect(rows[0].outerText).to.equal(sessionCommand)
            })
        })
    })

    it('delete session', () => {
        cy.get(
            '.session-table-list > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area >'
        ).then((rows) => {
            let createdSessionIdx = null
            for (let i = 0; i <= rows.length; i++) {
                if (rows[i].outerText.split('\n')[0] === sessionName) {
                    createdSessionIdx = i
                    break
                }
            }
            expect(createdSessionIdx).to.not.equal(null)
            cy.get(
                `[aria-rowindex="${
                    2 + createdSessionIdx
                }"] > .rs-table-cell-group > .rs-table-cell-first > .rs-table-cell-content > .table-cell-wrapper`
            ).click()
            cy.get('.red-button').click()

            cy.wait(500)

            cy.get(
                '.session-table-list > .rs-table > .rs-table-body-row-wrapper > .rs-table-body-wheel-area'
            ).should('not.contain', sessionName)
        })
    })
})
