/// <reference types="cypress" />

import axios from 'axios'
import Chance from 'chance'

Cypress.Commands.add('bypassLogin', (login = null, pass = null) => {
    const username = login ? login : Cypress.env('username')
    const password = pass ? pass : Cypress.env('password')

    return axios
        .post(Cypress.env('http_gate') + '/login', { username, password })
        .then(({ data }) => {
            const { refresh_token, access_token } = data
            localStorage.setItem('access_token', access_token)
            localStorage.setItem('refresh_token', refresh_token)
        })
})

export const randomJSON = () => {
    const chance = new Chance()
    let obj = {}
    for (let i = 0; i <= Math.floor(Math.random() * 10); i++) {
        let rng = 2 + Math.floor(Math.random() * 5)

        switch (rng) {
            case 1:
                obj[chance.word({ syllables: 5 })] = chance.bool()
                break
            case 2:
                obj[chance.word({ syllables: 5 })] = chance.integer()
                break
            case 3:
                obj[chance.word({ syllables: 5 })] = new Array(
                    Math.floor(Math.random() * 10)
                ).fill(Math.random() * 100)
                break
            default:
                obj[chance.word({ syllables: 5 })] = chance.word({
                    syllables: 5,
                })
                break
        }
    }
    return JSON.stringify(obj, false, 2)
}

//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
