class MicProcessor extends AudioWorkletProcessor {
    constructor() {
        super()
    }

    process(inputs, outputs, parameters) {
        this.port.postMessage(inputs)
        return true
    }
}

registerProcessor('mic_processor', MicProcessor)
