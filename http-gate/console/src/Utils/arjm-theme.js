export const colors = {
    default: 'black',
    background: '#ffff',
    background_warning: 'lightgray',
    string: '#587c3b',
    number: '#b76a51',
    colon: 'black',
    keys: 'black',
    keys_whiteSpace: '#9c6a93',
    primitive: '#5496aa',
}
