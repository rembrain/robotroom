import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'

import { urls } from '../../../Utils'
import Stream from './Stream'
import StreamWrapper from '../Utils/StreamWrapper'

import './MarkerOperator.css'

const BoxMarker = ({ Authorization, markerRobot, markerRobotConfig }) => {
    const [dimensions, setDimensions] = useState([1280, 720])
    useEffect(() => {
        if (markerRobotConfig) {
            const { width, height } = markerRobotConfig
            width && height
                ? setDimensions([width, height])
                : setDimensions([1280, 720])
        } else {
            setDimensions([1280, 720])
        }
    }, [markerRobotConfig])

    return (
        <div className="box-marker-stream margin-center-h">
            <StreamWrapper
                stream={<Stream />}
                exchange="camera0"
                token={Authorization}
                placeholderText="No Image"
                websocketURL={urls.webSocketHost + '/'}
                robotName={markerRobot}
                handleError={(ev) => {
                    console.log('Oh, no! Websocket error occured ', ev)
                }}
                canDownload={true}
                width={dimensions[0]}
                height={dimensions[1]}
                st
            />
        </div>
    )
}

const mapStateToProps = (state) => {
    const { markerRobot, markerRobotConfig, markerTask, userBin } = state.marker
    return {
        Authorization: state.auth.Authorization,
        markerRobotConfig,
        markerRobot,
    }
}

export default connect(mapStateToProps)(BoxMarker)
