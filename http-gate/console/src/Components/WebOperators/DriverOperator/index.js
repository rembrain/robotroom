import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { urls } from '../../../Utils'
import Stream from './Stream'
import StreamWrapper from '../Utils/StreamWrapper'

import './driver.css'

const DriverOperator = ({ markerRobot, markerRobotConfig, headers }) => {
    const [dimensions, setDimensions] = useState([800, 600])

    useEffect(() => {
        if (markerRobotConfig) {
            const { width, height } = markerRobotConfig
            width && height
                ? setDimensions([width, height])
                : setDimensions([800, 600])
        } else {
            setDimensions([800, 600])
        }
    }, [markerRobotConfig])

    return (
        <div className="box-marker-stream margin-center-h">
            <StreamWrapper
                stream={<Stream />}
                exchange="camera0"
                token={headers.Authorization}
                placeholderText="No Image"
                websocketURL={urls.webSocketHost}
                robotName={markerRobot}
                handleError={(ev) => {
                    console.log('Oh, no! Websocket error occured ', ev)
                }}
                canDownload={true}
                width={dimensions[0]}
                height={dimensions[1]}
            />
        </div>
    )
}

const mapStateToProps = (state) => {
    const { markerRobot, markerRobotConfig } = state.marker
    return {
        headers: state.auth,
        markerRobotConfig,
        markerRobot,
    }
}

export default connect(mapStateToProps)(DriverOperator)
