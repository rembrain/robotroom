import WebOperators from './WebOperators'
import Watchtower from './Watchtower'
import Users from './Users'
import Tasks from './Tasks'
import SystemMonitoring from './SystemMonitoring'
import Robots from './Robots'
import Retraining from './Retraining'
import Auth from './Auth'
import RobotSubscriptions from './RobotSubscriptions'

export * from './UI'
export {
    WebOperators,
    Watchtower,
    Users,
    Tasks,
    SystemMonitoring,
    Robots,
    Retraining,
    Auth,
    RobotSubscriptions,
}
