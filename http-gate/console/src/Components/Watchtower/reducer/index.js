import { slider } from './slider'
import { utils } from './utils'
import { data } from './data'
import { playlist } from './playlist'

export { slider, utils, data, playlist }
