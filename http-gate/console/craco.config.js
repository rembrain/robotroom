require('dotenv/config')
const CracoAntDesignPlugin = require('craco-antd')

module.exports = {
    plugins: [
        {
            plugin: CracoAntDesignPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: {
                            '@base-color': '#16425b',
                        },
                    },
                },
            },
        },
    ],
}
