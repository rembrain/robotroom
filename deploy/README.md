# Deploy

Instructions for deploying step by step fully on clear Ubuntu OS v20.04.

### Setting ports

It's very important! It should make only first time. Maybe some operations are rudimentary.

- Reset firewall

> **ATTENTION!  
> Do only if it's required(for example after install Linux) - It resets all settings and opened/closed ports!**

```
ufw reset
```

- allow ssh port:

```
ufw allow 22
ufw route allow 22
```

repeat it for ports 80, 443 and others which must be opened to outside

- run firewall: `ufw enable`

> **ATTENTION!**  
**Just notice for developers!**  
Docker makes changes **directly on your iptables**, which are **NOT shown with the util 'ufw'**. That is if 'ufw' even can block some ports like 15672 for rabbitmq - Docker ignores it by using **iptables** - and
**it's the reason why monitor.rembrain.ai:15672 will be seen outside!**
In order to avoid it docker compose file must open ports like:
>> `docker run ...-p 127.0.0.1:<ext pot>:<internal port>`
>
> it prevents your Docker from being accessed from outside.
>
>For docker-compose.yml:
>> ```
>>    ports:
>>      - "127.0.0.1:15672:15672"
>>```

### Nginx

- install Nginx
- copy(e.g. rewrite with its content to server file) file `deploy/nginx/server-before-certbot.conf`
  to `/etc/nginx/sites-available/default` on server.
- set certbot ([more info](https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal)):

```
# snap must be installed before if it does not exist!
sudo snap install core; sudo snap refresh core

# install certbot 
sudo snap install --classic certbot

# add link to certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot

# generate certificates - run it FOR EVERY DOMAIN!
sudo certbot certonly --nginx
```

- with `deploy/nginx/server.conf` fully rewrite `/etc/nginx/sites-available/default`
- restart nginx: `sudo service nginx reload`
- certificates expire after 90 days - for autoupdate set cron task:

```
crontab -e
```

add (or `@daily`)

```
@weekly certbot renew
```

#### Adding new domains with SSL to Nginx
>**ATTENITION!** 
> Change <your_domain_here> and <your_docker_container_port_here> on required once.
- run `nano /etc/nginx/sites-available/default` add into it:
```
# BLOCK #1
server {
      server_name <your_domain_here>;
      location / {
            try_files $uri $uri/ =404;
      }
}
# END BLOCK #1
```

- run in console:
  ```
  sudo service nginx reload
  sudo certbot certonly --nginx
  ```
  and select number of domain from list which was printed by _certbot_
  
- run again `nano /etc/nginx/sites-available/default`, remove **# BLOCK #1** fully and insert:

```
# BLOCK #2
server {
      server_name <your_domain_here>;

      location / {
            proxy_set_header Host $host;
            proxy_pass http://127.0.0.1:<your_docker_container_port_here>;
            proxy_redirect off;
      }

       # Certbot params
       listen [::]:443 ssl;
       listen 443 ssl;
       ssl_certificate /etc/letsencrypt/live/<your_domain_here>/fullchain.pem;
       ssl_certificate_key /etc/letsencrypt/live/<your_domain_here>/privkey.pem;
       include /etc/letsencrypt/options-ssl-nginx.conf;
       ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}
# END BLOCK #2
```
- reload Nginx `sudo service nginx reload`
  
---
### Docker

- create env-file `.env` (it's better to make outside of project)
  and add env vars - all required vars is in `deploy/config/example.env`

- add ssl certificates for ws connection(paths are in docker-compose.yml - `websocket-gate` - `volumes`)
- run compose file:

```
docker compose -f deploy/docker-compose.yml -p robot_room --env-file <path_to_env_file> up --build -d
```

### RabbitMq

- Add 'logstash' exchange thorough web! Login and passwords are in `docker-compose.yml`

### Authentication setup (Keycloak)

- Attention! Add random(it will not be checked) email for curr user thorough `Users` menu
- Create an `http-gate` realm
- Select the new realm in the top left
- Create an `http-gate` client in this realm, it will be used for backend authentication
- In the client settings do the following:
    - Set **Access Type** to `confidential`
    - Turn ON **Service Accounts Enabled**
    - Set **Valid Redirect URIs** to `http://*.localhost/*` if you are going to be running locally
    - Go to the **Mappers** tab, click on the `Add Builtin` button in the top right, and add all of the mappers. They
      will be used for user access control in the backend

- After setting up the client go to the **Credentials** tab in the client, set **Client Authenticator**
  to `Client Id and Secret`, copy the secret and paste it into the `env-file` file under `KEYCLOAK_CLIENT_SECRET`
  variable.

- Go to **Roles** on the left and add following roles:
    - `admin` for the administrative accounts
    - `robot` that will be used for authenticating the robots you'll be adding
    - `processor` that will be used for authenticating processors (ML actors)
    - `operator` for any non-admin users

- Now we're ready to create the user we'll login as into the system. Go to **Users** on the left, and in the opened
  table click `Add User` in the top right.
    - Attention! Be sure to set the user's Email(random - it will not be checked) and set it to verified (even if
      there's no email in use).
    - Create the user, then go to the **Credentials** tab of the newly created user and set their **password**
    - Go to the **Role Mappings** tab and assign the `admin` role to the user
    - This user (or another admin user) will be used by the monitoring service to access the frontend. Open `env-file`
      and replace `MONITOR_ADMIN_NAME` and `MONITOR_ADMIN_PASSWORD` with the new values.

- After it rerun docker compose containers.
