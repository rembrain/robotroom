## docker compose -f deploy/docker-compose.yml -p robot_room --env-file deploy/.env up --build -d
version: "3.8"

services:
  http-gate:
    build:
      context: ../
      dockerfile: http-gate/Dockerfile
    environment:
      - PORT=8000
      - WORKERS_PER_CORE=4
      - APP_HTTP_GATE_URL=int/https://monitor.rembrain.ai
      - APP_WEB_SOCKET_GATE_URL=int/wss://monitor.rembrain.ai/wssocket/

      # services
      - MONGODB_URI=mongodb://guest:guest@db:27017
      - RABBIT_ADDRESS=${RABBIT_ADDRESS}
      - REDIS_ADDRESS=redis

      # minio
      - S3_ADDRESS=http://minio:9000
      - S3_ACCESS_KEY=${S3_ACCESS_KEY}
      - S3_SECRET_KEY=${S3_SECRET_KEY}
      - BUCKET_NAME_LOGS=robots.log
      - BUCKET_NAME_ARCHIVE=robots.archive
      - BUCKET_NAME_ARTIFACTS=robots.artifacts
      - BUCKET_NAME_STREAMS=robots.streams
      - BUCKET_NAME_SESSIONS=robots.sessions
      - BUCKET_NAME_WEIGHTS=robots.weights

      # keycloak
      - KEYCLOAK_ADDRESS=http://keycloak:8080/auth/
      - KEYCLOAK_ADMIN_REALM=master
      - KEYCLOAK_ADMIN_USERNAME=${KEYCLOAK_ADMIN_USERNAME}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD}
      - KEYCLOAK_REALM=http-gate
      - KEYCLOAK_CLIENT_ID=${KEYCLOAK_CLIENT_ID}
      - KEYCLOAK_CLIENT_SECRET=${KEYCLOAK_CLIENT_SECRET}
    ports:
      - "127.0.0.1:8000:8000"

  monitor:
    build:
      context: ../
      dockerfile: monitor/deploy/Dockerfile
    environment:
      - REGISTER_ADDRESS=http://http-gate:8000
      - ADMIN_NAME=${MONITOR_ADMIN_NAME}
      - ADMIN_PASSWORD=${MONITOR_ADMIN_PASSWORD}
      - MONITOR_SLACK_ENABLED=false
      - MONITOR_TELEGRAM_ENABLED=false

      # services
      - MONGODB_URI=mongodb://guest:guest@db:27017
      - RABBIT_ADDRESS=${RABBIT_ADDRESS}
      - RABBIT_MGMT_URL=${RABBIT_MGMT_URL}
      - REDIS_ADDRESS=redis
      - S3_ADDRESS=http://minio:9000
      - S3_ACCESS_KEY=${S3_ACCESS_KEY}
      - S3_SECRET_KEY=${S3_SECRET_KEY}
      - BUCKET_NAME_LOGS=robots.log
      - BUCKET_NAME_ARCHIVE=robots.archive
      - BUCKET_NAME_ARTIFACTS=robots.artifacts
      - BUCKET_NAME_STREAMS=robots.streams
      - BUCKET_NAME_SESSIONS=robots.sessions
      - BUCKET_NAME_WEIGHTS=robots.weights
      - STORAGE_DAYS_ARCHIVE=365
      - STORAGE_DAYS_STREAMS=1

  websocket-gate:
    build:
      context: ../
      dockerfile: websocket_gate/deploy/Dockerfile
    environment:
      - HOST=0.0.0.0
      - PORT=5443
      - MONGODB_URI=mongodb://guest:guest@db:27017
      - RABBIT_ADDRESS=${RABBIT_ADDRESS}

      # keycloak
      - KEYCLOAK_ADDRESS=http://keycloak:8080/auth/
      - KEYCLOAK_ADMIN_REALM=master
      - KEYCLOAK_ADMIN_USERNAME=${KEYCLOAK_ADMIN_USERNAME}
      - KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD}
      - KEYCLOAK_REALM=http-gate
      - KEYCLOAK_CLIENT_ID=${KEYCLOAK_CLIENT_ID}
      - KEYCLOAK_CLIENT_SECRET=${KEYCLOAK_CLIENT_SECRET}
    ports:
      - "127.0.0.1:5443:5443"

  rabbit-master:
    image: rabbitmq:3.8-management
    environment:
      - RABBITMQ_DEFAULT_USER=${RABBIT_LOGIN}
      - RABBITMQ_DEFAULT_PASS=${RABBIT_PASSWORD}
    ports:
      - "127.0.0.1:15672:15672"

  redis:
    image: redis:6.0

  db:
    image: mongo:5.0.12
    environment:
      MONGO_INITDB_ROOT_USERNAME: 'guest'
      MONGO_INITDB_ROOT_PASSWORD: 'guest'

  postgres:
    image: postgres:14.4-alpine
    volumes:
      - robot_room_postgres_data:/var/lib/postgresql/data/
    environment:
      - POSTGRES_DB=keycloak
      - POSTGRES_PASSWORD=keycloak

  keycloak:
    image: quay.io/keycloak/keycloak:16.1.0

    environment:
      - DB_VENDOR=postgres
      - DB_ADDR=postgres
      - DB_DATABASE=keycloak
      - DB_USER=postgres
      - DB_PASSWORD=keycloak

      - KEYCLOAK_HOSTNAME=auth.rembrain.ai
      - KEYCLOAK_USER=${KEYCLOAK_ADMIN_USERNAME}
      - KEYCLOAK_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD}

      - JAVA_OPTS=-Dkeycloak.profile=preview
      - JDBC_PARAMS="useSSL=false"
    ports:
      - "127.0.0.1:8443:8443"

  minio:
    image: bitnami/minio:2022.8.26
    volumes:
      - robot_room_minio:/usr/src/app/
    environment:
      - MINIO_DEFAULT_BUCKETS=robots.streams,robots.archive,robots.artifacts,robots.log,robots.sessions,robots.weights
      - MINIO_ROOT_USER=${S3_ACCESS_KEY}
      - MINIO_ROOT_PASSWORD=${S3_SECRET_KEY}
    ports:
      - "127.0.0.1:9000:9000"
      - "127.0.0.1:9001:9001"

  loki-service:
    image: grafana/loki:2.6.1
    command: "-config.file=/etc/loki/config.yaml"
    expose:
      - 3100
    volumes:
      - ./config/loki-config.yaml:/etc/loki/config.yaml

  loki-logstash:
    image: grafana/logstash-output-loki:2.6.1-amd64
    environment:
      - RABBIT_LOGIN=${RABBIT_LOGIN}
      - RABBIT_PASSWORD=${RABBIT_PASSWORD}
    volumes:
      - ./config/logstash.conf:/usr/share/logstash/pipeline/logstash.conf
    depends_on:
      - loki-service

  grafana:
    image: grafana/grafana:9.1.2
    environment:
      - GF_SECURITY_ADMIN_USER=${GF_SECURITY_ADMIN_USER}
      - GF_SECURITY_ADMIN_PASSWORD=${GF_SECURITY_ADMIN_PASSWORD}
      - GF_ANALYTICS_REPORTING_ENABLED=false
      - GF_ANALYTICS_CHECK_FOR_UPDATES=false
      - GF_ANALYTICS_CHECK_FOR_PLUGIN_UPDATES=false
      - GF_USERS_ALLOW_SIGN_UP=false
      - GF_AUTH_ANONYMOUS_ENABLED=false
    depends_on:
      - loki-service
    entrypoint:
      - sh
      - -euc
      - |
        mkdir -p /etc/grafana/provisioning/datasources
        cat <<EOF > /etc/grafana/provisioning/datasources/ds.yaml
        apiVersion: 1
        datasources:
          - name: Loki
            type: loki
            access: proxy
            url: http://loki-service:3100
            jsonData:
              httpHeaderName1: "X-Scope-OrgID"
            secureJsonData:
              httpHeaderValue1: "tenant1"
        EOF
        /run.sh
    ports:
      - "127.0.0.1:8010:3000"

volumes:
  robot_room_postgres_data:
  robot_room_minio:
