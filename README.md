# Rembrain RobotRoom

This repository contains all the backend services required to run a full robotroom deployment

## Deploy
See [here](deploy/README.md)


Directory structure
---

- **common** - Common services for working with rabbit, mongodb, etc.
- **deploy** - Deploy
- **http-gate** - Web application and API source
- **monitor** - Monitors robot and system health, also does some utility functions like repacking image data
- **telegram-bot** -  Sends messages to telegram when robots are offline
- **tests** -  Integration and websocket tests
- **tools** -  Miscellaneous tools
- **websocket_gate** - Backend service for websockets communication, pipes robot data to RabbitMQ
