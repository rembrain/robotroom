﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.VR;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace openTKVRtest1
{
    public static class Utils
    {
        static public OpenTK.Matrix3 convertQtoM33(float[] q)
        {
            OpenTK.Quaternion q_= new OpenTK.Quaternion(q[1], q[2], q[3], q[0]);
            return Matrix3.CreateFromQuaternion(q_);
        }
        static public OpenTK.Matrix4 convertM34toM44(HmdMatrix34_t m)
        {
            //var res = new OpenTK.Matrix4(m.m0,m.m1,m.m2,m.m3,m.m4,m.m5,m.m6,m.m7,m.m8,m.m9,m.m10,m.m11,0f,0f,0f,1.0f);
            var res = new OpenTK.Matrix4(m.m0, m.m4, m.m8, 0.0f, m.m1, m.m5, m.m9, 0.0f, m.m2, m.m6, m.m10, 0.0f, m.m3, m.m7, m.m11, 1.0f);
            return res;
        }
        static public OpenTK.Matrix4 convertToOpenTKM4(HmdMatrix44_t m)
        {
            //var res = new OpenTK.Matrix4(m.m0, m.m1, m.m2, m.m3, m.m4, m.m5, m.m6, m.m7, m.m8, m.m9, m.m10, m.m11, m.m12, m.m13, m.m14, m.m15);
            var res = new OpenTK.Matrix4(m.m0, m.m4, m.m8, m.m12, m.m1, m.m5, m.m9, m.m13, m.m2, m.m6, m.m10, m.m14, m.m3, m.m7, m.m11, m.m15);
            return res;
        }

        public static Bitmap GenerateTexture(string text, bool chosen)
        {
            Bitmap bmp = new Bitmap(256, 128,System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            

            // Create font and brush.
            Font drawFont = new Font("Arial", 45);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            g.DrawString(text, drawFont, drawBrush, new PointF(10f, 30f));
            if (chosen)
            {
                SolidBrush brush = new SolidBrush(Color.FromArgb(34,177,76));
                g.FillRectangle(brush, new Rectangle(0, 0, 256, 10));
                g.FillRectangle(brush, new Rectangle(0, 118, 256, 10));
                g.FillRectangle(brush, new Rectangle(0, 0, 10, 128));
                g.FillRectangle(brush, new Rectangle(246, 0, 10, 128));
            }

            bmp.Save("1.bmp");
            return bmp;
        }

        
        public static void LoadTexture(Bitmap bitmap,ref int texture)
        {
            
            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

            if(texture==0)
                texture=GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texture);

            BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            bitmap.UnlockBits(data);


            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

            GL.BindTexture(TextureTarget.Texture2D, 0);
        }
        public static byte[] GetBytesFromBitmap(Bitmap bitmap)
        {
            BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            byte[] array = new byte[bitmap.Height * data.Stride];
            Marshal.Copy(data.Scan0, array, 0, array.Length);
            bitmap.UnlockBits(data);
            return array;
        }
        public static Bitmap GetBitmapFromBytes(byte[] buf,int w,int h)
        {
            Bitmap bitmap = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Marshal.Copy(buf,0,data.Scan0, buf.Length);
            bitmap.UnlockBits(data);
            return bitmap;
        }
        public static void DrawImage(int texture,int width,int height)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.PushMatrix();
            GL.LoadIdentity();

            GL.Ortho(0, width, 0, height, -1, 1);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.LoadIdentity();

            GL.Disable(EnableCap.Lighting);

            GL.Enable(EnableCap.Texture2D);


            GL.BindTexture(TextureTarget.Texture2D, texture);

            GL.Begin(BeginMode.Quads);

            GL.TexCoord2(0, 0);
            GL.Vertex3(0, height, 0);

            GL.TexCoord2(1, 0);
            GL.Vertex3(width, height, 0);

            GL.TexCoord2(1, 1);
            GL.Vertex3(width, 0, 0);

            GL.TexCoord2(0, 1);
            GL.Vertex3(0, 0, 0);

            GL.End();

            GL.Disable(EnableCap.Texture2D);
            GL.PopMatrix();

            GL.MatrixMode(MatrixMode.Projection);
            GL.PopMatrix();

            GL.MatrixMode(MatrixMode.Modelview);
        }
        public static Vector3 GetXYZ(Point p,float[,] camM, float[,] depthMap)
        {
            int x1_d = p.X / 2;
            int y1_d = p.Y / 2;
            float z = depthMap[x1_d, y1_d];
            if (z == 0)
                return new Vector3(0);

            float u = ((float)p.X - camM[0, 2]) / camM[0, 0];
            float v = ((float)p.Y - camM[1, 2]) / camM[1, 1];
            return new Vector3(u * z, v * z, z);
        }

        public static Vector3 CalcCircleCenter(Point p1, Point p2, float[,] camM, float[,] depthMap,float R)
        {
            int x1_d = p1.X / 2;
            int y1_d = p1.Y / 2;
            float z = depthMap[x1_d, y1_d];

            if (z == 0)
                return new Vector3(0);

            float u1 = ((float)p1.X - camM[0, 2]) / camM[0, 0];
            float v1 = ((float)p1.Y - camM[1, 2]) / camM[1, 1];
            float u2 = ((float)p2.X - camM[0, 2]) / camM[0, 0];
            float v2 = ((float)p2.Y - camM[1, 2]) / camM[1, 1];

            Vector3 p1_3d = new Vector3(u1 * z, v1 * z, z);


            float minD = 10000.0f;
            Vector3 bestV=new Vector3();
            float bestDz = 0;
            for (float dz = 0; dz < R * 3; dz += R / 50.0f)
            {
                float z2 = z + dz;
                Vector3 p2_3d_hyp = new Vector3(u2 * z2, v2 * z2, z2);
                float dist = Math.Abs((p2_3d_hyp - p1_3d).Length-R);
                if (dist < minD)
                {
                    minD = dist;
                    bestV = p2_3d_hyp;
                    bestDz = dz;
                }
            }
            return bestV;

        }
    }
}
