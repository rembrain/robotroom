﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.VR;
using System.Drawing;
using OpenTK;
using System.Runtime.InteropServices;
using OpenTK.Graphics.OpenGL;

namespace openTKVRtest1
{
    public class ControllerModel
    {
        public ControllerModel(string sRenderModelName)
        {
            m_glIndexBuffer = 0;
            m_glVertArray = 0;
            m_glVertBuffer = 0;
            m_glTexture = 0;
            m_sModelName = sRenderModelName;
        }
        float[] ar = null;
        float[] xyz = null;
        float[] uvs = null;
        short[] indexes = null;
        public bool BInit(RenderModel_t vrModel, RenderModel_TextureMap_t vrDiffuseTexture )
        {
            
            ar = new float[8*vrModel.unVertexCount];
            Marshal.Copy(vrModel.rVertexData, ar, 0, ar.Length);

            xyz = new float[3*vrModel.unVertexCount];
            for (int i = 0; i < vrModel.unVertexCount; i++)
            {
                xyz[i * 3] = ar[i * 8];
                xyz[i * 3+1] = ar[i * 8+1];
                xyz[i * 3+2] = ar[i * 8+2];
            }
            uvs = new float[2 * vrModel.unVertexCount];
            for (int i = 0; i < vrModel.unVertexCount; i++)
            {
                uvs[i * 2] = ar[i * 8 + 6];
                uvs[i * 2+1] = ar[i * 8 + 7];
            }
            indexes = new short[vrModel.unTriangleCount * 3];
            Marshal.Copy(vrModel.rIndexData, indexes, 0, indexes.Length);

            
            //vertices
            m_glVertBuffer = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, m_glVertBuffer);
            GL.BufferData(BufferTarget.ArrayBuffer,(int)(xyz.Length * sizeof(float)), xyz, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            
            // Create and populate the index buffer
            m_glIndexBuffer = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, m_glIndexBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, ((int)System.Runtime.InteropServices.Marshal.SizeOf(typeof(UInt16))) * (int)vrModel.unTriangleCount * 3, indexes, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            //uvs
            m_uvBuffer= GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, m_uvBuffer);
            GL.BufferData(BufferTarget.ArrayBuffer, (int)(uvs.Length * sizeof(float)), uvs, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            //texture
            m_glTexture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, m_glTexture);


            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, vrDiffuseTexture.unWidth, vrDiffuseTexture.unHeight, 0, PixelFormat.Rgba, PixelType.Byte, vrDiffuseTexture.rubTextureMapData);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

            GL.BindTexture(TextureTarget.Texture2D, 0);
           

            m_unVertexCount = (int)vrModel.unTriangleCount * 3;

    
            return true;
        }
        
        public void Dispose()
        {
            if (m_glVertBuffer != 0)
                GL.DeleteBuffer(m_glVertBuffer);

            if(m_glIndexBuffer!=0)
                GL.DeleteBuffer(m_glIndexBuffer);
            
            if(m_uvBuffer!=0)
                GL.DeleteBuffer(m_uvBuffer);

            if (m_glTexture != 0)
                GL.DeleteTexture(m_glTexture);
                

            
        }
        public void Draw()
        {

            GL.BindBuffer(BufferTarget.ArrayBuffer, this.m_glVertBuffer);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);


            GL.BindBuffer(BufferTarget.ArrayBuffer, m_uvBuffer);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, m_glIndexBuffer);


            GL.BindTexture(TextureTarget.Texture2D, m_glTexture);
            // Draw:            

            GL.DrawElements(BeginMode.Triangles, m_unVertexCount, DrawElementsType.UnsignedShort, IntPtr.Zero);


            // Disable:
            GL.BindTexture(TextureTarget.Texture2D,0);
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);


        }
        public string GetName()
        {
            return m_sModelName;
        }
        int m_glVertBuffer;
        int m_glIndexBuffer;
        int m_glVertArray;
        int m_uvBuffer;
        int m_glTexture;
        int m_unVertexCount;
        string m_sModelName;
    }


}
