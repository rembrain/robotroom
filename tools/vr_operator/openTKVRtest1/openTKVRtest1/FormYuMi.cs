﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace openTKVRtest1
{
    public partial class FormYuMi : Form
    {
        public FormYuMi()
        {
            InitializeComponent();

        }

        public Robots.RobotYumi robot = null;

        private void timer1_Tick(object sender, EventArgs e)
        {
            var joints_left = robot.get_joints_left();
            var joints_right = robot.get_joints_right();
            var pos_right = robot.get_pos_right();
            if (joints_right != null)
            {
                string s = "";
                for (int i = 0; i < joints_right.Length; i++)
                    s += joints_right[i] + " ";
                textBox1.Text = s;

                listBox1.Items.Clear();
                for (int i = 0; i < robot.calibration_points.Count; i++)
                {
                    s = "";
                    for (int j = 0; j < robot.calibration_points[i].Length; j++)
                        s+=robot.calibration_points[i][j]+" ";
                    listBox1.Items.Add(s);
                }
                if (plus_press)
                {
                    if ((curArm != -1) && (currentAxis != -1))
                    {
                        if (curArm == 1)
                        {
                            joints_right[currentAxis] += 2.0f;
                            robot.MoveJointsRight(joints_right);
                        }
                    }
                }
                if (minus_press)
                {
                    if ((curArm != -1) && (currentAxis != -1))
                    {
                        if (curArm == 1)
                        {
                            joints_right[currentAxis] -= 2.0f;
                            robot.MoveJointsRight(joints_right);
                        }
                    }
                }
            }
            if (joints_left != null)
            {
                string s = "";
                for (int i = 0; i < joints_left.Length; i++)
                    s += joints_left[i] + " ";
                textBox2.Text = s;
                

            }

        }

        int currentAxis = -1;
        int curArm = -1;
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 0;
            curArm = 1;
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 1;
            curArm = 1;
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 2;
            curArm = 1;
        }
        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 3;
            curArm = 1;
        }
        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 4;
            curArm = 1;
        }
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 5;
            curArm = 1;
        }
        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 6;
            curArm = 1;
        }

        void MouseWheel(object sender, MouseEventArgs args)
        {
            
        }

        bool plus_press = false;
        bool minus_press = false;
        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            plus_press = true;
        }
        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            plus_press = false;
        }
        private void button1_MouseLeave(object sender, EventArgs e)
        {
            plus_press = false;
        }

        private void button2_MouseDown(object sender, MouseEventArgs e)
        {
            minus_press = true;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            minus_press = false;
        }

        private void button2_MouseUp(object sender, MouseEventArgs e)
        {
            minus_press = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            robot.MoveJointsRight(new float[] { 56.0f, -100.0f, 33.0f, 0f, 27f, 36f, -82f });
        }

        private void button4_Click(object sender, EventArgs e)
        {
            robot.MoveJointsLeft(new float[] { -56.0f, -100.0f, 33.0f, 0f, 27f, 36f, 82f });
        }

        private void button5_Click(object sender, EventArgs e)
        {
            robot.MoveJointsRight(new float[] { 0f, -130f, 30f, 0f, 40f, 0f,-135f });
        }

        //delete all points
        private void button7_Click(object sender, EventArgs e)
        {
            robot.calibration_points.Clear();
            robot.savePoints();
        }
        //add current point
        private void button8_Click(object sender, EventArgs e)
        {
            robot.calibration_points.Add(robot.get_joints_right());
            robot.savePoints();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            robot.MoveJointsLeft(new float[] { 0f, -130f, 30f, 0f, 40f, 0f, 135f });
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            robot.goto_test();
        }
    }
}
