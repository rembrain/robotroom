﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OpenTK;
using System.IO;

namespace openTKVRtest1.Robots
{
    public class RobotYumi : Robot
    {
        public RobotYumi(List<object> _commands, object _locker)
        {
            locker = _locker;
            commands = _commands;
        }

        private float[] joints_right = null;
        private float[] pos_right = null;
        private float[] joints_left = null;
        private float[] pos_left = null;
        private List<object> commands;
        private object locker;

        protected override void ParseReadings()
        {
            if (currect_readings["pose"]["right_joints"] != null)
            {
                joints_right = new float[7];

                for (int i = 0; i < 7; i++)
                {
                    joints_right[i] = (float)currect_readings["pose"]["right_joints"][i];

                }

            }

            if (currect_readings["pose"]["right_pos"] != null)
            {
                pos_right = new float[3];
                for (int i = 0; i < 3; i++)
                    pos_right[i] = (float)currect_readings["pose"]["right_pos"][i];
            }

            if (currect_readings["pose"]["left_joints"] != null)
            {
                joints_left = new float[7];

                for (int i = 0; i < 7; i++)
                {
                    joints_left[i] = (float)currect_readings["pose"]["left_joints"][i];

                }

            }

            if (currect_readings["pose"]["left_pos"] != null)
            {
                pos_left = new float[3];
                for (int i = 0; i < 3; i++)
                    pos_left[i] = (float)currect_readings["pose"]["left_pos"][i];
            }
            base.ParseReadings();
        }

        public void MoveJointsRight(float[] joints)
        {
            lock (locker)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict["op"] = "setJ_right";
                dict["joints"] = JsonConvert.SerializeObject(joints);
                commands.Add(dict);
            }
        }
        public void MoveJointsLeft(float[] joints)
        {
            lock (locker)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict["op"] = "setJ_left";
                dict["joints"] = JsonConvert.SerializeObject(joints);
                commands.Add(dict);
            }
        }
        public override void SendCalibrationData()
        {
            lock (locker)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict["op"] = "calibrate";
                dict["pairs"] = JsonConvert.SerializeObject(associated_points);
                commands.Add(dict);
            }
        }
        public override int get_joints_count()
        {
            return 7;
        }

        public float[] get_joints_left()
        {
            lock (locker)
            {
                return joints_left;
            }
        }
        public float[] get_joints_right()
        {
            lock (locker)
            {
                return joints_right;
            }
        }
        public float[] get_pos_right()
        {
            lock (locker)
            {
                return pos_right;
            }
        }
        public float[] get_pos_left()
        {
            lock (locker)
            {
                return pos_left;
            }
        }

        public override void goto_calib_point(int index)
        {
            MoveJointsRight(calibration_points[index]);
        }
        public override void goto_pos_and_orient(Vector3 pos, Matrix3 rot, int arm_index)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            if (arm_index == 0)//left
                dict["op"] = "goto_left";
            if (arm_index == 1)//right
                dict["op"] = "goto_right";

            dict["position"] = JsonConvert.SerializeObject(new float[] { pos.X, -pos.Y, -pos.Z });

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot = m1 * rot * m1;
            dict["rotation"] = JsonConvert.SerializeObject(new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } });
            //string goto_command=JsonConvert.SerializeObject(dict);
            //File.WriteAllText("pos.txt", goto_command);
            commands.Add(dict);
            

        }
        public override void pick_up_pos_and_orient(Vector3 pos, Matrix3 rot, int arm_index)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            if (arm_index == 0)//left
                dict["op"] = "pickup_left";
            if (arm_index == 1)//right
                dict["op"] = "pickup_right";

            dict["position"] = JsonConvert.SerializeObject(new float[] { pos.X, -pos.Y, -pos.Z });

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot = m1 * rot * m1;
            dict["rotation"] = JsonConvert.SerializeObject(new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } });
            //string goto_command=JsonConvert.SerializeObject(dict);
            //File.WriteAllText("pos.txt", goto_command);
            commands.Add(dict);


        }
        public override void save_pos_and_orient(Vector3 pos, Matrix3 rot,int arm_index)
        {
            
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict["op"] = "remember";
                dict["position"] = JsonConvert.SerializeObject(new float[] { pos.X, -pos.Y, -pos.Z });


                Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
                rot = m1 * rot * m1;
                dict["rotation"] = JsonConvert.SerializeObject(new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } });
                dict["arm"] = arm_index.ToString();
                commands.Add(dict);
            
           
        }

        public override void associate_calib_point(int index, float x, float y, float z)
        {
            if ((calibration_points.Count != 0)&&(pos_right!=null))
            {
                if ((associated_points.Count != calibration_points.Count))
                {
                    associated_points.Clear();
                    for (int i = 0; i < calibration_points.Count; i++)
                    {
                        associated_points.Add(null);
                    }
                }

                if(index< calibration_points.Count)
                { 
                    float[] ps = new float[6];
                    ps[0] = x ;
                    ps[1] = -y ;
                    ps[2] = -z ;

                    ps[3] = pos_right[0] ;
                    ps[4] = pos_right[1] ;
                    ps[5] = pos_right[2] ;

                    associated_points[index] = ps;
                }
            }
        }
        public override void actuator(object description)
        {
            string s = (string)description;
            Dictionary<string, string> command = new Dictionary<string, string>();
            command["op"] = s;
            commands.Add(command);
        }
        public void goto_test()
        {

            Dictionary<string,string> goto_command = (Dictionary<string, string>)JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText("pos.txt"));
            commands.Add(goto_command);

        }

        public override void requestAutoMode()
        {
            
                Dictionary<string, string> command = new Dictionary<string, string>();
                command["op"] = "ask_for_ml";
                commands.Add(command);
            
        }
    }
}
