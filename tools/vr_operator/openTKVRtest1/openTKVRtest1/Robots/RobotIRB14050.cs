﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OpenTK;

namespace openTKVRtest1.Robots
{
    public class RobotIRB14050 : Robot
    {
        public RobotIRB14050(List<object> _commands, object _locker)
        {
            locker = _locker;
            commands = _commands;
        }

        private float[] joints = null;
        private float[] pos= null;
        private float[] q = null;

        private List<object> commands;
        private object locker;

        protected override void ParseReadings()
        {
            if (currect_readings != null)
            {
                if (currect_readings["pose"]["joints"] != null)
                {
                    joints = new float[7];
                    if (currect_readings["pose"]["joints"].Count == 7)
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            joints[i] = (float)currect_readings["pose"]["joints"][i].Value;
                        }
                    }

                }
                if (currect_readings["pose"]["pos"] != null)
                {
                    if (currect_readings["pose"]["pos"].Count == 3)
                    {
                        {
                            pos = new float[3];
                            for (int i = 0; i < 3; i++)
                                pos[i] = (float)currect_readings["pose"]["pos"][i];
                        }

                    }
                }
                if(currect_readings["pose"]["rot"]!=null)
                {
                    if (currect_readings["pose"]["rot"].Count == 4)
                    {
                        q = new float[4];
                        for (int i = 0; i < 4; i++)
                            q[i] = (float)currect_readings["pose"]["rot"][i];
                    }
                }
            }
            base.ParseReadings();
        }

        public void MoveJoints(float[] joints)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["op"] = "setJ";
            dict["joints"] = JsonConvert.SerializeObject(joints);
            lock (locker)
            {
                commands.Add(dict);
            }
        }
        public override void SendCalibrationData()
        {
            lock (locker)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict["op"] = "calibrate";
                dict["pairs"] = JsonConvert.SerializeObject(associated_points);
                commands.Add(dict);
            }
        }
        public override void SendCalibrationData2d3d(float[,]points3D,int[,]points2D,float[,] camM)
        {
            lock (locker)
            {
                List<float[]> points = new List<float[]>();
                for (int i = 0; i < points3D.GetLength(0); i++)
                {
                    points.Add(new float[5] { points3D[i,0], points3D[i,1], points3D[i,2], (float)points2D[i,0], (float)points2D[i,1] });
                }
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict["op"] = "calibrate_2d3d";
                dict["pairs"] = JsonConvert.SerializeObject(points);
                dict["camera_matrix"] = JsonConvert.SerializeObject(camM);
                commands.Add(dict);
            }
        }
        public override int get_joints_count()
        {
            return 7;
        }

        public float[] get_joints()
        {
            lock (locker)
            {
                return joints;
            }
        }

        public float[] get_pos()
        {
            lock (locker)
            {
                return pos;
            }
        }
        public float[] get_q()
        {
            lock (locker)
            {
                return q;
            }
        }
        public override void goto_calib_point(int index)
        {
            MoveJoints(calibration_points[index]);
        }
        public override void goto_pos_and_orient(Vector3 pos, Matrix3 rot,int arm_index)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            
            dict["op"] = "goto";
            
            dict["pos"] = JsonConvert.SerializeObject(new float[] { pos.X*1000.0f, -pos.Y * 1000.0f, -pos.Z * 1000.0f });

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot = m1 * rot * m1;
            dict["rot"] = JsonConvert.SerializeObject(new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } });
            //string goto_command=JsonConvert.SerializeObject(dict);
            //File.WriteAllText("pos.txt", goto_command);
            commands.Add(dict);


        }
        
        public override void pick_up_put_down_pos_and_orient(Vector3 pos1, Matrix3 rot1,Vector3 pos2,Matrix3 rot2, int arm_index)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["op"] = "pickup_putdown";


            dict["pos1"] = JsonConvert.SerializeObject(new float[] { pos1.X*1000f, -pos1.Y * 1000f, -pos1.Z * 1000f });
            dict["pos2"] = JsonConvert.SerializeObject(new float[] { pos2.X*1000f, -pos2.Y * 1000f, -pos2.Z * 1000f });

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot1 = m1 * rot1 * m1;
            dict["rot1"] = JsonConvert.SerializeObject(new float[,] { { rot1.M11, rot1.M21, rot1.M31 }, { rot1.M12, rot1.M22, rot1.M32 }, { rot1.M13, rot1.M23, rot1.M33 } });

            rot2 = m1 * rot2 * m1;
            dict["rot2"] = JsonConvert.SerializeObject(new float[,] { { rot2.M11, rot2.M21, rot2.M31 }, { rot2.M12, rot2.M22, rot2.M32 }, { rot2.M13, rot2.M23, rot2.M33 } });

            //string goto_command=JsonConvert.SerializeObject(dict);
            //File.WriteAllText("pos.txt", goto_command);
            commands.Add(dict);


        }
        public override void pick_up_put_down_box(Vector3 pos1, Matrix3 rot1, Vector3 pos2, Matrix3 rot2,List<Vector4> box, int arm_index)
        {
            if (box.Count != 4)
                throw new Exception("box.Count != 4");

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["op"] = "pickup_putdown_box";


            dict["pos1"] = JsonConvert.SerializeObject(new float[] { pos1.X * 1000f, -pos1.Y * 1000f, -pos1.Z * 1000f });
            dict["pos2"] = JsonConvert.SerializeObject(new float[] { pos2.X * 1000f, -pos2.Y * 1000f, -pos2.Z * 1000f });

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot1 = m1 * rot1 * m1;
            dict["rot1"] = JsonConvert.SerializeObject(new float[,] { { rot1.M11, rot1.M21, rot1.M31 }, { rot1.M12, rot1.M22, rot1.M32 }, { rot1.M13, rot1.M23, rot1.M33 } });

            rot2 = m1 * rot2 * m1;
            dict["rot2"] = JsonConvert.SerializeObject(new float[,] { { rot2.M11, rot2.M21, rot2.M31 }, { rot2.M12, rot2.M22, rot2.M32 }, { rot2.M13, rot2.M23, rot2.M33 } });

            dict["box"] = JsonConvert.SerializeObject(new float[,] { {box[0].X*1000f,-box[0].Y*1000f, -box[0].Z*1000f }, { box[1].X * 1000f, -box[1].Y * 1000f, -box[1].Z * 1000f }, 
                { box[2].X*1000f, -box[2].Y*1000f, -box[2].Z*1000f }, { box[3].X*1000f, -box[3].Y*1000f, -box[3].Z*1000f } });


            //string goto_command=JsonConvert.SerializeObject(dict);
            //File.WriteAllText("pos.txt", goto_command);
            commands.Add(dict);


        }
        public override void pick_up_pos_and_orient(Vector3 pos, Matrix3 rot, int arm_index)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["op"] = "pickup";


            dict["position"] = JsonConvert.SerializeObject(new float[] { pos.X, -pos.Y, -pos.Z });

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot = m1 * rot * m1;
            dict["rotation"] = JsonConvert.SerializeObject(new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } });
            //string goto_command=JsonConvert.SerializeObject(dict);
            //File.WriteAllText("pos.txt", goto_command);
            commands.Add(dict);


        }
        public override void save_pos_and_orient(Vector3 pos, Matrix3 rot, int arm_index)
        {

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["op"] = "remember";
            dict["position"] = JsonConvert.SerializeObject(new float[] { pos.X, -pos.Y, -pos.Z });


            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot = m1 * rot * m1;
            dict["rotation"] = JsonConvert.SerializeObject(new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } });
            commands.Add(dict);


        }
        public void goto_const()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["op"] = "goto_robot_axes";
            dict["pos"] = JsonConvert.SerializeObject(new float[] { -257.5747477174124f, 250.81529477922015f, 742.7444314092115f });
            dict["rot"] = JsonConvert.SerializeObject(new float[] { 0.7223817233644917f, 0.07569322232073164f, -0.5224305967903186f, 0.4466558556438759f });
            commands.Add(dict);
        }

        public override void associate_calib_point(int index, float x, float y, float z)
        {
            if ((calibration_points.Count != 0) && (pos != null))
            {
                if ((associated_points.Count != calibration_points.Count))
                {
                    associated_points.Clear();
                    for (int i = 0; i < calibration_points.Count; i++)
                    {
                        associated_points.Add(null);
                    }
                }

                if (index < calibration_points.Count)
                {
                    float[] ps = new float[6];
                    ps[0] = x * 1000.0f;
                    ps[1] = -y * 1000.0f;
                    ps[2] = -z * 1000.0f;

                    ps[3] = pos[0];
                    ps[4] = pos[1];
                    ps[5] = pos[2];

                    associated_points[index] = ps;
                }
            }
        }
        public override void actuator(object description)
        {
            string s = (string)description;
            Dictionary<string, string> command = new Dictionary<string, string>();
            command["op"] = s;
            commands.Add(command);
        }
        public override void requestAutoMode()
        {

            Dictionary<string, string> command = new Dictionary<string, string>();
            command["op"] = "ask_for_ml";
            commands.Add(command);

        }
    }
}
