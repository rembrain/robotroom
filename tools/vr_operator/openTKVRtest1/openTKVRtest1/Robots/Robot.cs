﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OpenTK;

namespace openTKVRtest1.Robots
{
    public class Robot
    {
        protected object locker = new object();
        public dynamic currect_readings;
        public List<float[]> calibration_points;
        public List<Dictionary<string,float[]>> obstacles;
        public List<float[]> associated_points;

        public Robot()
        {
            calibration_points = new List<float[]>();
            associated_points = new List<float[]>();
            obstacles = new List<Dictionary<string, float[]>>();
            Properties.Settings settings=Properties.Settings.Default;
            StringCollection strs=settings.calibration_points;
            if (strs != null)
            {
                for (int i = 0; i < strs.Count; i++)
                {
                    Newtonsoft.Json.Linq.JArray ar=((Newtonsoft.Json.Linq.JArray)JsonConvert.DeserializeObject(strs[i]));
                    float[] points = new float[ar.Count];
                    for (int j = 0; j < points.Length; j++)
                        points[j] = (float)ar[j];
                    calibration_points.Add(points);
                }
            }
       
        }
        public void savePoints()
        {
            StringCollection strs = new StringCollection();
            
            for (int i = 0; i < calibration_points.Count; i++){
                strs.Add(JsonConvert.SerializeObject(calibration_points[i]));    
            }
            
            Properties.Settings.Default.calibration_points = strs;
            Properties.Settings.Default.Save();
        }
        public void set_sensors(dynamic readings)
        {
            lock (locker)
            {
                currect_readings = readings;
                ParseReadings();
            }
        }
        protected virtual void ParseReadings()
        {
        }

        public virtual int get_joints_count()
        {return 0; }
        public virtual string get_state_machine()
        { return""; }
        public virtual string get_reason()
        { return ""; }
        public virtual double get_calibaccuracy()
        { return 0; }

        public virtual void goto_calib_point(int index)
        {
        }

        public virtual void associate_calib_point(int index, float x, float y, float z)
        {
        }
        public virtual void SendCalibrationData()
        {
        }
        public virtual void SendCalibrationData2d3d(float[,] points3D, int[,] points2D, float[,] camM)
        {
        }

        public virtual void goto_pos_and_orient(Vector3 pos, Matrix3 rot,int arm_index)
        { }
        public virtual void goto_pos_and_orient_in_robot(Vector3 pos, Matrix3 rot, int arm_index)
        { }
        public virtual void pick_up_pos_and_orient(Vector3 pos, Matrix3 rot, int arm_index)
        { }
        public virtual void pick_up_put_down_pos_and_orient(Vector3 pos1, Matrix3 rot1, Vector3 pos2, Matrix3 rot2, int arm_index)
        { }
        public virtual void pick_up_put_down_box(Vector3 pos1, Matrix3 rot1, Vector3 pos2, Matrix3 rot2, List<Vector4> box,int arm_index)
        { }
        public virtual void save_pos_and_orient(Vector3 pos, Matrix3 rot,int arm_index)
        { }
        public virtual void goto_pos(Vector3 pos)
        { }
        


        public virtual void actuator(object description)
        { }

        public virtual void requestAutoMode()
        { }
        public virtual void updateConfig()
        { }
        public virtual void send_bare_command(Dictionary<string, object> command) { }
        

    }
}
