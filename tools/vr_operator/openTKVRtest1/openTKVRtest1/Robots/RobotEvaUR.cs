﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OpenTK;

namespace openTKVRtest1.Robots
{
    public class RobotEvaUR : Robot
    {
        public RobotEvaUR(List<object> _commands, object _locker)
        {
            locker = _locker;
            commands = _commands;
            updateConfig();
        }

        public override void updateConfig()
        {
            string robot_name = Properties.Settings.Default.robot_name;
            string res = ConnectionUtils.get_api_request(Properties.Settings.Default.register_server, "config?robot=" + robot_name);

            Dictionary<string, Dictionary<string,string>> response = JsonConvert.DeserializeObject<Dictionary<string, Dictionary< string, string>>>(res);
            Dictionary<string, dynamic> config = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(response["answer"]["value"]);
            try
            {
                config_calib = config["calibration"].ToObject(typeof(float[,]));
                calib_accuracy = config["calibration_accuracy"];
            }
            catch
            { }
            
        }

        private float[] joints = null;
        private float[] pos = null;
        private float[] q = null;
        private Matrix3 R;
        private string error_state = "";
        private string state_machine_state = "";
        private string reason = "";
        private bool calibrated = false;
        private double calib_accuracy = 0;

        private float[,] config_calib = null;

        private List<object> commands;
        private object locker;


        protected override void ParseReadings()
        {
            joints = new float[6];pos = new float[3];q = new float[4];
            if (currect_readings != null)
            {
                var joints_ = currect_readings["joints"];
                var position = currect_readings["position"];
                var pos_ = position["position"];
                var q_ = position["orientation"];
                error_state = currect_readings["arm_state"];
                calibrated = currect_readings["calibrated"];

                for (int i = 0; i < 6; i++)
                    joints[i] = (float)joints_[i].Value*180.0f/(float)Math.PI;

                
                pos[0] = (float)pos_["x"].Value*1000.0f;
                pos[1] = (float)pos_["y"].Value * 1000.0f;
                pos[2] = (float)pos_["z"].Value * 1000.0f;
                q[0] = (float)q_["w"].Value;
                q[1] = (float)q_["x"].Value;
                q[2] = (float)q_["y"].Value;
                q[3] = (float)q_["z"].Value;
                
                R = Utils.convertQtoM33(q);

                state_machine_state = currect_readings["state_machine"];

                try
                {
                    reason = currect_readings["reason"];
                }
                catch { }
                

            }
            base.ParseReadings();
        }

        public void MoveJoints(float[] joints)
        {
            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
            dict["op"] = "setJ";
            float[] joints_ = new float[joints.Length];
            for (int i = 0; i < joints.Length; i++)
                joints_[i] = joints[i];

            dict["joints"] = joints_;
            lock (locker)
            {
                commands.Add(dict);
            }
        }
        public void SendCalibrationData_pairs(List<float[]> pairs)
        {
            lock (locker)
            {
                Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
                dict["op"] = "calibrate";
                dict["pairs"] = pairs;
                commands.Add(dict);
            }
        }
        public override void SendCalibrationData()
        {
            lock (locker)
            {
                Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
                dict["op"] = "calibrate";
                dict["pairs"] = associated_points;
                commands.Add(dict);
            }
        }
        
        public override int get_joints_count()
        {
            return 6;
        }

        public float[] get_joints()
        {
            lock (locker)
            {
                return joints;
            }
        }
        public string get_error()
        {
            return error_state;
        }

        public float[] get_pos()
        {
            lock (locker)
            {
                return pos;
            }
        }
        public float[] get_q()
        {
            lock (locker)
            {
                return q;
            }
        }
        public Matrix3 get_R()
        {
            lock (locker)
            {
                return R;
            }
        }
        public override string get_state_machine()
        {
            return state_machine_state;
        }
        public override void goto_calib_point(int index)
        {
            MoveJoints(calibration_points[index]);
        }
        public override void goto_pos_and_orient_in_robot(Vector3 pos, Matrix3 rot, int arm_index)
        {
            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();

            dict["op"] = "goto";

            dict["pos"] = new float[] { pos.X, pos.Y, pos.Z };

            dict["rot"] = new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } };
            dict["source"] = "operator";

            commands.Add(dict);
        }
        public override void goto_pos_and_orient(Vector3 pos, Matrix3 rot, int arm_index)
        {
            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();

            dict["op"] = "goto";

            float[] p=new float[] { pos.X * 1000.0f, -pos.Y * 1000.0f, -pos.Z * 1000.0f };
            Console.WriteLine(p[0].ToString()+" "+p[1].ToString()+" "+p[2].ToString());
            float[,] M = config_calib;
            dict["pos"] = new float[] {M[0,0]*p[0]+M[0,1]*p[1]+M[0,2]*p[2]+M[0,3],
                                        M[1,0]*p[0]+M[1,1]*p[1]+M[1,2]*p[2]+M[1,3],
                                        M[2,0]*p[0]+M[2,1]*p[1]+M[2,2]*p[2]+M[2,3]};



            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot = m1 * rot * m1;

            Matrix3 MC = new Matrix3(M[0, 0], M[1, 0], M[2, 0], M[0, 1], M[1, 1], M[2,1], M[0, 2], M[1, 2], M[2, 2]);
            Matrix3 res = rot * MC;


            dict["rot"] = new float[,] { { res.M11, res.M21, res.M31 }, { res.M12, res.M22, res.M32 }, { res.M13, res.M23, res.M33 } };
            dict["source"] = "operator";

            commands.Add(dict);


        }

        public override void pick_up_put_down_pos_and_orient(Vector3 pos1, Matrix3 rot1, Vector3 pos2, Matrix3 rot2, int arm_index)
        {
            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
            dict["op"] = "pickup_putdown";

            float[,] M = config_calib;
            float[] p = new float[] { pos1.X * 1000f, -pos1.Y * 1000f, -pos1.Z * 1000f };
            dict["pos1"]= new float[] {M[0,0]*p[0]+M[0,1]*p[1]+M[0,2]*p[2]+M[0,3],
                                        M[1,0]*p[0]+M[1,1]*p[1]+M[1,2]*p[2]+M[1,3],
                                        M[2,0]*p[0]+M[2,1]*p[1]+M[2,2]*p[2]+M[2,3]};
            p = new float[] { pos2.X * 1000f, -pos2.Y * 1000f, -pos2.Z * 1000f };
            dict["pos2"] = new float[] {M[0,0]*p[0]+M[0,1]*p[1]+M[0,2]*p[2]+M[0,3],
                                        M[1,0]*p[0]+M[1,1]*p[1]+M[1,2]*p[2]+M[1,3],
                                        M[2,0]*p[0]+M[2,1]*p[1]+M[2,2]*p[2]+M[2,3]};

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot1 = m1 * rot1 * m1;

            Matrix3 MC = new Matrix3(M[0, 0], M[1, 0], M[2, 0], M[0, 1], M[1, 1], M[2, 1], M[0, 2], M[1, 2], M[2, 2]);
            rot1 = rot1 * MC;

            dict["rot1"] = new float[,] { { rot1.M11, rot1.M21, rot1.M31 }, { rot1.M12, rot1.M22, rot1.M32 }, { rot1.M13, rot1.M23, rot1.M33 } };

            rot2 = m1 * rot2 * m1;
            rot2 = rot2 * MC;
            dict["rot2"] = new float[,] { { rot2.M11, rot2.M21, rot2.M31 }, { rot2.M12, rot2.M22, rot2.M32 }, { rot2.M13, rot2.M23, rot2.M33 } };

            dict["calibration"] = M;
            dict["source"] = "operator";

            commands.Add(dict);


        }
        public override void pick_up_put_down_box(Vector3 pos1, Matrix3 rot1, Vector3 pos2, Matrix3 rot2, List<Vector4> box, int arm_index)
        {
            if (box.Count != 4)
                throw new Exception("box.Count != 4");

            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
            dict["op"] = "pickup_putdown_box";
            float[,] M = config_calib;
            float[] p = new float[] { pos1.X * 1000f, -pos1.Y * 1000f, -pos1.Z * 1000f };
            dict["pos1"] = new float[] {M[0,0]*p[0]+M[0,1]*p[1]+M[0,2]*p[2]+M[0,3],
                                        M[1,0]*p[0]+M[1,1]*p[1]+M[1,2]*p[2]+M[1,3],
                                        M[2,0]*p[0]+M[2,1]*p[1]+M[2,2]*p[2]+M[2,3]};
            p = new float[] { pos2.X * 1000f, -pos2.Y * 1000f, -pos2.Z * 1000f };
            dict["pos2"] = new float[] {M[0,0]*p[0]+M[0,1]*p[1]+M[0,2]*p[2]+M[0,3],
                                        M[1,0]*p[0]+M[1,1]*p[1]+M[1,2]*p[2]+M[1,3],
                                        M[2,0]*p[0]+M[2,1]*p[1]+M[2,2]*p[2]+M[2,3]};

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot1 = m1 * rot1 * m1;

            Matrix3 MC = new Matrix3(M[0, 0], M[1, 0], M[2, 0], M[0, 1], M[1, 1], M[2, 1], M[0, 2], M[1, 2], M[2, 2]);
            rot1 = rot1 * MC;

            dict["rot1"] = new float[,] { { rot1.M11, rot1.M21, rot1.M31 }, { rot1.M12, rot1.M22, rot1.M32 }, { rot1.M13, rot1.M23, rot1.M33 } };

            rot2 = m1 * rot2 * m1;
            rot2 = rot2 * MC;
            dict["rot2"] = new float[,] { { rot2.M11, rot2.M21, rot2.M31 }, { rot2.M12, rot2.M22, rot2.M32 }, { rot2.M13, rot2.M23, rot2.M33 } };

            dict["calibration"] = M;


            float[,] b = new float[4,3];
            for (int i = 0; i < 4; i++)
            {
                p = new float[] { box[i].X * 1000f, -box[i].Y * 1000f, -box[i].Z * 1000f };
                p=new float[] {M[0,0]*p[0]+M[0,1]*p[1]+M[0,2]*p[2]+M[0,3],
                                        M[1,0]*p[0]+M[1,1]*p[1]+M[1,2]*p[2]+M[1,3],
                                        M[2,0]*p[0]+M[2,1]*p[1]+M[2,2]*p[2]+M[2,3]};
                b[i, 0] = p[0]; b[i, 1] = p[1]; b[i, 2] = p[2];
            }

            dict["box"] = b;
            dict["source"] = "operator";

            commands.Add(dict);


        }
        public override void pick_up_pos_and_orient(Vector3 pos, Matrix3 rot, int arm_index)
        {
            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
            dict["op"] = "pickup";


            dict["position"] = new float[] { pos.X, -pos.Y, -pos.Z };

            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot = m1 * rot * m1;
            dict["rotation"] = new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } };
            dict["source"] = "operator";
            commands.Add(dict);


        }
        public override void save_pos_and_orient(Vector3 pos, Matrix3 rot, int arm_index)
        {

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["op"] = "remember";
            dict["position"] = JsonConvert.SerializeObject(new float[] { pos.X, -pos.Y, -pos.Z });


            Matrix3 m1 = new Matrix3(1f, 0, 0, 0, -1f, 0, 0, 0, -1f);
            rot = m1 * rot * m1;
            dict["rotation"] = JsonConvert.SerializeObject(new float[,] { { rot.M11, rot.M21, rot.M31 }, { rot.M12, rot.M22, rot.M32 }, { rot.M13, rot.M23, rot.M33 } });
            dict["source"] = "operator";
            commands.Add(dict);


        }

        public override void associate_calib_point(int index, float x, float y, float z)
        {
            if ((calibration_points.Count != 0) && (pos != null))
            {
                if ((associated_points.Count != calibration_points.Count))
                {
                    associated_points.Clear();
                    for (int i = 0; i < calibration_points.Count; i++)
                    {
                        associated_points.Add(null);
                    }
                }

                if (index < calibration_points.Count)
                {
                    float[] ps = new float[6];
                    ps[0] = x * 1000.0f;
                    ps[1] = -y * 1000.0f;
                    ps[2] = -z * 1000.0f;

                    ps[3] = pos[0];
                    ps[4] = pos[1];
                    ps[5] = pos[2];

                    associated_points[index] = ps;
                }
            }
        }
        public override void actuator(object description)
        {
            string s = (string)description;
            Dictionary<string, string> command = new Dictionary<string, string>();
            command["op"] = s;
            command["source"] = "operator";
            commands.Add(command);
        }
        public override void send_bare_command(Dictionary<string,object> command)
        {
            commands.Add(command);
        }
        public override void requestAutoMode()
        {

            Dictionary<string, string> command = new Dictionary<string, string>();
            command["op"] = "ask_for_ml";
            command["source"] = "operator";
            commands.Add(command);

        }
        public override string get_reason()
        {
            return reason;
        }
        public override double get_calibaccuracy()
        {
            return calib_accuracy;
        }
    }
}

