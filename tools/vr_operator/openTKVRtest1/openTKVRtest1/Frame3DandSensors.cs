﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Newtonsoft.Json;


namespace openTKVRtest1
{
    public class Frame3DandSensors
    {
        public UInt32[] indices;
        public float[] vertices;
        public float[] uvs;
        public Bitmap bitmap;
        public Bitmap depth;
        public float[,] camM;
        public float[,] depthMap;
        public int w;
        public int h;
        public dynamic sensors_readings;

        

        
        
        public Frame3DandSensors(byte[] rgb, byte[] depth_png,string json,Decoder dec,ref int frame_index,bool JPG)
        {
            int width = Properties.Settings.Default.width;
            int height = Properties.Settings.Default.height;
            int width_rgb = Properties.Settings.Default.width_rgb;
            int height_rgb = Properties.Settings.Default.height_rgb;
            float kx = (float)Properties.Settings.Default.width / (float)Properties.Settings.Default.width_rgb;
            float ky = (float)Properties.Settings.Default.height / (float)Properties.Settings.Default.height_rgb;

            dynamic tail_object = JsonConvert.DeserializeObject(json);


            float fx = tail_object["fx"]*kx;
            float fy = tail_object["fy"] * ky;
            float cx = tail_object["ppx"] * kx;
            float cy = tail_object["ppy"] * ky;

            camM = new float[,] { { tail_object["fx"], 0, tail_object["ppx"] }, { 0, tail_object["fy"], tail_object["ppy"] }, { 0, 0, 1.0f } };

            uvs = new float[width*height*2];
            vertices= new float[width * height * 3];


            System.IO.MemoryStream stream = null;
            if (JPG)
            {
                stream = new System.IO.MemoryStream(rgb);
                bitmap = new Bitmap(stream);
            }
            else
            {

                if (tail_object["keyframe"].Value == 1)
                {
                    frame_index = 0;
                }
                if (frame_index != tail_object["frameindex"].Value)
                {
                    frame_index = 0;
                    bitmap = new Bitmap(width_rgb, height_rgb);
                }
                else
                {
                    byte[] rgb_decoded = dec.Decode(rgb);
                    bitmap = Utils.GetBitmapFromBytes(rgb_decoded, dec.m_width, dec.m_height);
                    frame_index += 1;
                }
            }
            
            
            this.w = width;
            this.h = height;
            

            stream= new System.IO.MemoryStream(depth_png);
            BitmapEx depth_ex = new BitmapEx(stream);
            byte[] array = depth_ex.ImageData;

            byte[] depth_rgb8 = new byte[width*height*3];
            depthMap = new float[width, height];

            for (var y = 0; y < height; y++)
                for (var x = 0; x < width; x++)
                {
                    int z = array[(x + y * width) * 2] + array[(x + y * width) * 2+1] * 256;
                    if (z < 5000)
                    {
                        depth_rgb8[3 * (x + y * width)] = (byte)(z * 255.0 / 2000);
                        depth_rgb8[3 * (x + y * width) + 1] = (byte)(z * 255.0 / 2000);
                        depth_rgb8[3 * (x + y * width) + 2] = (byte)(z * 255.0 / 2000);
                    }
                    else
                    {
                        depth_rgb8[3 * (x + y * width)] = 255;
                        depth_rgb8[3 * (x + y * width) + 1] = 255;
                        depth_rgb8[3 * (x + y * width) + 2] = 255;
                    }
                    depthMap[x, y] = z;
                }
            depth = Utils.GetBitmapFromBytes(depth_rgb8, width, height);

            if ( (depth.Width != width) || (depth.Height != height))
            {
                Console.WriteLine("depth.Width "+depth.Width.ToString()+ " "+ "depth.Height " + depth.Height.ToString());
                throw new Exception("depth width and height are not default");
            }
            if ((bitmap.Width != width_rgb) || (bitmap.Height != height_rgb))
            {
                Console.WriteLine("bitmap.Width " + bitmap.Width.ToString() + " " + "bitmap.Height " + bitmap.Height.ToString());
                throw new Exception("bitmap width and height are not default");
            }


            float zf = 0;
            int i = 0;
            float kdz = 0.15f;
            int L = 0;
            bool[] boolAr = new bool[width*height];
            float[] zar = new float[width * height];

            for (var y = 0; y < height; y++)
                for (var x = 0; x < width; x++)
                {
                    
                    zf=(float)((int)array[i*2]+256*(int)array[i * 2+1])/1000.0f;
                    
                    vertices[i * 3] = ((float)x - cx) * zf / fx;
                    vertices[i * 3+1] = -((float)y - cy) * zf / fy;
                    vertices[i * 3 + 2] = -zf;
                    
                    
                    zar[i] = zf;
                    uvs[i * 2] = (float)(x) / (float)width;
                    uvs[i * 2 + 1] = (float)y / (float)height;
                    i += 1;
                }
            i = 0;
            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    if ((x != 0)&&(y!=0))
                    {
                        float dz = kdz * zar[i];
                        if ((zar[i] != 0) && (zar[(i - 1)] != 0) && (zar[(i - width)] != 0) && (zar[(i - width - 1)] != 0) &&
                                ((Math.Abs(zar[(i - 1)] - zar[i]) < dz) && (Math.Abs(zar[(i - width)] - zar[i]) < dz) && (Math.Abs(zar[(i - width - 1)] - zar[i]) < dz)
                                && (Math.Abs(zar[(i - 1)] - zar[(i - width - 1)]) < dz)))
                        {
                            L += 6;
                            boolAr[i] = true;
                        }
                    }
                    i += 1;

                }

            indices = new UInt32[L];
            i = 0;
            int l1 = 0;
            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    if (boolAr[i])
                    {
                        indices[l1 * 6] = (UInt32)(i - width - 1);
                        indices[l1 * 6 + 1] = (UInt32)i;
                        indices[l1 * 6 + 2] = (UInt32)(i - width);
                        indices[l1 * 6 + 3] = (UInt32)(i - width - 1);
                        indices[l1 * 6 + 4] = (UInt32)(i - 1);
                        indices[l1 * 6 + 5] = (UInt32)(i);
                        l1 += 1;
                    }
                    i += 1;

                }

            //save bitmap and depth
            //depth.Save("temp/" + frame_index.ToString() + ".bmp");

            

           
        }

        ~Frame3DandSensors()
        {
            if(bitmap!=null)
                bitmap.Dispose();
            if (depth != null)
                depth.Dispose();
        }


    }
}
