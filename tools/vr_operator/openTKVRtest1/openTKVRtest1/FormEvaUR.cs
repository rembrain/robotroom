﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using OpenTK;
using Newtonsoft.Json;

namespace openTKVRtest1
{
    public partial class FormEvaUR : Form
    {

        float[] minJoints;
        float[] maxJoints = new float[6] { 180.0f, 74f, 47f, 180f, 11f, 180f};

        float[] evaHomePosition = new float[] { 0f, 52.5f, -125.4f, 0f, -78.5f, 0.0f };
        float[] urHomePosition = new float[] { 191.7995f, -77.74813f, -120.6078f, -21.28748f, 90.66904f, 0.0f };
        float[] xarmHomePosition = new float[] { -87f, -39f, -52f, 0f, 57f, 0f};
        public object locker = null;
        public List<object> images_to_show=null;

        float[,] camM = null;
        float[,] depthMap = null;

        string robotType = "";


        public FormEvaUR(string type)
        {
            robotType = type;

            InitializeComponent();
            pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox2.Image = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            backgroundWorker1.RunWorkerAsync();

            if (type == "Eva")
            {
                minJoints=new float[6] { -180f, -159f, -165f, -1805f, -159f, -180f };
                maxJoints= new float[6] { 180f, 74f, 47f, 180f, 11f, 180f };
            }
            if (type == "UR10e")
            {
                minJoints = new float[6] { -358f, -358f, -358f, -358f, -358f, -358f };
                maxJoints = new float[6] { 358f, 358f, 358f, 358f, 358f, 358f };
            }
            if (type == "XArm6")
            {
                minJoints = new float[6] { -358f, -115f, -223f, -358f, -95f, -358f };
                maxJoints = new float[6] { 358f, 118f,      9f,  358f, 178f,  358f };
            }
        }

        public Robots.RobotEvaUR robot = null;
        int timer1_idex = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            var joints = robot.get_joints();
            var pos = robot.get_pos();
            var q = robot.get_q();
            if(robot.currect_readings!=null)
                if (robot.currect_readings.ContainsKey("moving"))
                {
                    if ((bool)robot.currect_readings["moving"].Value)     
                        textBox4.Text = "moving";
                    else
                        textBox4.Text = "not moving";
                }

            if ((joints != null)&&(pos!=null)&&(q!=null))
            {
                string s = "";
                textBox5.Text = robot.get_error();
                textBox6.Text = robot.get_state_machine();
                if (robot.get_state_machine() == "NEED_OPERATOR")
                    textBox7.Text = robot.get_reason();
                else
                    textBox7.Text = "";

                if (robot.get_state_machine() == "NEED_OPERATOR")
                {
                    if (timer1_idex % 3 == 0)
                    {
                        if (textBox6.BackColor == Color.White)
                            textBox6.BackColor = Color.Orange;
                        else
                            textBox6.BackColor = Color.White;
                    }
                    timer1_idex++;
                }else
                    textBox6.BackColor = Color.White;
                for (int i = 0; i < joints.Length; i++)
                    s += joints[i] + " ";
                textBox1.Text = s;
                s = "";
                for (int i = 0; i < pos.Length; i++)
                    s += pos[i] + " ";
                for (int i = 0; i < q.Length; i++)
                    s += q[i] + " ";
                textBox2.Text = s;

                textBox13.Text =robot.get_calibaccuracy().ToString("0.00");



                if (listBox1.Items.Count != robot.calibration_points.Count)
                {
                    listBox1.Items.Clear();
                    for (int i = 0; i < robot.calibration_points.Count; i++)
                    {
                        s = "";
                        for (int j = 0; j < robot.calibration_points[i].Length; j++)
                            s += robot.calibration_points[i][j] + " ";
                        listBox1.Items.Add(s);
                    }
                }

                if (listBox2.Items.Count != robot.obstacles.Count)
                {
                    listBox1.Items.Clear();
                    for (int i = 0; i < robot.calibration_points.Count; i++)
                    {
                        s = "";
                        for (int j = 0; j < robot.calibration_points[i].Length; j++)
                            s += robot.calibration_points[i][j] + " ";
                        listBox1.Items.Add(s);
                    }
                }

                textBox11.Text = robot.get_R().ToString();
                textBox12.Text = next_calib_point_index.ToString();

                
                /*if (plus_press)
                {
                    if ((curArm != -1) && (currentAxis != -1))
                    {
                        if (curArm == 1)
                        {
                            joints[currentAxis] += 2.0f;
                            robot.MoveJoints(joints);
                        }
                    }
                }
                if (minus_press)
                {
                    if ((curArm != -1) && (currentAxis != -1))
                    {
                        if (curArm == 1)
                        {
                            joints[currentAxis] -= 2.0f;
                            robot.MoveJoints(joints);
                        }
                    }
                }*/
            }
            
        }

        int currentAxis = -1;
        int curArm = -1;
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 0;
            curArm = 0;
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 1;
            curArm = 0;
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 2;
            curArm = 0;
        }
        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 3;
            curArm = 0;
        }
        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 4;
            curArm = 0;
        }
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 5;
            curArm = 0;
        }


        void MouseWheel(object sender, MouseEventArgs args)
        {
            
        }

        bool plus_press = false;
        bool minus_press = false;

        private void button4_MouseDown(object sender, MouseEventArgs e)
        {
            plus_press = true;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            plus_press = false;
        }

        private void button4_MouseUp(object sender, MouseEventArgs e)
        {
            plus_press = false;
        }

        private void button2_MouseDown(object sender, MouseEventArgs e)
        {
            minus_press = true;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            minus_press = false;
        }

        private void button2_MouseUp(object sender, MouseEventArgs e)
        {
            minus_press = false;
        }

        //delete all points
        private void button7_Click(object sender, EventArgs e)
        {
            robot.calibration_points.Clear();
            robot.savePoints();
        }
        //add current point
        private void button8_Click(object sender, EventArgs e)
        {
            float[] temp=robot.get_joints();
            float[] clone = new float[temp.Length];
            temp.CopyTo(clone, 0);
            robot.calibration_points.Add(clone);
            robot.savePoints();
        }

        void sendBin(string name)
        {
            dynamic points = JsonConvert.DeserializeObject(File.ReadAllText("obs.txt"));

            if (points.Count != 1)
                MessageBox.Show("You need to set just one bin");
            else
            {

                //get config

                /*string res = ConnectionUtils.post_api_request(Properties.Settings.Default.register_server, "config?robot=" + Properties.Settings.Default.robot_name);
                dynamic temp = JsonConvert.DeserializeObject(res);
                Dictionary<string, dynamic> config = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(temp["value"].Value);

                Matrix4 m4 = new Matrix4((float)config["calibration"][0][0], (float)config["calibration"][0][1], (float)config["calibration"][0][2], (float)config["calibration"][0][3],
                    (float)config["calibration"][1][0], (float)config["calibration"][1][1], (float)config["calibration"][1][2], (float)config["calibration"][1][3],
                    (float)config["calibration"][2][0], (float)config["calibration"][2][1], (float)config["calibration"][2][2], (float)config["calibration"][2][3],
                    0, 0, 0, 1f);


                int i = 0;
                List<object> results = new List<object>();



                List<float[]> points3d = new List<float[]>();

                foreach (dynamic pointset in points)
                {
                    Vector4 p1_ = new Vector4((float)pointset[0][0] * 1000f, -(float)pointset[0][1] * 1000f, -(float)pointset[0][2] * 1000f, 1f);
                    p1_ = Vector4.Transform(m4, p1_);
                    Vector3 p1 = new Vector3(p1_.X, p1_.Y, p1_.Z);

                    Vector4 p2_ = new Vector4((float)pointset[1][0] * 1000f, -(float)pointset[1][1] * 1000f, -(float)pointset[1][2] * 1000f, 1f);
                    p2_ = Vector4.Transform(m4, p2_);
                    Vector3 p2 = new Vector3(p2_.X, p2_.Y, p2_.Z);

                    Vector4 p3_ = new Vector4((float)pointset[2][0] * 1000f, -(float)pointset[2][1] * 1000f, -(float)pointset[2][2] * 1000f, 1f);
                    p3_ = Vector4.Transform(m4, p3_);
                    Vector3 p3 = new Vector3(p3_.X, p3_.Y, p3_.Z);

                    Vector4 p4_ = new Vector4((float)pointset[3][0] * 1000f, -(float)pointset[3][1] * 1000f, -(float)pointset[3][2] * 1000f, 1f);
                    p4_ = Vector4.Transform(m4, p4_);
                    Vector3 p4 = new Vector3(p4_.X, p4_.Y, p4_.Z);

                    Vector3 pO = p1;
                    Vector3 ox = p2 - pO;
                    Vector3 oy_ = p3 - pO;
                    Vector3 oz_ = p4 - pO;

                    Vector3 oz = Vector3.Cross(ox,oy_);
                    oz.Normalize();
                    oz *= oz_.Length;

                    Vector3 oy = -Vector3.Cross(ox, oz);
                    oy.Normalize();
                    oy *= oy_.Length;


                    points3d.Add(new float[] { pO.X, pO.Y, pO.Z });
                    points3d.Add(new float[] { pO.X+ox.X, pO.Y+ox.Y, pO.Z+ox.Z });
                    points3d.Add(new float[] { pO.X + oy.X, pO.Y + oy.Y, pO.Z + oy.Z });
                    points3d.Add(new float[] { pO.X + oz.X, pO.Y + oz.Y, pO.Z + oz.Z });
                    points3d.ToArray();
                }
                string bin_name = name;

                if (!config.ContainsKey(bin_name))
                {
                    config.Add(bin_name, points3d);
                }
                else
                    config[bin_name] = points3d;

                string value = JsonConvert.SerializeObject(config);
                res = ConnectionUtils.post_api_request(Properties.Settings.Default.register_server, "set_config?robot=" + Properties.Settings.Default.robot_name
                    + "&value=" + value, Properties.Settings.Default.LastSession);
                res = res;
                */

            }
        }
        private void button3_Click_1(object sender, EventArgs e)
        {
            sendBin("bin");
            
        }

        //PLUS
        private void button4_Click(object sender, EventArgs e)
        {
             if(currentAxis != -1)
             {
                var joints = robot.get_joints();
                if (joints[currentAxis] < maxJoints[currentAxis])
                {
                    joints[currentAxis] += 2.0f;
                    robot.MoveJoints(joints);
                }
             }
        }
        //MINUS
        private void button2_Click(object sender, EventArgs e)
        {
            if (currentAxis != -1)
             {
                var joints = robot.get_joints();
                if (joints[currentAxis] > minJoints[currentAxis])
                {
                    joints[currentAxis] -= 2.0f;
                    robot.MoveJoints(joints);
                }
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if ((images_to_show != null) && (locker != null))
            {
                var start=DateTime.Now;

                Bitmap rgb = null;
                Bitmap depth = null;
                lock (locker)
                {
                    if (images_to_show.Count > 0)
                    {
                        rgb = (Bitmap)((List<object>)images_to_show[0])[0];
                        depth = (Bitmap)((List<object>)images_to_show[0])[1];
                        camM = (float[,])((List<object>)images_to_show[0])[2];
                        depthMap= (float[,])((List<object>)images_to_show[0])[3];




                    }
                }
                if ((rgb != null) && (depth != null))
                {
                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.DrawImage(rgb, 0f, 0f, (float)pictureBox1.Width, (float)pictureBox1.Height);
                    }
                    pictureBox1.Image = bmp;
                    rgb.Dispose();
                    bmp = new Bitmap(pictureBox2.Image);
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.DrawImage(depth, 0f, 0f, (float)pictureBox2.Width, (float)pictureBox2.Height);
                    }
                    pictureBox2.Image = bmp;
                    depth.Dispose();

                    lock (locker)
                    {
                        images_to_show.RemoveAt(0);
                    }
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (currentAxis != -1)
            {
                var joints = robot.get_joints();
                if (joints[currentAxis] < maxJoints[currentAxis])
                {
                    joints[currentAxis] += 10.0f;
                    robot.MoveJoints(joints);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (currentAxis != -1)
            {
                var joints = robot.get_joints();
                if (joints[currentAxis] > minJoints[currentAxis])
                {
                    joints[currentAxis] -= 10.0f;
                    robot.MoveJoints(joints);
                }
            }
        }

        int cur_calib_index = -1;
        
        private void button6_Click(object sender, EventArgs e)
        {
            cur_calib_index = int.Parse(textBox3.Text);
            if (cur_calib_index < robot.calibration_points.Count)
            {
                robot.goto_calib_point(cur_calib_index);

            }
            else
                cur_calib_index = -1;
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (robotType == "XArm6")
            {
                robot.MoveJoints(xarmHomePosition);
            }
            if (robotType == "UR10e")
            {
                robot.MoveJoints(urHomePosition);
                
             }
            if (robotType == "Eva")
            {
                robot.MoveJoints(evaHomePosition);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            robot.actuator("reset_state");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            robot.actuator("ask_for_ml_continual");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            robot.updateConfig();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            robot.actuator("ask_for_idle");
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> command=new Dictionary<string, object>();
            command.Add("op", "ask_for_operator");
            command.Add("source", "operator");
            command.Add("reason", "operator request");
            robot.send_bare_command(command);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            robot.actuator("ask_for_ml_one_shot");
        }

        private void button17_Click(object sender, EventArgs e)
        {
                sendBin("bin_source");
        }

        int next_calib_point_index = 0;
        float[] start_calib_pos = null;
        Matrix3 start_calibr_R ;
        List<Vector3[]> calib_pairs = new List<Vector3[]>();
        Vector3[] cur_pair = null;

        private void button18_Click(object sender, EventArgs e)
        {
            int xi=0,yi=0,zi = 0;
            switch (next_calib_point_index)
            {
                case 1:
                    xi = 1;
                    break;
                case 2:
                    xi = 1;
                    yi = 1;
                    break;
                case 3:
                    xi = 0;
                    yi = 1;
                    break;
                case 4:
                    xi = 0;
                    yi = 1;
                    zi = 1;
                    break;
                case 5:
                    xi = 1;
                    yi = 1;
                    zi = 1;
                    break;
                case 6:
                    xi = 1;
                    yi = 0;
                    zi = 1;
                    break;
                case 7:
                    xi = 0;
                    xi = 0;
                    xi = 0;
                    xi = 0;
                    yi = 0;
                    zi = 1;
                    break;
            }
            if ((next_calib_point_index == 0)&&(start_calib_pos==null))
            {
                start_calib_pos= robot.get_pos();
                start_calibr_R = robot.get_R();
            }
            if ((next_calib_point_index == 0)&&(calib_pairs.Count>0))
            {
                if (robotType == "Eva")
                    robot.MoveJoints(evaHomePosition);
                else if (robotType == "UR10e")
                    robot.MoveJoints(urHomePosition);
                else if (robotType == "XArm6")
                    robot.MoveJoints(xarmHomePosition);
                else
                    throw new Exception("Unknown robot type");
                Thread.Sleep(3000);
                calib_pairs = calib_pairs;
                List<float[]> pairs = new List<float[]>();
                for (int i = 0; i < calib_pairs.Count; i++)
                {
                    float[] ps = new float[6];
                    ps[0] = calib_pairs[i][0].X; ps[1] = calib_pairs[i][0].Y; ps[2] = calib_pairs[i][0].Z;
                    ps[3] = calib_pairs[i][1].X; ps[4] = calib_pairs[i][1].Y; ps[5] = calib_pairs[i][1].Z;
                    pairs.Add(ps);
                }
                robot.SendCalibrationData_pairs(pairs);
                calib_pairs.Clear();
                cur_pair = null;
                return;
            }

            bool X = true;
            bool plus = true;
            if (radioButton7.Checked) { X = true; plus = true; }
            if (radioButton8.Checked) { X = true; plus = false; }
            if (radioButton9.Checked) { X = false; plus = true; }
            if (radioButton10.Checked) { X = false; plus = false; }

            float W = float.Parse(textBox9.Text);
            float H = float.Parse(textBox10.Text);
            float D = float.Parse(textBox14.Text);

            float[] pos = new float[3];
            start_calib_pos.CopyTo(pos, 0);

            float k = 1.0f;
            if (!plus) k = -1.0f;

            pos[2] -= (float)zi * D;
            if (X)
            {
                pos[0] += (float)yi * H * k;
                pos[1] += ((float)xi - 0.5f) * W;
            }
            else
            {
                pos[0]+= ((float)xi - 0.5f) *  W;
                pos[1] += (float)yi * H * k;
            }

            robot.goto_pos_and_orient_in_robot(new Vector3(pos[0], pos[1], pos[2]), start_calibr_R,0);

            cur_pair = new Vector3[2];
            calib_pairs.Add(cur_pair);


            next_calib_point_index++;
            if (next_calib_point_index == 8)
                next_calib_point_index = 0;
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (robotType == "XArm6")
            {
                robot.MoveJoints(new float[] { -100f, -15f, -66f, 0f, 56f, -14f });
            }
            if (robotType == "UR10e")
            {
                robot.MoveJoints(new float[] {175f,- 91.4f, -128.6078f, -11.28748f, 90.66904f, -45.0f });

            }
            if (robotType == "Eva")
            {
                robot.MoveJoints(new float[] { -0.0f, 41.46681f, -138.4183f, -1.279946f, -78.48323f, 0.0f });
            }
            next_calib_point_index = 0;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if ((start.X >= 0) && (end.X >= 0))e.Graphics.DrawLine(Pens.Green, start, end);

            if (start.X >= 0)e.Graphics.FillEllipse(new SolidBrush(Color.Red), start.X-3, start.Y-3, 7, 7);
            if(end.X>=0)e.Graphics.FillEllipse(new SolidBrush(Color.Blue), end.X-3, end.Y-3, 7, 7);

        }

        Point start=new Point(-1,-1);
        Point end = new Point(-1,-1);
        bool pressed = false;
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                pressed = true;
                end = new Point(-1, -1);
                start = new Point(e.X, e.Y);
            }
            else
            {
                Vector3 v=Utils.GetXYZ(e.Location, camM, depthMap);
                MessageBox.Show(e.X.ToString() + " " + e.Y.ToString()+"\n"+v.X.ToString()+" "+v.Y.ToString()+" "+v.Z.ToString(),"pos");
            }
        }

        Vector3 prev_point = new Vector3();

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            pressed = false;
            float r = 0f;
            if (robotType == "UR10e")
                r = 10.5f;
            if (robotType == "XArm6")
                r = 16f;

            Vector3 p3d=Utils.CalcCircleCenter(start, end, camM, depthMap, r);

            if (p3d.Z == 0)
                MessageBox.Show("a gap in depth map, try again");
            else
            {
                float shift = 0;
                if (float.TryParse(textBox8.Text, out shift))
                {
                    float[] t = robot.get_pos();
                    Vector3 robot_pos = new Vector3(t[0], t[1], t[2]);
                    Vector3 shift_v = new Vector3(0, 0, shift);
                    Matrix3 R = robot.get_R().Inverted();
                    robot_pos = robot_pos + R * shift_v;

                    cur_pair[0] = p3d;
                    cur_pair[1] = robot_pos;

                    Console.WriteLine(p3d.X.ToString() + " " + p3d.Y.ToString() + " " + p3d.Z.ToString());
                    if (prev_point.X != 0)
                    {
                        Console.WriteLine((p3d - prev_point).Length.ToString());
                        prev_point = p3d;
                    }
                    else
                        prev_point = p3d;

                } else
                    MessageBox.Show("You should enter shift");
                
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if(pressed)
                end = new Point(e.X, e.Y);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            pressed = false;
        }

        private void button20_Click(object sender, EventArgs e)
        {
            robot.actuator("update_config");
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void button21_Click(object sender, EventArgs e)
        {
            robot.actuator("go_home_safely");
        }

        private void radioButton10_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button22_Click(object sender, EventArgs e)
        {
            robot.actuator("remember_pick");
        }

        private void button23_Click(object sender, EventArgs e)
        {
            robot.actuator("vacuum_on");
        }

        private void button24_Click(object sender, EventArgs e)
        {
            robot.actuator("vacuum_off");
        }
    }
}
