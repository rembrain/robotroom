﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace openTKVRtest1
{
    public enum UI_states { MainMenu,Calibration_mode,YuMi_mode,save_mode , obstacles_mode};

    /// <summary>
    /// UI state machine is required to control states of the UI
    /// The main purpose of this abstraction is just to mantain visible state of all UI elements in adequate state
    /// </summary>
    public class UI_StateMachine
    {
        List<UI_elements.UI_element> m_static_elements;
        List<UI_elements.UI_element>[] m_controllers_UI;

        public UI_states currentState = UI_states.MainMenu;
        public UI_StateMachine(List<UI_elements.UI_element> static_elements, List<UI_elements.UI_element>[] controllers_UI)
        {
            m_static_elements = static_elements;
            m_controllers_UI = controllers_UI;
        }

        public void setState(UI_states state)
        {
            if (state == UI_states.MainMenu)
            {
                getControlByName("main_menu_button").visible = false;
                getControlByName("calib_mode_button").visible = true;
                getControlByName("yumi_mode_button").visible = true;
                getControlByName("obstacle_mode_button").visible = true;
                getControlByName("yumi_mode_mode_button").visible = false;
                getControlByName("yumi_mode_moving_button").visible = false;
                getControlByName("yumi_mode_notmoving_button").visible = false;
                getControlByName("yumi_mode_pickup_button").visible = false;
                getControlByName("yumi_mode_pickbox_button").visible = false;
                getControlByName("yumi_mode_free_button").visible = false;
                getControlByName("save_mode_button").visible = true;
                getControlByName("calib_point_button").visible = false;
                getControlByName("calib_perform_button").visible = false;
                getControlByName("calib_status_button").visible = false;
                getControlByName("box_cursor_right").visible = false;
                getControlByName("box_cursor_left").visible = false;
                
                getControlByName("obst_point_button").visible = false;

                UI_elements.UI_element cursor = getControlByName("static_cursor");
                if (cursor != null)
                    cursor.visible = false;

                UI_elements.UI_element cursor_and_box = getControlByName("static_cursor_and_box");
                if (cursor_and_box != null)
                    cursor_and_box.visible = false;

                cursor = getControlByName("box_ball_cursor");
                if (cursor != null)
                    cursor.visible = false;
                currentState = UI_states.MainMenu;
                
            }
            if (state == UI_states.Calibration_mode)
            {
                getControlByName("main_menu_button").visible = true;
                getControlByName("calib_mode_button").visible = false;
                getControlByName("yumi_mode_button").visible = false;
                getControlByName("obstacle_mode_button").visible = false;
                getControlByName("yumi_mode_moving_button").visible = false;
                getControlByName("yumi_mode_notmoving_button").visible = false;
                getControlByName("yumi_mode_mode_button").visible = false;
                getControlByName("yumi_mode_pickup_button").visible = false;
                getControlByName("yumi_mode_pickbox_button").visible = false;
                getControlByName("yumi_mode_free_button").visible = false;
                getControlByName("save_mode_button").visible = false;
                getControlByName("calib_point_button").visible = true;
                getControlByName("calib_perform_button").visible = true;
                getControlByName("calib_status_button").visible = true;
                getControlByName("box_cursor_right").visible = false;
                getControlByName("box_cursor_left").visible = false;
                getControlByName("obst_point_button").visible = false;
                UI_elements.UI_element cursor = getControlByName("static_cursor");
                if (cursor != null)
                    cursor.visible = true;
                UI_elements.UI_element cursor_and_box = getControlByName("static_cursor_and_box");
                if (cursor_and_box != null)
                    cursor_and_box.visible = true;
                cursor = getControlByName("box_ball_cursor");
                if (cursor != null)
                    cursor.visible = false;

                currentState = UI_states.Calibration_mode;
            }
            if (state == UI_states.YuMi_mode)
            {
                getControlByName("main_menu_button").visible = true;
                getControlByName("calib_mode_button").visible = false;
                getControlByName("yumi_mode_button").visible = false;
                getControlByName("obstacle_mode_button").visible = false;
                getControlByName("yumi_mode_moving_button").visible = false;
                getControlByName("yumi_mode_notmoving_button").visible = false;
                getControlByName("yumi_mode_mode_button").visible = true;
                getControlByName("yumi_mode_pickup_button").visible = true;
                getControlByName("yumi_mode_pickbox_button").visible = true;
                getControlByName("yumi_mode_free_button").visible = true;
                getControlByName("save_mode_button").visible = false;
                getControlByName("calib_point_button").visible = false;
                getControlByName("calib_perform_button").visible = false;
                getControlByName("calib_status_button").visible = false;
                getControlByName("box_cursor_right").visible = true;
                getControlByName("box_cursor_left").visible = true;
                getControlByName("obst_point_button").visible = false;
                UI_elements.UI_element cursor = getControlByName("static_cursor");
                if (cursor != null)
                    cursor.visible = false;
                UI_elements.UI_element cursor_and_box = getControlByName("static_cursor_and_box");
                if (cursor_and_box != null)
                    cursor_and_box.visible = false;
                cursor = getControlByName("box_ball_cursor");
                if (cursor != null)
                    cursor.visible = false;

                currentState = UI_states.YuMi_mode;
            }
            if (state == UI_states.save_mode)
            {
                getControlByName("main_menu_button").visible = true;
                getControlByName("calib_mode_button").visible = false;
                getControlByName("yumi_mode_button").visible = false;
                getControlByName("obstacle_mode_button").visible = false;
                getControlByName("yumi_mode_moving_button").visible = false;
                getControlByName("yumi_mode_notmoving_button").visible = false;
                getControlByName("yumi_mode_mode_button").visible = true;
                getControlByName("yumi_mode_pickup_button").visible = false;
                getControlByName("yumi_mode_pickbox_button").visible = false;
                getControlByName("yumi_mode_free_button").visible = false;
                getControlByName("save_mode_button").visible = false;
                getControlByName("calib_point_button").visible = false;
                getControlByName("calib_perform_button").visible = false;
                getControlByName("calib_status_button").visible = false;
                getControlByName("box_cursor_right").visible = true;
                getControlByName("box_cursor_left").visible = true;
                getControlByName("obst_point_button").visible = false;
                UI_elements.UI_element cursor = getControlByName("static_cursor");
                if (cursor != null)
                    cursor.visible = false;
                UI_elements.UI_element cursor_and_box = getControlByName("static_cursor_and_box");
                if (cursor_and_box != null)
                    cursor_and_box.visible = false;
                cursor = getControlByName("box_ball_cursor");
                if (cursor != null)
                    cursor.visible = false;

                currentState = UI_states.save_mode;
            }
            if (state == UI_states.obstacles_mode)
            {
                getControlByName("main_menu_button").visible = true;
                getControlByName("calib_mode_button").visible = false;
                getControlByName("yumi_mode_button").visible = false;
                getControlByName("obstacle_mode_button").visible = false;
                getControlByName("yumi_mode_moving_button").visible = false;
                getControlByName("yumi_mode_notmoving_button").visible = false;
                getControlByName("yumi_mode_mode_button").visible = false;
                getControlByName("yumi_mode_pickup_button").visible = false;
                getControlByName("yumi_mode_pickbox_button").visible = false;
                getControlByName("yumi_mode_free_button").visible = false;
                getControlByName("save_mode_button").visible = false;
                getControlByName("calib_point_button").visible = false;
                getControlByName("calib_perform_button").visible = false;
                getControlByName("calib_status_button").visible = false;
                getControlByName("box_cursor_right").visible = false;
                getControlByName("box_cursor_left").visible = false;
                getControlByName("obst_point_button").visible = true;


                UI_elements.UI_element cursor = getControlByName("static_cursor");
                if (cursor != null)
                    cursor.visible = false;
                UI_elements.UI_element cursor_and_box = getControlByName("static_cursor_and_box");
                if (cursor_and_box != null)
                    cursor_and_box.visible = false;
                cursor = getControlByName("box_ball_cursor");
                if (cursor != null)
                    cursor.visible = false;

                currentState = UI_states.obstacles_mode;
            }
        }

        private UI_elements.UI_element getControlByName(string name)
        {
            for (int i = 0; i < m_static_elements.Count; i++)
            {
                if (m_static_elements[i].name == name)
                    return m_static_elements[i];
            }
            for (int i = 0; i < m_controllers_UI.Length; i++)
                for (int j = 0; j < m_controllers_UI[i].Count; j++)
                {
                    if (m_controllers_UI[i][j].name == name)
                        return m_controllers_UI[i][j];
                }
            return null;
        }
    }
}
