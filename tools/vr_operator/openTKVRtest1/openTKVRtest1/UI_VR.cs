﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using Newtonsoft.Json;
using System.IO;

namespace openTKVRtest1
{
    public class UI_VR
    {
        public List<UI_elements.UI_element> static_elements = new List<UI_elements.UI_element>();
        public List<UI_elements.UI_element>[] controllers_UI = new List<UI_elements.UI_element>[2];

        //lighting
        float[] specularPosition =new float[] { 1.0f, 3.0f, 3.0f, 1.0f };
        float[] ambientColor = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
        public Vector4 pointerShift = new Vector4(0f, 0f, -0.07f, 1.0f);
        public Vector4 pointerShift_box = new Vector4(0f, 0f, -0.03f, 1.0f);

        Robots.Robot robot = null;
        UI_StateMachine ui_StateMachine;

        Vector3 current_cursor_position_left;
        Matrix3 current_cursor_orientation_left;
        Vector3 current_cursor_position_right;
        Matrix3 current_cursor_orientation_right;

        Matrix4 vr_to_camera_matrix ;

        public UI_VR(Robots.Robot rs,Matrix4 objectMatrix)
        {
            robot = rs;
            vr_to_camera_matrix=objectMatrix.Inverted();
            

            for (int i = 0; i < controllers_UI.Length; i++)
                controllers_UI[i] = new List<UI_elements.UI_element>();
        }
        public void Init()
        {
            GL.Enable(EnableCap.Normalize);
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("main_menu_button", new Vector3(-0.5f, 1.5f, -0.2f), 0.1f, 0.1f, 0.03f, /*Active*/true,
                                "images/main_menu.png", "images/main_menu_chosen.png", onClick: onMainMenuClick)));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("calib_mode_button", new Vector3(-0.3f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/true,
                                "images/calibration_mode.png", "images/calibration_mode_chosen.png", onClick: onCalibraionClick)));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_button", new Vector3(-0.0f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f,/*Active*/ true,
                                "images/yumi_mode.png", "images/yumi_mode_chosen.png",onClick:onYumiMode)));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("save_mode_button", new Vector3(0.3f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f,/*Active*/true,
                               Utils.GenerateTexture("save", false), Utils.GenerateTexture("save", true), onClick: onSaveMode)));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("obstacle_mode_button", new Vector3(0.6f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f,/*Active*/true,
                               Utils.GenerateTexture("obstacles", false), Utils.GenerateTexture("obstacles", true), onClick: onObstacleMode)));

            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("calib_point_button", new Vector3(-0.3f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f,/*Active*/true,
                                Utils.GenerateTexture("Point 0",false), Utils.GenerateTexture("Point 0", true),onClick:onPointClick)));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("calib_perform_button", new Vector3(0f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/true,
                                Utils.GenerateTexture("Perform", false), Utils.GenerateTexture("Perform", true),onClick:onPerformCalib)));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("calib_status_button", new Vector3(0.3f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/false,
                                Utils.GenerateTexture("status", false), Utils.GenerateTexture("status", true))));

            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_pickup_button", new Vector3(-0.25f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/true,
                                Utils.GenerateTexture("pick up", false), Utils.GenerateTexture("pick up", true), onClick: onPickUpMode)));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_pickbox_button", new Vector3(-0.0f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/true,
                                Utils.GenerateTexture("pick box", false), Utils.GenerateTexture("pick box", true), onClick: onPickBoxMode)));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_free_button", new Vector3(0.25f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/true,
                                Utils.GenerateTexture("free", false), Utils.GenerateTexture("free", true),onClick:onFreeMode)));

            if(this.Yumi_free_mode==Yumi_mode.free)
                static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_mode_button", new Vector3(0.5f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/false,
                                Utils.GenerateTexture("free", false), Utils.GenerateTexture("free", true))));
            if (this.Yumi_free_mode == Yumi_mode.pickbox)
                static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_mode_button", new Vector3(0.5f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/false,
                                Utils.GenerateTexture("pickbox", false), Utils.GenerateTexture("pickbox", true))));
            if (this.Yumi_free_mode == Yumi_mode.pickup)
                static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_mode_button", new Vector3(0.5f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/false,
                                Utils.GenerateTexture("pickup", false), Utils.GenerateTexture("pickup", true))));

            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_notmoving_button", new Vector3(0.75f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/false,
                                Utils.GenerateTexture("not moving", false), Utils.GenerateTexture("not moving", false))));
            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("yumi_mode_moving_button", new Vector3(0.75f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f, /*Active*/false,
                                Utils.GenerateTexture("moving", false), Utils.GenerateTexture("moving", false))));

            static_elements.Add((UI_elements.UI_element)(new UI_elements.UI_button("obst_point_button", new Vector3(-0.3f, 1.5f, -0.2f), 0.2f, 0.1f, 0.03f,/*Active*/true,
                                Utils.GenerateTexture("Obst 0", false), Utils.GenerateTexture("save", true), onClick: onObstClick)));




            controllers_UI[0].Add((UI_elements.UI_element)new UI_elements.UI_pointer_ball("ball_cursor_left",new Vector3(pointerShift.X, pointerShift.Y, pointerShift.Z), 0.0025f,new float[3] { 1.0f,0f,0f}));
            controllers_UI[0].Add((UI_elements.UI_element)new UI_elements.UI_pointer_box("box_cursor_left", new Vector3(pointerShift_box.X, pointerShift_box.Y, pointerShift_box.Z), 0.02f,0.005f,0.02f, new float[3] { 1.0f, 0f, 0f }));

            controllers_UI[1].Add((UI_elements.UI_element)new UI_elements.UI_pointer_ball("ball_cursor_right", new Vector3(pointerShift.X, pointerShift.Y, pointerShift.Z), 0.0025f, new float[3] { 0.0f, 1f, 0.0f }));
            controllers_UI[1].Add((UI_elements.UI_element)new UI_elements.UI_pointer_box("box_cursor_right", new Vector3(pointerShift_box.X, pointerShift_box.Y, pointerShift_box.Z), 0.02f, 0.005f, 0.02f, new float[3] { 0.0f, 1f, 0.0f }));


            ui_StateMachine = new UI_StateMachine(static_elements,controllers_UI);
            ui_StateMachine.setState(UI_states.MainMenu);
        }
        public void Dispose()
        {
            for (int i = 0; i < controllers_UI.Length; i++)
                for (int j = 0; j < controllers_UI[i].Count; j++)
                    controllers_UI[i][j].Dispose();
            for (int i = 0; i < static_elements.Count; i++)
                static_elements[i].Dispose();
        }

        
        public void handleCursorPosition(Vector3 position,Matrix3 orientation,int index)
        {
           
            if (index == 0)
            {
                current_cursor_position_left = position;
                for (int i = 0; i < static_elements.Count; i++)
                {
                    if(static_elements[i].active)
                        static_elements[i].updateState(position);
                }
                current_cursor_orientation_left = orientation;
            }
            if (index == 1)
            {
                current_cursor_position_right = position;
                current_cursor_orientation_right = orientation;


            }
        }

        public void Click(int arm_index)
        {
            bool button_clicked = false;
            if (arm_index == 0)
            {
                

                bool[] visible = new bool[static_elements.Count];
                for (int i = 0; i < static_elements.Count; i++)
                {
                    if (static_elements[i].visible)
                        visible[i] = true;
                }
                for (int i = 0; i < static_elements.Count; i++)
                {
                    if (static_elements[i].active && visible[i])
                    {
                        if (static_elements[i].click())
                            button_clicked = true;
                    }
                }

                
            }
            //perform click tasks for different modes
            if (!button_clicked)
            {
                onClickSpace(arm_index);
            }
        }
        public void TouchPad(int arm_index)
        {
            onTouchPad(arm_index);
        }
        public void Grip(int arm_index)
        {
            onGrip(arm_index);
        }

        public void Draw_static()
        {
            update_UIstate();

            GL.Enable(EnableCap.Lighting);
            GL.Enable(EnableCap.Light0);
            GL.Enable(EnableCap.ColorMaterial);
            GL.Enable(EnableCap.Normalize);

            GL.Light(LightName.Light0, LightParameter.Specular, new float[] { 0.7f, 0.7f, 0.7f, 1.0f });
            GL.Light(LightName.Light0, LightParameter.Position, specularPosition);
            GL.LightModel(LightModelParameter.LightModelAmbient, ambientColor);
            GL.Material(MaterialFace.Front, MaterialParameter.Specular, new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
            

            for (int i = 0; i < static_elements.Count; i++)
                if(static_elements[i].visible)
                    static_elements[i].Draw();

            GL.Disable(EnableCap.Normalize);
            GL.Disable(EnableCap.Light0);
            GL.Disable(EnableCap.Lighting);
            GL.Disable(EnableCap.ColorMaterial);

            

        }
        public void Draw_contorller_ui(int index)
        {
            GL.Enable(EnableCap.Lighting);
            GL.Enable(EnableCap.Light0);
            GL.Enable(EnableCap.ColorMaterial);
            GL.Enable(EnableCap.Normalize);


            //GL.Light(LightName.Light0, LightParameter.Position, specularPosition);*/
            GL.Light(LightName.Light0, LightParameter.Specular, new float[] { 0.1f, 0.1f, 0.1f, 1.0f });
            //GL.Light(LightName.Light0, LightParameter.Diffuse, new float[] { 0.1f, 0.1f, 0.1f, 1.0f });

            GL.LightModel(LightModelParameter.LightModelTwoSide,1);
            GL.Material(MaterialFace.Front, MaterialParameter.Specular, new float[] { 0.2f, 0.2f, 0.2f, 1.0f });
            GL.Material(MaterialFace.Front, MaterialParameter.Diffuse, new float[] { 0.5f, 0.5f, 0.5f, 1.0f });
            GL.Material(MaterialFace.Front, MaterialParameter.Shininess, 128.0f);
            GL.ShadeModel(ShadingModel.Smooth);


            for (int i = 0; i < controllers_UI[index].Count; i++)
                if (controllers_UI[index][i].visible)
                    controllers_UI[index][i].Draw();

            GL.Disable(EnableCap.Normalize);
            GL.Disable(EnableCap.Light0);
            GL.Disable(EnableCap.Lighting);
            GL.Disable(EnableCap.ColorMaterial);
        }
        private UI_elements.UI_element getControlByName(string name)
        {
            for (int i = 0; i < static_elements.Count; i++)
            {
                if (static_elements[i].name == name)
                    return static_elements[i];
            }
            for (int j = 0; j < controllers_UI.Length; j++)
            {
                for (int i = 0; i < controllers_UI[j].Count; i++)
                {
                    if (controllers_UI[j][i].name == name)
                        return controllers_UI[j][i];
                }
            }
            return null;
        }

        #region USER EVENTS PROCESSING

        private void update_UIstate()
        {
            if (ui_StateMachine.currentState == UI_states.YuMi_mode)
                if (robot.currect_readings != null)
                if (robot.currect_readings.ContainsKey("moving"))
                {
                    if ((bool)robot.currect_readings["moving"].Value)
                    {
                        ((UI_elements.UI_button)getControlByName("yumi_mode_moving_button")).visible = true;
                        ((UI_elements.UI_button)getControlByName("yumi_mode_notmoving_button")).visible = false;
                    }
                    else
                    {
                        ((UI_elements.UI_button)getControlByName("yumi_mode_moving_button")).visible = false;
                        ((UI_elements.UI_button)getControlByName("yumi_mode_notmoving_button")).visible = true;
                    }
                }
        }

        private void flushInternalUIstate()
        {
            one_more_point_to_put_down = true;
            points_obs = new List<float[]>();
            obs = new List<List<float[]>>();
            box_corners = new List<Vector3>();
        }


        private void onCalibraionClick()
        {
            Console.WriteLine("onCalibraionClick");
            flushInternalUIstate();
            ui_StateMachine.setState(UI_states.Calibration_mode);

        }
        private void onMainMenuClick()
        {
            Console.WriteLine("onMainMenuClick");
            flushInternalUIstate();
            ui_StateMachine.setState(UI_states.MainMenu);
        }

        int nextPoint = 0;
        int curPoint = 0;
        private void onPointClick()
        {
            Console.WriteLine("onPointClick");
            if (robot.calibration_points.Count > 0)
            {
                robot.goto_calib_point(nextPoint);
                curPoint = nextPoint;
                nextPoint++;
                if (nextPoint >= robot.calibration_points.Count)
                    nextPoint = 0;
                ((UI_elements.UI_button)getControlByName("calib_point_button")).SetTexture(Utils.GenerateTexture("Point " + curPoint.ToString(), false), Utils.GenerateTexture("Point " + curPoint.ToString(), true));
            }
        }

        private void onObstClick()
        {
            Console.WriteLine("onObstClick");
            if (obs.Count > 0)
            {
                string s=JsonConvert.SerializeObject(obs);
                File.WriteAllText("obs.txt", s);
            }
        }

        bool vacuum = false;
        private void onTouchPad(int arm_index)
        {
            if (ui_StateMachine.currentState == UI_states.YuMi_mode)
            {
                UI_elements.UI_element cursor = null;

                switch (Yumi_free_mode)
                {
                    case Yumi_mode.free:
                        if (arm_index == 1)
                        {
                            if (vacuum)
                                robot.actuator("vacuum_off");
                            else
                                robot.actuator("vacuum_on");
                            vacuum = !vacuum;
                        }
                        break;
                    case Yumi_mode.pickup:
                        cursor = getControlByName("static_cursor");
                        if (cursor != null)
                        {
                            cursor.Dispose();
                            static_elements.Remove(cursor);
                        }
                        flushInternalUIstate();
                        break;
                    case Yumi_mode.pickbox:
                        cursor = getControlByName("static_cursor");
                        if (cursor != null)
                        {
                            cursor.Dispose();
                            static_elements.Remove(cursor);
                        }
                        cursor = getControlByName("box_ball_cursor");
                        if (cursor != null)
                        {
                            cursor.Dispose();
                            controllers_UI[1].Remove(cursor);
                        }

                        getControlByName("ball_cursor_right").visible = true;
                        getControlByName("box_cursor_right").visible = true;
                        flushInternalUIstate();
                        break;
                    
                }

            }
        }
        private void onGrip(int arm_index)
        {
            if (arm_index == 0)//left
            {
                robot.requestAutoMode();
            }
        }
        bool one_more_point_to_put_down = true;
        Matrix3 rot1;
        Vector3 pos1;
        List<float[]> points_obs = new List<float[]>();
        List<List<float[]>> obs = new List<List<float[]>>();
        List<Vector3> box_corners = new List<Vector3>();

        private void onClickSpace(int arm_index)
        {
            Console.WriteLine("onClickSpace");
            //calibration mode
            if (ui_StateMachine.currentState == UI_states.Calibration_mode)
            {
                if (arm_index == 0)//Left
                {
                    Vector4 temp = new Vector4(current_cursor_position_left);


                    temp.W = 1.0f;
                    Vector4 camera_pos_4 = Vector4.Transform(temp, vr_to_camera_matrix);

                    Console.WriteLine("point " + camera_pos_4.X.ToString() + " " + camera_pos_4.Y.ToString() + " " + camera_pos_4.Z.ToString());
                    robot.associate_calib_point(curPoint, camera_pos_4.X, camera_pos_4.Y, camera_pos_4.Z);

                    UI_elements.UI_element cursor = getControlByName("static_cursor");
                    if (cursor == null)
                    {
                        static_elements.Add(new UI_elements.UI_pointer_ball("static_cursor", current_cursor_position_left, 0.005f, new float[] { 0f, 1f, 0f }));
                    }
                    else
                    {
                        cursor.Dispose();
                        static_elements.Remove(cursor);
                        static_elements.Add(new UI_elements.UI_pointer_ball("static_cursor", current_cursor_position_left, 0.005f, new float[] { 0f, 1f, 0f }));

                    }
                }
            }
            //yumi mode
            if (ui_StateMachine.currentState == UI_states.YuMi_mode)
            {

                if (arm_index == 1)//Right
                {
                    Matrix3 rot_vr_to_camera = new Matrix3(vr_to_camera_matrix);
                    Vector4 temp = new Vector4(current_cursor_position_right);
                    temp.W = 1.0f;
                    Vector4 camera_pos_4 = Vector4.Transform(temp, vr_to_camera_matrix);

                    Matrix3 tool_orientation = current_cursor_orientation_right * rot_vr_to_camera;
                    Vector3 pos = new Vector3(camera_pos_4);
                    switch (Yumi_free_mode)
                    {
                        case Yumi_mode.free:
                            robot.goto_pos_and_orient(pos, tool_orientation, arm_index);
                            break;
                        case Yumi_mode.pickup:
                            if (one_more_point_to_put_down)
                            {
                                UI_elements.UI_element cursor = getControlByName("static_cursor");
                                if (cursor != null)
                                {
                                    cursor.Dispose();
                                    static_elements.Remove(cursor);
                                }
                                static_elements.Add(new UI_elements.UI_pointer_ball("static_cursor", current_cursor_position_right, 0.005f, new float[] { 0f, 1f, 0f }));
                                rot1 = tool_orientation;
                                pos1 = pos;
                                one_more_point_to_put_down = false;
                            }
                            else
                            {
                                robot.pick_up_put_down_pos_and_orient(pos1, rot1, pos, tool_orientation, 0);
                                one_more_point_to_put_down = true;
                            }
                            break;
                        case Yumi_mode.pickbox://place 4 points of box corners, then a point where to pick and one point to put
                            if (box_corners.Count < 4)
                            {
                                box_corners.Add(new Vector3(current_cursor_position_right.X, current_cursor_position_right.Y, current_cursor_position_right.Z));
                                UI_elements.UI_element cursor = getControlByName("static_cursor");
                                if (cursor != null)
                                {
                                    cursor.Dispose();
                                    static_elements.Remove(cursor);
                                }
                            }
                            else
                            {
                                if (one_more_point_to_put_down)
                                {
                                    //remove cursor
                                    UI_elements.UI_element cursor = getControlByName("static_cursor");
                                    if (cursor != null)
                                    {
                                        cursor.Dispose();
                                        static_elements.Remove(cursor);
                                    }
                                    static_elements.Add(new UI_elements.UI_pointer_ball("static_cursor", current_cursor_position_right, 0.005f, new float[] { 0f, 1f, 0f }));
                                    rot1 = tool_orientation;
                                    pos1 = pos;


                                    one_more_point_to_put_down = false;

                                    getControlByName("ball_cursor_right").visible=false;
                                    getControlByName("box_cursor_right").visible = false;

                                    //add box ball cursor
                                    cursor = getControlByName("box_ball_cursor");
                                    if (cursor != null)
                                    {
                                        cursor.Dispose();
                                        controllers_UI[1].Remove(cursor);
                                    }
                                    List<Vector3> trans_points = new List<Vector3>();
                                    box_corners = Geometry.ToRectangle.Orthogonalize(box_corners);
                                    for (int i = 0; i < box_corners.Count; i++)
                                    {
                                        Matrix3 m = current_cursor_orientation_right;
                                        Matrix3 m_inv=m.Inverted();
                                        Vector3 dt=-Vector3.Transform(m_inv, current_cursor_position_right);
                                        Vector3 p = Vector3.Transform(m_inv, box_corners[i]) + dt;
                                        p.X += pointerShift.X; p.Y += pointerShift.Y; p.Z += pointerShift.Z;
                                        trans_points.Add(p);

                                    }
                                    controllers_UI[1].Add((UI_elements.UI_element)new UI_elements.UI_pointer_ball_and_box("box_ball_cursor", new Vector3(pointerShift.X, pointerShift.Y, pointerShift.Z), 0.0025f, trans_points, new float[3] { 0.0f, 1f, 0.0f }, new float[3] { 1.0f, 0f, 0.0f }));
                                }
                                else
                                {
                                    UI_elements.UI_element cursor = getControlByName("box_ball_cursor");
                                    if (cursor != null)
                                    {
                                        cursor.Dispose();
                                        controllers_UI[1].Remove(cursor);
                                    }
                                    
                                    getControlByName("ball_cursor_right").visible = true;
                                    getControlByName("box_cursor_right").visible = true;
                                    
                                    one_more_point_to_put_down = true;

                                    List<Vector4> box_in_camera = new List<Vector4>();
                                    for (int i = 0; i < box_corners.Count; i++)
                                    {
                                        temp = new Vector4(box_corners[i]);
                                        temp.W = 1.0f;
                                        Vector4 temp_cam = Vector4.Transform(temp, vr_to_camera_matrix);
                                        box_in_camera.Add(temp_cam);
                                    }
                                    robot.pick_up_put_down_box(pos1, rot1, pos, tool_orientation,box_in_camera, 0);

                                    box_corners = new List<Vector3>();
                                }
                            }
                            break;
                    }
                }
            }
            //remember mode
            if (ui_StateMachine.currentState == UI_states.save_mode)
            {
                if (arm_index == 1)//Right
                {
                    Matrix3 rot_vr_to_camera = new Matrix3(vr_to_camera_matrix);
                    Vector4 temp = new Vector4(current_cursor_position_right);
                    temp.W = 1.0f;
                    Vector4 camera_pos_4 = Vector4.Transform(temp, vr_to_camera_matrix);

                    Matrix3 tool_orientation = current_cursor_orientation_right * rot_vr_to_camera;
                    Vector3 pos = new Vector3(camera_pos_4);
                    robot.save_pos_and_orient(pos, tool_orientation, arm_index);
                }
            }
            //obstacles mode
            if (ui_StateMachine.currentState == UI_states.obstacles_mode)
            {
                if (arm_index == 0)//left
                {
                    Matrix3 rot_vr_to_camera = new Matrix3(vr_to_camera_matrix);
                    Vector4 temp = new Vector4(current_cursor_position_left);
                    temp.W = 1.0f;
                    Vector4 camera_pos_4 = Vector4.Transform(temp, vr_to_camera_matrix);


                    points_obs.Add(new float[] { (float)camera_pos_4.X, (float)camera_pos_4.Y, (float)camera_pos_4.Z });
                    if (points_obs.Count == 4)
                    {
                        //add obstacle 
                        
                        obs.Add(points_obs);
                        points_obs = new List<float[]>();
                    }
                }

                UI_elements.UI_element cursor = getControlByName("static_cursor");
                if (cursor == null)
                {
                    static_elements.Add(new UI_elements.UI_pointer_ball("static_cursor", current_cursor_position_left, 0.005f, new float[] { 0f, 1f, 0f }));
                }
                else
                {
                    cursor.Dispose();
                    static_elements.Remove(cursor);
                    static_elements.Add(new UI_elements.UI_pointer_ball("static_cursor", current_cursor_position_left, 0.005f, new float[] { 0f, 1f, 0f }));

                }
            }
        }
        private void onPerformCalib()
        {
            Console.WriteLine("onPerfmormCalib");
            //check if all points were associated

            bool Ok = true;
            if (robot.associated_points.Count == robot.calibration_points.Count)
            {
                for (int i = 0; i < robot.calibration_points.Count; i++)
                {
                    if (robot.associated_points[i] == null)
                        Ok = false;
                }
            }
            else
                Ok = false;
            if (!Ok)
            {
                ((UI_elements.UI_button)getControlByName("calib_status_button")).SetTexture(Utils.GenerateTexture("Failed", false), Utils.GenerateTexture("Failed", true));
            }
            else
            {
                //send data to robot
                robot.SendCalibrationData();
            }
        }
        private void onYumiMode()
        {
            Console.WriteLine("onYumiMode");
            flushInternalUIstate();
            ui_StateMachine.setState(UI_states.YuMi_mode);
        }
        private void onSaveMode()
        {
            Console.WriteLine("onSaveMode");
            flushInternalUIstate();
            ui_StateMachine.setState(UI_states.save_mode);
        }
        private void onObstacleMode()
        {
            Console.WriteLine("onObstacleMode");
            flushInternalUIstate();
            ui_StateMachine.setState(UI_states.obstacles_mode);
        }

        enum Yumi_mode{free,pickup,pickbox};
        Yumi_mode Yumi_free_mode = Yumi_mode.pickbox;
        private void onPickUpMode()
        {
            
            Console.WriteLine("onPickUpMode");
            Yumi_free_mode = Yumi_mode.pickup;
            ((UI_elements.UI_button)getControlByName("yumi_mode_mode_button")).SetTexture(Utils.GenerateTexture("pickup", false), Utils.GenerateTexture("pickup", true));
        }
        private void onPickBoxMode()
        {

            Console.WriteLine("onPickBoxMode");
            Yumi_free_mode = Yumi_mode.pickbox;
            ((UI_elements.UI_button)getControlByName("yumi_mode_mode_button")).SetTexture(Utils.GenerateTexture("pickbox", false), Utils.GenerateTexture("pickbox", true));
        }
        private void onFreeMode()
        {
            
            Console.WriteLine("onFreeMode");
            Yumi_free_mode = Yumi_mode.free;
            ((UI_elements.UI_button)getControlByName("yumi_mode_mode_button")).SetTexture(Utils.GenerateTexture("free", false), Utils.GenerateTexture("free", true));
        }
        #endregion
    }
}

