﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.VR;
using OpenTK;

namespace openTKVRtest1
{
    public class ControllerInfo
    {
        public ControllerInfo()
        {
            
        }
        public ulong m_source=0;
        public ulong m_actionPose =0 ;
        public ulong m_actionHaptic = 0;
        public bool trackable = false;
        public int deviceIndex = 0;
        
        public Matrix4 m_rmat4Pose;
        public ControllerModel m_pRenderModel = null;
            
        public string m_sRenderModelName;

        public static string GetTrackedDeviceString(CVRSystem vr,uint unDevice, ETrackedDeviceProperty prop)
        {
            ETrackedPropertyError error = new ETrackedPropertyError();
            uint L=vr.GetStringTrackedDeviceProperty(unDevice, prop, null, 0, ref error);
            if (L == 0)
            {
                return "";
            }
            StringBuilder strBuilder=new StringBuilder();
            L = vr.GetStringTrackedDeviceProperty(unDevice, prop, strBuilder, L, ref error);
            return strBuilder.ToString();
        }

        
    }
}
