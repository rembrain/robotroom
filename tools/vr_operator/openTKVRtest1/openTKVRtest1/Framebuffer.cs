﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using System.Drawing.Imaging;

namespace openTKVRtest1
{
    public class Framebuffer
    {
        int FBO=0;
        public int texture_color=0;
        int texture_depth=0;
        DrawBuffersEnum[] buffers = null;


        public Framebuffer()
        {

        }
        public void Dispose()
        {
            if (texture_color != 0) GL.DeleteTexture(texture_color);
            if (texture_depth != 0) GL.DeleteTexture(texture_depth);
            if (FBO!=0)GL.DeleteFramebuffer(FBO);
            
        }

        private void GenerateColorTexture(int width, int height)
        {
            texture_color = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texture_color);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
        }

        private void GenerateDepthTexture(int width, int height)
        {
            texture_depth = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texture_depth);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent24, width, height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);


        }

        public void GenerateFBO(int width, int height)
        {
            FBO = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO);
            GenerateColorTexture(width, height);
            GenerateDepthTexture(width, height);


            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, texture_color, 0);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, texture_depth, 0);

            if (GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer) != FramebufferErrorCode.FramebufferComplete)
            {
                Console.WriteLine("Error! FrameBuffer is not complete");
            }
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }
        public void bind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO);
        }
        public void unbind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }
    }
}
