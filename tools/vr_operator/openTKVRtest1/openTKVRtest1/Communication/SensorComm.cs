﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using System.Threading;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace openTKVRtest1
{
    public class SensorComm
    {
        static int maxListSize = 16;
        int frame_index = 0;

        string server_to_connect;

        public SensorComm(string address)
        {
            server_to_connect = address;
        }
        public void Run(List<object> sharedList, object locker)
        {
            string robot_name = Properties.Settings.Default.robot_name;

            while (true)
            {

                DateTime start = DateTime.Now;
                
                Console.WriteLine("connect to rabbit");
                var factory = new ConnectionFactory();
                factory.Uri = new System.Uri(server_to_connect);
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    var queueName = channel.QueueDeclare().QueueName;
                    channel.QueueBind(queue: queueName,
                                      exchange: "state_" + robot_name,
                                      routingKey: "");

                    Console.WriteLine(" [*] Waiting for packets.");

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        //channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);



                        var body = ea.Body.ToArray();
                        lock (locker)
                        {
                            string s = System.Text.Encoding.UTF8.GetString(body);
                            try
                            {
                                dynamic obj = JsonConvert.DeserializeObject(s);
                                sharedList.Add(obj);
                            }
                            catch
                            {
                                Console.WriteLine("not json in sensor readings");
                            }

                        }


                    };
                    channel.BasicConsume(queue: queueName,
                                         autoAck: true,

                                         consumer: consumer);
                    while (true)//!close)
                    {
                        Thread.Sleep(3000);
                    }
                }

            }
           

        }
    }
}
