﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using System.Threading;
using System.Drawing;
using System.IO;
using Newtonsoft.Json;
using System.Drawing.Imaging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace openTKVRtest1
{
    public class Communication
    {
        static int maxListSize=16;

        const int VP8_1280_PNG = 0;
        const int JPG_PNG = 1;

        string server_to_connect;
        Decoder dec = null;

        public Communication(string address,Decoder dec_)
        {
            server_to_connect = address;
            dec = dec_;
        }
        public void Run(List<object> sharedList,object locker)
        {
            string robot_name = Properties.Settings.Default.robot_name;
            int frame_index = 0;

            while (true)
            {

                DateTime start = DateTime.Now;
                Console.WriteLine("connect to rabbit");
                var factory = new ConnectionFactory();
                factory.Uri=new System.Uri(server_to_connect);
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    var queueName = channel.QueueDeclare().QueueName;
                    channel.QueueBind(queue: queueName,
                                      exchange: "camera0_" + robot_name,
                                      routingKey: "");

                    Console.WriteLine(" [*] Waiting for packets.");

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        //Console.WriteLine("dt " + (DateTime.Now - start).TotalMilliseconds.ToString());
                        start = DateTime.Now;


                        var body = ea.Body.ToArray();
                        if ((body[0] == VP8_1280_PNG)||(body[0]==JPG_PNG))
                        {
                            int L1 = BitConverter.ToInt32(body, 0 + 1);
                            int L2 = BitConverter.ToInt32(body, 4 + 1);
                            int L3 = BitConverter.ToInt32(body, 8 + 1);

                            if (L1 + L2 + L3 + 12+1 == body.Length)
                            {
                                byte[] ar1 = new byte[L1];
                                for (int i = 0; i < L1; i++) ar1[i] = body[12 + i+1];
                                byte[] ar2 = new byte[L2];
                                for (int i = 0; i < L2; i++) ar2[i] = body[12 + i + L1 + 1];

                                byte[] ar3 = new byte[L3];
                                for (int i = 0; i < L3; i++) ar3[i] = body[12 + i + L1 + L2 + 1];
                                string json_str = Encoding.UTF8.GetString(ar3);


                                Frame3DandSensors frame = new Frame3DandSensors(ar1, ar2, json_str, dec, ref frame_index,body[0]==JPG_PNG);
                                //Console.WriteLine(json_str);


                                lock (locker)
                                {
                                    if (sharedList.Count < maxListSize)
                                        sharedList.Add(frame);
                                    //Console.WriteLine(sharedList.Count.ToString());
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("wrong packet format");
                        }
                        


                    };
                    channel.BasicConsume(queue: queueName,
                                         autoAck: true,

                                         consumer: consumer);
                    while (true)//!close)
                    {
                        Thread.Sleep(3000);
                    }
                }

            }

        }
    }
}
