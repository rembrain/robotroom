﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using System.Threading;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace openTKVRtest1
{


        public class CommandComm
        {
            static int maxListSize = 16;

            string server_to_connect;

            public CommandComm(string address)
            {
                server_to_connect = address;
            }
            public void Run(List<object> commandList, object locker)
            {
                string robot_name = Properties.Settings.Default.robot_name;

                while (true)
                {

                    DateTime start = DateTime.Now;

                    Console.WriteLine("connect to rabbit");
                    var factory = new ConnectionFactory();
                    factory.Uri = new System.Uri(server_to_connect);
                    using (var connection = factory.CreateConnection())
                        using (var channel = connection.CreateModel())
                        {

                            while(true)
                            {
                                Thread.Sleep(30);
                                
                                    
                                    object toSend = null;
                                    if (commandList.Count > 0)
                                    {
                                        toSend = commandList[0];

                                        commandList.RemoveAt(0);
                                    }
                                    if (toSend != null)
                                    {
                                        string s = JsonConvert.SerializeObject(toSend);
                                        channel.BasicPublish("commands_" + robot_name, "", body: System.Text.Encoding.UTF8.GetBytes(s));
                                  
                                    }
                                
                            }
                    

                        }
                }

                

                 while (true)
                {
                    Console.WriteLine("connect to ws");


                    using (var ws = new WebSocket(server_to_connect))
                    {
                        bool close = false;

                        
                        ws.OnClose += (sender, e) =>
                        {
                            Console.WriteLine("socket closed");
                            close = true;
                        };
                        ws.OnError += (sender, e) =>
                        {
                            Console.WriteLine("socket error");
                            close = true;
                        };

                        ws.Connect();
                        int i = 0;
                        while (!close)
                        {
                            Thread.Sleep(30);
                            lock (locker)
                            {
                            object toSend = null;
                            if (commandList.Count > 0)
                            {
                                toSend=commandList[0];
                                
                                commandList.RemoveAt(0);
                            }
                            if(toSend!=null)
                            {
                                string s = JsonConvert.SerializeObject(toSend);
                                ws.Send(System.Text.Encoding.UTF8.GetBytes(s));
                            }
                            
                            }
                        i++;
                        if (i % 100 == 0)
                            if (!ws.Ping())
                                break;
                        }
                    }


                }

            }
        }

}
