﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace openTKVRtest1.Geometry
{
    public static class SphereGenerator
    {
        public static void GenSphere(float radius, Vector3 pos, ref float[] vertices, ref float[] normals)
        {
            int sectorCount = 32;
            int stackCount = 32;
            List<float> vertices_list = new List<float>();
            List<float> normals_list = new List<float>();

            float x, y, z, xy;                              // vertex position
            float nx, ny, nz, lengthInv = 1.0f / radius;    // vertex normal
            float s, t;                                     // vertex texCoord

            float sectorStep = 2.0f * (float)Math.PI / (float)sectorCount;
            float stackStep = (float)Math.PI / (float)stackCount;
            float sectorAngle, stackAngle;

            for (int i = 0; i <= stackCount; ++i)
            {
                stackAngle = (float)Math.PI / 2.0f - i * stackStep;        // starting from pi/2 to -pi/2
                xy = radius * (float)Math.Cos((double)stackAngle);             // r * cos(u)
                z = radius * (float)Math.Sin((double)stackAngle);              // r * sin(u)

                // add (sectorCount+1) vertices per stack
                // the first and last vertices have same position and normal, but different tex coords
                for (int j = 0; j <= sectorCount; ++j)
                {
                    sectorAngle = j * sectorStep;           // starting from 0 to 2pi

                    // vertex position (x, y, z)
                    x = xy * (float)Math.Cos(sectorAngle);             // r * cos(u) * cos(v)
                    y = xy * (float)Math.Sin(sectorAngle);             // r * cos(u) * sin(v)
                    vertices_list.Add(x+pos.X);
                    vertices_list.Add(y+pos.Y);
                    vertices_list.Add(z+pos.Z);

                    // normalized vertex normal (nx, ny, nz)
                    nx = x * lengthInv;
                    ny = y * lengthInv;
                    nz = z * lengthInv;
                    normals_list.Add(nx);
                    normals_list.Add(ny);
                    normals_list.Add(nz);

                }
            }

            List<float> normals_list_1 = new List<float>();
            List<float> vertices_list_1 = new List<float>();
            int k1, k2;
            for (int i = 0; i < stackCount; ++i)
            {
                k1 = i * (sectorCount + 1);     // beginning of current stack
                k2 = k1 + sectorCount + 1;      // beginning of next stack

                for (int j = 0; j < sectorCount; ++j, ++k1, ++k2)
                {
                    // 2 triangles per sector excluding first and last stacks
                    // k1 => k2 => k1+1
                    if (i != 0)
                    {
                        vertices_list_1.Add(vertices_list[k1*3]); vertices_list_1.Add(vertices_list[k1 * 3+1]); vertices_list_1.Add(vertices_list[k1 * 3+2]);
                        vertices_list_1.Add(vertices_list[k2*3]); vertices_list_1.Add(vertices_list[k2 * 3+1]); vertices_list_1.Add(vertices_list[k2 * 3+2]);
                        vertices_list_1.Add(vertices_list[3*(k1+1)]); vertices_list_1.Add(vertices_list[3*(k1 + 1)+1]); vertices_list_1.Add(vertices_list[3*(k1 + 1)+2]);

                        normals_list_1.Add(normals_list[3*k1]); normals_list_1.Add(normals_list[3 * k1+1]); normals_list_1.Add(normals_list[3 * k1+2]);
                        normals_list_1.Add(normals_list[3*k2]); normals_list_1.Add(normals_list[3 * k2+1]); normals_list_1.Add(normals_list[3 * k2+2]);
                        normals_list_1.Add(normals_list[3*(k1+1)]); normals_list_1.Add(normals_list[3 * (k1 + 1)+1]); normals_list_1.Add(normals_list[3 * (k1 + 1)+2]);
                    }

                    // k1+1 => k2 => k2+1
                    if (i != (stackCount - 1))
                    {
                        vertices_list_1.Add(vertices_list[3*(k1+1)]); vertices_list_1.Add(vertices_list[3 * (k1 + 1)+1]); vertices_list_1.Add(vertices_list[3 * (k1 + 1)+2]);
                        vertices_list_1.Add(vertices_list[3*k2]); vertices_list_1.Add(vertices_list[3 * k2+1]); vertices_list_1.Add(vertices_list[3 * k2+2]);
                        vertices_list_1.Add(vertices_list[3*(k2 + 1)]); vertices_list_1.Add(vertices_list[3 * (k2 + 1)+1]); vertices_list_1.Add(vertices_list[3 * (k2 + 1)+2]);

                        normals_list_1.Add(normals_list[3 * (k1 + 1)]); normals_list_1.Add(normals_list[3 * (k1 + 1)+1]); normals_list_1.Add(normals_list[3 * (k1 + 1)+2]);
                        normals_list_1.Add(normals_list[3*k2]); normals_list_1.Add(normals_list[3 * k2+1]); normals_list_1.Add(normals_list[3 * k2+2]);
                        normals_list_1.Add(normals_list[3*(k2 + 1)]); normals_list_1.Add(normals_list[3 * (k2 + 1)+1]); normals_list_1.Add(normals_list[3 * (k2 + 1)+2]);
                    }
                }
            }
            normals = normals_list_1.ToArray();
            vertices = vertices_list_1.ToArray();
        }
    }
}
