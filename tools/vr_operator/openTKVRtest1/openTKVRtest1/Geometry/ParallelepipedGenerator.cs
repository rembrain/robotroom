﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace openTKVRtest1.Geometry
{
    public static class ParallelepipedGenerator
    {
        public static void GenParallelepiped(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4, ref float[] vertices, ref float[] normals)
        {
            List<Vector3[]> triangles = new List<Vector3[]>();
            Vector3 p5 = p4 + (p2 - p1) + (p3 - p1);

            Vector3 p6 = p1 + (p2 - p1) + (p3 - p1);
            Vector3 p7 = p4 + (p2 - p1);
            Vector3 p9 = p4 + (p3 - p1);

            Vector3 center = new Vector3((p2 + p3).X / 2.0f, (p2 + p3).Y / 2.0f, (p2 + p3).Z / 2.0f)+ new Vector3((p4 + p5).X / 2.0f, (p4 + p5).Y / 2.0f, (p4 + p5).Z / 2.0f);
            center.X /= 2.0f; center.Y /= 2.0f; center.Z /= 2.0f;

            triangles.Add(new Vector3[] { p1, p2, p3 });
            triangles.Add(new Vector3[] { p6, p3, p2 });

            triangles.Add(new Vector3[] { p1, p2, p4 });
            triangles.Add(new Vector3[] { p4, p2, p7 });

            triangles.Add(new Vector3[] { p1, p3, p4 });
            triangles.Add(new Vector3[] { p9, p3, p4 });

            triangles.Add(new Vector3[] { p4, p9, p5 });
            triangles.Add(new Vector3[] { p4, p5, p7 });

            triangles.Add(new Vector3[] { p5, p6, p2 });
            triangles.Add(new Vector3[] { p5, p2, p7 });

            triangles.Add(new Vector3[] { p3, p9, p6 });
            triangles.Add(new Vector3[] { p9, p5, p6 });

            List<float> vertices_ = new List<float>();
            List<float> normals_ = new List<float>();
            for (int i = 0; i < triangles.Count; i++)
            {
                Vector3 n=Vector3.Cross(triangles[i][1] - triangles[i][0], triangles[i][2] - triangles[i][0]);
                n.Normalize();
                //if (Vector3.Dot(n, triangles[i][0] - center) < 0)
                //    n = -n;
                vertices_.Add(triangles[i][0].X);vertices_.Add(triangles[i][0].Y);vertices_.Add( triangles[i][0].Z);
                vertices_.Add(triangles[i][1].X); vertices_.Add(triangles[i][1].Y); vertices_.Add(triangles[i][1].Z);
                vertices_.Add(triangles[i][2].X); vertices_.Add(triangles[i][2].Y); vertices_.Add(triangles[i][2].Z);
                normals_.Add(n.X);normals_.Add(n.Y);normals_.Add(n.Z);
                normals_.Add(n.X); normals_.Add(n.Y); normals_.Add(n.Z);
                normals_.Add(n.X); normals_.Add(n.Y); normals_.Add(n.Z);

            }

            vertices = vertices_.ToArray();
            normals = normals_.ToArray();
        }
    }
}
