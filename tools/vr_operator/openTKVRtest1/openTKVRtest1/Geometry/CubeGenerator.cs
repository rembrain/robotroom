﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;


namespace openTKVRtest1.Geometry
{
    public static class CubeGenerator
    {
        public static void GenCube(float width, float height, float depth, Vector3 pos, ref float[] vertices, ref float[] normals,ref float[] uvs)
        {
            vertices = new float[] {-width/2.0f,-height/2.0f,-depth/2.0f, -width/2.0f,-height/2.0f,  depth/2.0f, -width/2.0f, height/2.0f,  depth/2.0f,  // Left Side
		                            -width/2.0f,-height/2.0f,-depth/2.0f, -width/2.0f, height/2.0f, depth/2.0f, -width/2.0f, height/2.0f,-depth/2.0f,  // Left Side
		                             width/2.0f, height/2.0f,-depth/2.0f, -width/2.0f,-height/2.0f,-depth/2.0f, -width/2.0f, height/2.0f,-depth/2.0f,  // Back Side
		                             width/2.0f,-height/2.0f, depth/2.0f, -width/2.0f,-height/2.0f,-depth/2.0f,  width/2.0f,-height/2.0f,-depth/2.0f,  // Bottom Side
		                             width/2.0f, height/2.0f,-depth/2.0f,  width/2.0f,-height/2.0f,-depth/2.0f, -width/2.0f,-height/2.0f,-depth/2.0f,  // Back Side
		                             width/2.0f,-height/2.0f, depth/2.0f, -width/2.0f,-height/2.0f, depth/2.0f, -width/2.0f,-height/2.0f,-depth/2.0f,  // Bottom Side
		                            -width/2.0f, height/2.0f, depth/2.0f, -width/2.0f,-height/2.0f, depth/2.0f,  width/2.0f,-height/2.0f, depth/2.0f,  // Front Side
		                             width/2.0f, height/2.0f, depth/2.0f,  width/2.0f,-height/2.0f,-depth/2.0f,  width/2.0f, height/2.0f,-depth/2.0f,  // Right Side
		                             width/2.0f,-height/2.0f,-depth/2.0f,  width/2.0f, height/2.0f, depth/2.0f,  width/2.0f,-height/2.0f, depth/2.0f,  // Right Side
		                             width/2.0f, height/2.0f, depth/2.0f,  width/2.0f, height/2.0f,-depth/2.0f, -width/2.0f, height/2.0f,-depth/2.0f,  // Top Side
		                             width/2.0f, height/2.0f, depth/2.0f, -width/2.0f, height/2.0f,-depth/2.0f, -width/2.0f, height/2.0f, depth/2.0f,  // Top Side
		                             width/2.0f, height/2.0f, depth/2.0f, -width/2.0f, height/2.0f, depth/2.0f,  width/2.0f,-height/2.0f, depth/2.0f //Front
            };
            uvs = new float[] { 0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,// Left Side
                                0.0f,0.0f,0.0f,0.0f,0.0f,0.0f, // Left Side
                                0,0,0,0,0,0,
                                0,0,0,0,0,0,
                                0,0,0,0,0,0,
                                0,0,0,0,0,0,
                                0f,0f,0f,1.0f,1.0f,1.0f,//front
                                0,0,0,0,0,0,
                                0,0,0,0,0,0,
                                0,0,0,0,0,0,
                                0,0,0,0,0,0,
                                1.0f,0f,0f,0f,1f,1f,//Front
                                };
            normals = new float[] {
                        -1.0f, 0.0f, 0.0f,-1.0f, 0.0f, 0.0f,-1.0f, 0.0f, 0.0f, // Left Side
                        -1.0f, 0.0f, 0.0f,-1.0f, 0.0f, 0.0f,-1.0f, 0.0f, 0.0f, // Left Side
		                0.0f, 0.0f, -1.0f,0.0f, 0.0f, -1.0f,0.0f, 0.0f, -1.0f, // Back Side
		                0.0f,-1.0f, 0.0f,0.0f,-1.0f, 0.0f,0.0f,-1.0f, 0.0f, // Bottom Side
		                0.0f, 0.0f, -1.0f,0.0f, 0.0f, -1.0f,0.0f, 0.0f, -1.0f, // Back Side
		                0.0f, -1.0f, 0.0f,0.0f, -1.0f, 0.0f,0.0f, -1.0f, 0.0f, // Bottom Side
		                0.0f, 0.0f, 1.0f,0.0f, 0.0f, 1.0f,0.0f, 0.0f, 1.0f, // front Side
		                1.0f, 0.0f, 0.0f,1.0f, 0.0f, 0.0f,1.0f, 0.0f, 0.0f, // right Side
		                1.0f, 0.0f, 0.0f,1.0f, 0.0f, 0.0f,1.0f, 0.0f, 0.0f, // right Side
		                0.0f, 1.0f, 0.0f,0.0f, 1.0f, 0.0f,0.0f, 1.0f, 0.0f, // top Side
		                0.0f, 1.0f, 0.0f,0.0f, 1.0f, 0.0f,0.0f, 1.0f, 0.0f, // top Side
		                0.0f, 0.0f, 1.0f,0.0f, 0.0f, 1.0f,0.0f, 0.0f, 1.0f // front Side

	        };
            for (int i = 0; i < (int)(vertices.Length / 3); i++)
            {
                vertices[i * 3] += pos.X;
                vertices[i * 3 + 1] += pos.Y;
                vertices[i * 3 + 2] += pos.Z;
            }
        }

    }
}
