﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace openTKVRtest1.Geometry
{
    public static class ToRectangle
    {
        public static Dictionary<string, float[]> perform(List<Vector3> points)
        {
            Dictionary<string, float[]> dict = new Dictionary<string, float[]>();
            dict.Add("sizes", new float[] { 0.1f, 0.1f, 0.1f });
            dict.Add("position", new float[] { 0.7f, 0.4f, 0.2f });
            dict.Add("q", new float[] { 0f, 0f, 0f, 1f });
            return dict;

        }
        public static List<Vector3> Orthogonalize(List<Vector3> ps)
        {
            List<Vector3> result = new List<Vector3>();
            result.Add(ps[0]);
            result.Add(ps[1]);

            Vector3 d1 = ps[1] - ps[0];
            Vector3 d2 = ps[2] - ps[0];
            Vector3 d1n = d1.Normalized();
            result.Add(ps[2] - Vector3.Multiply(d1n, Vector3.Dot(d1, d2) / d1.Length));

            Vector3 n = Vector3.Cross(d1, d2);
            n.Normalize();

            Vector3 d3 = ps[3] - ps[0];
            result.Add(ps[0]+Vector3.Multiply(n,Vector3.Dot(d3, n)));
            return result;
            
        }
    }
}

