﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.VR;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using System.Drawing.Imaging;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;


namespace openTKVRtest1
{
    public class VR
    {
        Bitmap bitmap=null;
        Framebuffer fbo_left = null;
        Framebuffer fbo_right = null;

        int indexBufferId;
        int vertexBufferId;
        int uvsBufferId;


        int rgbTexture = 0;
        
        int width = 1280;
        int height = 1280;
        float fNearClip = 0.1f;
 	    float fFarClip = 50.0f;
        Matrix4 objectMatrix;
        ControllerInfo[] hands = new ControllerInfo[2];
        UI_VR ui_vr;


        public VR(Robots.Robot robot_state)
        {
            
            var shift=JsonConvert.DeserializeObject<Dictionary<string,float>>(Properties.Settings.Default.object_shift);
            var rotate = JsonConvert.DeserializeObject<Dictionary<string, float>>(Properties.Settings.Default.object_rotate);
            objectMatrix = Matrix4.CreateFromAxisAngle(new Vector3(rotate["x"], rotate["y"], rotate["z"]), rotate["alpha"])*Matrix4.CreateTranslation(new Vector3(shift["x"],shift["y"],shift["z"]));

            ui_vr = new UI_VR(robot_state, objectMatrix);

        }

        public void Dispose()
        {
            if (Valve.VR.OpenVR.Compositor != null)
            {
                for (int i = 0; i < hands.Length; i++)
                {
                    if (hands[i].m_pRenderModel != null)
                        hands[i].m_pRenderModel.Dispose();
                }
                ui_vr.Dispose();
                fbo_left.Dispose();
                fbo_right.Dispose();
                GL.DeleteBuffer(indexBufferId);
                GL.DeleteBuffer(vertexBufferId);
                GL.DeleteBuffer(uvsBufferId);
            }
        }
        int hmd_index = 0;

        CVRSystem vrSystem = null;
        public Matrix4 left_projection;
        public Matrix4 right_projection;
        public Matrix4 mat4eyePosLeft;
        public Matrix4 mat4eyePosRight;
        public Matrix4 mat4HMDPose;
        public ulong m_actionsetDemo;
        List<ControllerModel> m_vecRenderModels=new List<ControllerModel>();

        public void Init()
        { 
            var error = EVRInitError.None;
            this.vrSystem = OpenVR.Init(ref error, EVRApplicationType.VRApplication_Scene);
            if (error != EVRInitError.None)
            {
                // handle init error
                Console.WriteLine(error);
            }
            else
            {

                for (uint i = 0; i < OpenVR.k_unMaxTrackedDeviceCount; i++)
                {
                    var deviceClass = vrSystem.GetTrackedDeviceClass(i);
                    if (deviceClass != ETrackedDeviceClass.Invalid)
                    {
                        Console.WriteLine("OpenVR device at " + i + ": " + deviceClass);
                        if (deviceClass == ETrackedDeviceClass.HMD)
                        {
                            hmd_index = (int)i;
                        }
                    }
                }
                ETrackedPropertyError perror = new ETrackedPropertyError();

                //uint L = vrSystem.GetStringTrackedDeviceProperty((uint)4, ETrackedDeviceProperty.Prop_RenderModelName_String, null, 0, ref perror);
                //StringBuilder s = new StringBuilder();
                //vrSystem.GetStringTrackedDeviceProperty((uint)4, ETrackedDeviceProperty.Prop_RenderModelName_String, s, L, ref perror);

                this.fbo_left = new Framebuffer();
                this.fbo_left.GenerateFBO(width, height);

                this.fbo_right = new Framebuffer();
                this.fbo_right.GenerateFBO(width, height);

                this.left_projection = Utils.convertToOpenTKM4(vrSystem.GetProjectionMatrix(EVREye.Eye_Left, fNearClip, fFarClip));
                this.right_projection = Utils.convertToOpenTKM4(vrSystem.GetProjectionMatrix(EVREye.Eye_Right, fNearClip, fFarClip));
                this.mat4eyePosLeft = Utils.convertM34toM44(vrSystem.GetEyeToHeadTransform(EVREye.Eye_Left));
                this.mat4eyePosLeft.Invert();
                this.mat4eyePosRight = Utils.convertM34toM44(vrSystem.GetEyeToHeadTransform(EVREye.Eye_Right));
                this.mat4eyePosRight.Invert();


                hands[0] = new ControllerInfo();
                hands[1] = new ControllerInfo();


                //init UI
                ui_vr.Init();

                indexBufferId = GL.GenBuffer();
                vertexBufferId = GL.GenBuffer();
                uvsBufferId = GL.GenBuffer();
            }
        }

        ControllerModel FindOrLoadRenderModel(string pchRenderModelName)
        {
            ControllerModel model = null;
            for (int i = 0; i < m_vecRenderModels.Count; i++)
            {
                if (m_vecRenderModels[i].GetName() == pchRenderModelName)
                    model = m_vecRenderModels[i];
            }

            if (model == null)
            {
                IntPtr pModel = IntPtr.Zero;

                EVRRenderModelError err;

                while (true)
                {
                    err = OpenVR.RenderModels.LoadRenderModel_Async(pchRenderModelName, ref pModel);
                    if (err != EVRRenderModelError.Loading)
                        break;
                    Thread.Sleep(1);
                }
                if (err != EVRRenderModelError.None)
                {
                    Console.WriteLine("unable to load render model");
                    return null;
                }



                IntPtr pTexture = IntPtr.Zero;
                while (true)
                {
                    err = OpenVR.RenderModels.LoadTexture_Async(((RenderModel_t)(Marshal.PtrToStructure(pModel, typeof(RenderModel_t)))).diffuseTextureId, ref pTexture);
                    if (err != EVRRenderModelError.Loading)
                        break;
                    Thread.Sleep(1);

                }
                if (err != EVRRenderModelError.None)
                {
                    Console.WriteLine("unable to load texture");
                    return null;
                }
                model = new ControllerModel(pchRenderModelName);
                if (!model.BInit((RenderModel_t)(Marshal.PtrToStructure(pModel, typeof(RenderModel_t))), (RenderModel_TextureMap_t)(Marshal.PtrToStructure(pTexture, typeof(RenderModel_TextureMap_t)))))
                {
                    Console.WriteLine("unable to create render model");
                }
                else
                {
                    m_vecRenderModels.Add(model);
                }
                OpenVR.RenderModels.FreeRenderModel(pModel);
                OpenVR.RenderModels.FreeTexture(pTexture);



            }
            
            return model;
        }



        public void WaitGetPoses()
        {
            if (Valve.VR.OpenVR.Compositor != null)
            { 
                TrackedDevicePose_t[] allPoses_render = new TrackedDevicePose_t[OpenVR.k_unMaxTrackedDeviceCount];
                TrackedDevicePose_t[] allPoses_game = new TrackedDevicePose_t[OpenVR.k_unMaxTrackedDeviceCount];
            
                Valve.VR.OpenVR.Compositor.WaitGetPoses(allPoses_render, allPoses_game);
                HmdMatrix34_t pose = allPoses_render[OpenVR.k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking;
                this.mat4HMDPose = Utils.convertM34toM44(pose);

                for (int i = 0; i < 2; i++)
                {
                    hands[i].trackable = allPoses_render[hands[i].deviceIndex].bPoseIsValid;

                    if (allPoses_render[hands[i].deviceIndex].bPoseIsValid)
                    {
                        hands[i].m_rmat4Pose = Utils.convertM34toM44(allPoses_game[hands[i].deviceIndex].mDeviceToAbsoluteTracking);

                        //var temp = Utils.convertM34toM44(allPoses_game[hands[i].deviceIndex].mDeviceToAbsoluteTracking);
                        
                    }

                }


                this.mat4HMDPose.Invert();
            }
        }
        

        public void PushToVR()
        {
            if (Valve.VR.OpenVR.Compositor != null)
            {

                DateTime start = DateTime.Now;

                var bounds = new VRTextureBounds_t() { uMax = 1, vMax = 1 };
                var tex_left = new Texture_t() { eType = ETextureType.OpenGL, eColorSpace = EColorSpace.Gamma, handle = new IntPtr(fbo_left.texture_color) };

                var tex_right = new Texture_t() { eType = ETextureType.OpenGL, eColorSpace = EColorSpace.Gamma, handle = new IntPtr(fbo_right.texture_color) };
                var error = Valve.VR.OpenVR.Compositor.Submit(EVREye.Eye_Left, ref tex_left, ref bounds, EVRSubmitFlags.Submit_Default);
                error = Valve.VR.OpenVR.Compositor.Submit(EVREye.Eye_Right, ref tex_right, ref bounds, EVRSubmitFlags.Submit_Default);
                //Valve.VR.OpenVR.Compositor.PostPresentHandoff();



                //Console.WriteLine(DateTime.Now - start);
            }
        }
        public void HandleInput()
        {
            if (Valve.VR.OpenVR.Compositor != null)
            {
                int I = 0;
                var types = new List<string>();
                /*for (int i = 0; i < 256; i++)
                {
                    string controller_type = ControllerInfo.GetTrackedDeviceString(this.vrSystem, (uint)(i), ETrackedDeviceProperty.Prop_ControllerType_String);
                    types.Add(controller_type);
                }*/
                for (int i = 0; i <= 256; i++)
                {
                    string controller_type = ControllerInfo.GetTrackedDeviceString(this.vrSystem, (uint)(i), ETrackedDeviceProperty.Prop_ControllerType_String);

                    
                    if ((controller_type == "knuckles") && (I < 2))
                    {

                        string sRenderModelName = "{indexcontroller}valve_controller_knu_1_0_";
                        if (i == 3)
                            sRenderModelName += "right";
                        else
                            sRenderModelName += "left";
                        if (hands[I].m_sRenderModelName != sRenderModelName)
                        {
                            hands[I].m_sRenderModelName = sRenderModelName;
                            hands[I].m_pRenderModel = FindOrLoadRenderModel(sRenderModelName);
                        }
                        hands[I].deviceIndex = i;
                        I += 1;
                    }
                    else
                    {
                        string sRenderModelName = ControllerInfo.GetTrackedDeviceString(this.vrSystem, (uint)(i), ETrackedDeviceProperty.Prop_RenderModelName_String);
                        if ((sRenderModelName.Contains("vr_controller_vive")) && (I < 2))
                        {


                            if (hands[I].m_sRenderModelName != sRenderModelName)
                            {

                                hands[I].m_sRenderModelName = sRenderModelName;
                                hands[I].m_pRenderModel = FindOrLoadRenderModel(sRenderModelName);
                            }
                            hands[I].deviceIndex = i;
                            I += 1;
                        }
                    }

                }

                //pass controller positions to UI
                for (int i = 0; i < hands.Length; i++)
                {
                    Vector4 v = Vector4.Transform(ui_vr.pointerShift, hands[i].m_rmat4Pose);
                    Matrix3 m3 = new Matrix3(hands[i].m_rmat4Pose);

                    ui_vr.handleCursorPosition(new Vector3(v.X, v.Y, v.Z),m3, i);
                }

                VREvent_t ev = new VREvent_t();
                while (vrSystem.PollNextEvent(ref ev, (uint)Marshal.SizeOf(typeof(VREvent_t))))
                {
                    if (ev.eventType == (uint)EVREventType.VREvent_ButtonPress)
                    {
                        Console.WriteLine("device " + ev.trackedDeviceIndex + " " + ev.data.ToString());

                        if (ev.data.controller.button == (uint)EVRButtonId.k_EButton_SteamVR_Trigger)
                        {
                            Console.WriteLine("trigger");
                            if (hands[0].deviceIndex == ev.trackedDeviceIndex)
                                ui_vr.Click(0);
                            if (hands[1].deviceIndex == ev.trackedDeviceIndex)
                                ui_vr.Click(1);
                        }
                        if (ev.data.controller.button == (uint)EVRButtonId.k_EButton_SteamVR_Touchpad)
                        {
                            Console.WriteLine("touchpad");
                            if (hands[0].deviceIndex == ev.trackedDeviceIndex)
                                ui_vr.TouchPad(0);
                            if (hands[1].deviceIndex == ev.trackedDeviceIndex)
                                ui_vr.TouchPad(1);
                        }
                        if (ev.data.controller.button == (uint) EVRButtonId.k_EButton_Grip)
                        {
                            Console.WriteLine("grip");
                            if (hands[0].deviceIndex == ev.trackedDeviceIndex)
                                ui_vr.Grip(0);
                            if (hands[1].deviceIndex == ev.trackedDeviceIndex)
                                ui_vr.Grip(1);
                        }
                    }

                }
            }


        }

        public void DrawViewport(Frame3DandSensors frame,Matrix4 eye_projection_matrix, Matrix4 eye_matrix,int vertexBufferId, int uvsBufferId,int indexBufferId)
        {
            GL.Enable(EnableCap.DepthTest);
            //GL.Enable(EnableCap.CullFace);
            GL.Enable(EnableCap.Texture2D);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            
            
            //set camera position and orientation
            GL.MatrixMode(MatrixMode.Projection);
            Matrix4 resultM = eye_projection_matrix ;
            GL.LoadMatrix(ref resultM);


            GL.MatrixMode(MatrixMode.Modelview);

            Matrix4 resultMV = mat4HMDPose*eye_matrix;
            GL.LoadMatrix(ref resultMV);
            //draw staitc UI
            
            ui_vr.Draw_static();
            

            //draw object
            resultMV = objectMatrix*  mat4HMDPose* eye_matrix;
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref resultMV);

            
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);
            
            
            GL.BindBuffer(BufferTarget.ArrayBuffer, uvsBufferId);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            GL.TexCoordPointer(2,TexCoordPointerType.Float, 0, IntPtr.Zero);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferId);

            
            GL.BindTexture(TextureTarget.Texture2D, rgbTexture);
            // Draw:            


            GL.DrawElements(BeginMode.Triangles,frame.indices.Length,DrawElementsType.UnsignedInt,IntPtr.Zero);
            

            // Disable:
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
            GL.BindTexture(TextureTarget.Texture2D, 0);


            for (int i = 0; i < 2; i++)
            {
                if ((hands[i].trackable)&&(hands[i].m_pRenderModel!=null))
                {
                    GL.MatrixMode(MatrixMode.Modelview);
                    resultMV = hands[i].m_rmat4Pose *  mat4HMDPose* eye_matrix;
                    GL.LoadMatrix(ref resultMV);

                    hands[i].m_pRenderModel.Draw();
                }
                //draw controller ui
                ui_vr.Draw_contorller_ui(i);
            }
            
            GL.Disable(EnableCap.Texture2D);

            


        }
        
        public void Draw(Frame3DandSensors frame,int width_window,int height_window)
        {
            if (Valve.VR.OpenVR.Compositor != null)
            {
                DateTime start = DateTime.Now;

                GL.Viewport(0, 0, width, height);

                //draw 3D 

                GL.BindBuffer(BufferTarget.ElementArrayBuffer, indexBufferId);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(frame.indices.Length * sizeof(UInt32)), frame.indices, BufferUsageHint.StaticDraw);


                GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(frame.vertices.Length * sizeof(float)), frame.vertices, BufferUsageHint.StaticDraw);



                GL.BindBuffer(BufferTarget.ArrayBuffer, uvsBufferId);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(frame.uvs.Length * sizeof(float)), frame.uvs, BufferUsageHint.StaticDraw);

                Utils.LoadTexture(frame.bitmap, ref rgbTexture);



                for (int i = 0; i < 2; i++)
                {
                    Matrix4 eye_matrix;
                    Matrix4 eye_projection_matrix;
                    if (i == 0)//left eye
                    {
                        eye_matrix = this.mat4eyePosLeft;
                        eye_projection_matrix = this.left_projection;
                        fbo_left.bind();
                    }
                    else//right
                    {
                        eye_matrix = this.mat4eyePosRight;
                        eye_projection_matrix = this.right_projection;
                        fbo_right.bind();
                    }

                    DrawViewport(frame, eye_projection_matrix, eye_matrix, vertexBufferId, uvsBufferId, indexBufferId);

                    if (i == 0) fbo_left.unbind();
                    else fbo_right.unbind();
                }

                GL.Viewport(0, 0, width_window, height_window);
                DrawViewport(frame, this.left_projection, this.mat4eyePosLeft, vertexBufferId, uvsBufferId, indexBufferId);




                //Console.WriteLine("dt " + (DateTime.Now - start).TotalMilliseconds.ToString());
            }

        }

    }
}
