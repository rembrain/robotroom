﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace openTKVRtest1.UI_elements
{
    public class UI_element
    {
        public UI_element()
        { 
        }
        public virtual void Dispose()
        { }
        public virtual void updateState(Vector3 v)
        { }
        public virtual bool click()
        { return false; }
        public virtual void Draw()
        { }

        public bool active;
        public bool visible=true;
        public string name = "noname";
    }
}
