﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace openTKVRtest1.UI_elements
{
    public class UI_button: UI_element
    {
        float[] vertices = null;
        float[] normals = null;
        float[] uvs = null;
        int vertexBuf = 0;
        int normalBuf = 0;
        int uvsBufferId=0;
        int textureNotChosen = 0;
        int textureChosen = 0;
        int textureCurrent = 0;
        Vector3 center = new Vector3();
        float w = 0;
        float h = 0;
        float d = 0;
        Action on_click = null;
        bool chosen = false;
        public UI_button(string _name,Vector3 pos,float width,float height,float depth,bool Active,string notActiveBmp,string activeBmp,Action onClick=null) : base()
        {
            Utils.LoadTexture(new Bitmap(notActiveBmp), ref textureNotChosen);
            Utils.LoadTexture(new Bitmap(activeBmp), ref textureChosen);

            Init(_name, pos, width, height, depth, Active, onClick);
        }
        public UI_button(string _name, Vector3 pos, float width, float height, float depth, bool Active, Bitmap notActiveBmp, Bitmap activeBmp, Action onClick = null) : base()
        {
            notActiveBmp.Save("1.bmp");
            Utils.LoadTexture(notActiveBmp, ref textureNotChosen);
            Utils.LoadTexture(activeBmp, ref textureChosen);

            notActiveBmp.Dispose();
            activeBmp.Dispose();

            Init(_name, pos, width, height, depth, Active, onClick);
        }

        public void Init(string _name, Vector3 pos, float width, float height, float depth, bool Active, Action onClick)
        {
            name = _name;
            on_click = onClick;
            active = Active;

            center = pos;
            w = width;
            h = height;
            d = depth;
            //generate cube
            Geometry.CubeGenerator.GenCube(width, height, depth, pos, ref vertices, ref normals, ref uvs);



            textureCurrent = textureNotChosen;

            vertexBuf = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuf);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            uvsBufferId = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, uvsBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(uvs.Length * sizeof(float)), uvs, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);



            normalBuf = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, normalBuf);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * sizeof(float)), normals, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public void SetTexture(Bitmap not_chosen, Bitmap chosen)
        {
            GL.DeleteTexture(textureChosen);
            GL.DeleteTexture(textureNotChosen);
            Utils.LoadTexture(not_chosen, ref textureNotChosen);
            Utils.LoadTexture(chosen, ref textureChosen);
            
        }

        public override void Dispose()
        {
            GL.DeleteBuffer(vertexBuf);
            GL.DeleteBuffer(normalBuf);
            GL.DeleteBuffer(uvsBufferId);
            GL.DeleteTexture(textureChosen);
            GL.DeleteTexture(textureNotChosen);
        }

        public override void Draw()
        {

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuf);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);

            GL.BindBuffer(BufferTarget.ArrayBuffer, normalBuf);
            GL.EnableClientState(ArrayCap.NormalArray);
            GL.NormalPointer(NormalPointerType.Float, 0, IntPtr.Zero);

            GL.BindBuffer(BufferTarget.ArrayBuffer, uvsBufferId);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, IntPtr.Zero);

            GL.BindTexture(TextureTarget.Texture2D, textureCurrent);

            GL.DrawArrays(BeginMode.Triangles, 0, (int)(vertices.Length/3));
            

            GL.BindTexture(TextureTarget.Texture2D, 0);

            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.NormalArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);

        }
        public override bool click()
        {
            if ((on_click != null) && (chosen))
            {
                on_click();
                return true;
            }
            base.click();
            return false;
        }
        public override void updateState(Vector3 v)
        {
            if ((Math.Abs(v.X - center.X) < w / 2) && (Math.Abs(v.Y - center.Y) < h / 2) && (Math.Abs(v.Z - center.Z) < d / 2))
            {
                textureCurrent = textureChosen;
                chosen = true;
            }
            else
            {
                textureCurrent = textureNotChosen;
                chosen = false;
            }
            base.updateState(v);
        }



    }
}
