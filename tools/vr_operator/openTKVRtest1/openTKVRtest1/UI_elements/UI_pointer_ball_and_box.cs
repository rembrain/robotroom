﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Threading.Tasks;

namespace openTKVRtest1.UI_elements
{
    class UI_pointer_ball_and_box : UI_element
    {
        float[] vertices = null;
        float[] colors = null;
        float[] normals = null;
        int vertexBuf = 0;
        int colorBuf = 0;
        int normalBuf = 0;
        public UI_pointer_ball_and_box(string _name, Vector3 pos, float radius, List<Vector3> corners, float[] color_box,float[] color_ball) : base()
        {
            active = false;
            name = _name;

            //generate cube
            float[] vertices1 = null;
            float[] vertices2 = null;
            float[] normals1 = null;
            float[] normals2 = null;
            Geometry.SphereGenerator.GenSphere(radius, pos, ref vertices1, ref normals1);
            Geometry.ParallelepipedGenerator.GenParallelepiped(corners[0], corners[1], corners[2], corners[3], ref vertices2, ref normals2);

            vertices = new float[vertices1.Length+vertices2.Length];
            vertices1.CopyTo(vertices, 0); vertices2.CopyTo(vertices, vertices1.Length);
            normals = new float[normals1.Length + normals2.Length];
            normals1.CopyTo(normals, 0); normals2.CopyTo(normals, normals1.Length);


            float[] colors1 = new float[vertices1.Length];
            for (int i = 0; i < (int)(vertices1.Length / 3); i++)
            {
                colors1[i * 3] = color_ball[0];
                colors1[i * 3 + 1] = color_ball[1];
                colors1[i * 3 + 2] = color_ball[2];
            }
            float[] colors2 = new float[vertices2.Length];
            for (int i = 0; i < (int)(vertices2.Length / 3); i++)
            {
                colors2[i * 3] = color_box[0];
                colors2[i * 3 + 1] = color_box[1];
                colors2[i * 3 + 2] = color_box[2];
            }
            colors = new float[colors1.Length + colors2.Length];
            colors1.CopyTo(colors, 0);colors2.CopyTo(colors, colors1.Length);

            vertexBuf = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuf);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            colorBuf = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, colorBuf);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(colors.Length * sizeof(float)), colors, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            normalBuf = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, normalBuf);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * sizeof(float)), normals, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

        }

        public override void Dispose()
        {
            GL.DeleteBuffer(vertexBuf);
            GL.DeleteBuffer(normalBuf);
            GL.DeleteBuffer(colorBuf);
        }

        public override void Draw()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, colorBuf);
            GL.EnableClientState(ArrayCap.ColorArray);
            GL.ColorPointer(3, ColorPointerType.Float, 0, IntPtr.Zero);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuf);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);

            GL.BindBuffer(BufferTarget.ArrayBuffer, normalBuf);
            GL.EnableClientState(ArrayCap.NormalArray);
            GL.NormalPointer(NormalPointerType.Float, 0, IntPtr.Zero);

            GL.DrawArrays(BeginMode.Triangles, 0, (int)(vertices.Length / 3));

            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.NormalArray);
            GL.DisableClientState(ArrayCap.ColorArray);

        }
        public override bool click()
        {
            return base.click();
        }
        public override void updateState(Vector3 v)
        {
            base.updateState(v);
        }
    }
}

