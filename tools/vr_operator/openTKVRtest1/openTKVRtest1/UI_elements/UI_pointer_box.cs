﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace openTKVRtest1.UI_elements
{
    class UI_pointer_box : UI_element
    {
        float[] vertices = null;
        float[] colors = null;
        float[] normals = null;
        float[] uvs = null;
        int vertexBuf = 0;
        int colorBuf = 0;
        int normalBuf = 0;
        public UI_pointer_box(string _name, Vector3 pos, float width,float height,float depth, float[] color) : base()
        {
            active = false;
            name = _name;

            //generate cube
            Geometry.CubeGenerator.GenCube(width, height, depth, pos, ref vertices, ref normals, ref uvs);

            colors = new float[vertices.Length];
            for (int i = 0; i < (int)(vertices.Length / 3); i++)
            {
                colors[i * 3] = color[0];
                colors[i * 3 + 1] = color[1];
                colors[i * 3 + 2] = color[2];
            }

            vertexBuf = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuf);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            colorBuf = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, colorBuf);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(colors.Length * sizeof(float)), colors, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            normalBuf = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, normalBuf);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * sizeof(float)), normals, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

        }

        public override void Dispose()
        {
            GL.DeleteBuffer(vertexBuf);
            GL.DeleteBuffer(normalBuf);
            GL.DeleteBuffer(colorBuf);
        }

        public override void Draw()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, colorBuf);
            GL.EnableClientState(ArrayCap.ColorArray);
            GL.ColorPointer(3, ColorPointerType.Float, 0, IntPtr.Zero);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuf);
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);

            GL.BindBuffer(BufferTarget.ArrayBuffer, normalBuf);
            GL.EnableClientState(ArrayCap.NormalArray);
            GL.NormalPointer(NormalPointerType.Float, 0, IntPtr.Zero);

            GL.DrawArrays(BeginMode.Triangles, 0, (int)(vertices.Length / 3));

            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.NormalArray);
            GL.DisableClientState(ArrayCap.ColorArray);

        }
        public override bool click()
        {
            return base.click();
        }
        public override void updateState(Vector3 v)
        {
            base.updateState(v);
        }
    }
}
