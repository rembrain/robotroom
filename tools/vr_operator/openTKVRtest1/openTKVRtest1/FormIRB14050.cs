﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using OpenTK;
using Newtonsoft.Json;

namespace openTKVRtest1
{
    public partial class FormIRB14050 : Form
    {

        float[] minJoints = new float[7] {-165.0f, -140f,-120f,-285f,-85f,-225f,-165f};
        float[] maxJoints = new float[7] { 165.0f, 40f, 77f, 285f, 135f, 225f, 165f };
        public object locker = null;
        public List<object> images_to_show=null;

        float[,] camM = null;


        public FormIRB14050()
        {
            InitializeComponent();
            pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox2.Image = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            backgroundWorker1.RunWorkerAsync();
        }

        public Robots.RobotIRB14050 robot = null;

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            var joints = robot.get_joints();
            var pos = robot.get_pos();
            var q = robot.get_q();
            if(robot.currect_readings!=null)
                if (robot.currect_readings.ContainsKey("moving"))
                {
                    if ((bool)robot.currect_readings["moving"].Value)     
                        textBox4.Text = "moving";
                    else
                        textBox4.Text = "not moving";
                }
            if ((joints != null)&&(pos!=null)&&(q!=null))
            {
                string s = "";
                for (int i = 0; i < joints.Length; i++)
                    s += joints[i] + " ";
                textBox1.Text = s;
                s = "";
                for (int i = 0; i < pos.Length; i++)
                    s += pos[i] + " ";
                for (int i = 0; i < q.Length; i++)
                    s += q[i] + " ";
                textBox2.Text = s;




                if (listBox1.Items.Count != robot.calibration_points.Count)
                {
                    listBox1.Items.Clear();
                    for (int i = 0; i < robot.calibration_points.Count; i++)
                    {
                        s = "";
                        for (int j = 0; j < robot.calibration_points[i].Length; j++)
                            s += robot.calibration_points[i][j] + " ";
                        listBox1.Items.Add(s);
                    }
                }

                if (listBox2.Items.Count != robot.obstacles.Count)
                {
                    listBox1.Items.Clear();
                    for (int i = 0; i < robot.calibration_points.Count; i++)
                    {
                        s = "";
                        for (int j = 0; j < robot.calibration_points[i].Length; j++)
                            s += robot.calibration_points[i][j] + " ";
                        listBox1.Items.Add(s);
                    }
                }

                
                /*if (plus_press)
                {
                    if ((curArm != -1) && (currentAxis != -1))
                    {
                        if (curArm == 1)
                        {
                            joints[currentAxis] += 2.0f;
                            robot.MoveJoints(joints);
                        }
                    }
                }
                if (minus_press)
                {
                    if ((curArm != -1) && (currentAxis != -1))
                    {
                        if (curArm == 1)
                        {
                            joints[currentAxis] -= 2.0f;
                            robot.MoveJoints(joints);
                        }
                    }
                }*/
            }
            
        }

        int currentAxis = -1;
        int curArm = -1;
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 0;
            curArm = 1;
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 1;
            curArm = 1;
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 2;
            curArm = 1;
        }
        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 3;
            curArm = 1;
        }
        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 4;
            curArm = 1;
        }
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 5;
            curArm = 1;
        }
        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            currentAxis = 6;
            curArm = 1;
        }

        void MouseWheel(object sender, MouseEventArgs args)
        {
            
        }

        bool plus_press = false;
        bool minus_press = false;

        private void button4_MouseDown(object sender, MouseEventArgs e)
        {
            plus_press = true;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            plus_press = false;
        }

        private void button4_MouseUp(object sender, MouseEventArgs e)
        {
            plus_press = false;
        }

        private void button2_MouseDown(object sender, MouseEventArgs e)
        {
            minus_press = true;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            minus_press = false;
        }

        private void button2_MouseUp(object sender, MouseEventArgs e)
        {
            minus_press = false;
        }

        //delete all points
        private void button7_Click(object sender, EventArgs e)
        {
            robot.calibration_points.Clear();
            robot.savePoints();
        }
        //add current point
        private void button8_Click(object sender, EventArgs e)
        {
            float[] temp=robot.get_joints();
            float[] clone = new float[temp.Length];
            temp.CopyTo(clone, 0);
            robot.calibration_points.Add(clone);
            robot.savePoints();
        }


        private void button3_Click_1(object sender, EventArgs e)
        {


            dynamic points = JsonConvert.DeserializeObject(File.ReadAllText("obs.txt"));

            //get config

            string res = ConnectionUtils.get_api_request(Properties.Settings.Default.register_server, "config?robot=" + Properties.Settings.Default.robot_name);
            dynamic config = JsonConvert.DeserializeObject(res);
            config = JsonConvert.DeserializeObject(config["answer"]["value"].Value);

            Matrix4 m4 = new Matrix4((float)config["calibration"][0][0], (float)config["calibration"][0][1], (float)config["calibration"][0][2], (float)config["calibration"][0][3] / 1000.0f,
                (float)config["calibration"][1][0], (float)config["calibration"][1][1], (float)config["calibration"][1][2], (float)config["calibration"][1][3]/1000.0f,
                (float)config["calibration"][2][0], (float)config["calibration"][2][1], (float)config["calibration"][2][2], (float)config["calibration"][2][3] / 1000.0f,
                0, 0, 0, 1f);
                
                
            int i = 0; 
            List<object> results = new List<object>();



            foreach(dynamic pointset in points)
            {
                Vector4 p1_ = new Vector4((float)pointset[0][0], -(float)pointset[0][1], -(float)pointset[0][2],1f);
                p1_ = Vector4.Transform(m4, p1_);
                Vector3 p1=new Vector3(p1_.X, p1_.Y, p1_.Z);

                Vector4 p2_ = new Vector4((float)pointset[1][0], -(float)pointset[1][1], -(float)pointset[1][2],1f);
                p2_ = Vector4.Transform(m4, p2_);
                Vector3 p2 = new Vector3(p2_.X, p2_.Y, p2_.Z);

                Vector4 p3_ = new Vector4((float)pointset[2][0], -(float)pointset[2][1], -(float)pointset[2][2],1f);
                p3_ = Vector4.Transform(m4, p3_);
                Vector3 p3 = new Vector3(p3_.X, p3_.Y, p3_.Z);

                Vector4 p4_ = new Vector4((float)pointset[3][0], -(float)pointset[3][1], -(float)pointset[3][2],1f);
                p4_ = Vector4.Transform(m4, p4_);
                Vector3 p4 = new Vector3(p4_.X, p4_.Y, p4_.Z);

                Vector3 height = p3 - p1;
                Vector3 width = p2 - p1;
                Vector3 y = height.Normalized();
                Vector3 x = width - Vector3.Dot(y, width)*y;
                x.Normalize();
                Vector3 z = Vector3.Cross(x, y);

                float H = height.Length * 2.0f;
                float W = width.Length * 2.0f;
                float D = (p4-p1).Length * 2.0f;

                Dictionary<string, object> result = new Dictionary<string, object>();
                result.Add("position", new float[] { p1.X, p1.Y, p1.Z });
                
                result.Add("sizes", new float[] { W, H, D });

                Matrix3 m = new Matrix3(x.X, x.Y, x.Z, y.X, y.Y, y.Z, z.X, z.Y, z.Z);
                Quaternion q=m.ExtractRotation();

                result.Add("orientation",new float[] { q.W, q.X, q.Y, q.Z });
                
                result.Add("name","box" + i.ToString());
                i += 1;
                results.Add(result);
            }

            string s = JsonConvert.SerializeObject(results);
            
        }

        //PLUS
        private void button4_Click(object sender, EventArgs e)
        {
             if(currentAxis != -1)
             {
                var joints = robot.get_joints();
                if (joints[currentAxis] < maxJoints[currentAxis])
                {
                    joints[currentAxis] += 2.0f;
                    robot.MoveJoints(joints);
                }
             }
        }
        //MINUS
        private void button2_Click(object sender, EventArgs e)
        {
            if (currentAxis != -1)
             {
                var joints = robot.get_joints();
                if (joints[currentAxis] > minJoints[currentAxis])
                {
                    joints[currentAxis] -= 2.0f;
                    robot.MoveJoints(joints);
                }
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if ((images_to_show != null) && (locker != null))
            {
                Bitmap rgb = null;
                Bitmap depth = null;
                lock (locker)
                {
                    if (images_to_show.Count > 0)
                    {
                        rgb = (Bitmap)((List<object>)images_to_show[0])[0];
                        depth = (Bitmap)((List<object>)images_to_show[0])[1];
                        camM = (float[,])((List<object>)images_to_show[0])[2];




                    }
                }
                if ((rgb != null) && (depth != null))
                {
                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.DrawImage(rgb, 0f, 0f, (float)pictureBox1.Width, (float)pictureBox1.Height);
                    }
                    pictureBox1.Image = bmp;
                    rgb.Dispose();
                    bmp = new Bitmap(pictureBox2.Image);
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.DrawImage(depth, 0f, 0f, (float)pictureBox2.Width, (float)pictureBox2.Height);
                    }
                    pictureBox2.Image = bmp;
                    depth.Dispose();

                    lock (locker)
                    {
                        images_to_show.RemoveAt(0);
                    }
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (currentAxis != -1)
            {
                var joints = robot.get_joints();
                if (joints[currentAxis] < maxJoints[currentAxis])
                {
                    joints[currentAxis] += 10.0f;
                    robot.MoveJoints(joints);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (currentAxis != -1)
            {
                var joints = robot.get_joints();
                if (joints[currentAxis] > minJoints[currentAxis])
                {
                    joints[currentAxis] -= 10.0f;
                    robot.MoveJoints(joints);
                }
            }
        }

        int cur_calib_index = -1;
        
        private void button6_Click(object sender, EventArgs e)
        {
            cur_calib_index = int.Parse(textBox3.Text);
            if (cur_calib_index < robot.calibration_points.Count)
            {
                robot.goto_calib_point(cur_calib_index);

            }
            else
                cur_calib_index = -1;
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //robot.MoveJoints(new float[] { -54.8f ,37f, 55.4f, -163.24f, 2.58f, 84f, -91.2f });
  
            robot.MoveJoints(new float[] { 79.9f, - 81.8f, 52.6f, - 47.76f, - 67.34f, - 131.65f, 57.88f });

            //robot.MoveJoints(new float[] { -81.8f, - 22.2f, 43.4f, - 97.84f, 62.73f, - 42.01f, - 39.83f });
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
