﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace openTKVRtest1
{
    public partial class LoginForm : Form
    {
        public string Login = "";
        public string Password = "";
        public string server = "";
        public LoginForm()
        {
            InitializeComponent();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult=DialogResult.Cancel;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Login = textBox2.Text;
            Password = textBox3.Text;
            server = textBox1.Text;
            Properties.Settings.Default.register_server = server;
            if (checkBox1.Checked)
            {
                Properties.Settings.Default.Login = textBox2.Text;
                Properties.Settings.Default.Password = textBox3.Text;
                Properties.Settings.Default.robot_name = textBox4.Text;
                Properties.Settings.Default.register_server = server;
            }
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            textBox1.Text= Properties.Settings.Default.register_server;
            textBox2.Text = Properties.Settings.Default.Login;
            textBox3.Text = Properties.Settings.Default.Password;
            textBox4.Text = Properties.Settings.Default.robot_name;
        }
    }
}
