﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace openTKVRtest1
{
    public class Decoder
    {
        [DllImport("vpx.dll")]
        static private extern int create_codec(int codec_index, int w, int h);
        [DllImport("vpx.dll")]
        static private extern int kill_codec(int codec_index);
        [DllImport("vpx.dll")]
        static private extern int decode(int codec_index,byte[] buf,int input_L,int width,int height,[Out]byte[] rgb_data);

        public int m_width;
        public int m_height;
        public Decoder(int width, int height)
        {
            m_width = width;
            m_height = height;
            int res = create_codec(0, width, height);
            if (res != 0)
                Console.WriteLine("can not create codec");

        }

        public byte[] Decode(byte[] buf)
        {
            byte[] res = new byte[m_height * m_width * 3];
            if (decode(0,buf,buf.Length,m_width,m_height,res) != 0)
            {
                Console.WriteLine("fail in decoding");
            }
            return res;
        }
        ~Decoder()
        {
            kill_codec(0);
        }
    }
}
