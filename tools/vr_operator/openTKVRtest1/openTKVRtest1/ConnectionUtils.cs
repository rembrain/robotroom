﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace openTKVRtest1
{
    public static class ConnectionUtils
    {
        public static string accessToken = "";
        public static string refreshToken = "";

        public static bool Login(string server,string login,string password)
        {
            WebRequest request = WebRequest.Create(server + "/login?email=" +login +"&password="+password);
            request.Method = "POST";

            var s = "{\"username\":\"" + login + "\",\"password\":\"" + password + "\"}";
            var data = Encoding.Default.GetBytes(s);

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(data, 0, data.Length);

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    dataStream = response.GetResponseStream();
                    byte[] buf = new byte[8000];
                    dataStream.Read(buf, 0, buf.Length);
                    s=Encoding.Default.GetString(buf);
                    accessToken= JsonConvert.DeserializeObject<Dictionary<string, string>>(s)["access_token"];

                    return true;
                } else
                {
                    return false;
                 }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
            

        }

        public static string post_api_request(string server,string request_text)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "/api/"+ request_text);
            request.Method = "POST";
            request.Headers.Add("Authorization", accessToken);
            
            try
            {
                WebResponse wr = request.GetResponse();

                using (StreamReader sr = new StreamReader(wr.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return "";
            }
        }
        public static string get_api_request(string server, string request_text)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(server + "/api/" + request_text);
            request.Method = "GET";
            request.Headers.Add("Authorization", accessToken);

            try
            {
                WebResponse wr = request.GetResponse();

                using (StreamReader sr = new StreamReader(wr.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
            catch
            {
                return "";
            }
        }
    }
}
