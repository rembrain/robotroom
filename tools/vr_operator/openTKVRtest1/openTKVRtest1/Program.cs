﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Threading;
using System.Net;
using Newtonsoft.Json;

namespace openTKVRtest1
{
    
    class MyApplication
    {



       

        static List<object> sharedList = new List<object>();
        static List<dynamic> sensorList = new List<dynamic>();
        static List<object> commands = new List<object>();
        static List<object> images_to_from = new List<object>();
        static object locker = new object();
        public static void threadFunc()
        {
            int w = Properties.Settings.Default.width_rgb;
            int h = Properties.Settings.Default.height_rgb;
            Decoder dec = new Decoder(w, h);
            var com = new Communication(Properties.Settings.Default.rabbit_server, dec);
            com.Run(sharedList, locker);
        }
        public static void sensorThreadFunc()
        {
            var com = new SensorComm(Properties.Settings.Default.rabbit_server);
            com.Run(sensorList, locker);
        }
        public static void commandThreadFunc()
        {
            var com = new CommandComm(Properties.Settings.Default.rabbit_server);
            com.Run(commands, locker);
        }
        public static void formThreadFunc(object robot_state)
        {
            if (Properties.Settings.Default.robot == "IRB14050")
            {
                var form = new FormIRB14050();
                form.locker = locker;
                form.images_to_show = images_to_from;
                form.robot = (Robots.RobotIRB14050)robot_state;
                form.ShowDialog();
            }
            if ((Properties.Settings.Default.robot == "Eva")|| (Properties.Settings.Default.robot == "UR10e") || (Properties.Settings.Default.robot == "XArm6"))
            {
                var form = new FormEvaUR(Properties.Settings.Default.robot);
                form.locker = locker;
                form.images_to_show = images_to_from;
                form.robot = (Robots.RobotEvaUR)robot_state;
                form.ShowDialog();
            }

        }

        

        [STAThread]
        public static void Main()
        {
            //Login
            while (true)
            {
                LoginForm form = new LoginForm();
                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string session = "";
                    if (ConnectionUtils.Login(form.server, form.Login, form.Password))
                    {
                        Properties.Settings.Default.LastSession = session;

                        string res = ConnectionUtils.get_api_request(form.server, "config?robot=" + Properties.Settings.Default.robot_name);
                        Dictionary<string, Dictionary<string, string>> response = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(res);
                        Dictionary<string, dynamic> config = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(response["answer"]["value"]);
                        Properties.Settings.Default.robot = config["arm_type"];
                        Properties.Settings.Default.Save();
                        break;
                    }
                }
            }
            
            Robots.Robot robot_state = null;
            if (Properties.Settings.Default.robot == "IRB14050")
                robot_state = new Robots.RobotIRB14050(commands, locker);
            if (Properties.Settings.Default.robot == "Eva")
                robot_state = new Robots.RobotEvaUR(commands, locker);
            if (Properties.Settings.Default.robot == "UR10e")
                robot_state = new Robots.RobotEvaUR(commands, locker);
            if (Properties.Settings.Default.robot == "XArm6")
                robot_state = new Robots.RobotEvaUR(commands, locker);
            if (robot_state == null)
            {
                Console.WriteLine("wrong robot");
                return;
            }
            var vr = new openTKVRtest1.VR(robot_state);



            var thread = new Thread(threadFunc);
            thread.IsBackground = true;
            thread.Start();
            var threadSensor = new Thread(sensorThreadFunc);
            threadSensor.IsBackground = true;
            threadSensor.Start();
            var threadCommand = new Thread(commandThreadFunc);
            threadCommand.IsBackground = true;
            threadCommand.Start();

            var form_thread = new Thread(formThreadFunc);
            form_thread.IsBackground = true;
            form_thread.Start(robot_state);

            
            
            using (var game = new GameWindow())
            {
                DateTime start = DateTime.Now;
                game.Load += (sender, e) =>
                {
                    // setup settings, load textures, sounds
                    game.VSync = OpenTK.VSyncMode.Off;

                    GL.ClearColor(0.1f, 0.2f, 0.5f, 0.0f);
                    //GL.Enable(EnableCap.DepthTest);
                    

                    vr.Init();

                };

                game.Resize += (sender, e) =>
                {
                    GL.Viewport(0, 0, game.Width, game.Height);
                };

                game.UpdateFrame += (sender, e) =>
                {
                     

                    // add game logic, input handling
                    // Get current state
                    KeyboardState keyboardState = OpenTK.Input.Keyboard.GetState();
                    // Check Key Presses
                    if (keyboardState[Key.Escape])
                    {
                        game.Exit();
                    }
                };
               
                Frame3DandSensors curFrame = null;

               
                DateTime start_=DateTime.Now;
                int frame_index = 0;
                game.RenderFrame += (sender, e) =>
                {
                    bool wasNew = false;

                    dynamic sensor_readings = null;
                    lock (locker)
                    {
                        if (sharedList.Count > 0)
                        {
                            curFrame = (Frame3DandSensors)sharedList[0];
                            List<object> pack = new List<object>();
                            pack.Add((Bitmap)curFrame.bitmap.Clone());pack.Add((Bitmap)curFrame.depth.Clone());pack.Add(curFrame.camM);pack.Add(curFrame.depthMap);
                            images_to_from.Add(pack);


                            sharedList.RemoveAt(0);
                            wasNew = true;

                            if (frame_index % 100 == 0)
                            {
                                Console.WriteLine("in drawing fps "+(100.0/(DateTime.Now-start_).TotalSeconds).ToString());
                                start_ = DateTime.Now;
                                
                            }
                            frame_index += 1;
                        }
                        if (sensorList.Count > 0)
                        {
                            sensor_readings = sensorList[0];
                            sensorList.RemoveAt(0);
                        }
                    }

                    vr.WaitGetPoses();


                    if(sensor_readings!=null)
                        robot_state.set_sensors(sensor_readings);
                    if (curFrame != null)
                    {
                        
                        vr.Draw(curFrame, game.Width, game.Height);
                    }
                    vr.PushToVR();
                    vr.HandleInput();

                    game.SwapBuffers();

                };

                // Run the game at 60 updates per second
                game.Run(60.0);
                vr.Dispose();
            }
        }
    }
}