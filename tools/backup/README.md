## Backup 

####Running (by scheduler)

- in folder `config`:
    - copy `example.env`, 
    - rename this new file to `.env`,
    - update variables into it; 
       
- create file `backup_db` into  folder`/etc/cron.d/` and add in it: 

```
0 */7 * * * root /usr/bin/python3 -u [full path to tools/backup/main.py from root folder of server] >> [log_file]
```
It will dump every **7** hours.  
**Attention**: Folder with log file must exist!
 

- upload file to **cron**: `crontab /etc/cron.d/backup_db`

Backup files will be created in folders which have set in **env-file**.
For checking logs of **cron**: `cat /var/log/syslog`

####Running (by handle)
Just run file from base folder: `python3 -u tools/backup/main.py`