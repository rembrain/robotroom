# ATTENTION! It is only for local tests!
import os
import shutil
import typing as T
from collections import namedtuple
from contextlib import contextmanager
from pathlib import Path
from traceback import format_exc
from uuid import uuid4

import pymongo
import pymysql

from tools.backup.src import utils

__all__ = ['']

BACKUP_FOLDER = Path(os.path.dirname(__file__), 'test_dumps')


@contextmanager
def _mysql_connect(
        env_vars: namedtuple
) -> T.Generator[T.Tuple[pymysql.Connection, pymysql.cursors.Cursor], None, None]:
    with pymysql.connect(
            host='localhost',
            user=env_vars.MYSQL_USER,
            password=env_vars.MYSQL_PASSWORD,
            database=env_vars.MYSQL_DB
    ) as connect, connect.cursor() as cursor:
        yield connect, cursor


def _check_mongo(env_vars: namedtuple) -> None:
    uniq_value = str(uuid4())
    db_name = table_name = 'test'
    mongo_url = 'mongodb://root:root@localhost:27017'

    print('Testing MONGO DB backup...')
    mc = pymongo.MongoClient(mongo_url, connect=False, maxPoolSize=1)
    mc.drop_database(db_name)
    mct: pymongo.collection.Collection = mc[db_name][table_name]

    # add document
    mct.insert_one({'x': 123, 'y': uniq_value})
    if not mct.find_one({'y': uniq_value}):
        raise Exception('Value was not added to Mongo DB.')

    # take dump
    mongo_container_id: str = utils.get_container_id(env_vars.MONGO_DB_CONTAINER_NAME)
    dump_file: Path = utils.create_dump(
        mongo_container_id,
        f'mongodump --archive -u {env_vars.MONGO_DB_USER} -p {env_vars.MONGO_DB_PASSWORD}',
        BACKUP_FOLDER.joinpath(f'{env_vars.MONGO_DB_BACKUP_FOLDER}'),
        'dump.gz'
    )

    # remove document
    mct.delete_one({'y': uniq_value})
    if mct.find_one({'y': uniq_value}):
        raise Exception('Value was not removed from Mongo DB.')

    # load dump
    utils.load_dump(
        mongo_container_id,
        f'mongorestore --archive -u {env_vars.MONGO_DB_USER} -p {env_vars.MONGO_DB_PASSWORD}',
        dump_file,
    )
    if not mct.find_one({'y': uniq_value}):
        raise Exception('Value did not exist after loading dump to Mongo DB.')

    print('MONGO DB backup checked successfully!')


def _check_mysql(env_vars: namedtuple) -> None:
    print('Testing MYSQL backup...')
    uniq_value = str(uuid4())
    table_name = 'test'

    with _mysql_connect(env_vars) as (connect, cursor):
        # add record
        cursor.execute(
            f'CREATE TABLE IF NOT EXISTS `{table_name}`'
            f'(`x` varchar(255), `y` varchar(255))'
        )
        cursor.execute(
            f'INSERT INTO `{table_name}` (`x`, `y`) VALUES ("123", "{uniq_value}")'
        )
        connect.commit()

        cursor.execute(f'SELECT COUNT(*) FROM `{table_name}` WHERE `y`="{uniq_value}"')
        if cursor.fetchone()[0] != 1:
            raise Exception('Value was not added to MYSQL.')

    # take dump
    mysql_container_id: str = utils.get_container_id(env_vars.MYSQL_CONTAINER_NAME)
    dump_file: Path = utils.create_dump(
        mysql_container_id,
        f'mysqldump -u{env_vars.MYSQL_USER} '
        f'-p{env_vars.MYSQL_PASSWORD}  --no-tablespaces {env_vars.MYSQL_DB}',
        BACKUP_FOLDER.joinpath(f'{env_vars.MYSQL_BACKUP_FOLDER}'),
        'dump.sql'
    )

    # remove document
    with _mysql_connect(env_vars) as (connect, cursor):
        cursor.execute(f'TRUNCATE `{table_name}`')
        cursor.execute(f'SELECT COUNT(*) FROM `{table_name}` WHERE `y`="{uniq_value}"')
        if cursor.fetchone()[0] != 0:
            raise Exception('Value was not remove from MYSQL.')

    # load dump
    print('Load dump to MYSQL - please wait - it is long operation.')
    utils.load_dump(
        mysql_container_id,
        f'mysql -u{env_vars.MYSQL_USER} -p{env_vars.MYSQL_PASSWORD} {env_vars.MYSQL_DB}',
        dump_file
    )

    # check after load
    with _mysql_connect(env_vars) as (connect, cursor):
        cursor.execute(f'SELECT COUNT(*) FROM `{table_name}` WHERE `y`="{uniq_value}"')
        if cursor.fetchone()[0] != 1:
            raise Exception('Value did not exist after loading dump to MYSQL.')

    print('MYSQL backup checked successfully!')


try:
    print('Start tests ...')
    env_file = Path('..').joinpath('config', 'example.env')
    env_vars: namedtuple = utils.get_env_vars_from_file(env_file)

    _check_mongo(env_vars)
    _check_mysql(env_vars)

    print('Tests completed successfully!')
except Exception:
    print('Error: ', format_exc())
finally:
    if BACKUP_FOLDER.exists():
        shutil.rmtree(BACKUP_FOLDER, ignore_errors=True)
