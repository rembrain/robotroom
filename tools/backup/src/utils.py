import os
import shutil
import subprocess
import sys
import typing as T
from collections import namedtuple
from datetime import datetime, timedelta
from pathlib import Path

DUMP_FOLDER_DATE_FORMAT = '%d-%m-%Y_%H-%M-%S'


# noinspection PyArgumentList
def get_env_vars_from_file(env_file: Path) -> namedtuple:
    if not env_file.exists():
        raise Exception('Env file does not exist.')

    with open(env_file) as f:
        data: T.Generator = (
            (i for i in line.strip().split('='))
            for line in f.readlines()
            if line and not line.isspace() and not line.startswith('#')
        )
        env_vars = dict(data)
        return namedtuple('_', env_vars)(**env_vars)


def get_container_id(container_name: str) -> str:
    command = f'docker ps -q --filter="NAME={container_name}" --filter status=running'
    container_id: str = subprocess.check_output(command, shell=True).decode().strip()

    if not container_id:
        raise Exception("Could not find active container.")

    if " " in container_id:
        raise Exception("There are more than one container.")

    return container_id


def create_dump(container_id: str, shell_command: str, base_backups_folder: str, file_name: str) -> Path:
    current_date: str = datetime.now().strftime(DUMP_FOLDER_DATE_FORMAT)

    # create folders
    backup_folder: Path = Path(base_backups_folder).joinpath(current_date)
    backup_folder.mkdir(parents=True, exist_ok=True)
    dump_file: Path = backup_folder.joinpath(file_name)

    full_command = f'docker exec {container_id} sh -c "{shell_command}" > {dump_file}'
    print('CONTAINER LOGS: \n', '*' * 80)
    result: subprocess.CompletedProcess = subprocess.run(full_command, shell=True, stdout=sys.stdout, stderr=sys.stdout)
    print('', '*' * 80, '\nCONTAINER LOGS finished.\n')

    if result.returncode != 0:
        if backup_folder.exists():
            shutil.rmtree(backup_folder, ignore_errors=True)

        raise Exception("FAILED: Operation was interrupted. Getting backup failed - check container logs.")

    return dump_file


def load_dump(container_id: str, shell_command: str, dump_file: Path) -> None:
    full_command = f'docker exec -i {container_id} sh -c "{shell_command}" < {dump_file}'
    result: subprocess.CompletedProcess = subprocess.run(full_command, shell=True, stdout=sys.stdout, stderr=sys.stdout)

    if result.returncode != 0 and dump_file.exists():
        shutil.rmtree(dump_file.parent, ignore_errors=True)


def remove_old_dumps(base_backups_folder: str, expire_days: int = 7) -> None:
    for f in os.listdir(base_backups_folder):
        if (datetime.now() - datetime.strptime(f, DUMP_FOLDER_DATE_FORMAT)) >= timedelta(days=expire_days):
            shutil.rmtree(Path(base_backups_folder).joinpath(f), ignore_errors=True)
            print('Backup', f, 'removed.')
