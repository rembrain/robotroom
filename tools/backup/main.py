"""It creates backups for Mongo DB and MYSQL."""
import sys
from collections import namedtuple
from datetime import datetime
from pathlib import Path

# hook - it allows to run from base folder
# it must be before imports from project folders
sys.path.insert(0, str(Path(__file__).parent.parent.parent))

from tools.backup.src import utils

if __name__ == '__main__':
    print(f'Run backup operations (date={datetime.now()})...')

    # get env vars from file
    env_file = Path(__file__).parent.joinpath('config', '.env')
    env_vars: namedtuple = utils.get_env_vars_from_file(env_file)

    print(f'Run backup for MONGO DB ...')
    mongo_container_id: str = utils.get_container_id(env_vars.MONGO_DB_CONTAINER_NAME)
    utils.create_dump(
        mongo_container_id,
        f'mongodump --archive '
        f'-u {env_vars.MONGO_DB_USER} -p {env_vars.MONGO_DB_PASSWORD} '
        f'--numParallelCollections=1',
        env_vars.MONGO_DB_BACKUP_FOLDER,
        'dump.gz'
    )
    print("Backup Mongo DB data completed successfully.")

    print(f'Run backup for MYSQL ...')
    mysql_container_id: str = utils.get_container_id(env_vars.MYSQL_CONTAINER_NAME)
    utils.create_dump(
        mysql_container_id,
        f'mysqldump -u{env_vars.MYSQL_USER} '
        f'-p{env_vars.MYSQL_PASSWORD}  --no-tablespaces {env_vars.MYSQL_DB}',
        env_vars.MYSQL_BACKUP_FOLDER,
        'dump.sql'
    )
    print('Backup MYSQL data completed successfully.')

    print('Remove old dumps...')
    utils.remove_old_dumps(env_vars.MONGO_DB_BACKUP_FOLDER)
    utils.remove_old_dumps(env_vars.MYSQL_BACKUP_FOLDER)

    print('Backup operations completed successfully.\n\n')
