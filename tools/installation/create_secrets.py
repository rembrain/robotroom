import os

print('This script will help to form passwords.yml in the folder with secrets, make sure you use SUDO.')

print('Use 1-aws or 2-minio:')
MODE = int(input('MODE:'))

print('--------------')
print('S3')
if MODE == 1:
    with open('passwords_sample_without_minio.yml', 'r') as f:
        password_sample = f.read()

    S3_REGION = input('S3_REGION:')
    password_sample = password_sample.replace('{S3_REGION}', S3_REGION)
elif MODE == 2:
    with open('passwords_sample.yml', 'r') as f:
        password_sample = f.read()

    S3_ADDRESS = input('S3_ADDRESS:')
    password_sample = password_sample.replace('{S3_ADDRESS}', S3_ADDRESS)
else:
    print("You haven't chosen a mode of operation")
    exit(1)

S3_ACCESS_KEY = input('S3_ACCESS_KEY:')
password_sample = password_sample.replace('{S3_ACCESS_KEY}', S3_ACCESS_KEY)
S3_SECRET_KEY = input('S3_SECRET_KEY:')
password_sample = password_sample.replace('{S3_SECRET_KEY}', S3_SECRET_KEY)
ROBOTS_LOG_BUCKET = input('ROBOTS_LOG_BUCKET:')
password_sample = password_sample.replace('{ROBOTS_LOG_BUCKET}', ROBOTS_LOG_BUCKET)
ROBOTS_ARCHIVE_BUCKET = input('ROBOTS_ARCHIVE_BUCKET:')
password_sample = password_sample.replace('{ROBOTS_ARCHIVE_BUCKET}', ROBOTS_ARCHIVE_BUCKET)
ROBOTS_ARTIFACTS_BUCKET = input('ROBOTS_ARTIFACTS_BUCKET:')
password_sample = password_sample.replace('{ROBOTS_ARTIFACTS_BUCKET}', ROBOTS_ARTIFACTS_BUCKET)
ROBOTS_STREAMS_BUCKET = input('ROBOTS_STREAMS_BUCKET:')
password_sample = password_sample.replace('{ROBOTS_STREAMS_BUCKET}', ROBOTS_STREAMS_BUCKET)
ROBOTS_SESSIONS_BUCKET = input('ROBOTS_SESSIONS_BUCKET:')
password_sample = password_sample.replace('{ROBOTS_SESSIONS_BUCKET}', ROBOTS_SESSIONS_BUCKET)
ROBOTS_WEIGTHS_BUCKET = input('ROBOTS_WEIGTHS_BUCKET:')
password_sample = password_sample.replace('{ROBOTS_WEIGTHS_BUCKET}', ROBOTS_WEIGTHS_BUCKET)

print('--------------')
print('KEYCLOAK:')
KEYCLOAK_ADMIN_USERNAME = input('KEYCLOAK_ADMIN_USERNAME:')
password_sample = password_sample.replace('{KEYCLOAK_ADMIN_USERNAME}', KEYCLOAK_ADMIN_USERNAME)
KEYCLOAK_ADMIN_PASSWORD = input('KEYCLOAK_ADMIN_PASSWORD:')
password_sample = password_sample.replace('{KEYCLOAK_ADMIN_PASSWORD}', KEYCLOAK_ADMIN_PASSWORD)
KEYCLOAK_ADDRESS = input('KEYCLOAK_ADDRESS:')
password_sample = password_sample.replace('{KEYCLOAK_ADDRESS}', KEYCLOAK_ADDRESS)
KEYCLOAK_CLIENT_SECRET = input('KEYCLOAK_CLIENT_SECRET:')
password_sample = password_sample.replace('{KEYCLOAK_CLIENT_SECRET}', KEYCLOAK_CLIENT_SECRET)

print('--------------')
print('RABBITMQ')
RABBIT_USERNAME = input('RABBIT_USERNAME:')
password_sample = password_sample.replace('{RABBIT_USERNAME}', RABBIT_USERNAME)
RABBIT_PASSWORD = input('RABBIT_PASSWORD:')
password_sample = password_sample.replace('{RABBIT_PASSWORD}', RABBIT_PASSWORD)

print('--------------')
print('SYSTEM DATA')
RANDOM_API_SECRET_KEY = input('RANDOM_API_SECRET_KEY:')
password_sample = password_sample.replace('{RANDOM_API_SECRET_KEY}', RANDOM_API_SECRET_KEY)
AUTH_GOOGLE_CLIENT_ID = input('AUTH_GOOGLE_CLIENT_ID:')
password_sample = password_sample.replace('{AUTH_GOOGLE_CLIENT_ID}', AUTH_GOOGLE_CLIENT_ID)
MONITOR_USERNAME = input('MONITOR_USERNAME:')
password_sample = password_sample.replace('{MONITOR_USERNAME}', MONITOR_USERNAME)
MONITOR_PASSWORD = input('MONITOR_PASSWORD:')
password_sample = password_sample.replace('{MONITOR_PASSWORD}', MONITOR_PASSWORD)
API_URL = input('API_URL:')
password_sample = password_sample.replace('{API_URL}', API_URL)
WS_GATE_URL = input('WS_GATE_URL:')
password_sample = password_sample.replace('{WS_GATE_URL}', WS_GATE_URL)

print('--------------')
print('Saving to /secrets/passwords.yml')

if not os.path.exists('/secrets'):
    os.mkdir('/secrets')

with open('/secrets/passwords.yml', 'w') as f:
    f.write(password_sample)
