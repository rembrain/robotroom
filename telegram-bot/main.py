import logging
import os
import time
from typing import Optional

import telebot
from python_logging_rabbitmq import RabbitMQHandler
from rembrain_robot_framework.logger import get_log_handler

from common.src.services import register_connection

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
    rabbit: RabbitMQHandler = get_log_handler({"project": "brainless_robot", "subsystem": "telegram-bot"})
    logging.root.addHandler(rabbit)

    logging.info("start logging")

    bot = telebot.TeleBot(os.environ["BOT_TOKEN"])
    prev_robots: Optional[list] = None
    connected = True

    while True:
        try:
            connection = register_connection.Connection(
                os.environ["MONITOR_ADDRESS"], os.environ["ADMIN_NAME"], os.environ["ADMIN_PASSWORD"]
            )

            result: dict = connection.get_request("robots_status", "api")["answer"]
            robots = [
                key for key in result.keys() if result[key]["camera0_connected"] and result[key]["sensors_connected"]
            ]
            channel_id: str = os.environ["CHANNEL_ID"]
            if not connected:
                bot.send_message(channel_id, f"backend is back")
            connected = True

            logging.info(f"Robots :{robots}")

            if prev_robots is not None:

                for r in robots:
                    if r not in prev_robots:
                        bot.send_message(channel_id, f"robot online: {r}")

                for r in prev_robots:
                    if r not in robots:
                        bot.send_message(channel_id, f"robot offline: {r}")

            prev_robots = robots
        except Exception as e:
            logging.error(e, exc_info=True)

            if connected:
                bot.send_message(channel_id, f"backend has been lost")
            connected = False

        time.sleep(10.0)
