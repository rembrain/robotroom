class MongoCollection:
    ADMIN = "coordinator_adminka"
    ARCHIVE = "archive"
    MONITORING = "monitoring"
    LOGS = "logs"
    SESSIONS = "sessions"
    SESSION_TRAIN = "session_train"

    ALL_VALUES = (ADMIN, ARCHIVE, MONITORING, LOGS, SESSIONS, SESSION_TRAIN)
