import os
from typing import Any, Optional

import aio_pika
from aio_pika.connection import ConnectionType


class RabbitService:
    def __init__(self):
        self.connection: Optional[ConnectionType] = None
        self.channel: Optional[aio_pika.Channel] = None

    async def open(self) -> aio_pika.Channel:
        self.connection: ConnectionType = await aio_pika.connect(os.environ["RABBIT_ADDRESS"])
        self.channel: aio_pika.Channel = await self.connection.channel()
        return self.channel

    async def close(self) -> None:
        await self.channel.close()
        await self.connection.close()

    async def __aenter__(self) -> aio_pika.Channel:
        return await self.open()

    async def __aexit__(self, exc_type: Any, exc_val: Exception, exc_tb: Any) -> None:
        await self.close()
