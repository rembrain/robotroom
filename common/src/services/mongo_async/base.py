import typing as T
from abc import abstractmethod

from motor.motor_asyncio import AsyncIOMotorClient

from common.src.enums import MongoCollection
from common.src.utils import get_mongo_async_connect


class MongoAsyncBaseService:
    def __init__(self, mongo_client: T.Optional[AsyncIOMotorClient] = None, *args, **kwargs):
        # if there are not mongo client - create personal one.
        if mongo_client is None:
            self.connection = get_mongo_async_connect()
        else:
            self.connection = mongo_client

        collection_name: str = self.collection()
        if collection_name not in MongoCollection.ALL_VALUES:
            raise Exception("Unknown Mongo collection.")

        self.db = self.connection[collection_name]

    @abstractmethod
    def collection(self) -> str:
        raise NotImplemented
