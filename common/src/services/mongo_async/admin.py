from common.src.enums import MongoCollection
from common.src.services.mongo_async.base import MongoAsyncBaseService


# todo some queries should update for performance
# todo https://docs.mongodb.com/manual/reference/method/js-collection/
class MongoAsyncAdminService(MongoAsyncBaseService):
    def collection(self):
        return MongoCollection.ADMIN

    async def check_acess_to_robot(self, user_email: str, robot_name: str) -> bool:
        if not user_email or not robot_name:
            return False

        count: int = await self.db.user_robot_pairs.count_documents(
            {"user_email": user_email, "robot": robot_name},
            limit=1
        )
        return count == 1
