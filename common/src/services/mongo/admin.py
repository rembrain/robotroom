import json
from typing import Optional, List

from common.src.enums import MongoCollection
from common.src.services.mongo import MongoBaseService


# todo some queries should update for performance
# todo https://docs.mongodb.com/manual/reference/method/js-collection/
class MongoAdminService(MongoBaseService):
    def collection(self):
        return MongoCollection.ADMIN

    # todo is 'get_or_insert' method in mongo?
    def add_task(self, author_name: str, name: str, task: str) -> bool:
        if not self.db.tasks.find_one({"author": author_name, "name": name}):
            self.db.tasks.insert_one({"author": author_name, "name": name, "task": task})
            return True

        return False

    # todo is 'exist' method in mongo?
    def check_task(self, author_name: str, name: str) -> bool:
        if self.db.tasks.find_one({"author": author_name, "name": name}):
            return True

        return False

    def get_task(self, author_name: str, name: str) -> Optional[dict]:
        task = self.db.tasks.find_one({"author": author_name, "name": name})
        if task is None:
            return None

        return {
            "author": task["author"],
            "name": task["name"],
            "task": json.loads(task["task"]),
        }

    def get_all_tasks(self, author_name: str = "") -> List[dict]:
        if author_name != "":
            tasks = self.db.tasks.find({"author": author_name})
        else:
            tasks = self.db.tasks.find({})

        return [{
            "author": task["author"],
            "name": task["name"],
            "task": json.loads(task["task"]),
        } for task in tasks]

    def del_task(self, author_name: str, name: str) -> None:
        self.db.tasks.delete_one({"author": author_name, "name": name})

    def get_config(self, robot_name: str) -> Optional[dict]:
        config = self.db.configs.find_one({"robot": robot_name})
        if config is None:
            return None

        return {
            "robot": config["robot"],
            "value": config["value"]
        }

    def get_all_configs(self) -> List[dict]:
        configs = self.db.configs.find({})
        return [{
            "robot": config["robot"],
            "value": config["value"]
        } for config in configs]

    # todo is 'insert_or_update' method in mongo?
    def set_config(self, robot_name: str, value: str) -> None:
        if not self.db.configs.find_one({"robot": robot_name}):
            self.db.configs.insert_one({"robot": robot_name, "value": value})
        else:
            self.db.configs.update({"robot": robot_name}, {"$set": {"value": value}})

    def get_available_robots(self, user_email: str) -> list:
        robots = self.db.user_robot_pairs.find({"user_email": user_email})
        return [r["robot"] for r in robots]

    # todo is 'get_or_insert' method in mongo?
    def add_external_user_robot_pair(self, user_email: str, robot_name: str) -> bool:
        if not self.db.user_robot_pairs.find_one({"user_email": user_email, "robot": robot_name}):
            self.db.user_robot_pairs.insert_one({"user_email": user_email, "robot": robot_name})
            return True

        return False

    # todo is 'find_and_delete' method in mongo?
    def del_external_user_robot_pair(self, user_email: str, robot_name: str) -> bool:
        if self.db.user_robot_pairs.find_one({"user_email": user_email, "robot": robot_name}) is None:
            return False

        self.db.user_robot_pairs.delete_one({"user_email": user_email, "robot": robot_name})
        return True

    def add_subscription_robot(self, user_email, robot_name):
        if not self.db.subscription_robot.find_one({"user_email": user_email, "robot": robot_name}):
            self.db.subscription_robot.insert_one({"user_email": user_email, "robot": robot_name})
            return True

        return False

    def get_subscription_robot(self, user_email):
        robots = self.db.subscription_robot.find({"user_email": user_email})
        res = []
        for r in robots:
            res.append(r["robot"])
        return res

    def del_subscription_robot(self, user_email, robot_name):
        if self.db.subscription_robot.find_one({"user_email": user_email, "robot": robot_name}) is None:
            return False

        self.db.subscription_robot.delete_one({"user_email": user_email, "robot": robot_name})
        return True

    def del_all_subscription_robot(self, robot_name):
        if self.db.subscription_robot.find_one({"robot": robot_name}) is None:
            return False

        self.db.subscription_robot.delete_many({"robot": robot_name})
        return True
