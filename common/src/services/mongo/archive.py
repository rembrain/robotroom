from common.src.enums import MongoCollection
from common.src.services.mongo import MongoBaseService

from bson import ObjectId


class MongoArchiveService(MongoBaseService):
    def collection(self) -> str:
        return MongoCollection.ARCHIVE

    def get_filter_count_archive_streams(self, filter) -> int:
        return self.db["streams"].count_documents(filter)

    def get_robots_archive_streams(self) -> list:
        return self.db["streams"].distinct("robot")

    def get_filter_pagination_archive_streams(
            self, page_num, page_size, filter_params=None, sort="datetime", sort_direction=-1
    ) -> list:
        if filter_params is None:
            filter_params = {}

        skips = page_size * (page_num - 1)
        cursor = self.db["streams"].find(filter_params).sort(sort, sort_direction).skip(skips).limit(page_size)

        return [{
            "robot": x["robot"],
            "datetime": x["datetime"].strftime("%Y-%b-%d : %H-%M-%S.%f"),
            "path": x["path"]
        } for x in cursor]

    def get_filter_count_archive_command(self, filter):
        return self.db["command"].count_documents(filter)

    def get_robots_archive_command(self) -> list:
        return self.db["command"].distinct("robot")

    def get_list_command(self) -> list:
        return self.db["command"].distinct("command")

    def get_filter_pagination_archive_command(
            self, page_num, page_size, filter=None, sort="datetime", sort_direction=-1
    ) -> list:
        if filter is None:
            filter = {}

        skips = page_size * (page_num - 1)
        cursor = self.db["command"].find(filter).sort(sort, sort_direction).skip(skips).limit(page_size)

        result = []
        for command_entity in cursor:
            result.append(self.transform_db_entity(command_entity))

        return result

    def update_command_by_id(self, id: str, updated_data):
        filter = {"_id": ObjectId(id)}
        return self.db['command'].update_one(filter, update={"$set": updated_data})

    def save_command(self, data):
        return self.db["command"].insert_one(data)

    def delete_command_by_id(self, id: str):
        return self.db["command"].delete_one({"_id": ObjectId(id)})

    def get_command_by_id(self, id: str):
        command_entity = self.db["command"].find_one({"_id": ObjectId(id)})
        if command_entity is None:
            return command_entity

        return self.transform_db_entity(command_entity)

    def get_command_by_path(self, path: str):
        return self.db["command"].find_one({"path": path})

    def transform_db_entity(self, db_entity):
        data = {
            "robot": db_entity["robot"],
            "datetime": db_entity["datetime"].strftime("%Y-%b-%d : %H-%M-%S.%f"),
            "path": db_entity["path"],
            "id": str(db_entity["_id"]),
            "content": db_entity.get("content", {}),
        }

        if db_entity.get("command") is not None:
            data["command"] = db_entity.get("command")

        return data
