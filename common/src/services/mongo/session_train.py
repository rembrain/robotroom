from common.src.enums import MongoCollection
from common.src.services.mongo import MongoSessionsService

from passlib.hash import sha256_crypt

class MongoSessionTrainService(MongoSessionsService):
    def collection(self) -> str:
        return MongoCollection.SESSION_TRAIN

    def add_record(self, data):
        return self.db.records.insert_one(data)

    def get_user(self, user_id):
        if not self.db.users.find_one({"user_id": user_id}):
            return False
        else:
            return True

    def get_user_data(self, user_id):
        user_data = self.db.users.find_one({"user_id": user_id})
        if not user_data:
            return None
        return {"username": user_data["username"], "password": user_data["password"]}

    def add_user(self, user_id, username, password):
        if not self.db.users.find_one({"user_id": user_id}):
            self.db.users.insert_one({"user_id": user_id, "username": username, "password": password})
        else:
            self.db.users.delete_one({"user_id": user_id})
            self.db.users.insert_one({"user_id": user_id, "username": username, "password": password})

    def delete_user(self, user_id):
        if not self.db.users.find_one({"user_id": user_id}):
            return False
        else:
            self.db.users.delete_one({"user_id": user_id})
            return True
