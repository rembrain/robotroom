from common.src.enums import MongoCollection
from common.src.services.mongo import MongoBaseService


class MongoMonitoringService(MongoBaseService):
    def collection(self) -> str:
        return MongoCollection.MONITORING
