import json
import time
import pymongo

from common.src.enums import MongoCollection
from common.src.services.mongo import MongoBaseService


class MongoLogsService(MongoBaseService):
    def collection(self) -> str:
        return MongoCollection.LOGS

    def add_record(self, robot_name: str, filenames: str, params: str):
        self.db.records.insert_one({
            "robot_name": robot_name,
            "time": time.time(),
            "filenames": filenames,
            "params": params,
        })

    def get_records(self, robot_name, skip=None, limit=None):
        result = []
        sort_way = pymongo.DESCENDING
        temp = self.db.logs.records.find({"robot_name": robot_name}).sort("$natural", sort_way)
        records_count = temp.count()

        if limit is not None:
            if skip is None:
                temp = self.db.logs.records.find(
                    {"robot_name": robot_name}
                ).sort("$natural", sort_way).limit(limit)
            else:
                temp = self.db.logs.records.find(
                    {"robot_name": robot_name}
                ).sort("$natural", sort_way).skip(skip).limit(limit)

        for t in temp:
            fields = ("robot_name", "filenames", "params", "time")
            if all(name in t for name in fields):
                item = {}

                if isinstance(t["filenames"], str):
                    item["filenames"] = json.loads(t["filenames"])
                else:
                    item["filenames"] = t["filenames"]

                if isinstance(t["params"], str):
                    item["params"] = json.loads(t["params"])
                else:
                    item["params"] = t["params"]

                item["time"] = t["time"]
                result.append(item)

        return result, records_count

    def get_all_records(self, robot_name: str):
        res = []
        temp = self.db.records.find({"robot_name": robot_name})
        fields = ("robot_name", "filenames", "params", "time")

        for t in temp:
            if all(name in t for name in fields):
                res.append({
                    "robot_name": t["robot_name"], "filenames": t["filenames"], "params": t["params"],
                    "time": t["time"]
                })

        return res

    def get_records_count(self, robot_name: str):
        return self.db.records.find({"robot_name": robot_name}).count()
