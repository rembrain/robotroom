from bson import ObjectId

from common.src.enums import MongoCollection
from common.src.services.mongo import MongoBaseService


class MongoSessionsService(MongoBaseService):
    def collection(self) -> str:
        return MongoCollection.SESSIONS

    def add_record(self, data):
        return self.db.records.insert_one(data)

    def update_records(self, session_id, data):
        return self.db.records.update({'_id': ObjectId(session_id)}, {"$set": data})

    def find_record(self, params):
        return self.db.records.find_one(params)

    def find_records(self, params):
        return self.db.records.find(params)

    def delete_record(self, params):
        self.db.records.delete_one(params)
