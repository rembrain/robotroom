from typing import Union, Set

import requests
from keycloak import KeycloakOpenID
from keycloak.exceptions import raise_error_from_response, KeycloakGetError


class KeycloakOpenIDWithTokenExchange(KeycloakOpenID):
    __URL_TOKEN = "realms/{realm-name}/protocol/openid-connect/token"

    def token_exchange(
            self,
            subject_token: str = "",
            subject_issuer: str = "",
            grant_type: str = "urn:ietf:params:oauth:grant-type:token-exchange",
            subject_token_type: str = "urn:ietf:params:oauth:token-type:access_token",
            requested_token_type: str = "urn:ietf:params:oauth:token-type:refresh_token",
            requested_subject: str = "wburke",
            **extra
    ) -> Union[dict, Set[str]]:
        payload = {
            "subject_token": subject_token,
            "subject_issuer": subject_issuer,
            "client_id": self.client_id,
            "grant_type": grant_type,
            "subject_token_type": subject_token_type,
            "requested_token_type": requested_token_type,
            "requested_subject": requested_subject
        }

        if extra:
            payload.update(extra)

        payload = self._add_secret_key(payload)
        data_raw: requests.Response = self.connection.raw_post(
            self.__URL_TOKEN.format(**{"realm-name": self.realm_name}),
            data=payload
        )
        return raise_error_from_response(data_raw, KeycloakGetError)
