import json
import logging
import os
from typing import Any, Union

from fastapi import Depends, HTTPException
from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakError

from .api_key import APIKeyHeader401
from .open_id import KeycloakOpenIDWithTokenExchange

Authorization = APIKeyHeader401(name='Authorization')

_cached_keycloak_openid = None
_cached_keycloak_admin = None


def get_keycloak_openid():
    """
    Returns a connection to keycloak via client's secret
    The connection is then cached for the duration of the program
    """
    global _cached_keycloak_openid
    if _cached_keycloak_openid is None:
        _cached_keycloak_openid = KeycloakOpenIDWithTokenExchange(
            server_url=os.environ["KEYCLOAK_ADDRESS"],
            client_id=os.environ["KEYCLOAK_CLIENT_ID"],
            realm_name=os.environ["KEYCLOAK_REALM"],
            client_secret_key=os.environ["KEYCLOAK_CLIENT_SECRET"]
        )
    return _cached_keycloak_openid


def get_keycloak_admin():
    """
    Returns an admin connection to keycloak
    The connection is then cached for the duration of the program
    """
    global _cached_keycloak_admin
    if _cached_keycloak_admin is None:
        _cached_keycloak_admin = KeycloakAdmin(
            server_url=os.environ["KEYCLOAK_ADDRESS"],
            username=os.environ["KEYCLOAK_ADMIN_USERNAME"],
            password=os.environ["KEYCLOAK_ADMIN_PASSWORD"],
            realm_name=os.environ["KEYCLOAK_REALM"],
            user_realm_name=os.environ["KEYCLOAK_ADMIN_REALM"],
            auto_refresh_token=['get', 'put', 'post', 'delete']
        )
    return _cached_keycloak_admin


# todo rename to 'user_data' or 'user_info'
def login_request(authorization: str = Depends(Authorization)) -> Union[dict, set]:
    if authorization:
        try:
            user_info = get_keycloak_openid().userinfo(authorization)
        except KeycloakError as e:
            error: Any = json.loads(e.error_message.decode("utf-8"))
            logging.error("Could not log in to the system: " + error["error_description"])
            raise HTTPException(status_code=401, detail="Could not log in to the system")

    else:
        raise HTTPException(status_code=401, detail="There is no authorization token in the header")

    return user_info
