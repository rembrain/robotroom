from typing import Optional

from fastapi import Request, HTTPException, status
from fastapi.security import APIKeyHeader


class APIKeyHeader401(APIKeyHeader):
    async def __call__(self, request: Request) -> Optional[str]:
        api_key: str = request.headers.get(self.model.name)

        if not api_key:
            if self.auto_error:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Not authenticated"
                )

            return None

        return api_key
