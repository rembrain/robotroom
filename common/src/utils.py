import json
import os
import ssl
import typing as T

import pika


def play_voice(message) -> None:
    from common.src.services import register_connection

    con = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    con.post_request("voice_generation/" +
                     os.environ["ROBOT_NAME"], body={"text": message, "voice": "Joanna"})


def read_robot_config() -> T.Dict[str, T.Any]:
    from common.src.services import register_connection

    connect = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    return json.loads(connect.get_request(f"config?robot={os.environ['ROBOT_NAME']}")["answer"]["value"])


def set_robot_config(config: T.Any) -> None:
    from common.src.services import register_connection

    connect = register_connection.Connection(
        os.environ["REGISTER_ADDRESS"], os.environ["RRF_USERNAME"], os.environ["RRF_PASSWORD"]
    )
    return connect.put_request(f"config?robot={os.environ['ROBOT_NAME']}", body={"value": json.dumps(config)})


def is_local_debug() -> bool:
    return bool(os.environ.get("LOCAL_DEBUG_MODE", False))


def get_mock_jetson_module() -> type:
    return type(
        "GPIO",
        (object,),
        {
            "BOARD": 0,
            "IN": 0,
            "input": lambda x: ...,
            "setmode": lambda x: ...,
            "setup": lambda x, y: ...,
        },
    )


def get_rabbit_connection() -> pika.BlockingConnection:
    if "RABBIT_ADDRESS" in os.environ:
        auth_data, host = os.environ["RABBIT_ADDRESS"].split("@")
        user, password = auth_data.replace("amqp://", "").split(":")
        credentials = pika.PlainCredentials(user, password)

        if host != "rabbit-master/" and 'localhost' not in host:
            stared_host = ".".join(
                ["*"] + host.split(".")[-2:]).replace("/", "")

            context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
            context.load_default_certs()
            ssl_options = pika.SSLOptions(context, server_hostname=stared_host)
            connection_params = pika.ConnectionParameters(
                ssl_options=ssl_options, port=5671, host=host[:-1], credentials=credentials
            )
        else:
            connection_params = pika.ConnectionParameters(
                host=host[:-1], credentials=credentials)

    else:
        connection_params = pika.ConnectionParameters(host="rabbitmq")

    return pika.BlockingConnection(connection_params)


def get_mongo_connect(mongo_url: T.Optional[str] = None, pool_size: int = 10, connect: bool = False):
    from pymongo import MongoClient

    if mongo_url is None:
        mongo_url: str = os.environ["MONGODB_URI"]

    return MongoClient(mongo_url, connect=connect, maxPoolSize=pool_size)


def get_mongo_async_connect(mongo_url: T.Optional[str] = None, pool_size: int = 10, connect: bool = False):
    from motor.motor_asyncio import AsyncIOMotorClient

    if mongo_url is None:
        mongo_url: str = os.environ["MONGODB_URI"]

    return AsyncIOMotorClient(mongo_url, connect=connect, maxPoolSize=pool_size)
