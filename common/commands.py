import json
import logging


def send_robot_command(robot: str, command_content: dict, rabbit_channel):
    logging.info(f"Push command to robot {robot}: {command_content}")
    rabbit_channel.basic_publish(
        exchange=f"commands_{robot}",
        routing_key="",
        body=json.dumps(command_content).encode(),
    )
