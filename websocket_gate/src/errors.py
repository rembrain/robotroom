from typing import Optional


class ValidationLevel:
    AUTH: str = "Auth"
    ROBOT_OWNER: str = "RobotOwnership"
    REQUEST: str = "RequestValidation"


class RequestValidationError(Exception):
    def __init__(
            self,
            reason: str,
            *,
            user_message: Optional[str] = None,
            validation_level: str = ValidationLevel.REQUEST
    ):
        """
        :param reason: reason for the validation error
        :param user_message: what to send back to the user. If not specified then reason is sent back
        :param validation_level: where the validation error occured
        """

        self.reason = reason
        self.validation_level = validation_level
        self.message_to_user = user_message

        if self.message_to_user is None:
            self.message_to_user = self.reason
