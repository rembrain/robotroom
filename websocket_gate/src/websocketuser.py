import asyncio
import json
import logging
import string
import time
import uuid
from traceback import format_exc
from typing import Any, List, Optional, Set, Tuple, Union

import aio_pika
import aiormq.exceptions
from aio_pika import ExchangeType
from rembrain_robot_framework.models.bind_request import BindRequest
from rembrain_robot_framework.models.request import Request
from rembrain_robot_framework.ws import WsCommandType
from websockets.exceptions import ConnectionClosedError, ConnectionClosedOK
from websockets.legacy.server import WebSocketServerProtocol

from common.src.enums import UserGroup
from common.src.services.mongo_async.admin import MongoAsyncAdminService
from websocket_gate.src.errors import RequestValidationError, ValidationLevel
from websocket_gate.src.models.request import WsGateRequest
from websocket_gate.src.services.auth import AuthBase
from websocket_gate.src.services.log_adapter import WebsocketLogAdapter
from websocket_gate.src.services.monitoring import MonitoringService
from websocket_gate.src.services.rabbit_pool import RabbitPool
from websocket_gate.src.utils import stringify_ip

logger = WebsocketLogAdapter(logging.getLogger(__name__), {})


class WebSocketUser:
    """
    Class that handles a single websocket connection
    It is assumed that websocket connections are NOT reused between "queues" of a single process
    Instead, each websocket command uses one websocket,
        if another command is to be used, another websocket connection is initiated
    """

    PING_INTERVAL = 3
    RABBIT_CONNECTION_REPEAT = 3
    ALLOWED_BIND_KEY_SYMBOLS = f"{string.ascii_letters}{string.digits}_"

    mongo_connect = MongoAsyncAdminService()
    rabbit_pool = RabbitPool()

    def __init__(self, websocket: WebSocketServerProtocol, auth_service: AuthBase):
        self.connection_guid = uuid.uuid1()
        self.websocket = websocket
        self.auth = auth_service

        self.queue_buffer: Optional[asyncio.Queue] = None
        self.user_info: Optional[Union[dict, Set[str]]] = None

        self.robot_name: Optional[str] = None
        self.is_service: bool = False

        self._set_monitoring()
        logger.info("New websocket connection.", extra=self._log_context)

        # Since websockets are mostly used for a single type of command only (at least currently)
        # We can flag the push websockets so we can have some logs from them, while not spamming logs constantly
        self.push_command_logged: bool = False
        self._active_queues: List[aio_pika.Queue] = []

    async def request_loop_handler(self):
        request: Optional[WsGateRequest] = None

        try:
            # if websocket does not receive new request during timeout - it closes
            request_data: Union[str, bytes] = await asyncio.wait_for(self.websocket.recv(), timeout=30)
            request = WsGateRequest.create(request_data)

            await self.authenticate_user(request)
            await self.set_robot_name(request)

            # Validation done - process the command
            await self.process_command(request)

        except RequestValidationError as e:
            self._log_invalid_request(
                request, f"{e.validation_level}:{e.reason}")
            await self.send_response(e.message_to_user, close_socket=True)

        except asyncio.TimeoutError:
            await self.send_response("Websocket was closed by timeout.", close_socket=True)

        except (ConnectionClosedError, ConnectionResetError, ConnectionClosedOK):
            logger.info("Websocket was closed by client.",
                        extra=self._log_context)

        except Exception:
            logger.error(
                f"Unknown error: {format_exc()}", extra=self._log_context)
            await self.send_response("Unknown error. Please, repeat later.", close_socket=True)

        finally:
            # await self.monitoring.stop_monitoring()
            await self._cleanup()

    async def authenticate_user(self, request: WsGateRequest):
        """
        Authenticate user from the request
        """

        logging.info('request.username request.password '+request.username+' '+request.password)

        # if client have already authenticated
        if self.user_info:
            return

        if request.username:
            self.user_info = await self.auth.authenticate_user(request.username, request.password)
        else:
            self.user_info = await self.auth.authenticate_user_via_token(request.access_token)

        if "preferred_username" in self.user_info:
            self.monitoring.on_got_username(
                self.user_info["preferred_username"])

    async def set_robot_name(self, request: WsGateRequest):
        """
        Checks that request's user can access to robot and return it (robot_name).
        :raises RequestValidationError: Couldn't allow the user to access the robot.
            Reason is specified in exception
        :return: robot's name if the user can access the robot.
            If robot's name is unspecified and it is an allowed usecase (i.e. pushing logs), then we still return it
        """

        # user must be authenticated
        if not self.user_info:
            raise RequestValidationError(
                "User is not authenticated", validation_level=ValidationLevel.AUTH)

        robot_name: str = request.robot_name
        self.monitoring.on_parsed_robot_name(robot_name)

        # if this robot's connection  hasn't changed - continue
        # don't remove checking to None!
        if self.robot_name is not None and self.robot_name == robot_name:
            return

        user_email: Optional[str] = self.user_info.get("email")
        user_groups: Optional[List[str]] = self.user_info.get("groups")

        # User is operator or admin - check that robot exists
        if user_groups and (UserGroup.OPERATOR in user_groups or UserGroup.ADMIN in user_groups):
            self.is_service = True

            # todo know about it
            if not robot_name:
                # user (in fact he must be processor or service)  wants access to all robots
                # - i.e robot name is not important
                self.robot_name = ""
                return

            all_robots = await self.auth.get_users_with_role("robot")
            if robot_name not in (i.get("username") for i in all_robots):
                raise RequestValidationError(
                    f"Robot '{robot_name}' does not exist.", validation_level=ValidationLevel.ROBOT_OWNER
                )

        # User is a robot - check that it is not accessing some other robot
        elif user_groups and UserGroup.ROBOT in user_groups:
            robot_username: str = self.user_info["preferred_username"]

            if robot_username != robot_name:
                raise RequestValidationError(
                    f"Robot {robot_username} tried to access another robot ({robot_name})."
                    f"Access to robot {robot_name} denied.",
                    validation_level=ValidationLevel.ROBOT_OWNER,
                )

        # Otherwise check that a user has access to this robot in the database
        elif not await self.mongo_connect.check_acess_to_robot(user_email, robot_name):
            raise RequestValidationError(
                f"User with email {user_email} tried to access robot ({robot_name})" f" that it has not access to.",
                user_message=f"Robot '{robot_name}' does not exist or you aren't allowed to access it.",
                validation_level=ValidationLevel.ROBOT_OWNER,
            )

        # Empty 'robot_name' is allowed only for special user groups(admins or operators)!
        if not robot_name:
            raise RequestValidationError(
                "Robot name is absent.", validation_level=ValidationLevel.ROBOT_OWNER
            )

        self.robot_name = robot_name

    async def process_command(self, request: WsGateRequest):
        command: str = request.command
        if command == WsCommandType.PING:
            await self.ping_command()
            return

        self.monitoring.on_parsed_command(command, request.exchange)

        # if rabbit got error - repeat it with new connection
        for i in range(self.RABBIT_CONNECTION_REPEAT):
            try:
                if command == WsCommandType.PULL:
                    await self.pull_command(request)

                elif command == WsCommandType.BIDIRECTIONAL:
                    await asyncio.gather(self.pull_command(request), self.push_loop_command(request))

                elif command == WsCommandType.PUSH:
                    await self.push_command(request)

                elif command == WsCommandType.PUSH_LOOP:
                    await self.push_loop_command(request)

                # If command got handled, don't try to reconnect
                break

            except aio_pika.AMQPException:
                logger.error(
                    f"Error with RABBIT MQ: {format_exc()}", extra=self._log_context)
                # TODO: If RobustConnection doesn't reconnect properly, see what can be done w.r.t. re-initing it

            except aiormq.exceptions.ChannelInvalidStateError:
                logger.error(
                    f"Channel error: {format_exc()}", extra=self._log_context)
                await self.rabbit_pool.reconnect()

    async def pull_command(self, ws_gate_request: WsGateRequest) -> None:
        logger.info(
            f"Pull command, robot={self.robot_name}, username={ws_gate_request.username}, "
            f"exchange: {ws_gate_request.exchange}, time: {time.time()}",
            extra=self._log_context,
        )

        # reset buffer
        self.queue_buffer = asyncio.Queue()
        channel_pool = await self.rabbit_pool.get_channel_pool()

        async with channel_pool.acquire() as channel:
            exchange: aio_pika.Exchange = await channel.declare_exchange(
                ws_gate_request.exchange, type=ws_gate_request.exchange_type, durable=ws_gate_request.exchange_durable
            )

            queue: aio_pika.Queue = await channel.declare_queue(
                auto_delete=True,  # delete queue when channel will be closed
                durable=True,  # todo queue can stay in hard disk if there will be exception from rmq
            )

            self._active_queues.append(queue)

            bind_key: str = self._get_pull_bind_key(ws_gate_request)
            await queue.bind(exchange, routing_key=bind_key)
            await channel.set_qos(prefetch_count=1)

            await queue.consume(self._get_message, timeout=3.0, no_ack=True)

            while True:
                try:
                    message: Any = await asyncio.wait_for(self.queue_buffer.get(), timeout=self.PING_INTERVAL)
                    self.monitoring.on_sent_packet()
                    await self.send_response(message)

                except asyncio.TimeoutError:
                    message: str = (
                        f"Exchange={ws_gate_request.exchange} pull command didn't receive data "
                        f"for {self.PING_INTERVAL} sec. Sending ping..."
                    )
                    logger.debug(message, extra=self._log_context)
                    await self.send_response(WsCommandType.PING)

                    # Send monitoring
                    asyncio.ensure_future(self.monitoring.on_sent_ping())

    async def push_command(self, request: WsGateRequest) -> None:
        try:
            text_message: str = json.dumps(request.message)
        except Exception:
            logger.error(
                f"Request message json.dumps error: {format_exc()}", extra=self._log_context)
            await self.send_response("Cannot parse 'push' command message.")
            return

        self._log_push_once(request)

        channel_pool = await self.rabbit_pool.get_channel_pool()
        async with channel_pool.acquire() as channel:
            exchange: aio_pika.Exchange = await channel.declare_exchange(
                request.exchange, type=ExchangeType.FANOUT.value, durable=request.exchange_durable
            )
            self.monitoring.on_got_packet()
            await exchange.publish(aio_pika.Message(text_message.encode("utf-8")), routing_key="")
            await self.send_response("Command pushed successfully.")

    async def push_loop_command(self, ws_gate_request: WsGateRequest) -> None:
        logger.info(
            f"Push_loop command, robot={ws_gate_request.robot_name}, username={ws_gate_request.username} "
            f"exchange: {ws_gate_request.exchange}, time: {time.time()}",
            extra=self._log_context,
        )

        channel_pool = await self.rabbit_pool.get_channel_pool()

        async with channel_pool.acquire() as channel:
            exchange: aio_pika.Exchange = await channel.declare_exchange(
                ws_gate_request.exchange, type=ws_gate_request.exchange_type, durable=ws_gate_request.exchange_durable
            )
            await self.send_response("Push loop started.")

            try:
                while True:
                    message: Union[bytes, str] = await asyncio.wait_for(self.websocket.recv(), timeout=10)

                    if isinstance(message, bytes):
                        self.monitoring.on_got_packet()
                        message, routing_key = self._wrap_personal_message(
                            ws_gate_request, message)
                        await exchange.publish(aio_pika.Message(message), routing_key=routing_key)
                        await self.send_response(f"{WsCommandType.PUSH_LOOP} message was published.")
                    else:
                        message_data: dict = json.loads(message)
                        if message_data.get("command") == WsCommandType.PING:
                            await self.ping_command()
                            continue

                        raise Exception(
                            f"{WsCommandType.PUSH_LOOP} error: received string data "
                            f"that was not a service message: {message_data}."
                        )
            except asyncio.TimeoutError:
                message: str = f"Exchange={ws_gate_request.exchange} request timeout for '{WsCommandType.PUSH_LOOP}'."
                logger.info(message, extra=self._log_context)
                await self.send_response(message, close_socket=True)

    async def ping_command(self) -> None:
        # Don't do anything for manual ping commands and just ping back
        await self.send_response(WsCommandType.PING)

        # Got manual ping, send to monitoring then return
        # Sending to monitoring is fire-and-forget so we don't block socket handling
        asyncio.ensure_future(self.monitoring.on_got_ping())

    async def send_response(self, response_message: str, *, close_socket: bool = False) -> None:
        if close_socket:
            await self.websocket.close(reason=response_message)
        else:
            await self.websocket.send(response_message)

    async def _get_message(self, message, max_queue_size: int = 4) -> None:
        if self.queue_buffer.qsize() < max_queue_size:
            await self.queue_buffer.put(message.body)

    def _wrap_personal_message(self, ws_gate_request: WsGateRequest, message: bytes) -> Tuple[bytes, str]:
        if ws_gate_request.exchange_type != ExchangeType.TOPIC.value:
            return message, ""

        # it's a service process
        if self.is_service:
            bind_request = BindRequest.from_bson(message)
            return bind_request.request.to_bson(), bind_request.bind_key

        # it's a client process
        request = Request.from_bson(message)
        bind_key: str = self._get_push_bind_key(request)
        return BindRequest(bind_key=bind_key, request=request).to_bson(), bind_key

    def _get_push_bind_key(self, request: Request) -> str:
        if (
            not self.robot_name
            or not request.service_name
            # bind_key may have only letters, digits
            or not all(i in self.ALLOWED_BIND_KEY_SYMBOLS for i in (self.robot_name + request.service_name))
        ):
            raise RequestValidationError(
                "Incorrect bind key for push personal message.", validation_level=ValidationLevel.REQUEST
            )

        return f"{self.robot_name}.{request.service_name}"

    def _get_pull_bind_key(self, ws_gate_request: WsGateRequest) -> str:
        if ws_gate_request.exchange_type != ExchangeType.TOPIC.value:
            return ""

        if self.is_service:
            return ws_gate_request.exchange_bind_key

        # for client there should check that he's an owner of robot thorough first part of bind key
        if not ws_gate_request.exchange_bind_key.startswith(f"{self.robot_name}."):
            raise RequestValidationError(
                "Incorrect bind key for pull personal message.", validation_level=ValidationLevel.REQUEST
            )

        return ws_gate_request.exchange_bind_key

    async def _cleanup(self):
        """
        Cleans up leftover resources, e.g. pull queues that were created during websocket's lifetime
        """
        await asyncio.gather(*(queue.delete(if_unused=False, if_empty=False) for queue in self._active_queues))
        self._active_queues.clear()

    def _set_monitoring(self):
        self.monitoring = MonitoringService(self.rabbit_pool)
        self.monitoring.start_monitoring(
            self.connection_guid, stringify_ip(self.websocket.remote_address))

    # todo why should it send to logger evey time? how to find other better way?
    @property
    def _log_context(self):
        return {"stats": self.monitoring.connection_stats}

    def _log_invalid_request(self, request: Optional[WsGateRequest], reason: str):
        err_message = f"Websocket request fail. Reason: {reason}."
        if request:
            err_message += (
                f"Robot: {request.robot_name}, " f"Exchange: {request.exchange}, " f"Command: {request.command}"
            )

        logger.warning(err_message, extra=self._log_context)

    def _log_push_once(self, request):
        if not self.push_command_logged:
            # Log only once to reduce spam
            logger.info(
                f"Push command, robot={self.robot_name}, username={request.username}, "
                f"exchange: {request.exchange}, time: {time.time()}",
                extra=self._log_context,
            )
            self.push_command_logged = True
