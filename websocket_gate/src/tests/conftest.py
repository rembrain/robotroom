import sys
from unittest.mock import AsyncMock

import pytest

from websocket_gate.src.tests.mocks import RabbitMock


@pytest.mark.asyncio
@pytest.fixture(autouse=True)
async def excess_imports_fx():
    sys.modules["common.src.services.keycloak"] = AsyncMock()
    sys.modules["common.src.services.mongo"] = AsyncMock()
    sys.modules["websocket_gate.src.services.monitoring"] = AsyncMock()
    sys.modules["websocket_gate.src.services.rabbit_pool"] = AsyncMock()


@pytest.mark.asyncio
@pytest.fixture()
async def ws_user_fx():
    from websocket_gate.src.websocketuser import WebSocketUser

    def _set_monitoring(self): self.monitoring = AsyncMock()

    async def _cleanup(self): pass

    WebSocketUser._set_monitoring = _set_monitoring
    WebSocketUser._cleanup = _cleanup
    web_socket_user = WebSocketUser(AsyncMock(), AsyncMock())
    web_socket_user.rabbit_pool = RabbitMock()
    web_socket_user.user_info = True
    web_socket_user.robot_name = "tester"

    return web_socket_user
