import os
from os import environ
from time import sleep

from envyaml import EnvYAML
from rembrain_robot_framework import RobotProcess, RobotDispatcher
from rembrain_robot_framework.models.bind_request import BindRequest
from rembrain_robot_framework.processes import WsRobotProcess


class ClientBinApp(RobotProcess):
    TEST_MESSAGE = "Test message ClientRobotProc!"
    SERVICE_NAME = "bin_app"
    RESULT_FIELD = "new"

    def run(self):
        sleep(2)

        uuid = self.send_request(self.TEST_MESSAGE, service_name=self.SERVICE_NAME)
        response = self.wait_response(uuid)
        self.shared.result[self.RESULT_FIELD] = response


class ClientCalibration(ClientBinApp):
    TEST_MESSAGE = "Test message ClientRobotProc2!"
    SERVICE_NAME = "calibration"
    RESULT_FIELD = "new2"


class ServiceBinApp(RobotProcess):
    TEST_MESSAGE = "Test message for ServiceProc!"
    RESULT_FIELD = "old"

    def run(self):
        sleep(2)

        bind_request: BindRequest = self.consume()
        self.shared.result[self.RESULT_FIELD] = bind_request.request.data
        bind_request.request.data = self.TEST_MESSAGE
        self.publish(bind_request)


class ServiceCalibration(ServiceBinApp):
    TEST_MESSAGE = "Test message for ServiceProc2!"
    RESULT_FIELD = "old2"


def test_rpc() -> None:
    config = EnvYAML(os.path.join(os.path.dirname(__file__), "config", "config.yaml"))
    processes = {
        "client__bin_app": {"process_class": ClientBinApp, "keep_alive": False},
        "client__calibration": {"process_class": ClientCalibration, "keep_alive": False},
        "client_ws_push": {
            "process_class": WsRobotProcess,
            "keep_alive": False,
            "username": environ.get('CLIENT_USERNAME'),
            "password": environ.get('CLIENT_PASSWORD'),
        },
        "service_ws_pull__bin_app": {"process_class": WsRobotProcess, "keep_alive": False},
        "service_ws_pull__calibration": {"process_class": WsRobotProcess, "keep_alive": False},
        "service__bin_app": {"process_class": ServiceBinApp, "keep_alive": False},
        "service__calibration": {"process_class": ServiceCalibration, "keep_alive": False},
        "service_ws_push__bin_app": {"process_class": WsRobotProcess, "keep_alive": False},
        "service_ws_push__calibration": {"process_class": WsRobotProcess, "keep_alive": False},
        "client_ws_pull": {"process_class": WsRobotProcess, "keep_alive": False},
    }

    robot_dispatcher = RobotDispatcher(config, processes, in_cluster=False, project_description={})
    robot_dispatcher.start_processes()

    sleep(5)

    proc_result = robot_dispatcher.shared_objects["result"]
    assert len(proc_result) == 4

    requests = (
        ClientBinApp.TEST_MESSAGE, ClientCalibration.TEST_MESSAGE,
        ServiceBinApp.TEST_MESSAGE, ServiceCalibration.TEST_MESSAGE
    )

    assert all(item in requests for item in proc_result.values())

    assert proc_result[ClientBinApp.RESULT_FIELD] == ServiceBinApp.TEST_MESSAGE
    assert proc_result[ClientCalibration.RESULT_FIELD] == ServiceCalibration.TEST_MESSAGE
    assert proc_result[ServiceBinApp.RESULT_FIELD] == ClientBinApp.TEST_MESSAGE
    assert proc_result[ServiceCalibration.RESULT_FIELD] == ClientCalibration.TEST_MESSAGE

    robot_dispatcher.stop_logging()


if __name__ == '__main__':
    test_rpc()
