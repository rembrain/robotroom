import aio_pika


class RabbitMock:
    # Don't change name of this field!
    # It will connected with aio_pika.Message().body!
    body = "Test Message!"

    # for getting result of publish to RabbitMq
    def __init__(self):
        self.test_publish_message = None

    async def consume(self, _get_message_callback, *args, **kwargs):
        await _get_message_callback(self)

    async def publish(self, message: aio_pika.Message, *args, **kwargs):
        self.test_publish_message = message.body

    async def bind(self, *args, **kwargs): pass

    async def set_qos(self, *args, **kwargs): pass

    async def declare_exchange(self, *args, **kwargs): return self

    async def declare_queue(self, *args, **kwargs): return self

    async def get_channel_pool(self): return self

    def acquire(self): return self

    async def __aenter__(self): return self

    async def __aexit__(self, exc_type, exc_val, exc_tb): pass
