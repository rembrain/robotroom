import json

import pytest
from rembrain_robot_framework.ws import WsCommandType

from websocket_gate.src.tests.mocks import RabbitMock


@pytest.mark.asyncio
async def test_correct_ws_pull(ws_user_fx) -> None:
    test_result = None
    loop_reset_exception = "It's loop_reset_exception!"

    async def ws_recv():
        return json.dumps({
            "command": WsCommandType.PULL,
            "exchange": "test_ws_user",
            "robot_name": "tester",
            "access_token": "qwe",
        })

    async def ws_send(message):
        nonlocal test_result
        test_result = message
        raise BaseException(loop_reset_exception)

    # update ws methods (attribute 'websocket' has already set to AsyncMock into fixture!)
    ws_user_fx.websocket.recv = ws_recv
    ws_user_fx.websocket.send = ws_send

    with pytest.raises(BaseException) as exc_info:
        await ws_user_fx.request_loop_handler()

    assert loop_reset_exception in str(exc_info.value)
    assert test_result == RabbitMock.body


@pytest.mark.asyncio
async def test_correct_ws_push(ws_user_fx):
    test_message = {"test": "SOME TEST MESSAGE!"}
    test_ws_response = None
    loop_reset_exception = "It's loop_reset_exception!"

    async def ws_recv():
        return json.dumps({
            "command": WsCommandType.PUSH,
            "exchange": "test_ws_user",
            "robot_name": "tester",
            "access_token": "qwe",
            "message": test_message
        })

    async def ws_send(message):
        nonlocal test_ws_response
        test_ws_response = message
        raise BaseException(loop_reset_exception)

    ws_user_fx.websocket.recv = ws_recv
    ws_user_fx.websocket.send = ws_send

    with pytest.raises(BaseException) as exc_info:
        await ws_user_fx.request_loop_handler()

    assert loop_reset_exception in str(exc_info.value)
    assert json.loads(ws_user_fx.rabbit_pool.test_publish_message.decode()) == test_message
    assert test_ws_response == "Command pushed successfully."


@pytest.mark.asyncio
async def test_correct_ws_push_loop(ws_user_fx):
    test_message = b"SOME TEST MESSAGE!"
    test_ws_response = None
    loop_reset_exception = "It's loop_reset_exception!"
    repeat = False
    is_push_loop_started = False

    async def ws_recv():
        nonlocal repeat

        if repeat:
            return test_message
        else:
            repeat = True
            return json.dumps({
                "command": WsCommandType.PUSH_LOOP,
                "exchange": "test_ws_user",
                "robot_name": "tester",
                "access_token": "qwe",
            })

    async def ws_send(message):
        nonlocal test_ws_response
        test_ws_response = message

        if message == "Push loop started.":
            nonlocal is_push_loop_started
            is_push_loop_started = True
            return

        raise BaseException(loop_reset_exception)

    ws_user_fx.websocket.recv = ws_recv
    ws_user_fx.websocket.send = ws_send

    with pytest.raises(BaseException) as exc_info:
        await ws_user_fx.request_loop_handler()

    assert is_push_loop_started
    assert loop_reset_exception in str(exc_info.value)
    assert ws_user_fx.rabbit_pool.test_publish_message == test_message
    assert test_ws_response == f"{WsCommandType.PUSH_LOOP} message was published."


@pytest.mark.asyncio
@pytest.mark.parametrize("wc_recv_param, test_result_param", (
        ({"access_token": "q", }, "Some of required values are missing in the request."),
        ({"command": WsCommandType.PUSH, "exchange": "q", "robot_name": "q", },
         "Auth data is missing from the command."),
        ({"command": "q", "exchange": "q", "robot_name": "q", "access_token": "q"}, "Unknown command 'q'."),
))
async def test_invalid_request(ws_user_fx, wc_recv_param, test_result_param, mocker) -> None:
    test_result = None

    async def send(message):
        nonlocal test_result
        test_result = message

    mocker.patch.object(ws_user_fx.websocket, "recv", return_value=json.dumps(wc_recv_param))
    mocker.patch.object(ws_user_fx.websocket, "send", send)
    await ws_user_fx.request_loop_handler()

    assert test_result == test_result_param
