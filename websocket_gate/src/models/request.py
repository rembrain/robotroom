import json
import typing as T

from aio_pika import ExchangeType
from rembrain_robot_framework.ws import WsCommandType, WsRequest

from websocket_gate.src.errors import RequestValidationError


class WsGateRequest(WsRequest):
    exchange_durable: bool = False

    ALLOWED_EXCHANGE_TYPES: T.ClassVar[T.Tuple[str]] = (ExchangeType.FANOUT.value, ExchangeType.TOPIC.value)

    #  todo DOES processor_ping_sender REQUIRE self.robot_name ?
    # todo what will happen if these exchanges add data for alien robots ?
    SPECIAL_EXCHANGES: T.ClassVar[T.Tuple[str]] = ("logstash", "processor_ping_sender")

    @classmethod
    def create(cls, request_data: T.Union[str, bytes]):
        request = cls(**json.loads(str(request_data)))
        request.valid()
        request.fix_params()
        return request

    def fix_params(self):
        if self.exchange_type != ExchangeType.TOPIC.value and self.exchange not in self.SPECIAL_EXCHANGES:
            self.exchange += f"_{self.robot_name}"

        if self.exchange == "logstash":
            self.exchange_durable = True

        if self.exchange_type == ExchangeType.FANOUT.value:
            self.exchange_bind_key = ""

    def valid(self) -> None:
        """
        Validate request.
        :raises RequestValidationError: Request was invalid.
        """

        if self.command not in WsCommandType.ALL_VALUES:
            raise RequestValidationError(f"Unknown command '{self.command}'.")

        if not self.access_token and not (self.username and self.password):
            raise RequestValidationError("Auth data is missing from the command")

        # check exchange params
        if not self.exchange:
            raise RequestValidationError("Field 'exchange' is missed  in the request.")

        if self.exchange_type not in self.ALLOWED_EXCHANGE_TYPES:
            raise RequestValidationError(f"Unknown type of exchange: {self.exchange_type}")

        if self.exchange_type == ExchangeType.TOPIC.value:
            if self.exchange in self.SPECIAL_EXCHANGES:
                raise RequestValidationError(
                    f"Exchanges `{self.SPECIAL_EXCHANGES}` is not allowed for 'topic' exchange type"
                )
