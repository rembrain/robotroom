import platform
import time
from dataclasses import dataclass


@dataclass
class ConnectionStatistics:
    """Class for keeping statistics about the connection"""
    guid: str
    ip: str
    hostname: str = platform.node()
    command: str = None

    username: str = None
    robot: str = None
    exchange: str = None
    connection_start: float = time.time()
    connection_end: float = None

    pings_got: int = 0
    pings_sent: int = 0
    packets_got: int = 0
    packets_sent: int = 0
