import asyncio
import os
import typing as T

import aio_pika
from aio_pika.pool import Pool


class RabbitPool:
    def __init__(self):
        self._rabbit_address = os.environ["RABBIT_ADDRESS"]

        self._connection_pool_size = 2  # Total connections to the server (the less the better)
        self._channel_pool_size = 100  # Channels per connection

        self._connection_pool: T.Optional[Pool] = None
        self._channel_pool: T.Optional[Pool] = None

    async def _get_connection(self):
        return await aio_pika.connect_robust(self._rabbit_address)

    async def _get_channel(self) -> aio_pika.Channel:
        if not self._connection_pool:
            loop = asyncio.get_event_loop()
            self._connection_pool = Pool(self._get_connection, max_size=self._connection_pool_size, loop=loop)

        async with self._connection_pool.acquire() as connection:
            return await connection.channel()

    async def get_channel_pool(self) -> Pool:
        if not self._channel_pool:
            loop = asyncio.get_event_loop()
            self._channel_pool = Pool(self._get_channel, max_size=self._channel_pool_size, loop=loop)

        return self._channel_pool

    async def reconnect(self):
        """
        Sometimes the connection breaks, trying to reset the pool if it does
        """
        try:
            await self._channel_pool.close()
            await self._connection_pool.close()
        finally:
            # Set to null so we reconnect on the next call to get_channel_pool
            self._channel_pool = None
            self._connection_pool = None
