import dataclasses
import json
import logging

import time
import typing as T

from uuid import UUID

import aio_pika

from websocket_gate.src.models.statistics import ConnectionStatistics
from websocket_gate.src.services.rabbit_pool import RabbitPool

logger = logging.getLogger(__name__)


class MonitoringService:
    """
    Class that monitors statistics of websocket connection
    The monitoring object will log stats to two places:
        - Send a message to a PING_EXCHANGE_NAME exchange on Rabbit any time
        it gets or sends a ping
        - Send a ConnectionStatistics object to MongoDB on connection closure
    NOTE: because connections and WSGate can close unexpectedly, we
    log to MongoDB only on connection closure.
          This way we don't get left with some fantom
          unclosed connections in the database
    """

    PING_EXCHANGE_NAME = "websocket_ping_stream"
    MONGO_CONNECTION_COLLECTION = "websocket_connections"
    # mongo_connection = MongoMonitoringService()

    def __init__(self, rabbit_pool: RabbitPool):
        # self.db = self.mongo_connection.db
        self._rabbit_pool = rabbit_pool
        self.connection_stats: T.Optional[ConnectionStatistics] = None

    ##################################
    # CONNECTION MONITORING TO MONGO #
    ##################################

    def start_monitoring(self, guid: UUID, ip: str):
        self.connection_stats = ConnectionStatistics(guid=str(guid), ip=ip)

    def on_parsed_command(self, cmd: str, exchange: str):
        """
        Call when we parsed a command and want to set that
        this connection has this command associated
        """
        self.connection_stats.command = cmd
        self.connection_stats.exchange = exchange

    def on_parsed_robot_name(self, robot: str):
        self.connection_stats.robot = robot

    def on_got_username(self, username: str):
        self.connection_stats.username = username

    def on_got_packet(self):
        """
        Call when app received a packet from the websocket
        """
        self.connection_stats.packets_got += 1

    def on_sent_packet(self):
        """
        Call when app sent a packet to the websocket
        """
        self.connection_stats.packets_sent += 1

    async def stop_monitoring(self):
        """
        Stops monitoring and submits connection statistics to MongoDB
        Currently turned off while we're trying to figure out performance
         issues with wsgate
        """
        # self.connection_stats.connection_end = time.time()
        # stats_dict = dataclasses.asdict(self.connection_stats)
        # todo why does it create another one collection(db)?
        # collection = self.db[self.MONGO_CONNECTION_COLLECTION]
        # insert_fn = functools.partial(collection.insert_one, stats_dict)
        # await asyncio.gather(
        #     asyncio.get_event_loop().run_in_executor(None, insert_fn),
        #     self._on_conn_closed()
        # )

    #############################
    # PING MONITORING TO RABBIT #
    #############################

    async def on_got_ping(self):
        """
        Reports received a ping to a rabbit exchange
        """
        self.connection_stats.pings_got += 1
        msg = dataclasses.asdict(self.connection_stats)
        msg["action"] = "got_ping"

        await self._send_msg_to_rabbit(msg)

    async def on_sent_ping(self):
        """
        Reports sending a ping to a rabbit exchange
        """
        self.connection_stats.pings_sent += 1
        msg = dataclasses.asdict(self.connection_stats)
        msg["action"] = "sent_ping"

        await self._send_msg_to_rabbit(msg)

    async def _on_conn_closed(self):
        msg = dataclasses.asdict(self.connection_stats)
        msg["action"] = "connection_closed"
        await self._send_msg_to_rabbit(msg)

    async def _send_msg_to_rabbit(self, msg: dict):
        channel_pool = await self._rabbit_pool.get_channel_pool()

        async with channel_pool.acquire() as channel:
            ping_exchange = await channel.declare_exchange(self.PING_EXCHANGE_NAME, type="fanout", durable=False)
            msg["timestamp"] = time.time()
            pika_msg = aio_pika.Message(json.dumps(msg).encode("utf-8"))

            await ping_exchange.publish(pika_msg, routing_key="")
