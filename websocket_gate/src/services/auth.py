import asyncio
from abc import ABC, abstractmethod

from keycloak.exceptions import KeycloakAuthenticationError

from common.src.services.keycloak import get_keycloak_admin, get_keycloak_openid
from websocket_gate.src.errors import RequestValidationError, ValidationLevel


class AuthBase(ABC):
    @abstractmethod
    async def authenticate_user(self, username: str, password: str) -> dict:
        return NotImplemented

    @abstractmethod
    async def authenticate_user_via_token(self, token: str) -> dict:
        return NotImplemented

    @abstractmethod
    async def get_users_with_role(self, role: str) -> list:
        return NotImplemented


class KeycloakAuthService(AuthBase):
    def __init__(self):
        self.open_id = get_keycloak_openid()
        self.admin = get_keycloak_admin()

    async def authenticate_user(self, username: str, password: str) -> dict:
        token = self.get_user_token(username, password)
        return await self.authenticate_user_via_token(token)

    async def authenticate_user_via_token(self, token: str) -> dict:
        try:
            return await asyncio.get_event_loop().run_in_executor(None, self.open_id.userinfo, token)
        except KeycloakAuthenticationError as e:
            if e.response_code == 401:
                raise RequestValidationError(
                    "Access denied. Please check auth data",
                    validation_level=ValidationLevel.AUTH
                )

            raise

    def get_user_token(self, username: str, password: str) -> str:
        return self.open_id.token(username, password)["access_token"]

    async def get_users_with_role(self, role: str) -> list:
        return await asyncio.get_event_loop().run_in_executor(
            None, lambda: self.admin.get_realm_role_members(role_name=role)
        )


class DebugAuthService(AuthBase):
    MOCK_RESULT = {
        "username": "tester",
        "preferred_username": "tester",
        "email": "test@example.com",
        "groups": ["admin", "operator"],
    }

    async def authenticate_user(self, username: str, password: str) -> dict:
        return self.MOCK_RESULT

    async def authenticate_user_via_token(self, token: str) -> dict:
        return self.MOCK_RESULT

    async def get_users_with_role(self, role: str) -> list:
        return [self.MOCK_RESULT]
