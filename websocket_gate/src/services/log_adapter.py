import logging


class WebsocketLogAdapter(logging.LoggerAdapter):
    """
    Logging adapter that adds ip, guid and robot info to the output msg.
    """

    def process(self, msg, extra: dict):
        if "stats" not in extra["extra"]:
            return msg, extra

        stats = extra["extra"]["stats"]

        # Add [ip(guid) - robot] at the start of the message
        prepend = f"{stats.ip}({stats.guid})"
        if stats.robot:
            prepend += f" - {stats.robot}"

        return f"[{prepend}] {msg}", extra
