def stringify_ip(addr):
    if type(addr) is tuple:
        # IPv4
        return f"{addr[0]}:{addr[1]}"
    else:
        # IPv6 or None if not connected
        return str(addr)
