import asyncio
import logging
import os

import websockets
from websockets.legacy.server import WebSocketServerProtocol, WebSocketServer

from common.src.utils import is_local_debug
from websocket_gate.src.services.auth import KeycloakAuthService, DebugAuthService
from websocket_gate.src.websocketuser import WebSocketUser


class WebsocketApp:
    DEFAULT_WS_PORT = 5443

    def __init__(self):
        self._is_debug = is_local_debug()
        self._ssl_disabled = "WSGATE_SSL_DISABLED" in os.environ and os.environ["WSGATE_SSL_DISABLED"] != "0"

        if self._is_debug:
            logging.warning("LOCAL DEBUG MODE")
            self._auth_factory_fn = lambda: DebugAuthService()
        else:
            self._auth_factory_fn = lambda: KeycloakAuthService()

    async def _handler(self, websocket: WebSocketServerProtocol, path: str) -> None:
        auth_service = self._auth_factory_fn()
        socket_connection = WebSocketUser(websocket, auth_service)
        await socket_connection.request_loop_handler()

    def start_serve(self):
        if self._ssl_disabled or self._is_debug:
            logging.warning("Launching without SSL")
            server = websockets.serve(
                self._handler,
                os.environ.get("HOST", "localhost"),
                int(os.environ.get("PORT", self.DEFAULT_WS_PORT)),
                max_size=None,
                compression=None,
            )
        else:
            server: WebSocketServer = websockets.serve(
                self._handler, "0.0.0.0", self.DEFAULT_WS_PORT, max_size=None, compression=None
            )

        loop = asyncio.get_event_loop()
        loop.run_until_complete(server)
        loop.run_forever()

        # Clean up all pending tasks
        pending = asyncio.all_tasks()
        loop.run_until_complete(asyncio.gather(*pending))


if __name__ == "__main__":
    log_level = logging.DEBUG if is_local_debug() else logging.INFO
    logging.basicConfig(level=log_level, format="%(asctime)s %(name)s %(levelname)s: %(message)s")

    ws_app = WebsocketApp()
    ws_app.start_serve()
