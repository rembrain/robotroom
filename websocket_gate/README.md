# WebSocket Gate
It runs WS server for receiving/sending data from/to difference process 
through RabbitMQ.


## Params of request
- **username** - required if only **access_token** absents;
- **password** - required if only **access_token** absents;
- **access_token** - required if only **username** and **password**  absent;
- **command** - required, it must be one of values: ('pull', 'push', 'push_loop', 'ping');
- **robot_name** - required (if **exchange** has 'logstash' value - it's optional and be ignored);
- **message** - required if **command** is 'push' or 'push_loop';

- **exchange** - required - it is name of an exchange in RabbitMQ;
- **exchange_type** - optional - it must be 'fanout' (by default) or 'topic';